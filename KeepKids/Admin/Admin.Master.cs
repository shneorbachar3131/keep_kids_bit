﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using KeepKids.utils;

namespace KeepKids
{
    namespace Admin
    {


        public partial class Admin : System.Web.UI.MasterPage
        {
            protected void Page_Load(object sender, EventArgs e)
            {

                if (!IsPostBack)
                {
                    if (Session["ManagerName"] == null && Session["ParentsName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null && Session["TheKids"] == null)
                    {
                        Response.Redirect("Login.aspx");
                    }


                    if (Session["ParentsName"] == null && Session["AdminsName"] == null && (Session["ManagerName"] != null | Session["TheTeamMember"] != null))
                    {
                        if (Session["ManagerName"] != null)
                        {
                            Manager fn = (Manager)Session["ManagerName"];
                            if (Manager.UnreadMessages(fn.IDteamMember) > 0)
                            {
                                MessageBox.Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Manager.UnreadMessages(fn.IDteamMember) + "</span>";
                            }
                            Hide.Text = "<script> $('.HideManager').hide();</script>";
                            Name.Text = fn.nameTeamMember + " " + fn.FamilyTeamMember;
                            Image1.ImageUrl = "../pics/" + fn.PicManager;
                        }
                        else if (Session["TheTeamMember"] != null)
                        {
                            Hide.Text = "<script> $('.HideManager').hide();</script>";
                            Manager fn = (Manager)Session["TheTeamMember"];
                            Name.Text = fn.nameTeamMember + " " + fn.FamilyTeamMember;
                            Image1.ImageUrl = "../pics/" + fn.PicManager;
                        }
                    }
                    else if (Session["ManagerName"] == null && Session["AdminsName"] == null && (Session["ParentsName"] != null | Session["TheKids"] != null))
                    {
                        if (Session["ParentsName"] != null)
                        {
                            Parents fn1 = (Parents)Session["ParentsName"];
                            if (Parents.UnreadMessages(fn1.UserNameParents) > 0)
                            {
                                MessageBox.Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Parents.UnreadMessages(fn1.UserNameParents) + "</span>";
                            }
                            Hide.Text = "<script> $('.HideParents').hide();</script>";
                            Name.Text = fn1.NameFather + " ו" + fn1.NameMother;
                            Image1.ImageUrl = "../pics/" + fn1.PicKids;
                        }
                        else if (Session["TheKids"] != null)
                        {
                            Hide.Text = "<script> $('.HideParents').hide();</script>";
                            Parents fn1 = (Parents)Session["TheKids"];
                            Name.Text = fn1.NameFather + " ו" + fn1.NameMother;
                            Image1.ImageUrl = "../pics/" + fn1.PicKids;
                        }

                    }
                    else if (Session["ManagerName"] == null && Session["ParentsName"] == null && (Session["AdminsName"] != null | Session["TheAdmin"] != null))
                    {
                        if (Session["AdminsName"] != null)
                        {
                            Admins fn2 = (Admins)Session["AdminsName"];
                            if (Manager.UnreadMessages(fn2.IDadmin) > 0)
                            {
                                MessageBox.Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Manager.UnreadMessages(fn2.IDadmin) + "</span>";
                            }
                            Hide.Text = "<script> $('.HideAdmins').hide();</script>";
                            Name.Text = fn2.NameAdmin + " " + fn2.LastNameAdmin;
                            Image1.ImageUrl = "../pics/" + fn2.PicAdmin;
                        }
                        else if (Session["TheAdmin"] != null)
                        {
                            Hide.Text = "<script> $('.HideAdmins').hide();</script>";
                            Admins fn2 = (Admins)Session["TheAdmin"];
                            Name.Text = fn2.NameAdmin + " " + fn2.LastNameAdmin;
                            Image1.ImageUrl = "../pics/" + fn2.PicAdmin;
                        }

                    }


                }

            }


            protected void btnLogOut_Click(object sender, EventArgs e)
            {
                System.Web.Security.FormsAuthentication.SignOut();
                Session.Abandon();
                Response.Redirect("/index.aspx");
            }

        }

    }
}










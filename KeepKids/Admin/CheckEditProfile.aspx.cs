﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;

namespace KeepKids.Admin
{
    public partial class EditProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["ManagerName"] == null && Session["ParentsName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null && Session["TheKids"] == null)
                {
                    Response.Redirect("Login.aspx");
                }

                if (Session["ParentsName"] != null && Session["ManagerName"] == null && Session["AdminsName"] == null)
                {
                    Response.Redirect("EditProfileParents.aspx");

                }
                if (Session["ManagerName"] != null && Session["ParentsName"] == null && Session["AdminsName"] == null)
                {
                    Response.Redirect("EditProfileManager.aspx");

                }
                if (Session["AdminsName"] != null && Session["ManagerName"] == null && Session["ParentsName"] == null)
                {
                    Response.Redirect("EditProfileAdmins.aspx");
                }

            }
        }

        protected void btnEditProfile_Click(object sender, EventArgs e)
        {

        }
    }
}
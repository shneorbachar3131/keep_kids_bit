﻿using KeepKids.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;

namespace KeepKids
{
    namespace Admin
    {


        public partial class Default : System.Web.UI.Page
        {
            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                    //מנהלים
                    var admin = Admins.HowAdmin();
                    Admin.Value = admin.ToString();
                    //חברי צוות
                    var team = Manager.HowTeam();
                    Team.Value = team;
                    //הורים
                    var parent = Parents.HowParents();
                    Parent.Value = parent;
                    //ילדים
                    var kids = Parents.HowKids();
                    Kids.Value = kids;
                }

            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;

namespace KeepKids.Admin
{
    public partial class EditProfileAdmins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ChackSource();
            }

        }
        protected void ChackSource()
        {
            if (Session["ManagerName"] == null && Session["ParentsName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null && Session["TheKids"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            Admins EditAdmins = (Admins)Session["AdminsName"];

            FirstName.Text = EditAdmins.NameAdmin;
            LastName.Text = EditAdmins.LastNameAdmin;
            Email.Text = EditAdmins.UserNameAdmin;
            Phone.Text = EditAdmins.PhoneAdmin;
            Password.Text = EditAdmins.PasswordAdmin;
            Address.Text = EditAdmins.AddressAdmin;
            nameID.DataTextField = EditAdmins.NameCompany;
            kaskaval.Text = EditAdmins.IDadmin.ToString();
            Url.ImageUrl = "../pics/" + EditAdmins.PicAdmin;
            Url.AlternateText = EditAdmins.PicAdmin;

            //רשימת החברות
            nameID.DataSource = Manager.view();
            nameID.DataBind();
            nameID.DataTextField = "NameCompany";
            nameID.DataValueField = "IDcompany";
            nameID.DataBind();
            nameID.Items.Insert(0, new ListItem(" החברה בו הינך עובדת", "0"));
            for (int i = 0; i < nameID.Items.Count; i++)
            {
                if (nameID.Items[i].Value == EditAdmins.IDcompany.ToString())
                {
                    nameID.Items[i].Selected = true;
                    break;

                }
            }

        }


        protected void btnEditProfileAdmins_Click(object sender, EventArgs e)
        {
            if (PicUpload.FileName == "")
            {
                Session["AdminPic"] = Url.AlternateText;
            }
            if (PicUpload.FileName != "")
            {

                PicUpload.SaveAs(Path.Combine("C:\\Users\\שניאור\\source\\repos\\KeepKids\\KeepKids\\pics\\", PicUpload.FileName));
                Session["AdminPic"] = PicUpload.FileName;
            }



            Admins Tmp1 = new Admins()
            {
                IDcompany = int.Parse(nameID.SelectedValue),
                AddressAdmin = Address.Text,
                NameAdmin = FirstName.Text,
                LastNameAdmin = LastName.Text,
                PhoneAdmin = Phone.Text,
                UserNameAdmin = Email.Text,
                PasswordAdmin = Password.Text,
                PicAdmin = (string)Session["AdminPic"],
                IDadmin = int.Parse(kaskaval.Text)
            };

            if (Tmp1.CheckUpdate())
            {
                Session["AdminsName"] = Tmp1;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('העדכון בוצע בהצלחה');window.location ='Default.aspx';", true);

            }

            else
            {
                Worng.Text = "<b class='text-danger'>*שדה אחד או יותר אינם תקינים </b>";
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('העדכון בוטל ');window.location ='Default.aspx';", true);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
namespace KeepKids.Admin
{
    public partial class EditProfileManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ChackSource();
            }

        }




        protected void ChackSource()
        {
            if (Session["ManagerName"] == null && Session["ParentsName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null && Session["TheKids"] == null)
            {
                Response.Redirect("Login.aspx");
            }



            Manager EditManager = (Manager)Session["ManagerName"];

            FirstName.Text = EditManager.nameTeamMember;
            LastName.Text = EditManager.FamilyTeamMember;
            Phone.Text = EditManager.PhoneTeamMember;
            Email.Text = EditManager.UserNameTeamMember;
            AreYou.SelectedValue = EditManager.IDjob;
            nameID.DataTextField = EditManager.NameCompany;
            Password.Text = EditManager.PasswordTeamMember;
            kaskaval.Text = EditManager.IDteamMember.ToString();
            Url.ImageUrl = "../pics/" + EditManager.PicManager;
            Url.AlternateText = EditManager.PicManager;


            //עובדי החברה
            nameID.DataSource = Manager.view();
            nameID.DataBind();
            nameID.DataTextField = "NameCompany";
            nameID.DataValueField = "IDcompany";
            nameID.DataBind();
            nameID.Items.Insert(0, new ListItem("החברה בו הינך עובדת", "0"));
            for (int i = 0; i < nameID.Items.Count; i++)
            {
                if (nameID.Items[i].Value == EditManager.IDcompany.ToString())
                {
                    nameID.Items[i].Selected = true;
                    break;

                }
            }
            for (int i = 0; i < AreYou.Items.Count; i++)
            {
                if (AreYou.Items[i].Value == EditManager.IDjob)
                {
                    AreYou.Items[i].Selected = true;
                    break;

                }
            }

        }

        protected void btnEditProfileManager_Click(object sender, EventArgs e)
        {
            if (PicUpload.FileName == "")
            {
                Session["ManagerPic"] = Url.AlternateText;
            }
            if (PicUpload.FileName != "")
            {
                PicUpload.SaveAs(Path.Combine("C:\\Users\\שניאור\\source\\repos\\KeepKids\\KeepKids\\pics\\", PicUpload.FileName));
                Session["ManagerPic"] = PicUpload.FileName;
            }

            Manager Tmp1 = new Manager()
            {
                nameTeamMember = FirstName.Text,
                FamilyTeamMember = LastName.Text,
                PhoneTeamMember = Phone.Text,
                UserNameTeamMember = Email.Text,
                IDjob = AreYou.SelectedValue,
                IDcompany = int.Parse(nameID.SelectedValue),
                PasswordTeamMember = Password.Text,
                PicManager = (string)Session["ManagerPic"],
                IDteamMember = int.Parse(kaskaval.Text),
                FullName = FirstName.Text + " " + LastName.Text




            };


            if (Tmp1.CheckUpdate())
            {
                Session["ManagerName"] = Tmp1;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('העדכון בוצע בהצלחה');window.location ='Default.aspx';", true);

            }

            else
            {
                Worng.Text = "<b class='text-danger'>*שדה אחד או יותר אינם תקינים </b>";
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('העדכון בוטל ');window.location ='Default.aspx';", true);
        }
    }
}
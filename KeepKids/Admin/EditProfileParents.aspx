﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProfileParents.aspx.cs" Inherits="KeepKids.Admin.EditProfileParents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>עדכון פרטי הורים</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard." />
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard" />
    <meta name="author" content="PIXINVENT" />
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png" />
    <link rel="shortcut icon" type="image/x-icon" href="../pics/‏‏MYKIND.png" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors-rtl.min.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap-extended.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/colors.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/components.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/custom-rtl.css" />
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu-modern.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/login-register.css" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css" />
    <!-- END: Custom CSS-->
    <link href="css/mybackground.css" rel="stylesheet" />
</head>
<body class="vertical-layout vertical-menu-modern 1-column  bg-lighten-2 fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

    <form id="form1" runat="server" class="form-horizontal">
        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row mb-1">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                                <div class="card border-grey border-lighten-3 m-0">
                                    <div class="card-header border-0 pb-0">
                                        <div class="card-title text-center">
                                            <img src="../pics/‏‏MYKIND.png" alt="branding logo" />
                                        </div>
                                        <h2 class="card-subtitle line-on-side text-muted text-center  pt-2"><span>עדכון פרטי משתמש</span></h2>
                                        <h3 class=" text-center text-success  pt-2"><span>אלה הפרטים שלך כפי ששמורים אצלנו.<br />
                                            עדכן את הפרטים שברצונך לשנות</span></h3>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">

                                            <asp:Panel DefaultButton="btnEditProfileParents" runat="server">
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtFirstName" runat="server" ControlToValidate="NameKids" ErrorMessage="אנא הזן שם ילד" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                         <asp:DropDownList ID="NameKids" runat="server" DataTextField="NameKids" DataValueField="IDkids" class="form-control form-group position-relative has-icon-left" OnSelectedIndexChanged="ManagerTeam_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtLastName" runat="server" ControlToValidate="LastName" ErrorMessage="אנא הזן שם משפחה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="LastName" runat="server" CssClass="form-control" placeholder="שם משפחת הילד" />
                                                            <div class="form-control-position">
                                                                <i class="ft-user"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtNameFather" runat="server" ControlToValidate="NameFather" ErrorMessage="אבא, הזן את שמך" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="NameFather" runat="server" CssClass="form-control" placeholder="שם האבא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-user-secret"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtNameMother" runat="server" ControlToValidate="NameMother" ErrorMessage="אמא, הזיני את שמך" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="NameMother" runat="server" CssClass="form-control" placeholder="שם האמא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-user-secret"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtPhoneFather" runat="server" ControlToValidate="PhoneFather" ErrorMessage="אבא, הזן מספר נייד" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="REVTxtPhoneFather" runat="server" ControlToValidate="PhoneFather" ErrorMessage="הזן מספר נייד תקין בעל 10 ספרות" ValidationExpression="[0-9]{10}" Text="**" Font-Bold="True" ForeColor="Red"></asp:RegularExpressionValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="PhoneFather" runat="server" CssClass="form-control" placeholder="מספר נייד אבא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-phone"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtPhoneMother" runat="server" ControlToValidate="PhoneMother" ErrorMessage="אמא, הזיני מספר נייד" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="REVTxtPhoneMother" runat="server" ControlToValidate="PhoneMother" ErrorMessage="הזן מספר נייד תקין בעל 10 ספרות" ValidationExpression="[0-9]{10}" Text="**" Font-Bold="True" ForeColor="Red"></asp:RegularExpressionValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="PhoneMother" runat="server" CssClass="form-control" placeholder="מספר נייד אמא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-phone"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-12 col-sm-12 col-md-12">
                                                        <asp:RequiredFieldValidator ID="TxtEmail" runat="server" ControlToValidate="Email" ErrorMessage="אנא הזן כתובת מייל" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtEmail" runat="server" ControlToValidate="Email" ErrorMessage="הזן מייל חוקי" Text="**" Font-Bold="True" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="Email" runat="server" TextMode="Email" CssClass="form-control" placeholder="אימייל" />
                                                            <div class="form-control-position">
                                                                <i class="ft-mail"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtCompany" runat="server" ControlToValidate="ManagerTeam" ErrorMessage="אנא בחר מנהל צוות" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ManagerTeam" runat="server" DataTextField="FullName" DataValueField="IDteamMember" class="form-control form-group position-relative has-icon-left" OnSelectedIndexChanged="ManagerTeam_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameClass" ErrorMessage="אנא בחר כיתה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                                <asp:DropDownList ID="NameClass" runat="server" DataTextField="NameClass" DataValueField="IDclass" CssClass="form-control form-group position-relative has-icon-left">
                                                                    <asp:ListItem></asp:ListItem>
                                                                </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtPassword" runat="server" ControlToValidate="Password" ErrorMessage="אנא הזן סיסמא" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtPassword" runat="server" ControlToValidate="Password" ErrorMessage="לפחות 8 תווים  אות אחת קטנה, אות אחת גדולה, ובאנגלית " Text="**" Font-Bold="True" ForeColor="Red" ValidationExpression="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$"></asp:RegularExpressionValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="Password" runat="server" CssClass="form-control" placeholder="סיסמא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-key"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:CompareValidator ID="COMTxtRealPassword" runat="server" ErrorMessage="הסיסמא אינה תואמת" Font-Bold="True" Text="***" ControlToCompare="RealPassword" ControlToValidate="Password" ForeColor="Red"></asp:CompareValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="RealPassword" runat="server" TextMode="Password" CssClass="form-control" placeholder="אימות סיסמא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-key la la-key"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>








                                                <div class="row">

                                                    <div class="col-12 col-sm-12 col-md-12 ">
                                                        <div id="containerCH">
                                                            <b>העלאת תמונה</b>
                                                            <asp:FileUpload runat="server" ID="PicUpload" CssClass="btn btn-sm xor col-8 col-sm-12 col-md-12 " />
                                                        </div>
                                                    </div>

                                                    <style>
                                                        .xor {
                                                            background-color: #0026ff;
                                                            border: none;
                                                            color: #FFFFFF;
                                                            padding: 15px 32px;
                                                            text-align: center;
                                                            -webkit-transition-duration: 0.4s;
                                                            transition-duration: 0.4s;
                                                            text-decoration: none;
                                                            font-size: 14px;
                                                            cursor: pointer;
                                                            display: grid;
                                                            width: 250px;
                                                            margin: auto;
                                                            border: 2px dotted;
                                                        }

                                                        #containerCH {
                                                            text-align: center;
                                                        }
                                                    </style>
                                                    <script>
                                                        var input = document.querySelector("#PicUpload");

                                                        input.addEventListener("change", preview);
                                                        function preview() {
                                                            var fileObject = this.files[0];
                                                            var fileReader = new FileReader();
                                                            fileReader.readAsDataURL(fileObject);

                                                        }
                                                    </script>
                                                </div>
                                                <div class="row ">
                                                    <asp:Image runat="server" ID="Url" CssClass="col-12 col-sm-12 col-md-12 not" />
                                                    <style>
                                                        .not {
                                                            height: 50%;
                                                        }
                                                    </style>
                                                </div>
                                                <asp:Literal ID="Worng" runat="server" />

                                                <div class="card-body">
                                                    <asp:LinkButton ID="btnEditProfileParents" OnClick="btnEditProfileParents_Click" CssClass="btn btn-outline-success btn-block" runat="server"><i class="la la-save"></i>שמור שינויים</asp:LinkButton>
                                                    <asp:LinkButton ID="Cancel" OnClick="Cancel_Click" CausesValidation="false" CssClass="btn btn-outline-danger btn-block" runat="server"><i class="la la-undo"></i>בטל</asp:LinkButton>
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                                </div>
                                            </asp:Panel>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <script src="/app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->
        <!-- BEGIN: Page Vendor JS-->
        <script src="/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
        <!-- END: Page Vendor JS-->
        <!-- BEGIN: Theme JS-->
        <script src="/app-assets/js/core/app-menu.js"></script>
        <script src="/app-assets/js/core/app.js"></script>
        <!-- END: Theme JS-->
        <!-- BEGIN: Page JS-->
        <script src="/app-assets/js/scripts/forms/form-login-register.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-3.4.1.js"></script>
        <asp:Literal runat="server" ID="kaskaval"></asp:Literal>
                </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ManagerTeam" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>

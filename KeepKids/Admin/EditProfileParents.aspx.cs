﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;

namespace KeepKids.Admin
{
    public partial class EditProfileParents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                FillData();

            }

        }
        protected void FillData()
        {
            if (Session["ManagerName"] == null && Session["ParentsName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null && Session["TheKids"] == null)
            {
                Response.Redirect("Login.aspx");
            }


            Parents EditParents = (Parents)Session["ParentsName"];


            NameKids.DataSource = Parents.viewNameWithoutFamili(EditParents.UserNameParents);
            NameKids.DataBind();
            NameKids.DataTextField = "NameKids";
            NameKids.DataValueField = "IDkids";
            NameKids.DataBind();


            //FirstName.Text = EditParents.NameKids;
            LastName.Text = EditParents.LastNameKids;
            NameFather.Text = EditParents.NameFather;
            NameMother.Text = EditParents.NameMother;
            PhoneFather.Text = EditParents.PhoneFather;
            PhoneMother.Text = EditParents.PhoneMother;
            Email.Text = EditParents.UserNameParents;
            ManagerTeam.DataTextField = EditParents.FullName;
            NameClass.DataTextField = EditParents.NameClass;
            Password.Text = EditParents.PasswordParents;
            kaskaval.Text = EditParents.IDkids.ToString();
            Url.ImageUrl = "../pics/" + EditParents.PicKids;
            Url.AlternateText = EditParents.PicKids;

            //רשימת כיתות
            NameClass.DataSource = Parents.viewAllClass(EditParents.IDteamMember);
            NameClass.DataBind();
            NameClass.DataTextField = "NameClass";
            NameClass.DataValueField = "IDclass";
            NameClass.DataBind();
            NameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
            for (int i = 0; i < NameClass.Items.Count; i++)
            {
                if (NameClass.Items[i].Value == EditParents.IDclass.ToString())
                {
                    NameClass.Items[i].Selected = true;
                    break;

                }
            }




            //רשימת אחראי צוות
            ManagerTeam.DataSource = Manager.viewAll();
            ManagerTeam.DataBind();
            ManagerTeam.DataTextField = "FullName";
            ManagerTeam.DataValueField = "IDteamMember";
            ManagerTeam.DataBind();
            ManagerTeam.Items.Insert(0, new ListItem("שם מנהל הצוות", "0"));
            for (int i = 0; i < ManagerTeam.Items.Count; i++)
            {
                if (ManagerTeam.Items[i].Value == EditParents.IDteamMember.ToString())
                {
                    ManagerTeam.Items[i].Selected = true;
                    break;

                }
            }



        }
        protected void ManagerTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameClass.DataSource = Parents.viewAllClass(int.Parse(ManagerTeam.SelectedValue));
            NameClass.DataBind();
            NameClass.DataTextField = "NameClass";
            NameClass.DataValueField = "IDclass";
            NameClass.DataBind();
            NameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
            NameClass.SelectedIndex = 0;
        }
        protected void btnEditProfileParents_Click(object sender, EventArgs e)
        {

            if (PicUpload.FileName == "")
            {
                Session["ParentsPic"] = Url.AlternateText;
            }
            if (PicUpload.FileName != "")
            {
                PicUpload.SaveAs(Path.Combine("C:\\Users\\שניאור\\source\\repos\\KeepKids\\KeepKids\\pics\\", PicUpload.FileName));
                Session["ParentsPic"] = PicUpload.FileName;
            }

            Parents Tmp1 = new Parents()
            {
                IDteamMember = int.Parse(ManagerTeam.SelectedValue),
                IDclass = int.Parse(NameClass.SelectedValue),
                NameKids = NameKids.Text,
                LastNameKids = LastName.Text,
                NameFather = NameFather.Text,
                NameMother = NameMother.Text,
                PhoneFather = PhoneFather.Text,
                PhoneMother = PhoneMother.Text,
                UserNameParents = Email.Text,
                PasswordParents = Password.Text,
                PicKids = (string)Session["ParentsPic"],
                IDkids = int.Parse(kaskaval.Text)

            };




            if (Tmp1.CheckUpdate())
            {
                Session["ParentsName"] = Tmp1;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('העדכון בוצע בהצלחה');window.location ='Default.aspx';", true);

            }

            else
            {
                Worng.Text = "<b class='text-danger'>*שדה אחד או יותר אינם תקינים </b>";
            }


        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('העדכון בוטל ');window.location ='Default.aspx';", true);

        }


    }
}
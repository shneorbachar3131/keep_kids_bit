﻿<%@ Page Title=" הוספת פעילות חינוכית" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="EducationAct.aspx.cs" Inherits="KeepKids.Admin.EducationAct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/datedropper.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/timedropper.min.css">
    <link href="css/custme.css" rel="stylesheet" />
    <script src="js/jquery-3.4.1.js"></script>
    <!-- END: Page CSS-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body ">
                <!-- Basic form layout section start -->


                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card italya">
                            <div class="card-header italya">
                                <h1 class="font-large-2 text-center">יצירה חדשה של פעילות</h1>
                                <asp:Literal runat="server" ID="Worng"></asp:Literal>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">

                                    <div class="form-body">

                                        <div class="form-group row text-center HideAdmins">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4 col-6">
                                                <label>שם הכיתה</label><asp:RequiredFieldValidator ID="TxtNameClass" runat="server" ControlToValidate="NameClass" ErrorMessage="אנא בחר כיתה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="NameClass" runat="server" DataTextField="NameClass" DataValueField="IDclass" CssClass="form-control round">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-4 col-6">
                                                <label>תאריך</label><asp:RequiredFieldValidator ID="TxtdropTextColor" runat="server" ControlToValidate="dropTextColor" ErrorMessage="אנא הזן תאריך" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="dropTextColor" CssClass="form-control round" runat="server" placeholder="01/01/2019"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                        <div class=" form-group row text-center HideManager">
                                            <div class="col-md-4">
                                                <label>שם הגננת</label><asp:RequiredFieldValidator ID="TxtTheNameTeamMember" runat="server" ControlToValidate="TheNameTeamMember" ErrorMessage="אנא בחר שם גגנת" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="TheNameTeamMember" runat="server" DataTextField="nameTeamMember" DataValueField="IDteamMember" CssClass="form-control round" OnSelectedIndexChanged="TheNameTeamMember_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-4">
                                                <label>שם הכיתה</label><asp:RequiredFieldValidator ID="TxTTheNameClass" runat="server" ControlToValidate="TheNameClass" ErrorMessage="אנא בחר כיתה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="TheNameClass" runat="server" DataTextField="NameClass" DataValueField="IDclass" CssClass="form-control round">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-4">
                                                <label>תאריך</label><asp:RequiredFieldValidator ID="TxtdropBackgroundColor" runat="server" ControlToValidate="dropBackgroundColor" ErrorMessage="אנא הזן תאריך" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="dropBackgroundColor" CssClass="form-control round" runat="server" placeholder="01/01/2019"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group row text-center">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <label>תיאור הפעילות</label><asp:RequiredFieldValidator ID="TxtAction" runat="server" ControlToValidate="MyAction" ErrorMessage="אנא הזרק תוכן אירוע" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="MyAction" runat="server" CssClass="form-control round" TextMode="MultiLine" Rows="5" Height="100"></asp:TextBox>

                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <button type="button" id="Cancel" onclick="reload()" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i>בטל
                                               
                                        </button>
                                        <asp:LinkButton ID="btnCreate" OnClick="btnCreate_Click" CssClass="btn btn-primary" runat="server"><i class="la la-check-square-o"></i>צור</asp:LinkButton>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">
    <script>
        $('#Cancel').click(function () {
            location.reload();
        });
    </script>
    <script src="/app-assets/vendors/js/extensions/datedropper.min.js"></script>
    <script src="/app-assets/js/scripts/extensions/date-time-dropper.js"></script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>

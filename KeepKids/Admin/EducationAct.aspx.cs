﻿using KeepKids.BLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KeepKids.Admin
{
    public partial class EducationAct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }

        }

        protected void FillData()
        {
            if (Session["ManagerName"] == null && Session["TheTeamMember"] is null)
            {
                if (Session["AdminsName"] is null && Session["TheAdmin"] is null)
                {
                    if (Session["ParentsName"] == null && Session["TheKids"] is null)
                    {
                        Response.Redirect("Login.aspx");
                    }

                }
            }
            if (Session["ManagerName"] != null || Session["TheTeamMember"] != null)
            {
                dropBackgroundColor.Text = "01/01/2019";
                //כיתת לימוד
                Manager Tmp = (Manager)Session["ManagerName"];
                NameClass.DataSource = ToReport.ShowClass(Tmp.IDteamMember);
                NameClass.DataBind();
                NameClass.DataTextField = "NameClass";
                NameClass.DataValueField = "IDclass";
                NameClass.DataBind();
                NameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
                NameClass.SelectedIndex = 0;
            }
            if (Session["AdminsName"] != null || Session["TheAdmin"] != null)
            {
                dropTextColor.Text = "01/01/2019";
                Admins Tmp = (Admins)Session["AdminsName"];
                TheNameTeamMember.DataSource = Admins.viewNameTeamMember(Tmp.IDcompany);
                TheNameTeamMember.DataBind();
                TheNameTeamMember.DataTextField = "FullName";
                TheNameTeamMember.DataValueField = "IDteamMember";
                TheNameTeamMember.DataBind();
                TheNameTeamMember.Items.Insert(0, new ListItem("בחר גננת", "0"));

                TheNameClass.DataSource = Admins.ShowClass(int.Parse(TheNameTeamMember.SelectedValue));
                TheNameClass.DataBind();
                TheNameClass.DataTextField = "NameClass";
                TheNameClass.DataValueField = "IDclass";
                TheNameClass.DataBind();
                TheNameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
            }

        }


        protected void TheNameTeamMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            TheNameClass.DataSource = Admins.ShowClass(int.Parse(TheNameTeamMember.SelectedValue));
            TheNameClass.DataBind();
            TheNameClass.DataTextField = "NameClass";
            TheNameClass.DataValueField = "IDclass";
            TheNameClass.DataBind();
            TheNameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Session["ManagerName"] != null || Session["TheTeamMember"] != null)
            {
                DateTime dt = DateTime.ParseExact(dropTextColor.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);


                var dateAndTime = DateTime.Now;
                var date = dateAndTime.Date;
                DateTime chackdate = dt;

                if (chackdate < date)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('התאריך הנבחר אינו תקין!')", true);

                }
                else
                {
                    Manager.InserstEducationAct(int.Parse(NameClass.SelectedValue), dropTextColor.Text, MyAction.Text);
                    dropTextColor.Text = "";
                    MyAction.Text = "";
                    NameClass.SelectedIndex = 0;
                    Worng.Text = "<h2 class='text-success'>*יצירת האירוע בוצעה בהצלחה  </h2>";
                }



            }
            if (Session["AdminsName"] != null || Session["TheAdmin"] != null)
            {
                //בדיקת תקינות תאריך
                DateTime dt = DateTime.ParseExact(dropBackgroundColor.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var dateAndTime = DateTime.Now;
                var date = dateAndTime.Date;
                DateTime chackdate = dt;

                if (chackdate < date)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('התאריך הנבחר אינו תקין!')", true);

                }

                else
                {
                    Admins.InserstEducationAct(int.Parse(TheNameClass.SelectedValue), dropBackgroundColor.Text, MyAction.Text);
                    dropBackgroundColor.Text = "";
                    MyAction.Text = "";
                    TheNameTeamMember.SelectedIndex = 0;
                    TheNameClass.SelectedIndex = 0;
                    Worng.Text = "<h2 class='text-success'>*יצירת האירוע בוצעה בהצלחה </h2>";
                }
            }
        }

    }
}























//if (Session["AdminsName"] != null)
//{
//    Admins Tmp1 = (Admins)Session["AdminsName"];
//    //שם גן
//    KinderGarden.DataSource = ToReport.NameKinderGardenMethod(Tmp1.IDcompany, Tmp1.IDadmin);
//    KinderGarden.DataBind();
//    KinderGarden.DataTextField = "NameKindergarten";
//    KinderGarden.DataValueField = "IDkindergarten";
//    KinderGarden.DataBind();


//    //כיתת לימוד
//    NameClass.DataSource = ToReport.ShowClassAdmin(int.Parse(KinderGarden.SelectedValue));
//    NameClass.DataBind();
//    NameClass.DataTextField = "NameClass";
//    NameClass.DataValueField = "IDclass";
//    NameClass.DataBind();
//    NameClass.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));
//}
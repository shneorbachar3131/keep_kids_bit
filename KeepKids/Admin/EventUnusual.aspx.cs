﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using KeepKids.utils;

namespace KeepKids.Admin
{
    public partial class EventUnusual : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Session["ManagerName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null)
                {
                    Response.Redirect("Default.aspx");
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('אינך יכול להיכנס לאזור זה')", true);

                }
                //רשימת אחראי צוות
                if (Session["ManagerName"] != null)
                {
                    Manager Tmp1 = (Manager)Session["ManagerName"];
                    ManagerTeam.DataSource = Manager.viewByid(Tmp1.IDteamMember);
                    ManagerTeam.DataTextField = "FullName";
                    ManagerTeam.DataValueField = "IDteamMember";
                    ManagerTeam.DataBind();
                    for (int i = 0; i < ManagerTeam.Items.Count; i++)
                    {
                        if (ManagerTeam.Items[i].Value == Tmp1.IDteamMember.ToString())
                        {
                            ManagerTeam.Items[i].Selected = true;
                            break;

                        }
                    }


                    //רשימת ילדים
                    NameKids.DataSource = Manager.viewNameKids(Tmp1.IDteamMember);
                    NameKids.DataBind();
                    NameKids.DataTextField = "NameKids";
                    NameKids.DataValueField = "IDkids";
                    NameKids.DataBind();
                    NameKids.Items.Insert(0, new ListItem("שם ילד לדיווח", "0"));
                    NameKids.SelectedIndex = 0;
                }
                if (Session["AdminsName"] != null)
                {

                    Admins Tmp1 = (Admins)Session["AdminsName"];
                    ManagerTeam.DataSource = Admins.viewByAdmin(Tmp1.IDcompany);
                    ManagerTeam.DataTextField = "FullName";
                    ManagerTeam.DataValueField = "IDteamMember";
                    ManagerTeam.DataBind();
                    ManagerTeam.Items.Insert(0, new ListItem("שם מנהל הצוות", "0"));
                    ManagerTeam.SelectedIndex = 0;


                    //רשימת ילדים
                    NameKids.DataSource = Admins.viewNameKids(int.Parse(ManagerTeam.SelectedValue));
                    NameKids.DataBind();
                    NameKids.DataTextField = "NameKids";
                    NameKids.DataValueField = "IDkids";
                    NameKids.DataBind();
                    NameKids.Items.Insert(0, new ListItem("שם ילד לדיווח", "0"));
                    NameKids.SelectedIndex = 0;
                }

            }
        }



        protected void ManagerTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["AdminsName"] != null)
            {
                //רשימת ילדים
                NameKids.DataSource = Admins.viewNameKidsB(int.Parse(ManagerTeam.SelectedValue));
                NameKids.DataBind();
                NameKids.DataTextField = "NameKids";
                NameKids.DataValueField = "IDkids";
                NameKids.DataBind();
                NameKids.Items.Insert(0, new ListItem("שם ילד לדיווח", "0"));
            }
        }


        protected void CancelEventUnusual_Click(object sender, EventArgs e)
        {
            if (ManagerTeam.SelectedIndex == 0 && NameKids.SelectedIndex == 0 && ReviewEventUnusual.Text == "")
            {

                Response.Redirect("Default.aspx");
            }
            else
            {
                ManagerTeam.SelectedIndex = 0;
                NameKids.SelectedIndex = 0;
                ReviewEventUnusual.Text = " ";
            }
        }

        protected void SaveEventUnusual_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.ParseExact(dropTextWeight.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);


            var dateAndTime = DateTime.Now;
            var date = dateAndTime.Date;
            DateTime chackdate = dt;

            if (chackdate > date)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('התאריך הנבחר אינו תקין!')", true);

            }
            else
            {
                Parents Tmp1 = new Parents()
                {
                    IDteamMember = int.Parse(ManagerTeam.SelectedValue),
                    IDkids = int.Parse(NameKids.SelectedValue),
                    DescriptionEvent = ReviewEventUnusual.Text,
                    DateEventUnusual = dropTextWeight.Text,

                };

                if (Tmp1.CheckEventUnusual())
                {

                    Worng.Text = "<h2 class='text-success'>*האירוע החריג דווח בהצלחה </h2>";
                    GlobFuncs.SendMail(Tmp1.UserNameParents, "אירוע חריג קרה", Tmp1.FullName, "KeepKids  זה הגן שלי", "<html><body><tbody><tr><td width='100%' align='center' style='background-color:#91ccec ;padding:25px 10px 5px 10px'><table cellpadding='0' cellspacing='0' align='center' border='0' dir='rtl' style='max-width:580px!important;width:95%;height:auto;font-family:Arial'><tbody><tr><td width='100%' align='right' style='font-size:13px;font-family:Arial;direction:rtl;font-size:11pt;padding:0px 0px 10px 0px'></td></tr><tr><td width='100%' align='right' style='font-size:11pt;font-family:Arial;direction:rtl;padding:0px 0px 3px 0px'>שלום</td></tr><tr><td width='100%' align='right' style='font-family:Arial;padding:10px 0px 10px 0px'><table cellpadding='0' cellspacing='0' align='right' border='0' dir='rtl' style='max-width:580px!important;width:99%;height:auto;font-family:Arial;border:1px solid #dfdfdf;background-color:#ffffff'><tbody><tr><td width='100%' align='center' style='padding:10px 0px 10px 0px'><table cellpadding='0' cellspacing='0' align='center' border='0' dir='rtl' style='max-width:580px!important;width:94%;height:auto;font-family:Arial'><tbody><tr><td width='80%' align='right' style='padding:10px 0px 0px 0px'><table cellpadding='0' cellspacing='0' align='right' border='0' dir='rtl' width='100%'><tbody><tr><td width='100%' align='right' style='font-family:Arial;font-size:13pt;font-weight:bold;color:#505050;padding:0px 0px 10px 0px'>" + Tmp1.NameFather + "  " + "ו" + Tmp1.NameMother + "  " + "היקרים!! " + "</td></tr></tbody></table></td><td width='20%' align='left' style='vertical-align:top;padding:10px 10px 0px 0px'><img  src='C:\\Users\\שניאור\\source\\repos\\KeepKids\\KeepKids\\pics\\'," + Tmp1.PicKids + "  width='100%' border='0' style='display:block' class='CToWUd'</td></tr><tr><td width='100%' align='right' colspan='2' style='padding:10px 0px 10px 0px;font-family:Arial;font-size:11pt;color:#505050;line-height:180%'>" + Tmp1.DescriptionEvent + " </td></tr><tr><td width='100%' align='left' colspan='2'><b style='text-decoration:underline;font-family:arial;font-size:11pt;color:#ce1bef'>בשורות טובות, " + Tmp1.FullName + "</b></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></body></html>");


                    ManagerTeam.SelectedIndex = 0;
                    NameKids.SelectedIndex = 0;
                    ReviewEventUnusual.Text = "";
                    dropTextWeight.Text = "";
                }

                else
                {
                    Worng.Text = "<b class='text-danger'>*שדה אחד או יותר אינם תקינים </b>";
                }
            }

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs" Inherits="KeepKids.Admin.ForgetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>שחזור סיסמא</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"/>
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard."/>
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard"/>
    <meta name="author" content="PIXINVENT"/>
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png"/>
    <link rel="shortcut icon" type="image/x-icon" href="../pics/‏‏MYKIND.png"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet"/>

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors-rtl.min.css"/>
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap-extended.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/colors.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/components.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/custom-rtl.css"/>
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-content-menu.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css"/>
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css"/>
   <link href="css/background.css" rel="stylesheet" />
    <!-- END: Custom CSS-->

</head>
<body class="vertical-layout vertical-content-menu 1-column   blank-page" data-open="click" data-menu="vertical-content-menu" data-col="1-column">

    <form id="form1" runat="server" class="form-horizontal">
         <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                                <div class="card-header border-0 pb-0">
                                    <div class="card-title text-center">
                                        <img src="../pics/‏‏MYKIND.png" alt="branding logo"/>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>אנו נשלח לך קישור לאיפוס הסיסמה.</span></h6>
                                </div>
                                <asp:Panel DefaultButton="btnForgetPassword" runat="server">
                                <div class="card-content">
                                    <div class="card-body">
                                            <fieldset class="form-group position-relative has-icon-left">
                                                 <asp:TextBox ID="ForgetPass" runat="server" CssClass="form-control" TextMode="Email"   placeholder="כתובת המייל שלך"/>
                                                <asp:Literal ID="Worng" runat="server" />
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                            </fieldset>
                                            <asp:LinkButton ID="btnForgetPassword" OnClick="btnForgetPassword_Click" CssClass="btn btn-outline-info btn-lg btn-block" runat="server" ><i class="ft-unlock"></i>שחזר סיסמא</asp:LinkButton>
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="card-footer border-0">
                                    <p class="float-sm-left text-center"><a href="login.aspx" class="card-link">התחבר</a></p>
                                   
                                     <div class="btn-group float-right mb-2">
                                                    <button type="button" class="btn btn-secondary">חדש? הרשם!!</button>
                                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                                    </button>
                                                    <div class="dropdown-menu">
                                                       <a class="dropdown-item" href="RegisterAdmins.aspx">הרשמה מנהלים</a>
                                                    <a class="dropdown-item" href="register.aspx">הרשמה צוות</a>
                                                    <a class="dropdown-item" href="RegisterParents.aspx">הרשמה הורים</a>
                                                    </div>
                                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    </form>
    <!-- BEGIN: Vendor JS-->
    <script src="/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/vendors/js/ui/headroom.min.js"></script>
    <script src="/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/app-assets/js/core/app-menu.js"></script>
    <script src="/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/app-assets/js/scripts/forms/form-login-register.js"></script>
    <!-- END: Page JS-->
</body>
</html>

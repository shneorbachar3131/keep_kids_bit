﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using KeepKids.DAL;


namespace KeepKids.Admin
{
    public partial class MyKinderGarden : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();

            }
        }

        protected void FillData()
        {
            if (Session["ManagerName"] != null)
            {
                TheNameTeamMember.Visible = false;
                NameKids.Visible = false;
                HIDEManager.Visible = false;
                HIDEboth2.Visible = false;
                HIDEboth3.Visible = false;
                Manager Tmp = (Manager)Session["ManagerName"];
                idcompany.Value = Tmp.IDcompany.ToString();
                Url.ImageUrl = "../pics/" + Tmp.PicManager;
                NameTeamMember.Text = Tmp.FullName;
                KinderGarden.Text = Manager.NameKinderGarden(Tmp.IDteamMember);
                //List<Manager> Lstn = (List<Manager>)Session["ListKidsAndMore"];
                //ListKids.DataSource = Manager.ListKids(Tmp.IDteamMember);
                //ListKids.DataBind();

                NameTeamMember1.Text = Tmp.FullName;

                List<Manager> mm = (List<Manager>)Session["Mail"];
                RptMail.DataSource = Manager.ViewMail(Tmp.IDteamMember);
                RptMail.DataBind();

                NameClass.DataSource = ToReport.ShowClass(Tmp.IDteamMember);
                NameClass.DataBind();
                NameClass.DataTextField = "NameClass";
                NameClass.DataValueField = "IDclass";
                NameClass.DataBind();
                NameClass.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));
            }
            if (Session["ParentsName"] != null)
            {

                TheNameTeamMember.Visible = false;
                HIDEParents.Visible = false;
                HIDEboth1.Visible = false;
                HIDEboth2.Visible = false;
                Hide_edit1.Visible = false;
                Hide_edit3.Visible = false;
                Parents Tmp = (Parents)Session["ParentsName"];
                NameKids.DataSource = Parents.viewNameKidsById(Tmp.UserNameParents);
                NameKids.DataBind();
                NameKids.DataTextField = "NameKids";
                NameKids.DataValueField = "IDkids";
                NameKids.DataBind();

                NameClass.DataSource = Parents.ShowClass(int.Parse(NameKids.SelectedValue));
                NameClass.DataBind();
                NameClass.DataTextField = "NameClass";
                NameClass.DataValueField = "IDclass";
                NameClass.DataBind();

                int Class = int.Parse(NameClass.SelectedValue);
                ListKids.DataSource = Parents.ListKids(Class);
                ListKids.DataBind();

                NameKidsTitle.Text = Parents.ViewNameKids(int.Parse(NameKids.SelectedValue));
                Url.ImageUrl = "../pics/" + Parents.TakePicture(int.Parse(NameKids.SelectedValue));
                NameTeamMember.Text = Parents.FullNameTM(Tmp.IDteamMember);
                KinderGarden.Text = Manager.NameKinderGarden(Tmp.IDteamMember);

                List<Manager> mm = (List<Manager>)Session["Mail"];
                RptMail.DataSource = Manager.ViewMail(Tmp.IDteamMember);
                RptMail.DataBind();


            }
            if (Session["AdminsName"] != null)
            {
                HIDEboth1.Visible = false;
                HIDEboth3.Visible = false;
                NameKids.Visible = false;
                Admins Tmp = (Admins)Session["AdminsName"];
                idcompany.Value = Tmp.IDcompany.ToString();
                TheNameTeamMember.DataSource = Admins.viewNameTeamMember(Tmp.IDcompany);
                TheNameTeamMember.DataBind();
                TheNameTeamMember.DataTextField = "FullName";
                TheNameTeamMember.DataValueField = "IDteamMember";
                TheNameTeamMember.DataBind();

                //סתם בדיקה אפשר למחוק
                if (TheNameTeamMember.SelectedValue != "")
                {
                    NameClass.DataSource = Admins.ShowClass(int.Parse(TheNameTeamMember.SelectedValue));
                    NameClass.DataBind();
                    NameClass.DataTextField = "NameClass";
                    NameClass.DataValueField = "IDclass";
                    NameClass.DataBind();
                }
                //סתם בדיקה אפשר למחוק
                if (NameClass.SelectedValue != "")
                {
                    ListKids.DataSource = Admins.ListKidsOPTION(int.Parse(NameClass.SelectedValue), int.Parse(TheNameTeamMember.SelectedValue));
                    ListKids.DataBind();
                }

                KinderGarden.Text = Manager.NameKinderGarden(int.Parse(TheNameTeamMember.SelectedValue));
                Url.ImageUrl = "../pics/" + Admins.TakePicture(int.Parse(TheNameTeamMember.SelectedValue));
                RptMail.DataSource = Manager.ViewMail(int.Parse(TheNameTeamMember.SelectedValue));
                RptMail.DataBind();



                NewKinderGarden.DataSource = Admins.NewKinderGarden(Tmp.IDcompany);
                NewKinderGarden.DataBind();
                NewKinderGarden.DataTextField = "FullName";
                NewKinderGarden.DataValueField = "IDteamMember";
                NewKinderGarden.DataBind();


                nTM.DataSource = Admins.IDkindergarten(Tmp.IDcompany);
                nTM.DataBind();
                nTM.DataTextField = "FullName";
                nTM.DataValueField = "IDteamMember";
                nTM.DataBind();
            }



        }

        protected void NameClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["ManagerName"] != null)
            {
                //כל התלמידים
                Manager Tmp = (Manager)Session["ManagerName"];
                int Class = int.Parse(NameClass.SelectedValue);
                List<ToReport> LstRE = (List<ToReport>)Session["RE"];
                ListKids.DataSource = Manager.ListKids(Tmp.IDteamMember, Class);
                ListKids.DataBind();
            }
            if (Session["AdminsName"] != null)
            {
                ListKids.DataSource = Admins.ListKidsOPTION(int.Parse(NameClass.SelectedValue), int.Parse(TheNameTeamMember.SelectedValue));
                ListKids.DataBind();
            }

        }

        protected void NameKids_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["ParentsName"] != null)
            {
                NameClass.DataSource = Parents.ShowClass(int.Parse(NameKids.SelectedValue));
                NameClass.DataBind();
                NameClass.DataTextField = "NameClass";
                NameClass.DataValueField = "IDclass";
                NameClass.DataBind();

                ListKids.DataSource = Parents.ListKidsOPTION(int.Parse(NameKids.SelectedValue));
                ListKids.DataBind();

                NameKidsTitle.Text = Parents.ViewNameKids(int.Parse(NameKids.SelectedValue));
                Url.ImageUrl = "../pics/" + Parents.TakePicture(int.Parse(NameKids.SelectedValue));
                NameTeamMember.Text = Parents.FullNameTM2(int.Parse(NameKids.SelectedValue));
                KinderGarden.Text = Manager.NameKinderGarden2(int.Parse(NameKids.SelectedValue));

                RptMail.DataSource = Parents.ViewMail(int.Parse(NameKids.SelectedValue));
                RptMail.DataBind();

            }

        }
        //מנהל
        protected void TheNameTeamMember_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameClass.DataSource = Admins.ShowClass(int.Parse(TheNameTeamMember.SelectedValue));
            NameClass.DataBind();
            NameClass.DataTextField = "NameClass";
            NameClass.DataValueField = "IDclass";
            NameClass.DataBind();

            KinderGarden.Text = Manager.NameKinderGarden(int.Parse(TheNameTeamMember.SelectedValue));
            Url.ImageUrl = "../pics/" + Admins.TakePicture(int.Parse(TheNameTeamMember.SelectedValue));
            RptMail.DataSource = Manager.ViewMail(int.Parse(TheNameTeamMember.SelectedValue));
            RptMail.DataBind();


            //סתם בדיקה אפשר למחוק
            if (NameClass.SelectedValue != "")
            {
                ListKids.DataSource = Admins.ListKidsOPTION(int.Parse(NameClass.SelectedValue), int.Parse(TheNameTeamMember.SelectedValue));
                ListKids.DataBind();
            }
        }

        protected void CreateKinderGareden_Click(object sender, EventArgs e)
        {

            if (Session["AdminsName"] != null)
            {
                if (NameKinderGarden.Text is "" || Address.Text is "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('אנא הזרק תוכן')", true);

                }
                else
                {
                    AdminsDAL.InsertNewKinder(int.Parse(NewKinderGarden.SelectedValue), NameKinderGarden.Text, Address.Text);
                    // Response.Redirect(Request.RawUrl);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('נוסף בהצלחה ');window.location ='MyKinderGarden.aspx';", true);
                }

            }
        }

        protected void CreateClass_Click(object sender, EventArgs e)
        {
            if (CLASS.Text is "" || GroupAge.Text is "")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('אנא הזרק תוכן')", true);

            }
            else
            {
                if (Session["ManagerName"] != null)
                {
                    Manager Tmp = (Manager)Session["ManagerName"];
                    if (Tmp.CheckClass(CLASS.Text, Tmp.IDteamMember))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + CLASS.Text + " " + "קיים כבר" + "')", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('כיתה נוספה בהצלחה');window.location ='MyKinderGarden.aspx';", true);
                        Manager.AddClass(Manager.IDkinderGarden(Tmp.IDteamMember), CLASS.Text, GroupAge.Text);
                        CLASS.Text = null; GroupAge.Text = null;
                    }

                }
                if (Session["AdminsName"] != null)
                {
                    Admins o = new Admins();
                    if (o.CheckClass(CLASS.Text, int.Parse(nTM.SelectedValue)))
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + CLASS.Text + " " + "קיים כבר" + "')", true);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('כיתה נוספה בהצלחה');window.location ='MyKinderGarden.aspx';", true);
                        Manager.AddClass(int.Parse(nTM.SelectedValue), CLASS.Text, GroupAge.Text);
                        CLASS.Text = null; GroupAge.Text = null;
                    }

                }
            }


        }


    }
}
﻿<%@ Page Title="ההודעות שלי" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="MyMessageBox.aspx.cs" Inherits="KeepKids.Admin.MyMessageBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/app-email.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="css/custme.css" rel="stylesheet" />
    <!-- END: Page CSS-->
    <style>
        .alicen {
            text-align: center;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div id="bodyContainer">
        <div class="app-content content pt-1 emptyBlock1000">
            <div class="sidebar-left">
                <div class="sidebar">
                    <div class="email-app-menu col-md-12 card">
                        <h6 class="text-muted text-bold-500 mb-1">תיבת ההודעות</h6>
                        <div class="row">
                        <div class="col-6"><input type="text" id="search-term" class="btn border-blue" placeholder="כאן מחפשים" /></div>
                        <div class="col-4"><button type="button" id="search-button" value="search" class="btn"><i class="ficon ft-search"></i></button></div>                        
                       <div class="col-1"> <button type="button" id="next" class="btn float-right"><i class="la la-angle-double-left"></i></button></div>
                            </div>
                            
                            
                            
                        
                           <script>

                            function searchAndHighlight(searchTerm, selector){
                                if (searchTerm) {
                                    var selector = selector || "#bodyContainer";
                                    var searchTermRegEx = new RegExp(searchTerm, "ig");
                                    var matches = $(selector).text().match(searchTermRegEx);
                                    if (matches) {
                                        $('.highlighted').removeClass('highlighted');     //Remove old search highlights
                                        $(selector).html($(selector).html().replace(searchTermRegEx, "<span class='match'>" + searchTerm + "</span>"));
                                        $('.match:first').addClass('highlighted');
                                        $('#next').on('click', i = 1, function () {
                                            $('.match').removeClass('highlighted'); $('.match').eq(i).addClass('highlighted');
                                            if (i >= $('.match').length - 1) {
                                                i = 0;
                                            }
                                            i = i + 1;
                                        });




                                        if ($('.highlighted:first').length) {
                                            $(window).scrollTop($('.highlighted:first').position().top);
                                        }
                                        return true;
                                    }
                                }
                                return false;
                            }
                            $(document).ready(function () {
                                $('#search-button').on("click", function () {
                                    if (!searchAndHighlight($('#search-term').val())) {
                                        alert("No results found");
                                    }
                                });
                            });
                        </script>
                        <style>
                            .highlighted {
                                background-color: yellow;
                            }

                            .emptyBlock1000 {
                                height: 1000px;
                            }

                            .emptyBlock2000 {
                                height: 2000px;
                            }
                        </style>
                        <div class="list-group list-group-messages">
                            <asp:LinkButton ID="SendMessage" OnClick="SendMessage_Click" runat="server" CssClass="list-group-item list-group-item-action border-0" CausesValidation="false"><i class="la la-paper-plane-o mr-1"></i>שלח הודעה</asp:LinkButton>
                            <%--<a data-toggle="collapse" href="#collapse1" aria-expanded="true" aria-controls="collapse1" class="list-group-item list-group-item-action border-0"><i class="la la-paper-plane-o mr-1"></i>שלח הודעה</a>--%>
                            <asp:LinkButton ID="NewMessage" OnClick="NewMessage_Click" runat="server" CssClass="list-group-item list-group-item-action border-0" CausesValidation="false"><i class="ft-file mr-1"></i> הודעות חדשות</asp:LinkButton>
                            <%--<a  data-toggle="collapse" href="#collapse2" aria-expanded="false" aria-controls="collapse2" class="list-group-item list-group-item-action border-0"><i class="ft-file mr-1"></i> הודעות חדשות</a>--%>
                            <%--<a href="#" class="list-group-item list-group-item-action border-0"><i class="ft-star mr-1"></i> Starred<span class="badge badge-danger badge-pill float-right">3</span> </a>--%>
                            <asp:LinkButton ID="MessageReaded" OnClick="MessageReaded_Click" runat="server" CssClass="list-group-item list-group-item-action border-0" CausesValidation="false"><i class="la la-eye mr-1"></i>הודעות שנקראו</asp:LinkButton>
                            <asp:LinkButton ID="Items" OnClick="Items_Click" runat="server" CssClass="list-group-item list-group-item-action border-0" CausesValidation="false"><i class="ft-inbox mr-1"></i> פריטים ששלחתי </asp:LinkButton>
                            <%--<a href="#" class="list-group-item list-group-item-action border-0"><i class="ft-inbox mr-1"></i> פריטים שנשלחו </a>--%>
                            <%--<a href="#" class="list-group-item list-group-item-action border-0"><i class="ft-trash-2 mr-1"></i>אשפה</a>--%>
                            <asp:LinkButton ID="Trash" OnClick="Trash_Click" runat="server" CssClass="list-group-item list-group-item-action border-0" CausesValidation="false"> <i class="ft-trash-2 mr-1"></i>אשפה</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-right">
                <div class="content-body ">
                    <div class="card email-app-details">
                        <div class="card-content">
                            <div class="d-block d-lg-none mt-1 ml-1">
                                <%--<span class="btn btn-primary go-back"><i class="ft-arrow-left"></i> Back to Mails</span>--%>
                            </div>

                            <div class="email-app-title card-body" id="conten" runat="server">
                                <h3 class="list-group-item-heading" id="TitleTag" runat="server"></h3>
                            </div>



                            <div class="media-list">

                                <div id="collapse1" runat="server">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="TheSend" runat="server" CssClass="form-control" ReadOnly="true" />
                                                </div>
                                                <div class="col-md-6" id="HideSendToWho" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="SendToWho" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control" OnSelectedIndexChanged="SendToWho_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Value="0">למי תרצה לשלוח הודעה?</asp:ListItem>
                                                            <asp:ListItem Value="1">לכל הגננות</asp:ListItem>
                                                            <asp:ListItem Value="2">לגננת פרטני</asp:ListItem>
                                                            <asp:ListItem Value="3">לכל הורי גן ספציפי</asp:ListItem>
                                                            <asp:ListItem Value="4">להורה פרטני</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7"></div>
                                                <div class="col-md-4" id="HideKinderGarden" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="KinderGarden" runat="server" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideManagerTeam" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ManagerTeam" runat="server" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="TxtManagerTeam" runat="server" ControlToValidate="ManagerTeam" ErrorMessage="אנא בחר מנהל צוות" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideKinderGarden1" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="KinderGarden1" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="TxtKinderGarden1" runat="server" ControlToValidate="KinderGarden1" ErrorMessage="אנא בחר שם גן" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideNameKids" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="NameKids" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="TxtNameKids" runat="server" ControlToValidate="NameKids" ErrorMessage="אנא בחר שם בן ההורה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-1"></div>
                                            </div>


                                            <div class="row ">
                                                <div class="col-md-3">
                                                    <div class="form-group"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:TextBox ID="TitleMessage" runat="server" CssClass="form-control" placeholder="כותרת ההודעה" />
                                                        <asp:RequiredFieldValidator ID="TxtTitleMessage" runat="server" ControlToValidate="TitleMessage" ErrorMessage="אנא כתוב נושא להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group"></div>
                                                </div>
                                            </div>
                                            <h4 class="form text-center"><i class="fas fa-arrow-circle-up pr-1"></i>העלאת קובץ (אופציונלי)</h4>
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-12">
                                                    <div class="alicen">
                                                        <style>
                                                            input[type="file"] {
                                                                display: none;
                                                            }

                                                            .custom-file-upload {
                                                                border: 1px solid #ccc;
                                                                display: inline-block;
                                                                padding: 6px 12px;
                                                                cursor: pointer;
                                                            }
                                                        </style>
                                                        <label class="custom-file-upload">
                                                            <asp:FileUpload runat="server" ID="PicUpload" />
                                                            <i class="fas fa-arrow-circle-up fa-5x pr-2"></i><b>העלה קובץ</b>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:TextBox ID="ContentMessage" runat="server" CssClass="shneorbachar" TextMode="multiline" Columns="50" Rows="5" placeholder="תוכן ההודעה" />
                                                <asp:RequiredFieldValidator ID="TxtContentMessage" runat="server" ControlToValidate="ContentMessage" ErrorMessage="אנא כתוב תוכן להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>

                                            <asp:Button ID="Send" OnClick="Send_Click" Text="שלח הודעה" runat="server" CssClass="btn btn-success round btn-min-width mr-1 mb-1 waves-effect waves-light" />

                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                        </div>
                                    </div>
                                </div>

                                <div id="collapse2" runat="server">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="TheSendManager" runat="server" CssClass="form-control" ReadOnly="true" />
                                                </div>
                                                <div class="col-md-6" id="HideSendToWho1" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="SendToWho1" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control" OnSelectedIndexChanged="SendToWho1_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Value="0">למי תרצה לשלוח הודעה?</asp:ListItem>
                                                            <asp:ListItem Value="9">למנהל שלי</asp:ListItem>
                                                            <asp:ListItem Value="10">לכל ההורים</asp:ListItem>
                                                            <asp:ListItem Value="11">לכל הורי כיתה ספציפית</asp:ListItem>
                                                            <asp:ListItem Value="12">להורה פרטני</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row ">
                                                <div class="col-7"></div>
                                                <div class="col-md-4" id="HideMyAdmins" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="MyAdmins" runat="server" DataTextField="NameAdmin" DataValueField="IDadmin" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideNameKidsMANAGER" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="NameKidsMANAGER" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideNameClass" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="NameClass" runat="server" DataTextField="NameClass" DataValueField="IDclass" CssClass="form-control" OnSelectedIndexChanged="NameClass_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameClass" ErrorMessage="אנא בחר כיתה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideKidsClass" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="KidsClass" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control" OnSelectedIndexChanged="NameClass_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideNameKidsMANAGER1" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="NameKidsMANAGER1" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="NameKidsMANAGER1" ErrorMessage="אנא בחר בן הורה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-1"></div>
                                            </div>
                                            <div class="row ">
                                                <div class="col-md-3">
                                                    <div class="form-group"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:TextBox ID="TitleMessage1" runat="server" CssClass="form-control" placeholder="כותרת ההודעה" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TitleMessage1" ErrorMessage="אנא כתוב נושא להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group"></div>
                                                </div>
                                            </div>
                                            <h4 class="form text-center"><i class="fas fa-arrow-circle-up pr-1"></i>העלאת קובץ (אופציונלי)</h4>
                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-12">
                                                    <div class="alicen">
                                                        <style>
                                                            .alicen {
                                                                text-align: center;
                                                            }

                                                            input[type="file"] {
                                                                display: none;
                                                            }

                                                            .custom-file-upload {
                                                                border: 1px solid #ccc;
                                                                display: inline-block;
                                                                padding: 6px 12px;
                                                                cursor: pointer;
                                                            }
                                                        </style>
                                                        <label class="custom-file-upload">
                                                            <asp:FileUpload runat="server" ID="PicUpload1" />
                                                            <i class="fas fa-arrow-circle-up fa-5x pr-2"></i><b>העלה קובץ</b>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:TextBox ID="ContentMessage1" runat="server" CssClass="shneorbachar" TextMode="multiline" Columns="50" Rows="5" placeholder="תוכן ההודעה" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ContentMessage1" ErrorMessage="אנא כתוב תוכן להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>

                                            <asp:Button ID="Send1" OnClick="Send_Click" Text="שלח הודעה" runat="server" CssClass="btn btn-success round btn-min-width mr-1 mb-1 waves-effect waves-light" />

                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                        </div>
                                    </div>
                                </div>


                                <div id="collapse3" runat="server">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="TheSendParents" runat="server" CssClass="form-control" ReadOnly="true" />
                                                </div>
                                                <div class="col-md-6" id="HideSendToWho2" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="SendToWho2" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control" OnSelectedIndexChanged="SendToWho2_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem Value="0">למי תרצה לשלוח הודעה?</asp:ListItem>
                                                            <asp:ListItem Value="20">לגננת של הילד</asp:ListItem>
                                                            <asp:ListItem Value="21">לאחד מהורי ילדי הגן</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7"></div>
                                                <div class="col-md-4" id="HideIDteamMember" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="IDteamMember" runat="server" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="HideIDkids" runat="server">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="IDkids" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="IDkids" ErrorMessage="אנא בחר בן הורה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-1"></div>
                                            </div>
                                            <div class="row ">
                                                <div class="col-md-3">
                                                    <div class="form-group"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:TextBox ID="TitleMessage2" runat="server" CssClass="form-control" placeholder="כותרת ההודעה" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TitleMessage2" ErrorMessage="אנא כתוב נושא להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group"></div>
                                                </div>
                                            </div>
                                            <h4 class="form text-center"><i class="fas fa-arrow-circle-up pr-1"></i>העלאת קובץ (אופציונלי)</h4>

                                            <div class="row">
                                                <div class="col-12 col-sm-12 col-md-12">
                                                    <div class="alicen">
                                                        <style>
                                                            input[type="file"] {
                                                                display: none;
                                                            }

                                                            .custom-file-upload {
                                                                border: 1px solid #ccc;
                                                                display: inline-block;
                                                                padding: 6px 12px;
                                                                cursor: pointer;
                                                            }
                                                        </style>
                                                        <label class="custom-file-upload">
                                                            <asp:FileUpload runat="server" ID="PicUpload2" />
                                                            <i class="fas fa-arrow-circle-up fa-5x pr-2"></i><b>העלה קובץ</b>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:TextBox ID="ContentMessage2" runat="server" CssClass="shneorbachar" TextMode="multiline" Columns="50" Rows="5" placeholder="תוכן ההודעה" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ContentMessage2" ErrorMessage="אנא כתוב תוכן להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>

                                            <asp:Button ID="Send2" OnClick="Send_Click" Text="שלח הודעה" runat="server" CssClass="btn btn-success round btn-min-width mr-1 mb-1 waves-effect waves-light" />

                                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                        </div>
                                    </div>
                                </div>


                                <asp:Repeater ID="RptShowNewMessage" runat="server">
                                    <ItemTemplate>
                                        <div id="headingCollapse2" class="card-header p-0" runat="server">
                                            <a data-toggle="collapse" href="#link<%#Eval("IDmessage")%>" aria-expanded="false" aria-controls="link<%#Eval("IDmessage")%>" class="email-app-sender media border-0 collapsed">
                                                <div class="media-left pl-1">
                                                    <span class="avatar avatar-md">
                                                        <img class="media-object rounded-circle" src="../pics/<%#Eval("Picsender")%>"></span>
                                                </div>

                                                <div class="media-body w-100">
                                                    <h4 class="list-group-item-heading"><%#Eval("NaneSender")%></h4>
                                                    <h5>ל:<%#Eval("NameGetNotice")%></h5>
                                                    <p class="list-group-item-text">
                                                        <span><%#Eval("DateSendMessage")%></span>
                                                        <span class="float-right">
                                                            <i class="la la-reply mr-1"></i>
                                                            <i class="la la-arrow-right mr-1"></i>
                                                            <i class="la la-ellipsis-v"></i>

                                                        </span>
                                                    </p>
                                                </div>
                                            </a>



                                        </div>
                                        <div id="link<%#Eval("IDmessage")%>" role="tabpanel" aria-labelledby="headingCollapse2" class="card-collapse collapse" aria-expanded="false" style="">
                                            <div class="card-content">
                                                <div class="email-app-text card-body">
                                                    <div class="email-app-message">

                                                        <h2 class="text-center"><%#Eval("SubjectMessage")%></h2>
                                                        <p>
                                                            <%#Eval("ContentMessage")%>
                                                            <br />
                                                            <a href='../Files/<%#Eval("NameFile")%>'><%#Eval("NameFile")%></a>
                                                        </p>

                                                        <asp:LinkButton ID="Mark" OnClick="Mark_Click" CommandArgument='<%#Eval("IDmessage")%>' runat="server" CssClass="btn success m-md-0 p-md-0" CausesValidation="false"><i class="la la-eye"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="ToTheTrash" OnClick="ToTheTrash_Click" CommandArgument='<%#Eval("IDmessage")%>' runat="server" CssClass="btn danger m-md-0 p-md-0" CausesValidation="false"><i class="la la-trash-o"></i></asp:LinkButton>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>


                                <asp:Repeater ID="RptMessageReaded" runat="server">
                                    <ItemTemplate>
                                        <div id="collapse3" class="card-header p-0" runat="server">
                                            <a data-toggle="collapse" href="#link<%#Eval("IDmessage")%>" aria-expanded="false" aria-controls="link<%#Eval("IDmessage")%>" class="email-app-sender media border-0 collapsed">
                                                <div class="media-left pl-1">
                                                    <span class="avatar avatar-md">
                                                        <img class="media-object rounded-circle" src="../pics/<%#Eval("Picsender")%>"></span>
                                                </div>

                                                <div class="media-body w-100">
                                                    <h4 class="list-group-item-heading"><%#Eval("NaneSender")%></h4>
                                                    <h5>ל:<%#Eval("NameGetNotice")%></h5>
                                                    <p class="list-group-item-text">
                                                        <span><%#Eval("DateSendMessage")%></span>
                                                        <span class="float-right">
                                                            <i class="la la-reply mr-1"></i>
                                                            <i class="la la-arrow-right mr-1"></i>
                                                            <i class="la la-ellipsis-v"></i>

                                                        </span>
                                                    </p>
                                                </div>
                                            </a>



                                        </div>
                                        <div id="link<%#Eval("IDmessage")%>" role="tabpanel" aria-labelledby="collapse3" class="card-collapse collapse" aria-expanded="false" style="">
                                            <div class="card-content">
                                                <div class="email-app-text card-body">
                                                    <div class="email-app-message">

                                                        <h2 class="text-center"><%#Eval("SubjectMessage")%></h2>
                                                        <p>
                                                            <%#Eval("ContentMessage")%>
                                                            <br />
                                                            <a href='../Files/<%#Eval("NameFile")%>'><%#Eval("NameFile")%></a>
                                                        </p>
                                                        <asp:LinkButton ID="ToTheTrash1" OnClick="ToTheTrash1_Click" CommandArgument='<%#Eval("IDmessage")%>' runat="server" CssClass="btn danger m-md-0 p-md-0" CausesValidation="false"><i class="la la-trash-o"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>


                                <asp:Repeater ID="RptItems" runat="server">
                                    <ItemTemplate>
                                        <div id="collapse3" class="card-header p-0" runat="server">
                                            <a data-toggle="collapse" href="#link<%#Eval("IDmessage")%>" aria-expanded="false" aria-controls="link<%#Eval("IDmessage")%>" class="email-app-sender media border-0 collapsed">
                                                <div class="media-left pl-1">
                                                    <span class="avatar avatar-md">
                                                        <img class="media-object rounded-circle" src="../pics/<%#Eval("Picsender")%>"></span>
                                                </div>

                                                <div class="media-body w-100">
                                                    <h4 class="list-group-item-heading"><%#Eval("NaneSender")%></h4>
                                                    <h5>ל:<%#Eval("NameGetNotice")%></h5>
                                                    <p class="list-group-item-text">
                                                        <span><%#Eval("DateSendMessage")%></span>
                                                        <span class="float-right">
                                                            <i class="la la-reply mr-1"></i>
                                                            <i class="la la-arrow-right mr-1"></i>
                                                            <i class="la la-ellipsis-v"></i>

                                                        </span>
                                                    </p>
                                                </div>
                                            </a>



                                        </div>
                                        <div id="link<%#Eval("IDmessage")%>" role="tabpanel" aria-labelledby="collapse3" class="card-collapse collapse" aria-expanded="false" style="">
                                            <div class="card-content">
                                                <div class="email-app-text card-body">
                                                    <div class="email-app-message">

                                                        <h2 class="text-center"><%#Eval("SubjectMessage")%></h2>
                                                        <p>
                                                            <%#Eval("ContentMessage")%>
                                                            <br />
                                                            <a href='../Files/<%#Eval("NameFile")%>'><%#Eval("NameFile")%></a>
                                                        </p>
                                                        <asp:LinkButton ID="ToTheTrash2" OnClick="ToTheTrash2_Click" CommandArgument='<%#Eval("IDmessage")%>' runat="server" CssClass="btn danger m-md-0 p-md-0" CausesValidation="false"><i class="la la-trash-o"></i></asp:LinkButton>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <%--פריטים שקיבלתי והעברתי לסל המחזור--%>
                                <asp:Repeater ID="RptTrash" runat="server">
                                    <ItemTemplate>
                                        <div id="collapse3" class="card-header p-0" runat="server">
                                            <a data-toggle="collapse" href="#link<%#Eval("IDmessage")%>" aria-expanded="false" aria-controls="link<%#Eval("IDmessage")%>" class="email-app-sender media border-0 collapsed">
                                                <div class="media-left pl-1">
                                                    <span class="avatar avatar-md">
                                                        <img class="media-object rounded-circle" src="../pics/<%#Eval("Picsender")%>"></span>
                                                </div>

                                                <div class="media-body w-100">
                                                    <h4 class="list-group-item-heading"><%#Eval("NaneSender")%></h4>
                                                    <h5>ל:<%#Eval("NameGetNotice")%></h5>
                                                    <p class="list-group-item-text">
                                                        <span><%#Eval("DateSendMessage")%></span>
                                                        <span class="float-right">
                                                            <i class="la la-reply mr-1"></i>
                                                            <i class="la la-arrow-right mr-1"></i>
                                                            <i class="la la-ellipsis-v"></i>

                                                        </span>
                                                    </p>
                                                </div>
                                            </a>



                                        </div>
                                        <div id="link<%#Eval("IDmessage")%>" role="tabpanel" aria-labelledby="collapse3" class="card-collapse collapse" aria-expanded="false" style="">
                                            <div class="card-content">
                                                <div class="email-app-text card-body">
                                                    <div class="email-app-message">

                                                        <h2 class="text-center"><%#Eval("SubjectMessage")%></h2>
                                                        <p>
                                                            <%#Eval("ContentMessage")%>
                                                            <br />
                                                            <a href='../Files/<%#Eval("NameFile")%>'><%#Eval("NameFile")%></a>
                                                        </p>
                                                        <%--<asp:LinkButton ID="ToTheTrashForever1" OnClick="ToTheTrashForever1_Click" CommandArgument='<%#Eval("IDmessage")%>' runat="server" CssClass="btn danger m-md-0 p-md-0" CausesValidation="false"><i class="fas fa-trash-restore-alt fa-2x"></i></i></asp:LinkButton>--%>
                                                        <button type="button" onclick="TrashForever1(<%#Eval("IDmessage")%>)" class="btn danger m-md-0 p-md-0" id="cancel-button">
                                                            <i class="fas fa-trash-restore-alt fa-2x"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>


                                <h3 class="list-group-item-heading" id="TitleTag1" runat="server"></h3>
                                <%--פריטים ששלחתי וזרקתי לסל המחזור--%>
                                <asp:Repeater ID="RptTrash1" runat="server">
                                    <ItemTemplate>
                                        <div id="collapse4" class="card-header p-0" runat="server">
                                            <a data-toggle="collapse" href="#link<%#Eval("IDmessage")%>" aria-expanded="false" aria-controls="link<%#Eval("IDmessage")%>" class="email-app-sender media border-0 collapsed">
                                                <div class="media-left pl-1">
                                                    <span class="avatar avatar-md">
                                                        <img class="media-object rounded-circle" src="../pics/<%#Eval("Picsender")%>"></span>
                                                </div>

                                                <div class="media-body w-100">
                                                    <h4 class="list-group-item-heading"><%#Eval("NaneSender")%></h4>
                                                    <h5>ל:<%#Eval("NameGetNotice")%></h5>
                                                    <p class="list-group-item-text">
                                                        <span><%#Eval("DateSendMessage")%></span>
                                                        <span class="float-right">
                                                            <i class="la la-reply mr-1"></i>
                                                            <i class="la la-arrow-right mr-1"></i>
                                                            <i class="la la-ellipsis-v"></i>

                                                        </span>
                                                    </p>
                                                </div>
                                            </a>



                                        </div>
                                        <div id="link<%#Eval("IDmessage")%>" role="tabpanel" aria-labelledby="collapse4" class="card-collapse collapse" aria-expanded="false" style="">
                                            <div class="card-content">
                                                <div class="email-app-text card-body">
                                                    <div class="email-app-message">

                                                        <h2 class="text-center"><%#Eval("SubjectMessage")%></h2>
                                                        <p>
                                                            <%#Eval("ContentMessage")%>
                                                            <br />
                                                            <a href='../Files/<%#Eval("NameFile")%>'><%#Eval("NameFile")%></a>
                                                        </p>
                                                        <button type="button" onclick="TrashForever1(<%#Eval("IDmessage")%>)" class="btn danger m-md-0 p-md-0" id="cancel-button">
                                                            <i class="fas fa-trash-restore-alt fa-2x"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>


                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">
    <asp:Literal ID="Swal" runat="server" />
    <script src="/app-assets/js/scripts/pages/app-email.js"></script>
    <script src="js/MyJavaScript.js"></script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>
<%-- <div id="collapse2" >
                                <div class="card-content">
                                    <div class="email-app-text card-body">
                                        <div class="email-app-message">
                                            <p>Hi John,</p>
                                            <p>Thanks for your feedback ! Here's a new layout for a new Modern Admin theme.</p>
                                            <p>
                                                We will start the new application development soon once this will be completed, I will provide you more
                                                    details after this Saturday. Hope that will be fine for you.
                                            </p>
                                            <p>Hope your like it, or feel free to comment, feedback or rebound !</p>
                                            <p>Cheers~</p>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
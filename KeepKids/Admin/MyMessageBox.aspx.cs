﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using KeepKids.DAL;

namespace KeepKids.Admin
{
    public partial class MyMessageBox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ChackSource();
            }
        }


        protected void ChackSource()
        {
            Swal.Visible = false;
            HideIDkids.Visible = false;
            HideIDteamMember.Visible = false;
            TitleTag1.Visible = false;
            HideMyAdmins.Visible = false;
            HideKidsClass.Visible = false;
            HideNameKidsMANAGER1.Visible = false;
            HideNameClass.Visible = false;
            HideNameKidsMANAGER.Visible = false;
            HideKinderGarden.Visible = false;
            HideManagerTeam.Visible = false;
            HideKinderGarden1.Visible = false;
            HideNameKids.Visible = false;
            conten.Visible = false;
            RptItems.Visible = false;
            RptMessageReaded.Visible = false;
            collapse1.Visible = false;
            RptShowNewMessage.Visible = false;
            RptTrash.Visible = false;
            RptTrash1.Visible = false;
            collapse1.Visible = false; collapse2.Visible = false; collapse3.Visible = false;

            if (Session["ParentsName"] != null)
            {
                collapse2.Visible = false;
                collapse1.Visible = false;
                Parents Tmp = (Parents)Session["ParentsName"];
                TheSendParents.Text = "השולח:" + " " + Tmp.UserNameParents;
            }
            if (Session["ManagerName"] != null)
            {
                collapse1.Visible = false;
                collapse3.Visible = false;
                Manager Tmp = (Manager)Session["ManagerName"];
                TheSendManager.Text = "השולח:" + " " + Tmp.UserNameTeamMember;
            }
            if (Session["AdminsName"] != null)
            {
                collapse2.Visible = false;
                collapse3.Visible = false;
                Admins Tmp = (Admins)Session["AdminsName"];
                TheSend.Text = "השולח:" + " " + Tmp.UserNameAdmin;
            }

        }

        protected void SendMessage_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            TitleTag1.Visible = false;
            conten.Visible = true;
            TitleTag.InnerHtml = "<h3 class='list-group-item-heading text-center'>שלח הודעה חדשה</h3>";
            RptItems.Visible = false;
            RptMessageReaded.Visible = false;
            RptShowNewMessage.Visible = false;
            RptTrash.Visible = false;
            RptTrash1.Visible = false;
            if (Session["ParentsName"] != null)
            {
                collapse3.Visible = true;
            }
            if (Session["ManagerName"] != null)
            {
                collapse2.Visible = true;
            }
            if (Session["AdminsName"] != null)
            {
                collapse1.Visible = true;
            }
        }

        protected void NewMessage_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            TitleTag1.Visible = false;
            conten.Visible = true;
            TitleTag.InnerHtml = "<h3 class='list-group-item-heading text-center'>הודעות חדשות שהתקבלו </h3>";
            RptItems.Visible = false;
            RptMessageReaded.Visible = false;
            collapse1.Visible = false;
            collapse2.Visible = false;
            collapse3.Visible = false;
            RptTrash.Visible = false;
            RptTrash1.Visible = false;
            RptShowNewMessage.Visible = true;
            if (Session["ParentsName"] != null)
            {
                Parents Tmp = (Parents)Session["ParentsName"];
                RptShowNewMessage.DataSource = Parents.ShowMeMYNewMessage(Tmp.UserNameParents, 1);
                RptShowNewMessage.DataBind();

            }
            if (Session["ManagerName"] != null)
            {
                Manager Tmp = (Manager)Session["ManagerName"];
                RptShowNewMessage.DataSource = Manager.ShowMeMYNewMessage(Tmp.IDteamMember, 1);
                RptShowNewMessage.DataBind();
            }
            if (Session["AdminsName"] != null)
            {

                Admins Tmp = (Admins)Session["AdminsName"];
                RptShowNewMessage.DataSource = Admins.ShowMeMYNewMessage(Tmp.IDadmin, 1);
                RptShowNewMessage.DataBind();

            }

        }
        protected void MessageReaded_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            TitleTag1.Visible = false;
            conten.Visible = true;
            TitleTag.InnerHtml = "<h3 class='list-group-item-heading text-center'>הודעות שנקראו</h3>";
            RptItems.Visible = false;
            collapse1.Visible = false;
            collapse2.Visible = false;
            collapse3.Visible = false;
            RptShowNewMessage.Visible = false;
            RptTrash.Visible = false;
            RptTrash1.Visible = false;
            if (Session["ParentsName"] != null)
            {
                RptMessageReaded.Visible = true;
                Parents Tmp = (Parents)Session["ParentsName"];
                RptMessageReaded.DataSource = Parents.MessageReaded(Tmp.UserNameParents, 2);
                RptMessageReaded.DataBind();
            }
            if (Session["ManagerName"] != null)
            {
                RptMessageReaded.Visible = true;
                Manager Tmp = (Manager)Session["ManagerName"];
                RptMessageReaded.DataSource = Admins.MessageReaded(Tmp.IDteamMember, 2);
                RptMessageReaded.DataBind();
            }
            if (Session["AdminsName"] != null)
            {
                RptMessageReaded.Visible = true;
                Admins Tmp = (Admins)Session["AdminsName"];
                RptMessageReaded.DataSource = Admins.MessageReaded(Tmp.IDadmin, 2);
                RptMessageReaded.DataBind();

            }
        }

        protected void Items_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            TitleTag1.Visible = false;
            conten.Visible = true;
            TitleTag.InnerHtml = "<h3 class='list-group-item-heading text-center'>הודעות שנשלחו</h3>";
            collapse1.Visible = false;
            collapse2.Visible = false;
            collapse3.Visible = false;
            RptMessageReaded.Visible = false;
            RptShowNewMessage.Visible = false;
            RptTrash.Visible = false;
            RptTrash1.Visible = false;
            if (Session["ParentsName"] != null)
            {
                RptItems.Visible = true;
                Parents Tmp = (Parents)Session["ParentsName"];
                RptItems.DataSource = Parents.MyItems(Tmp.UserNameParents);
                RptItems.DataBind();
            }
            if (Session["ManagerName"] != null)
            {
                RptItems.Visible = true;
                Manager Tmp = (Manager)Session["ManagerName"];
                RptItems.DataSource = Manager.MyItems(Tmp.IDteamMember);
                RptItems.DataBind();
            }
            if (Session["AdminsName"] != null)
            {
                RptItems.Visible = true;
                Admins Tmp = (Admins)Session["AdminsName"];
                RptItems.DataSource = Admins.MyItems(Tmp.IDadmin);
                RptItems.DataBind();

            }

        }

        protected void Trash_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            conten.Visible = true;
            if (RptTrash.Items.Count == 0)
            {
                TitleTag.InnerHtml = "<h3 class='list-group-item-heading text-center'>אשפח</h3>";
            }
            else
            {
                TitleTag.InnerHtml = "<h3 class='list-group-item-heading text-center'>ההודעות שקיבלתי וזרקתי לאשפה</h3>";
            }
            collapse1.Visible = false;
            collapse2.Visible = false;
            collapse3.Visible = false;
            RptMessageReaded.Visible = false;
            RptItems.Visible = false;
            RptShowNewMessage.Visible = false;
            TitleTag1.Visible = true;
            if (Session["ParentsName"] != null)
            {
                RptTrash.Visible = true;
                RptTrash1.Visible = true;
                Parents Tmp = (Parents)Session["ParentsName"];
                RptTrash.DataSource = Parents.ShowMeMYTrashMessage(Tmp.UserNameParents, 3);
                RptTrash.DataBind();
                RptTrash1.DataSource = Parents.ShowMeMYTrashMessage1(Tmp.UserNameParents, 3);
                RptTrash1.DataBind();
                if (RptTrash1.Items.Count != 0)
                {
                    TitleTag1.InnerHtml = "<h3 class='list-group-item-heading text-center'>ההודעות ששלחתי וזרקתי לאשפה</h3>";
                }
            }
            if (Session["ManagerName"] != null)
            {
                RptTrash.Visible = true;
                RptTrash1.Visible = true;
                Manager Tmp = (Manager)Session["ManagerName"];
                RptTrash.DataSource = Manager.ShowMeMYTrashMessage(Tmp.IDteamMember, 3);
                RptTrash.DataBind();
                RptTrash1.DataSource = Manager.ShowMeMYTrashMessage1(Tmp.IDteamMember, 3);
                RptTrash1.DataBind();
                if (RptTrash1.Items.Count != 0)
                {
                    TitleTag1.InnerHtml = "<h3 class='list-group-item-heading text-center'>ההודעות ששלחתי וזרקתי לאשפה</h3>";
                }
            }
            if (Session["AdminsName"] != null)
            {
                RptTrash.Visible = true;
                RptTrash1.Visible = true;
                Admins Tmp = (Admins)Session["AdminsName"];
                RptTrash.DataSource = Admins.ShowMeMYTrashMessage(Tmp.IDadmin, 3);
                RptTrash.DataBind();
                RptTrash1.DataSource = Admins.ShowMeMYTrashMessage1(Tmp.IDadmin, 3);
                RptTrash1.DataBind();
                if (RptTrash1.Items.Count != 0)
                {
                    TitleTag1.InnerHtml = "<h3 class='list-group-item-heading text-center'>ההודעות ששלחתי וזרקתי לאשפה</h3>";
                }


            }
        }
        //מנהל השולח
        protected void SendToWho_SelectedIndexChanged(object sender, EventArgs e)
        {
            Swal.Visible = false;
            TitleTag1.Visible = false;
            Send.Text = "שלח הודעה";
            HideSendToWho.Visible = true;
            HideKinderGarden.Visible = false;
            HideManagerTeam.Visible = false;
            HideKinderGarden1.Visible = false;
            HideNameKids.Visible = false;
            Admins Tmp = (Admins)Session["AdminsName"];
            if (Session["AdminsName"] != null)
            {
                if (SendToWho.SelectedValue == "1")
                {
                    HideKinderGarden.Visible = true;
                    KinderGarden.DataSource = Admins.viewByAdmin(Tmp.IDcompany);
                    KinderGarden.DataBind();
                    KinderGarden.DataTextField = "FullName";
                    KinderGarden.DataValueField = "IDteamMember";
                    KinderGarden.DataBind();
                    KinderGarden.Items.Insert(0, new ListItem(" כל הגננות", "0"));
                    KinderGarden.Enabled = false;

                }
                if (SendToWho.SelectedValue == "2")
                {
                    HideManagerTeam.Visible = true;
                    ManagerTeam.DataSource = Admins.viewByAdmin(Tmp.IDcompany);
                    ManagerTeam.DataBind();
                    ManagerTeam.DataTextField = "FullName";
                    ManagerTeam.DataValueField = "IDteamMember";
                    ManagerTeam.DataBind();
                    ManagerTeam.Items.Insert(0, new ListItem("שם מנהל הצוות", "0"));
                    ManagerTeam.SelectedIndex = 0;
                }
                if (SendToWho.SelectedValue == "3")
                {
                    HideKinderGarden1.Visible = true;
                    KinderGarden1.DataSource = ToReport.NameKinderGardenMethod(Tmp.IDcompany, Tmp.IDadmin);
                    KinderGarden1.DataBind();
                    KinderGarden1.DataTextField = "NameKindergarten";
                    KinderGarden1.DataValueField = "IDkindergarten";
                    KinderGarden1.DataBind();
                    KinderGarden1.Items.Insert(0, new ListItem("שם הגן", "0"));
                }
                if (SendToWho.SelectedValue == "4")
                {
                    HideNameKids.Visible = true;
                    NameKids.DataSource = Admins.viewNameKids(Tmp.IDcompany);
                    NameKids.DataBind();
                    NameKids.DataTextField = "NameKids";
                    NameKids.DataValueField = "IDkids";
                    NameKids.DataBind();
                    NameKids.Items.Insert(0, new ListItem("שם בן ההורה", "0"));
                }

            }
        }
        //חבר צוות השולח
        protected void SendToWho1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Swal.Visible = false;
            TitleTag1.Visible = false;
            Send1.Text = "שלח הודעה";
            HideSendToWho1.Visible = true;
            HideMyAdmins.Visible = false;
            HideNameKidsMANAGER.Visible = false;
            HideNameClass.Visible = false;
            HideNameKidsMANAGER1.Visible = false;
            if (Session["ManagerName"] != null)
            {
                Manager Tmp = (Manager)Session["ManagerName"];
                if (SendToWho1.SelectedValue == "9")
                {
                    HideMyAdmins.Visible = true;
                    MyAdmins.DataSource = Manager.MyAdmins(Tmp.IDteamMember);
                    MyAdmins.DataBind();
                    MyAdmins.DataTextField = "NameAdmin";
                    MyAdmins.DataValueField = "IDadmin";
                    MyAdmins.DataBind();
                    MyAdmins.SelectedIndex = 0;
                }
                if (SendToWho1.SelectedValue == "10")
                {
                    HideNameKidsMANAGER.Visible = true;
                    NameKidsMANAGER.DataSource = Manager.viewNameKids(Tmp.IDteamMember);
                    NameKidsMANAGER.DataBind();
                    NameKidsMANAGER.DataTextField = "NameKids";
                    NameKidsMANAGER.DataValueField = "IDkids";
                    NameKidsMANAGER.DataBind();
                    NameKidsMANAGER.Items.Insert(0, new ListItem("כל ההורים", "0"));
                    NameKidsMANAGER.SelectedIndex = 0;
                    NameKidsMANAGER.Enabled = false;
                }
                if (SendToWho1.SelectedValue == "11")
                {
                    HideNameClass.Visible = true;
                    NameClass.DataSource = ToReport.ShowClass(Tmp.IDteamMember);
                    NameClass.DataBind();
                    NameClass.DataTextField = "NameClass";
                    NameClass.DataValueField = "IDclass";
                    NameClass.DataBind();
                    NameClass.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));

                }
                if (SendToWho1.SelectedValue == "12")
                {
                    HideNameKidsMANAGER1.Visible = true;
                    NameKidsMANAGER1.DataSource = Manager.viewNameKids(Tmp.IDteamMember);
                    NameKidsMANAGER1.DataBind();
                    NameKidsMANAGER1.DataTextField = "NameKids";
                    NameKidsMANAGER1.DataValueField = "IDkids";
                    NameKidsMANAGER1.DataBind();
                    NameKidsMANAGER1.Items.Insert(0, new ListItem("שם בן ההורה", "0"));
                    NameKidsMANAGER1.SelectedIndex = 0;

                }
            }

        }
        //הורה השולח
        protected void SendToWho2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Swal.Visible = false;
            TitleTag1.Visible = false;
            Send2.Text = "שלח הודעה";
            HideSendToWho2.Visible = true;
            HideIDteamMember.Visible = false;
            HideIDkids.Visible = false;
            if (Session["ParentsName"] != null)
            {
                Parents Tmp = (Parents)Session["ParentsName"];
                if (SendToWho2.SelectedValue == "20")
                {
                    HideIDteamMember.Visible = true;
                    IDteamMember.DataSource = Parents.KindergartenOfTeacher(Tmp.UserNameParents);
                    IDteamMember.DataBind();
                    IDteamMember.DataTextField = "FullName";
                    IDteamMember.DataValueField = "IDteamMember";
                    IDteamMember.DataBind();
                    IDteamMember.Items.Insert(0, new ListItem("בחר גננת", "0"));
                    IDteamMember.SelectedIndex = 0;

                }
                else if (SendToWho2.SelectedValue == "21")
                {
                    HideIDkids.Visible = true;
                    IDkids.DataSource = Parents.ListKidsToMessage(Tmp.UserNameParents);
                    IDkids.DataBind();
                    IDkids.DataTextField = "NameKids";
                    IDkids.DataValueField = "IDkids";
                    IDkids.DataBind();
                    IDkids.Items.Insert(0, new ListItem("בחר בן הורה", "0"));
                    IDkids.SelectedIndex = 0;
                }


            }
        }

        protected void NameClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            Manager Tmp = (Manager)Session["ManagerName"];
            HideNameClass.Visible = false;
            HideKidsClass.Visible = true;
            KidsClass.DataSource = Manager.ListKids(Tmp.IDteamMember, int.Parse(NameClass.SelectedValue));
            KidsClass.DataBind();
            KidsClass.DataTextField = "NameKids";
            KidsClass.DataValueField = "IDkids";
            KidsClass.DataBind();
            KidsClass.Items.Insert(0, new ListItem("כל הכיתה", "0"));
            KidsClass.SelectedIndex = 0;
            KidsClass.Enabled = false;
        }

        protected void Send_Click(object sender, EventArgs e)
        {
            Swal.Visible = true;
            //var dateTimeNow = DateTime.Now; 
            //var dateOnlyString = dateTimeNow.ToShortDateString();
            if (Session["ParentsName"] != null)
            {
                Parents Tmp = (Parents)Session["ParentsName"];
                if (HideIDteamMember.Visible == true)
                {
                    string picManager = Parents.OnlyPicManager(int.Parse(IDteamMember.SelectedValue));
                    string nameManager = Parents.OnlyNameManager(int.Parse(IDteamMember.SelectedValue));
                    if (PicUpload2.FileName == "")
                    {
                        ParentsDAL.InsertNewMessage(Tmp.IDkids, Tmp.PicKids, Tmp.NameFather + " ו" + Tmp.NameMother, 1, int.Parse(IDteamMember.SelectedValue), picManager, nameManager, 1, TitleMessage2.Text, ContentMessage2.Text, "", DateTime.Now.ToString());

                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload2.FileName);
                        PicUpload2.SaveAs(fld);
                        ParentsDAL.InsertNewMessage(Tmp.IDkids, Tmp.PicKids, Tmp.NameFather + " ו" + Tmp.NameMother, 1, int.Parse(IDteamMember.SelectedValue), picManager, nameManager, 1, TitleMessage2.Text, ContentMessage2.Text, PicUpload2.FileName, DateTime.Now.ToString());

                    }

                }
                if (HideIDkids.Visible == true)
                {
                    string picKids = Manager.OnlyPicKids(int.Parse(IDkids.SelectedValue));
                    string nameKids = Manager.OnlyNameKids(int.Parse(IDkids.SelectedValue));
                    if (PicUpload2.FileName == "")
                    {
                        ParentsDAL.InsertNewMessage(Tmp.IDkids, Tmp.PicKids, Tmp.NameFather + " ו" + Tmp.NameMother, 1, int.Parse(IDkids.SelectedValue), picKids, nameKids, 1, TitleMessage2.Text, ContentMessage2.Text, "", DateTime.Now.ToString());

                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload2.FileName);
                        PicUpload2.SaveAs(fld);
                        ParentsDAL.InsertNewMessage(Tmp.IDkids, Tmp.PicKids, Tmp.NameFather + " ו" + Tmp.NameMother, 1, int.Parse(IDkids.SelectedValue), picKids, nameKids, 1, TitleMessage2.Text, ContentMessage2.Text, PicUpload2.FileName, DateTime.Now.ToString());

                    }

                }
                Swal.Text = "<script>swal({ title: 'נשלח', text: '" + DateTime.Now.ToString() + "', icon: '../pics/message.png' });</script>";
                HideIDteamMember.Visible = false;
                HideIDkids.Visible = false;
                HideSendToWho2.Visible = true;
                SendToWho2.SelectedIndex = 0;
                TitleMessage2.Text = ""; ContentMessage2.Text = "";
            }
            if (Session["AdminsName"] != null)
            {
                Admins Tmp = (Admins)Session["AdminsName"];
                if (HideKinderGarden.Visible == true)
                {
                    if (PicUpload.FileName == "")
                    {
                        for (int i = 1; i < KinderGarden.Items.Count; i++)
                        {
                            string picManager = Admins.OnlyPicManager(int.Parse(KinderGarden.Items[i].Value));
                            string nameManager = Admins.OnlyNameManager(int.Parse(KinderGarden.Items[i].Value));
                            AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, int.Parse(KinderGarden.Items[i].Value), picManager, nameManager, 1, TitleMessage.Text, ContentMessage.Text, "", DateTime.Now.ToString());
                        }
                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload.FileName);
                        PicUpload.SaveAs(fld);
                        for (int i = 1; i < KinderGarden.Items.Count; i++)
                        {
                            string picManager = Admins.OnlyPicManager(int.Parse(KinderGarden.Items[i].Value));
                            string nameManager = Admins.OnlyNameManager(int.Parse(KinderGarden.Items[i].Value));
                            AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, int.Parse(KinderGarden.Items[i].Value), picManager, nameManager, 1, TitleMessage.Text, ContentMessage.Text, PicUpload.FileName, DateTime.Now.ToString());
                        }
                    }

                }
                if (HideManagerTeam.Visible == true)
                {
                    string picManager = Admins.OnlyPicManager(int.Parse(ManagerTeam.SelectedValue));
                    string nameManager = Admins.OnlyNameManager(int.Parse(ManagerTeam.SelectedValue));
                    if (PicUpload.FileName == "")
                    {
                        AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, int.Parse(ManagerTeam.SelectedValue), picManager, nameManager, 1, TitleMessage.Text, ContentMessage.Text, "", DateTime.Now.ToString());
                    }

                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload.FileName);
                        PicUpload.SaveAs(fld);
                        AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, int.Parse(ManagerTeam.SelectedValue), picManager, nameManager, 1, TitleMessage.Text, ContentMessage.Text, PicUpload.FileName, DateTime.Now.ToString());
                    }
                }
                if (HideKinderGarden1.Visible == true)
                {
                    List<Admins> IDKIDS = Admins.ParentsByKinderGarden(int.Parse(KinderGarden1.SelectedValue));
                    if (PicUpload.FileName == "")
                    {
                        for (int i = 0; i < IDKIDS.Count; i++)
                        {
                            string picKids = Admins.OnlyPicKids(IDKIDS[i].IDkids);
                            string nameKids = Admins.OnlyNameKids(IDKIDS[i].IDkids);
                            AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, IDKIDS[i].IDkids, picKids, nameKids, 1, TitleMessage.Text, ContentMessage.Text, "", DateTime.Now.ToString());
                        }
                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload.FileName);
                        PicUpload.SaveAs(fld);
                        for (int i = 0; i < IDKIDS.Count; i++)
                        {
                            string picKids = Admins.OnlyPicKids(IDKIDS[i].IDkids);
                            string nameKids = Admins.OnlyNameKids(IDKIDS[i].IDkids);
                            AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, IDKIDS[i].IDkids, picKids, nameKids, 1, TitleMessage.Text, ContentMessage.Text, PicUpload.FileName, DateTime.Now.ToString());
                        }
                    }

                }
                if (HideNameKids.Visible == true)
                {
                    string picKids = Admins.OnlyPicKids(int.Parse(NameKids.SelectedValue));
                    string nameKids = Admins.OnlyNameKids(int.Parse(NameKids.SelectedValue));
                    if (PicUpload.FileName == "")
                    {
                        AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, int.Parse(NameKids.SelectedValue), picKids, nameKids, 1, TitleMessage.Text, ContentMessage.Text, "", DateTime.Now.ToString());
                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload.FileName);
                        PicUpload.SaveAs(fld);
                        AdminsDAL.InsertNewMessage(Tmp.IDadmin, Tmp.PicAdmin, Tmp.NameAdmin + " " + Tmp.LastNameAdmin, 1, int.Parse(NameKids.SelectedValue), picKids, nameKids, 1, TitleMessage.Text, ContentMessage.Text, PicUpload.FileName, DateTime.Now.ToString());
                    }


                }
                Swal.Text = "<script>swal({ title: 'נשלח', text: '" + DateTime.Now.ToString() + "', icon: '../pics/message.png' });</script>";
                HideKinderGarden.Visible = false;
                HideManagerTeam.Visible = false;
                HideKinderGarden1.Visible = false;
                HideNameKids.Visible = false;
                HideSendToWho.Visible = true;
                SendToWho.SelectedIndex = 0;
                TitleMessage.Text = ""; ContentMessage.Text = "";
            }
            if (Session["ManagerName"] != null)
            {
                Manager Tmp = (Manager)Session["ManagerName"];
                if (HideNameKidsMANAGER.Visible == true)
                {
                    if (PicUpload1.FileName == "")
                    {
                        for (int i = 1; i < NameKidsMANAGER.Items.Count; i++)
                        {
                            string picKids = Manager.OnlyPicKids(int.Parse(NameKidsMANAGER.Items[i].Value));
                            string nameKids = Manager.OnlyNameKids(int.Parse(NameKidsMANAGER.Items[i].Value));
                            ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(NameKidsMANAGER.Items[i].Value), picKids, nameKids, 1, TitleMessage1.Text, ContentMessage1.Text, "", DateTime.Now.ToString());

                        }
                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload1.FileName);
                        PicUpload1.SaveAs(fld);
                        for (int i = 1; i < NameKidsMANAGER.Items.Count; i++)
                        {
                            string picKids = Manager.OnlyPicKids(int.Parse(NameKidsMANAGER.Items[i].Value));
                            string nameKids = Manager.OnlyNameKids(int.Parse(NameKidsMANAGER.Items[i].Value));
                            ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(NameKidsMANAGER.Items[i].Value), picKids, nameKids, 1, TitleMessage1.Text, ContentMessage1.Text, PicUpload1.FileName, DateTime.Now.ToString());

                        }
                    }

                }
                if (HideKidsClass.Visible == true)
                {
                    if (PicUpload1.FileName == "")
                    {
                        for (int i = 1; i < KidsClass.Items.Count; i++)
                        {
                            string picKids = Manager.OnlyPicKids(int.Parse(KidsClass.Items[i].Value));
                            string nameKids = Manager.OnlyNameKids(int.Parse(KidsClass.Items[i].Value));
                            ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(KidsClass.Items[i].Value), picKids, nameKids, 1, TitleMessage1.Text, ContentMessage1.Text, "", DateTime.Now.ToString());
                        }
                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload1.FileName);
                        PicUpload1.SaveAs(fld);
                        for (int i = 1; i < KidsClass.Items.Count; i++)
                        {
                            string picKids = Manager.OnlyPicKids(int.Parse(KidsClass.Items[i].Value));
                            string nameKids = Manager.OnlyNameKids(int.Parse(KidsClass.Items[i].Value));
                            ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(KidsClass.Items[i].Value), picKids, nameKids, 1, TitleMessage1.Text, ContentMessage1.Text, PicUpload1.FileName, DateTime.Now.ToString());
                        }
                    }

                }
                if (HideNameKidsMANAGER1.Visible == true)
                {
                    string picKids = Manager.OnlyPicKids(int.Parse(NameKidsMANAGER1.SelectedValue));
                    string nameKids = Manager.OnlyNameKids(int.Parse(NameKidsMANAGER1.SelectedValue));
                    if (PicUpload1.FileName == "")
                    {
                        ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(NameKidsMANAGER1.SelectedValue), picKids, nameKids, 1, TitleMessage1.Text, ContentMessage1.Text, "", DateTime.Now.ToString());

                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload1.FileName);
                        PicUpload1.SaveAs(fld);
                        ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(NameKidsMANAGER1.SelectedValue), picKids, nameKids, 1, TitleMessage1.Text, ContentMessage1.Text, PicUpload1.FileName, DateTime.Now.ToString());

                    }


                }
                if (HideMyAdmins.Visible == true)
                {
                    string picAdmins = Manager.OnlypicAdmins(Tmp.IDteamMember);
                    string nameAdmins = Manager.OnlynameAdmins(Tmp.IDteamMember);
                    if (PicUpload1.FileName == "")
                    {
                        ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(MyAdmins.SelectedValue), picAdmins, nameAdmins, 1, TitleMessage1.Text, ContentMessage1.Text, "", DateTime.Now.ToString());
                    }
                    else
                    {
                        string fld = Server.MapPath("~/Files/" + PicUpload1.FileName);
                        PicUpload1.SaveAs(fld);
                        ManagerDAL.InsertNewMessage(Tmp.IDteamMember, Tmp.PicManager, Tmp.FullName, 1, int.Parse(MyAdmins.SelectedValue), picAdmins, nameAdmins, 1, TitleMessage1.Text, ContentMessage1.Text, PicUpload1.FileName, DateTime.Now.ToString());
                    }


                }

                Swal.Text = "<script>swal({ title: 'נשלח', text: '" + DateTime.Now.ToString() + "', icon: '../pics/message.png' });</script>";
                HideMyAdmins.Visible = false;
                HideNameKidsMANAGER.Visible = false;
                HideNameClass.Visible = false;
                HideNameKidsMANAGER1.Visible = false;
                HideKidsClass.Visible = false;
                HideSendToWho1.Visible = true;
                SendToWho1.SelectedIndex = 0;
                TitleMessage1.Text = ""; ContentMessage1.Text = "";
            }


        }
        //סמן כנקרא
        protected void Mark_Click(object sender, EventArgs e)
        {

            if (Session["ParentsName"] != null)
            {

                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Parents.UpdateMarkTORead(ID);
                Parents Tmp = (Parents)Session["ParentsName"];
                Application["message"] = Parents.UnreadMessages(Tmp.UserNameParents);
                RptShowNewMessage.DataSource = Parents.ShowMeMYNewMessage(Tmp.UserNameParents, 1);
                RptShowNewMessage.DataBind();
                Swal.Visible = false;
                int co = Parents.UnreadMessages(Tmp.UserNameParents);
                if (co > 0)
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Parents.UnreadMessages(Tmp.UserNameParents) + "</span>";
                }
                else
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "";
                }
            }
            if (Session["ManagerName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Manager.UpdateMarkTORead(ID);
                Manager Tmp = (Manager)Session["ManagerName"];
                RptShowNewMessage.DataSource = Manager.ShowMeMYNewMessage(Tmp.IDteamMember, 1);
                RptShowNewMessage.DataBind();
                int co = Manager.UnreadMessages(Tmp.IDteamMember);
                if (co > 0)
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Manager.UnreadMessages(Tmp.IDteamMember) + "</span>";
                }
                else
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "";
                }
            }
            if (Session["AdminsName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Admins.UpdateMarkTORead(ID);
                Admins Tmp = (Admins)Session["AdminsName"];
                RptShowNewMessage.DataSource = Admins.ShowMeMYNewMessage(Tmp.IDadmin, 1);
                RptShowNewMessage.DataBind();
                int co = Manager.UnreadMessages(Tmp.IDadmin);
                if (co > 0)
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Manager.UnreadMessages(Tmp.IDadmin) + "</span>";
                }
                else
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "";
                }
            }

        }




        //העבר לאשפה
        protected void ToTheTrash_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            if (Session["ParentsName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Parents.UpdateToTheTrash(ID);
                Parents Tmp = (Parents)Session["ParentsName"];
                RptShowNewMessage.DataSource = Parents.ShowMeMYNewMessage(Tmp.UserNameParents, 1);
                RptShowNewMessage.DataBind();
                int co = Parents.UnreadMessages(Tmp.UserNameParents);
                if (co > 0)
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Parents.UnreadMessages(Tmp.UserNameParents) + "</span>";
                }
                else
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "";
                }

            }

            if (Session["ManagerName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Manager.UpdateToTheTrash(ID);
                Manager Tmp = (Manager)Session["ManagerName"];
                RptShowNewMessage.DataSource = Manager.ShowMeMYNewMessage(Tmp.IDteamMember, 1);
                RptShowNewMessage.DataBind();
                int co = Manager.UnreadMessages(Tmp.IDteamMember);
                if (co > 0)
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Manager.UnreadMessages(Tmp.IDteamMember) + "</span>";
                }
                else
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "";
                }
            }

            if (Session["AdminsName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Admins.UpdateToTheTrash(ID);
                Admins Tmp = (Admins)Session["AdminsName"];
                RptShowNewMessage.DataSource = Admins.ShowMeMYNewMessage(Tmp.IDadmin, 1);
                RptShowNewMessage.DataBind();
                int co = Manager.UnreadMessages(Tmp.IDteamMember);
                if (co > 0)
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "<span class='badge badge-pill badge-danger badge-glow ml-1'>" + Manager.UnreadMessages(Tmp.IDteamMember) + "</span>";
                }
                else
                {
                    ((Literal)Master.FindControl("MessageBox")).Text = "";
                }

            }
        }

        protected void ToTheTrash1_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            if (Session["ParentsName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Parents.UpdateToTheTrash1(ID);
                Parents Tmp = (Parents)Session["ParentsName"];
                RptMessageReaded.DataSource = Parents.MessageReaded(Tmp.UserNameParents, 2);
                RptMessageReaded.DataBind();
            }
            if (Session["ManagerName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Manager.UpdateToTheTrash1(ID);
                Manager Tmp = (Manager)Session["ManagerName"];
                RptMessageReaded.DataSource = Admins.MessageReaded(Tmp.IDteamMember, 2);
                RptMessageReaded.DataBind();
            }
            if (Session["AdminsName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Admins.UpdateToTheTrash1(ID);
                Admins Tmp = (Admins)Session["AdminsName"];
                RptMessageReaded.DataSource = Admins.MessageReaded(Tmp.IDadmin, 2);
                RptMessageReaded.DataBind();

            }
        }

        protected void ToTheTrash2_Click(object sender, EventArgs e)
        {
            Swal.Visible = false;
            if (Session["ParentsName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Parents.UpdateToTheTrash2(ID);
                Parents Tmp = (Parents)Session["ParentsName"];
                RptItems.DataSource = Parents.MyItems(Tmp.UserNameParents);
                RptItems.DataBind();

            }
            if (Session["ManagerName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Manager.UpdateToTheTrash2(ID);
                Manager Tmp = (Manager)Session["ManagerName"];
                RptItems.DataSource = Manager.MyItems(Tmp.IDteamMember);
                RptItems.DataBind();
            }
            if (Session["AdminsName"] != null)
            {
                int ID = int.Parse((sender as LinkButton).CommandArgument);
                Admins.UpdateToTheTrash2(ID);
                Admins Tmp = (Admins)Session["AdminsName"];
                RptItems.DataSource = Admins.MyItems(Tmp.IDadmin);
                RptItems.DataBind();

            }
        }


        //פריטים שקיבלתי ושלחתי לאשפח
        //protected void ToTheTrashForever1_Click(object sender, EventArgs e)
        //{
        //    if (Session["ParentsName"] != null)
        //    {
        //        int ID = int.Parse((sender as LinkButton).CommandArgument);
        //        Parents.TrashForever(ID);
        //        Parents Tmp = (Parents)Session["ParentsName"];
        //        RptTrash.DataSource = Parents.ShowMeMYTrashMessage(Tmp.UserNameParents, 3);
        //        RptTrash.DataBind();
        //        RptTrash1.DataSource = Parents.ShowMeMYTrashMessage1(Tmp.UserNameParents, 3);
        //        RptTrash1.DataBind();
        //    }
        //    if (Session["ManagerName"] != null) 
        //    {
        //        int ID = int.Parse((sender as LinkButton).CommandArgument);
        //        Manager.TrashForever(ID);
        //        Manager Tmp = (Manager)Session["ManagerName"];
        //        RptTrash.DataSource = Manager.ShowMeMYTrashMessage(Tmp.IDteamMember, 3);
        //        RptTrash.DataBind();
        //        RptTrash1.DataSource = Manager.ShowMeMYTrashMessage1(Tmp.IDteamMember, 3);
        //        RptTrash1.DataBind();
        //    }
        //    if (Session["AdminsName"] != null)
        //    {
        //        int ID = int.Parse((sender as LinkButton).CommandArgument);
        //        Admins.TrashForever(ID);
        //        Admins Tmp = (Admins)Session["AdminsName"];
        //        RptTrash.DataSource = Admins.ShowMeMYTrashMessage(Tmp.IDadmin, 3);
        //        RptTrash.DataBind();
        //        RptTrash1.DataSource = Admins.ShowMeMYTrashMessage1(Tmp.IDadmin, 3);
        //        RptTrash1.DataBind();

        //    }
        //}
        //פריטים ששלחתי והעברתי לאשפח
        //protected void ToTheTrashForever2_Click(object sender, EventArgs e)
        //{

        //}


    }
}
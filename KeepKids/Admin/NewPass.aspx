﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPass.aspx.cs" Inherits="KeepKids.Admin.NewPass" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>יצירת סיסמא חדשה</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard." />
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard" />
    <meta name="author" content="PIXINVENT" />
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png" />
    <link rel="shortcut icon" type="image/x-icon" href="../pics/‏‏MYKIND.png" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet" />

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors-rtl.min.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap-extended.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/colors.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/components.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/custom-rtl.css" />
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu-modern.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/login-register.css" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css" />
    <!-- END: Custom CSS-->

</head>
<body class="vertical-layout vertical-menu-modern 1-column  bg-full-screen-image blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <form id="form1" runat="server" class="form-horizontal">
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row mb-1">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                                <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                    <div class="card-header border-0 pb-0">
                                        <div class="card-title text-center">
                                            <img src="../pics/‏‏MYKIND.png" alt="branding logo" />
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <asp:Panel DefaultButton="btnNewPass" runat="server">
                                                <%--פקדי ולידציה--%>
                                                    <asp:RequiredFieldValidator ID="TxtPassword" runat="server" ErrorMessage="אנא הזן סיסמא"  Text="*" ControlToValidate="UserPassword"  Font-Bold="True" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <asp:TextBox ID="UserPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="הזן סיסמא חדשה" />
                                                    <%--//פקדי רגולר אקספריישן--%>
                                                  <asp:RegularExpressionValidator ID="REVTxtPassword" runat="server" ControlToValidate="UserPassword" ErrorMessage="לפחות 8 תווים אות אחת קטנה, אות אחת גדולה, ובאנגלית" Text="**" Font-Bold="True" ForeColor="Red" ValidationExpression="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$"></asp:RegularExpressionValidator>                                                    <div class="form-control-position">
                                                        <i class="la la-key"></i>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <asp:TextBox ID="AgainPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder=" אימות סיסמא חדשה" />
                                                    <%-- //קומפר ולידטור--%>
                                                    <asp:CompareValidator ID="COMTxtRealPassword" runat="server" ErrorMessage="הסיסמא אינה תואמת" Font-Bold="True" Text="+" ControlToCompare="AgainPassword" ControlToValidate="UserPassword" ForeColor="Red"></asp:CompareValidator>
                                                    <div class="form-control-position">
                                                        <i class="la la-key"></i>
                                                    </div>
                                                </fieldset>
                                                <asp:Literal ID="Worng" runat="server" />
                                                <div class="form-group row">
                                                    <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                                    </div>

                                                </div>
                                            </asp:Panel>

                                        </div>
                                        <div class="card-body">
                                            <asp:LinkButton ID="btnNewPass" OnClick="btnNewPass_Click" CssClass="btn btn-outline-danger btn-block " runat="server">עדכן</asp:LinkButton>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                            </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- BEGIN: Vendor JS-->
        <script src="/app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/app-assets/js/core/app-menu.js"></script>
        <script src="/app-assets/js/core/app.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/app-assets/js/scripts/forms/form-login-register.js"></script>
        <!-- END: Page JS-->

    </form>
</body>
</html>

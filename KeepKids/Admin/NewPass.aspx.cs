﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;


namespace KeepKids.Admin
{
    public partial class NewPass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["check if is null a"] is null && Session["check if is null b"] is null && Session["check if is null c"] is null)
            {
                Response.Redirect("ForgetPassword.aspx");
            }

            //חברי צוות
            Manager Tmp = new Manager();
            Tmp.PasswordTMrecovery = (string)Session["PasswordTMrecovery"];
            Database Db = new Database();
            string sql = "select * from TeamMember_3 where PasswordTMrecovery = '" + Tmp.PasswordTMrecovery + "'";
            DataTable Dt = Db.Execute(sql);
            for (int i = 0; i < Dt.Rows.Count; i++)
                if (Dt.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {
                    Manager Tmp11 = new Manager()
                    {
                        IDteamMember = (int)Dt.Rows[i]["IDteamMember"],
                        UserNameTeamMember = (string)Dt.Rows[i]["UserNameTeamMember"],
                        nameTeamMember = (string)Dt.Rows[i]["nameTeamMember"],
                        FamilyTeamMember = (string)Dt.Rows[i]["FamilyTeamMember"],
                        PicManager = (string)Dt.Rows[i]["PicManager"]

                    };
                    //.IDteamMember = (int)Dt2.Rows[i]["IDteamMember"]
                    Session["TheTeamMember"] = Tmp11;

                }
            //מנהלים
            Admins Tmp1 = new Admins();
            Tmp1.PasswordAdminRecovery = (string)Session["PasswordAdminRecovery"];
            Database Db1 = new Database();
            string sql1 = "select * from Admin_2 where PasswordAdminRecovery = '" + Tmp1.PasswordAdminRecovery + "'";
            DataTable Dt1 = Db1.Execute(sql1);
            for (int i = 0; i < Dt1.Rows.Count; i++)
                if (Dt1.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {
                    Admins Tmp12 = new Admins()
                    {
                        IDadmin = (int)Dt1.Rows[i]["IDadmin"],
                        UserNameAdmin = (string)Dt1.Rows[i]["UserNameAdmin"],
                        NameAdmin = (string)Dt1.Rows[i]["NameAdmin"],
                        LastNameAdmin = (string)Dt1.Rows[i]["LastNameAdmin"],
                        PicAdmin = (string)Dt1.Rows[i]["PicAdmin"],
                    };
                    //.IDteamMember = (int)Dt2.Rows[i]["IDteamMember"]
                    Session["TheAdmin"] = Tmp12;

                }
            //הורים
            Parents Tmp2 = new Parents();
            Tmp2.PasswordParentsRecovery = (string)Session["PasswordParentsRecovery"];
            Database Db2 = new Database();
            string sql2 = "select * from Kids_5 where PasswordParentsRecovery = '" + Tmp2.PasswordParentsRecovery + "'";
            DataTable Dt2 = Db2.Execute(sql2);
            for (int i = 0; i < Dt2.Rows.Count; i++)
                if (Dt2.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {
                    Parents Tmp13 = new Parents()
                    {
                        IDkids = (int)Dt2.Rows[i]["IDkids"],
                        UserNameParents = (string)Dt2.Rows[i]["UserNameParents"],
                        NameKids = (string)Dt2.Rows[i]["NameKids"],
                        LastNameKids = (string)Dt2.Rows[i]["LastNameKids"],
                        PicKids = (string)Dt2.Rows[i]["PicKids"],
                        NameFather = (string)Dt2.Rows[i]["NameFather"],
                        NameMother = (string)Dt2.Rows[i]["NameMother"]
                    };
                    //.IDteamMember = (int)Dt2.Rows[i]["IDteamMember"]
                    Session["TheKids"] = Tmp13;

                }





        }

        protected void btnNewPass_Click(object sender, EventArgs e)
        {
            //חברי צוות
            Manager Tmp11 = (Manager)Session["TheTeamMember"];
            if (Tmp11 != null)
            {
                Manager Tmp = new Manager()
                {

                    PasswordTMrecovery = UserPassword.Text,
                    IDteamMember = Tmp11.IDteamMember,
                    UserNameTeamMember = Tmp11.UserNameTeamMember
                };

                if (Tmp.InsertPass())
                {
                    Session["ResetPass"] = Tmp;
                    Response.Redirect("Default.aspx");

                }
            }

            //מנהלים
            Admins Tmp12 = (Admins)Session["TheAdmin"];
            if (Tmp12 != null)
            {

                Admins Tmp1 = new Admins()
                {

                    PasswordAdminRecovery = UserPassword.Text,
                    IDadmin = Tmp12.IDadmin,
                    UserNameAdmin = Tmp12.UserNameAdmin
                };
                if (Tmp1.InsertPass())
                {
                    Session["ResetPass"] = Tmp1;
                    Response.Redirect("Default.aspx");

                }
            }
            //הורים
            Parents Tmp13 = (Parents)Session["TheKids"];
            if (Tmp13 != null)
            {
                Parents Tmp2 = new Parents()
                {

                    PasswordParentsRecovery = UserPassword.Text,
                    IDkids = Tmp13.IDkids,
                    UserNameParents = Tmp13.UserNameParents
                };

                if (Tmp2.InsertPass())
                {
                    Session["ResetPass"] = Tmp2;
                    Response.Redirect("Default.aspx");

                }
            }
        }
    }
}






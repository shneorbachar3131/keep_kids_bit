﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using KeepKids.utils;


namespace KeepKids.Admin
{
    public partial class RegisterAdmins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //רשימת החברות
                nameID.DataSource = Manager.view();
                nameID.DataBind();
                nameID.DataTextField = "NameCompany";
                nameID.DataValueField = "IDcompany";
                nameID.DataBind();
                nameID.Items.Insert(0, new ListItem(" החברה בו הינך עובדת", "0"));
                nameID.SelectedIndex = 0;
            }

        }

        protected void btnRegisterAdmins_Click(object sender, EventArgs e)
        {
            PicUpload.SaveAs(Path.Combine("C:\\Users\\שניאור\\source\\repos\\KeepKids\\KeepKids\\pics\\", PicUpload.FileName));
            Session["AdminPic"] = PicUpload.FileName;

            Admins Tmp1 = new Admins()
            {
                IDcompany = int.Parse(nameID.SelectedValue),
                AddressAdmin = Address.Text,
                NameAdmin = FirstName.Text,
                LastNameAdmin = LastName.Text,
                PhoneAdmin = Phone.Text,
                UserNameAdmin = Email.Text,
                PasswordAdmin = Password.Text,
                PicAdmin = (string)Session["AdminPic"]



            };
            if (Tmp1.CheckMail())
            {

                Worng.Text = "<b class='text-danger'>*למייל זה כבר קיים חשבון  </b>";

            }
            else if (Tmp1.CheckRegister())
            {

                Session["AdminsName"] = Tmp1;
                GlobFuncs.SendMail(Tmp1.UserNameAdmin, "ברוכים הבאים", Tmp1.NameAdmin, "תודה שנרשמת לאתר KeepKids", "<html><body><tbody><tr><td width='100%' align='center' style='background-color:#91ccec ;padding:25px 10px 5px 10px'><table cellpadding='0' cellspacing='0' align='center' border='0' dir='rtl' style='max-width:580px!important;width:95%;height:auto;font-family:Arial'><tbody><tr><td width='100%' align='right' style='font-size:13px;font-family:Arial;direction:rtl;font-size:11pt;padding:0px 0px 10px 0px'></td></tr><tr><td width='100%' align='right' style='font-size:11pt;font-family:Arial;direction:rtl;padding:0px 0px 3px 0px'>שלום</td></tr><tr><td width='100%' align='right' style='font-family:Arial;padding:10px 0px 10px 0px'><table cellpadding='0' cellspacing='0' align='right' border='0' dir='rtl' style='max-width:580px!important;width:99%;height:auto;font-family:Arial;border:1px solid #dfdfdf;background-color:#ffffff'><tbody><tr><td width='100%' align='center' style='padding:10px 0px 10px 0px'><table cellpadding='0' cellspacing='0' align='center' border='0' dir='rtl' style='max-width:580px!important;width:94%;height:auto;font-family:Arial'><tbody><tr><td width='80%' align='right' style='padding:10px 0px 0px 0px'><table cellpadding='0' cellspacing='0' align='right' border='0' dir='rtl' width='100%'><tbody><tr><td width='100%' align='right' style='font-family:Arial;font-size:13pt;font-weight:bold;color:#505050;padding:0px 0px 10px 0px'>" + Tmp1.NameAdmin + "  " + Tmp1.LastNameAdmin + "  " + "היקר!! " + "</td></tr></tbody></table></td><td width='20%' align='left' style='vertical-align:top;padding:10px 10px 0px 0px'><img  src='https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg'  width='100%' border='0' style='display:block' class='CToWUd'</td></tr><tr><td width='100%' align='right' colspan='2' style='padding:10px 0px 10px 0px;font-family:Arial;font-size:11pt;color:#505050;line-height:180%'>שמחים שאתה כאן!<br/> תודה על הרשמתכם מקווים שתהנו ותפיקו את המירב המקסימלי בשימוש בפלטפורמה הנ''ל  </td></tr><tr><td width='100%' align='left' colspan='2'><b style='text-decoration:underline;font-family:arial;font-size:11pt;color:#ce1bef'>בשורות טובות</b></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></body></html>");
                Response.Redirect("Default.aspx");

            }

            else
            {
                Worng.Text = "<b class='text-danger'>*שדה אחד או יותר אינם תקינים </b>";
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using KeepKids.utils;

namespace KeepKids
{
    namespace Admin
    {
        public partial class RegisterParents : System.Web.UI.Page
        {
            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                    if (Session["ManagerName"] == null && Session["ParentsName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null && Session["TheKids"] == null)
                    {
                        Response.Redirect("Login.aspx");
                    }
                    if (Session["ManagerName"] != null)
                    {
                        Manager Tmp = (Manager)Session["ManagerName"];
                        //רשימת אחראי צוות
                        ManagerTeam.DataSource = Manager.viewOne(Tmp.IDteamMember);
                        ManagerTeam.DataBind();
                        ManagerTeam.DataTextField = "FullName";
                        ManagerTeam.DataValueField = "IDteamMember";
                        ManagerTeam.DataBind();
                        ManagerTeam.Items.Insert(0, new ListItem("שם הגננת", "0"));
                        ManagerTeam.SelectedIndex = 0;
                        //רשימת כיתות
                        NameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
                    }

                    if (Session["AdminsName"] != null)
                    {
                        Admins Tmp = (Admins)Session["AdminsName"];
                        //רשימת אחראי צוות
                        ManagerTeam.DataSource = Admins.viewByAdmin(Tmp.IDcompany);
                        ManagerTeam.DataBind();
                        ManagerTeam.DataTextField = "FullName";
                        ManagerTeam.DataValueField = "IDteamMember";
                        ManagerTeam.DataBind();
                        ManagerTeam.Items.Insert(0, new ListItem("שם הגננת", "0"));
                        ManagerTeam.SelectedIndex = 0;
                        //רשימת כיתות
                        NameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
                    }

                    if(Session["ParentsName"] != null)
                    {
                        Response.Redirect("Login.aspx");
                    }


                }
            }
            protected void ManagerTeam_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (Session["ManagerName"] != null)
                {
                    //רשימת כיתות
                    NameClass.DataSource = Parents.viewAllClass(int.Parse(ManagerTeam.SelectedValue));
                    NameClass.DataBind();
                    NameClass.DataTextField = "NameClass";
                    NameClass.DataValueField = "IDclass";
                    NameClass.DataBind();
                    NameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
                    NameClass.SelectedIndex = 0;
                }
                if (Session["AdminsName"] != null)
                {
                    //רשימת כיתות
                    NameClass.DataSource = Parents.viewAllClass(int.Parse(ManagerTeam.SelectedValue));
                    NameClass.DataBind();
                    NameClass.DataTextField = "NameClass";
                    NameClass.DataValueField = "IDclass";
                    NameClass.DataBind();
                    NameClass.Items.Insert(0, new ListItem("בחר כיתה", "0"));
                    NameClass.SelectedIndex = 0;
                }

            }

            protected void btnRegisterParents_Click(object sender, EventArgs e)
            {
                Session["AdminsName"] = null;
                Session["ManagerName"] = null;


                PicUpload.SaveAs(Path.Combine("C:\\Users\\שניאור\\source\\repos\\KeepKids\\KeepKids\\pics\\", PicUpload.FileName));

                Parents Tmp1 = new Parents()
                {
                    IDteamMember = int.Parse(ManagerTeam.SelectedValue),
                    IDclass = int.Parse(NameClass.SelectedValue),
                    NameKids = FirstName.Text,
                    LastNameKids = LastName.Text,
                    NameFather = NameFather.Text,
                    NameMother = NameMother.Text,
                    PhoneFather = PhoneFather.Text,
                    PhoneMother = PhoneMother.Text,
                    UserNameParents = Email.Text,
                    PasswordParents = Password.Text,
                    PicKids = PicUpload.FileName

                };

                if (Tmp1.CheckPassword())
                {
                    Worng.Text = "<b class='h3 text-danger' data-toggle = 'modal' data-target = '#inlineForm'>*למייל זה כבר קיים חשבון והסיסמא אינה תואמת  </b>";
                    //Worng.Text += "<div class='row text-center'><div class='col-lg-12 col-md-12 col-sm-12'><div class='form-group'><button type='button' class='btn btn-danger ' data-toggle='modal' data-target='#inlineForm'>אזהרה!! לחץ כאן.</button><div class='modal fade text-left' id='inlineForm' tabindex='-1' role='dialog' aria-labelledby='myModalLabel33' aria-hidden='true'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><form action='#'><div class='modal-body'></div></form></div></div></div></div></div></div>";

                }


                else if (Tmp1.CheckRegister())
                {

                    Session["ParentsName"] = Tmp1;
                    //GlobFuncs.SendMail(Tmp1.UserNameParents, "ברוכים הבאים", Tmp1.NameFather, "תודה שנרשמת לאתר KeepKids", "<html><body><b class='text-align'>מייל זה נשלח באופן אוטומטי <img src =https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg alt='למה ככה '/></b><h1 class='bg-primary'>ברכה והצלחה</h1></body></html>");
                    GlobFuncs.SendMail(Tmp1.UserNameParents, "ברוכים הבאים", Tmp1.NameFather, "תודה שנרשמת לאתר KeepKids", "<html><body><tbody><tr><td width='100%' align='center' style='background-color:#91ccec ;padding:25px 10px 5px 10px'><table cellpadding='0' cellspacing='0' align='center' border='0' dir='rtl' style='max-width:580px!important;width:95%;height:auto;font-family:Arial'><tbody><tr><td width='100%' align='right' style='font-size:13px;font-family:Arial;direction:rtl;font-size:11pt;padding:0px 0px 10px 0px'></td></tr><tr><td width='100%' align='right' style='font-size:11pt;font-family:Arial;direction:rtl;padding:0px 0px 3px 0px'>שלום</td></tr><tr><td width='100%' align='right' style='font-family:Arial;padding:10px 0px 10px 0px'><table cellpadding='0' cellspacing='0' align='right' border='0' dir='rtl' style='max-width:580px!important;width:99%;height:auto;font-family:Arial;border:1px solid #dfdfdf;background-color:#ffffff'><tbody><tr><td width='100%' align='center' style='padding:10px 0px 10px 0px'><table cellpadding='0' cellspacing='0' align='center' border='0' dir='rtl' style='max-width:580px!important;width:94%;height:auto;font-family:Arial'><tbody><tr><td width='80%' align='right' style='padding:10px 0px 0px 0px'><table cellpadding='0' cellspacing='0' align='right' border='0' dir='rtl' width='100%'><tbody><tr><td width='100%' align='right' style='font-family:Arial;font-size:13pt;font-weight:bold;color:#505050;padding:0px 0px 10px 0px'>" + Tmp1.NameFather + "  " + "ו" + Tmp1.NameMother + "  " + "היקרים!! " + "</td></tr></tbody></table></td><td width='20%' align='left' style='vertical-align:top;padding:10px 10px 0px 0px'><img  src='https://upload.wikimedia.org/wikipedia/commons/8/8d/Yarra_Night_Panorama%2C_Melbourne_-_Feb_2005.jpg'  width='100%' border='0' style='display:block' class='CToWUd'</td></tr><tr><td width='100%' align='right' colspan='2' style='padding:10px 0px 10px 0px;font-family:Arial;font-size:11pt;color:#505050;line-height:180%'>מזל טוב! כעת הוספתם את " + Tmp1.NameKids + " " + "היקר.  <br/> תודה על הרשמתכם מקווים שתהנו ותפיקו את המירב המקסימלי בשימוש בפלטפורמה הנ''ל  </td></tr><tr><td width='100%' align='left' colspan='2'><b style='text-decoration:underline;font-family:arial;font-size:11pt;color:#ce1bef'>בשורות טובות</b></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></body></html>");
                    Response.Redirect("Default.aspx");

                }

                else
                {
                    Worng.Text = "<b class='text-danger'>*שדה אחד או יותר אינם תקינים </b>";
                }

            }



        }
    }

}






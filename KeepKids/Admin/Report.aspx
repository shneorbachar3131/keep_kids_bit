﻿<%@ Page Title="דיווח" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="KeepKids.Admin.Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/app-contacts.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/datedropper.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/timedropper.min.css">
    <!-- END: Page CSS-->
    <link href="css/contant.css" rel="stylesheet" />
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css">
    <link href="css/custme.css" rel="stylesheet" />
    <script src="js/jquery-3.4.1.js"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row mb-1"></div>


            <div class="content-detached content-right">
                <div class="content-body">
                    <div class="content-overlay"></div>


                    <div class="card">
                                   
                                       
                        <div class="row mt-1">
                            <div class="col-md-2"></div>
                            <div class="col-6 col-md-4">
                                <asp:DropDownList ID="NameClass" runat="server" DataTextField="NameClass" DataValueField="IDclass" OnSelectedIndexChanged="NameClass_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="TxtNameClass" runat="server" ControlToValidate="NameClass" ErrorMessage="אנא בחר כיתה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-6 col-md-4">
                                <asp:TextBox runat="server" ID="dropBackgroundColor" CssClass="form-control" placeholder="01/01/2019"></asp:TextBox><asp:RequiredFieldValidator ID="TxtdropBackgroundColor" runat="server" ControlToValidate="dropBackgroundColor" ErrorMessage="אנא בחר תאריך" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-2 "></div>
                        </div>
                        <div class="card-head">
                            <div class="card-header">
                                <div class="heading-elements mt-0">
                                    <div class="modal fade" id="AddContactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <section class="contact-form">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel1">Add New Contact</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <fieldset class="form-group col-12">
                                                            <input type="text" id="contact-name" class="contact-name form-control" placeholder="Name">
                                                        </fieldset>
                                                        <fieldset class="form-group col-12">
                                                            <input type="text" id="contact-email" class="contact-email form-control" placeholder="Email">
                                                        </fieldset>
                                                        <fieldset class="form-group col-12">
                                                            <input type="text" id="contact-phone" class="contact-phone form-control" placeholder="Phone Number">
                                                        </fieldset>
                                                        <fieldset class="form-group col-12">
                                                            <input type="checkbox" id="favorite" class="contact-fav input-chk">
                                                            Favorite
                                                                   
                                                        </fieldset>
                                                        <fieldset class="form-group col-12">
                                                            <input type="file" class="form-control-file" id="user-image">
                                                        </fieldset>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <fieldset class="form-group position-relative has-icon-left mb-0">
                                                            <button type="button" id="add-contact-item" class="btn btn-info add-contact-item" data-dismiss="modal"><i class="la la-paper-plane-o d-block d-lg-none"></i><span class="d-none d-lg-block">Add New</span></button>
                                                        </fieldset>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="EditContactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <section class="contact-form">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Contact</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <fieldset class="form-group col-12">
                                                            <input type="text" id="name" class="name form-control" placeholder="Name">
                                                        </fieldset>
                                                        <fieldset class="form-group col-12">
                                                            <input type="text" id="email" class="email form-control" placeholder="Email">
                                                        </fieldset>
                                                        <fieldset class="form-group col-12">
                                                            <input type="text" id="phone" class="phone form-control" placeholder="Phone Number">
                                                        </fieldset>
                                                        <span id="fav" class="d-none"></span>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <fieldset class="form-group position-relative has-icon-left mb-0">
                                                            <button type="button" id="edit-contact-item" class="btn btn-info edit-contact-item" data-dismiss="modal"><i class="la la-paper-plane-o d-lg-none"></i><span class="d-none d-lg-block">Edit</span></button>
                                                        </fieldset>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 mb-2">
                                        <asp:LinkButton ID="BtnFound" OnClick="BtnFound_Click" CssClass="btn btn-success w-50 ml-md-3" runat="server">דווח</asp:LinkButton>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                    </div>
                                </div>
                                <!-- Task List table -->
                                <div class="table-responsive">
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle text-center">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 5%;">


                                                            <asp:CheckBox ID="CheckAll" runat="server" CssClass="CheckAll" AutoPostBack="true" Checked="false" OnCheckedChanged="CheckAll_CheckedChanged" />



                                                            <%--<input type="checkbox" class="input-chk" id="check-all"></th>--%>
                                                            <%--check-all--%>

                                                        <th style="width: 10%;">שם</th>
                                                        <th style="width: 85%;">הערות</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="RptRE" runat="server">
                                                        <ItemTemplate>

                                                            <tr>
                                                                <td style="width: 5%;">

                                                                    <asp:HiddenField ID="idkids" runat="server" Value='<%#Eval("IDkids") %>' />
                                                                    <asp:CheckBox ID="CheckBox2" runat="server" CssClass="input-chk check" Checked="false" /><%--input-chk check--%>
                                                                    <%--<input type="checkbox" class="input-chk check">--%>
                                                                </td>



                                                                <td style="width: 10%;">
                                                                    <div class="media">
                                                                        <div class="media-left pr-1">
                                                                            <span class="avatar avatar-sm  rounded-circle">
                                                                                <img src="../pics/<%#Eval("PicKids") %>" alt="חניך"><i></i></span>
                                                                        </div>
                                                                        <div class="media-body media-middle">
                                                                            <%#Eval("NameKids") %> <%#Eval("LastNameKids") %>
                                                                        </div>

                                                                    </div>
                                                                </td>
                                                                <td class="text-center" style="width: 85%;">
                                                                    <asp:TextBox runat="server" ID="Remarks" CssClass="form-control"></asp:TextBox>
                                                                </td>

                                                            </tr>

                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th style="width: 5%;"></th>
                                                        <th style="width: 25%;">שם</th>
                                                        <th style="width: 65%;">הערות</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="CheckAll" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>
    <asp:Literal ID="Hide" runat="server" />

    <%--<script>
        function LoadParents(Uid) {
            var Prod;
            $.ajax({
                type: "GET",
                url: "/api/Parents/" + Uid,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    Parents = JSON.parse(data);
                    $("#IDkids").val(Parents[0].IDkids);
                    $("#NameKids").val(Parents[0].NameKids);
                    $("#LastNameKids").val(Parents[0].LastNameKids);

                    //alert("תודה");
                },

                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        }
        function UpdateParents() {
            var ParentsOBJ = { IDkids: $("#IDkids").val(), NameKids: $("#NameKids").val(), LastNameKids: $("#LastNameKids").val() };
            $.ajax({
                type: "PUT",
                url: "/api/Parents/" + ParentsOBJ.IDkids,
                data: JSON.stringify(ParentsOBJ),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    Parents = JSON.parse(data);
                    $('#inlineForm').modal('hide');
                    location.reload();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }

            });

        }
        function Del(id) {
            var Answer = confirm("בטוח בכך שברצונך למחוק?");

            if (Answer === false)
                return false;

            $.ajax({
                type: "DELETE",
                url: "/api/Parents/" + id,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    location.reload();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }

            });
        }

    </script>--%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">

    <%-- <!-- BEGIN: Vendor JS-->
    <script src="/app-assets/vendors/js/vendors.min.js"></script>--%>
    <!-- BEGIN Vendor JS-->
    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/vendors/js/extensions/datedropper.min.js"></script>
    <script src="/app-assets/js/scripts/extensions/date-time-dropper.js"></script>
    <script src="/app-assets/vendors/js/tables/jquery.dataTables.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/jquery.raty.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/timedropper.min.js"></script>
    <!-- END: Page Vendor JS-->
    <%-- <!-- BEGIN: Theme JS-->
    <script src="/app-assets/js/core/app-menu.js"></script>
    <script src="/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->--%>
    <!-- BEGIN: Page JS-->
    <script src="/app-assets/js/scripts/pages/app-contacts.js"></script>
    <script src="js/ichack.js"></script>
    <!-- END: Page JS-->
    <script src="/app-assets/vendors/js/animation/jquery.appear.js"></script>
    <script src="/app-assets/js/scripts/animation/animation.js"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/vendors/js/charts/chartist.min.js"></script>
    <script src="/app-assets/vendors/js/charts/raphael-min.js"></script>
    <script src="/app-assets/vendors/js/charts/morris.min.js"></script>
    <script src="/app-assets/vendors/js/timeline/horizontal-timeline.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <%-- <script src="/app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
        <script src="/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js"></script>--%>
    <!-- END: Page JS-->

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>

















<%--<span class="dropdown">
<a id="btnSearchDrop31" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="dropdown-toggle dropdown-menu-right"><i class="la la-ellipsis-v"></i></a>
<span aria-labelledby="btnSearchDrop31" class="dropdown-menu mt-1 dropdown-menu-right">
<a data-toggle="modal" data-target="#EditContactModal" class="dropdown-item edit"><i class="ft-edit-2"></i>
Edit</a>
<a href="#" class="dropdown-item delete"><i class="ft-trash-2"></i>Delete</a>
<a href="#" class="dropdown-item"><i class="ft-plus-circle primary"></i>Projects</a>
<a href="#" class="dropdown-item"><i class="ft-plus-circle info"></i>Team</a>
<a href="#" class="dropdown-item"><i class="ft-plus-circle warning"></i>Clients</a>
<a href="#" class="dropdown-item"><i class="ft-plus-circle success"></i>Friends</a>
</span>
</span>--%>
<%--<input type="checkbox" class="input-chk check">dropBackgroundColor--%>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using System.Globalization;


namespace KeepKids
{
    namespace Admin
    {

        public partial class Report : System.Web.UI.Page
        {

            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                    FillData();

                }




            }

            protected void FillData()
            {
                if (Session["ManagerName"] == null && Session["TheTeamMember"] is null)
                {

                    Response.Redirect("Login.aspx");

                }
                if (Session["ManagerName"] != null)
                {
                    //כיתת לימוד
                    Manager Tmp = (Manager)Session["ManagerName"];
                    NameClass.DataSource = ToReport.ShowClass(Tmp.IDteamMember);
                    NameClass.DataBind();
                    NameClass.DataTextField = "NameClass";
                    NameClass.DataValueField = "IDclass";
                    NameClass.DataBind();
                    NameClass.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));
                }
            }


            protected void NameClass_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (Session["ManagerName"] != null)
                {
                    BtnFound.CssClass = "btn btn-success w-50 ml-md-3";
                    BtnFound.Text = "דווח";
                    //כל הכיתות
                    Manager Tmp = (Manager)Session["ManagerName"];
                    int Class = int.Parse(NameClass.SelectedValue);
                    RptRE.DataSource = ToReport.ShowKidsBYclass(Tmp.IDteamMember, Class);
                    RptRE.DataBind();

                }

            }
            protected void CheckAll_CheckedChanged(object sender, EventArgs e)
            {
                if (CheckAll.Checked == true)
                {
                    foreach (RepeaterItem item in RptRE.Items)
                    {
                        CheckBox CheckBox = (CheckBox)item.FindControl("CheckBox2");
                        CheckBox.Checked = true;
                    }
                }
                else
                {
                    foreach (RepeaterItem item in RptRE.Items)
                    {
                        CheckBox CheckBox = (CheckBox)item.FindControl("CheckBox2");
                        CheckBox.Checked = false;
                    }
                }
            }

            protected void BtnFound_Click(object sender, EventArgs e)
            {
                if (Session["ManagerName"] != null)
                {
                    Manager Tmp = (Manager)Session["ManagerName"];
                    Manager.DeleteBeforeInsert(Tmp.IDteamMember, int.Parse(NameClass.SelectedValue), dropBackgroundColor.Text);
                    int count = 0;
                    foreach (RepeaterItem i in RptRE.Items)
                    {
                        CheckBox chek = (CheckBox)i.FindControl("CheckBox2");
                        if (chek.Checked == true)
                        {
                            break;
                        }
                        if (chek.Checked == false)
                        {
                            count++;
                        }
                    }
                    DateTime dt = DateTime.ParseExact(dropBackgroundColor.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    var dateAndTime = DateTime.Now;
                    var date = dateAndTime.Date;
                    DateTime chackdate = dt;
                    if (chackdate > date)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('התאריך הנבחר אינו תקין!')", true);

                    }
                    if (count == RptRE.Items.Count)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('לא סומן אף תלמיד');window.location ='';", true);
                    }

                    else
                    {

                        foreach (RepeaterItem item in RptRE.Items)
                        {

                            CheckBox CB = (CheckBox)item.FindControl("CheckBox2");
                            if (CB.Checked == true)
                            {
                                HiddenField hf = (HiddenField)item.FindControl("idkids");
                                TextBox txtrem = (TextBox)item.FindControl("Remarks");
                                ToReport.InsertPresenceReport(Tmp.IDteamMember, int.Parse(NameClass.SelectedValue), dropBackgroundColor.Text, int.Parse(hf.Value), "1", txtrem.Text);
                                txtrem.Text = "";
                            }
                            if (CB.Checked == false)
                            {
                                HiddenField hf = (HiddenField)item.FindControl("idkids");
                                TextBox txtrem = (TextBox)item.FindControl("Remarks");
                                ToReport.InsertPresenceReport(Tmp.IDteamMember, int.Parse(NameClass.SelectedValue), dropBackgroundColor.Text, int.Parse(hf.Value), "0", txtrem.Text);
                                txtrem.Text = "";

                            }

                            CB.Checked = false;

                        }
                        CheckAll.Checked = false;
                        BtnFound.CssClass = "btn btn-info w-50 ml-md-3";
                        BtnFound.Text = "הדיווח נשלח";
                        dropBackgroundColor.Text = "";
                        NameClass.SelectedIndex = 0;
                    }
                }
            }


        }

    }
}























//List<ToReport> LstRE = (List<ToReport>)Session["RE"];

﻿<%@ Page Title="דיווח אישי" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ReportPersonal.aspx.cs" Inherits="KeepKids.Admin.ReportPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    
   <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/datedropper.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css">
    <script src="js/jquery-3.4.1.js"></script>
    <link href="css/custme.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <div class="app-content content">
        <div class="content-wrapper ">
            <div class="content-header row mb-1"></div>


            <div class="content-detached content-right ">
                <div class="content-body">
                    <div class="content-overlay"></div>


                    <div class="card italya">
                        <div class="card-body ">
                            <asp:Panel DefaultButton="SaveReportPersonal" runat="server">
                                <div class="form-body ">

                                    <h4 class="form-section"><i class="la la-paperclip"></i>דו''ח אישי</h4>
                                    <asp:Literal ID="Worng" runat="server" />
                                    <div class="row">
                                        <div class="col-3"></div>
                                        <div class="col-md-6">
                                            <%--<input type="text" class="form-control input-lg" id="dropBackgroundColor" placeholder="בחר תאריך" readonly="readonly">--%>
                                            <div class="card txtclaud">
                                                <fieldset class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                        </div>
                                                        <asp:TextBox runat="server" ID="dropTextWeight" CssClass="form-control" placeholder="בחירת תאריך"></asp:TextBox><asp:RequiredFieldValidator ID="TxtdropTextWeight" runat="server" ControlToValidate="dropTextWeight" ErrorMessage="אנא בחר תאריך" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>

                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-3"></div>
                                    </div>

                                    <div class="row pt-1 ">
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <b>שם הגננת</b>
                                                <asp:RequiredFieldValidator ID="TxtManagerTeam" runat="server" ControlToValidate="ManagerTeam" ErrorMessage="אנא בחר גננת" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ManagerTeam" runat="server" OnSelectedIndexChanged="ManagerTeam_SelectedIndexChanged" AutoPostBack="true" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control round">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <b>שם הילד</b>
                                                <asp:RequiredFieldValidator ID="TxtNameKids" runat="server" ControlToValidate="NameKids" ErrorMessage="אנא בחר תלמיד" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="NameKids" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control round">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:RequiredFieldValidator ID="TxtReviewReport" runat="server" ControlToValidate="ReviewReport" ErrorMessage="אנא כתוב דוח על התלמיד" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="ReviewReport" runat="server" CssClass="shneorbachar" TextMode="multiline" Columns="50" Rows="5" placeholder="כתוב כאן את הדיווח שלך" />
                                    </div>

                                </div>

                                <div class="row ">
                                    <div class="col-6 pl-4 pr-2">
                                        <asp:LinkButton ID="CancelReport" OnClick="CancelReport_Click" CausesValidation="false" CssClass="btn btn-danger btn-block" runat="server"><i class="ft-x"></i>בטל</asp:LinkButton>
                                    </div>

                                    <div class="col-6 pr-4 pl-2">
                                        <asp:LinkButton ID="SaveReportPersonal" OnClick="SaveReportPersonal_Click" CssClass="btn btn-success btn-block" runat="server"><i class="la la-check-square-o"></i>שלח</asp:LinkButton>

                                    </div>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">
    <script src="/app-assets/vendors/js/extensions/datedropper.min.js"></script>
    <script src="/app-assets/js/scripts/extensions/date-time-dropper.js"></script>
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>

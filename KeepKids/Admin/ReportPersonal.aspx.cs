﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using KeepKids.utils;


namespace KeepKids.Admin
{
    public partial class ReportPersonal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Session["ManagerName"] == null && Session["AdminsName"] == null && Session["TheTeamMember"] == null && Session["TheAdmin"] == null)
                {
                    Response.Redirect("Default.aspx");
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('אינך יכול להיכנס לאזור זה')", true);

                }
                if (Session["ManagerName"] != null)
                {
                    //רשימת אחראי צוות
                    Manager Tmp1 = (Manager)Session["ManagerName"];
                    ManagerTeam.DataSource = Manager.viewByid(Tmp1.IDteamMember);
                    ManagerTeam.DataBind();
                    ManagerTeam.DataTextField = "FullName";
                    ManagerTeam.DataValueField = "IDteamMember";
                    ManagerTeam.DataBind();
                    for (int i = 0; i < ManagerTeam.Items.Count; i++)
                    {
                        if (ManagerTeam.Items[i].Value == Tmp1.IDteamMember.ToString())
                        {
                            ManagerTeam.Items[i].Selected = true;
                            break;

                        }
                    }


                    //רשימת ילדים
                    NameKids.DataSource = Manager.viewNameKids(Tmp1.IDteamMember);
                    NameKids.DataBind();
                    NameKids.DataTextField = "NameKids";
                    NameKids.DataValueField = "IDkids";
                    NameKids.DataBind();
                    NameKids.Items.Insert(0, new ListItem("שם ילד לדיווח", "0"));
                    NameKids.SelectedIndex = 0;
                }
                if (Session["AdminsName"] != null)
                {
                    //רשימת אחראי צוות
                    Admins Tmp1 = (Admins)Session["AdminsName"];
                    ManagerTeam.DataSource = Admins.viewByAdmin(Tmp1.IDcompany);
                    ManagerTeam.DataBind();
                    ManagerTeam.DataTextField = "FullName";
                    ManagerTeam.DataValueField = "IDteamMember";
                    ManagerTeam.DataBind();
                    ManagerTeam.Items.Insert(0, new ListItem("שם מנהל הצוות", "0"));
                    ManagerTeam.SelectedIndex = 0;


                    //רשימת ילדים
                    NameKids.DataSource = Admins.viewNameKids(int.Parse(ManagerTeam.SelectedValue));
                    NameKids.DataBind();
                    NameKids.DataTextField = "NameKids";
                    NameKids.DataValueField = "IDkids";
                    NameKids.DataBind();
                    NameKids.Items.Insert(0, new ListItem("שם ילד לדיווח", "0"));
                    NameKids.SelectedIndex = 0;
                }
            }
        }

        protected void SaveReportPersonal_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.ParseExact(dropTextWeight.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);


            var dateAndTime = DateTime.Now;
            var date = dateAndTime.Date;
            DateTime chackdate = dt;

            if (chackdate > date)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('התאריך הנבחר אינו תקין!')", true);

            }
            else
            {
                Parents Tmp1 = new Parents()
                {
                    IDteamMember = int.Parse(ManagerTeam.SelectedValue),
                    IDkids = int.Parse(NameKids.SelectedValue),
                    Report = ReviewReport.Text,
                    DateReportPersonal = dropTextWeight.Text

                };
                if (Tmp1.CheckReport())
                {
                    Worng.Text = "<h2 class='text-success'>*הדיווח בוצע בהצלחה </h2>";
                    ManagerTeam.SelectedIndex = 0;
                    NameKids.SelectedIndex = 0;
                    ReviewReport.Text = "";
                    dropTextWeight.Text = "";
                }

                else
                {
                    Worng.Text = "<b class='text-danger'>*שדה אחד או יותר אינם תקינים </b>";
                }
            }

        }

        protected void CancelReport_Click(object sender, EventArgs e)
        {
            if (ManagerTeam.SelectedIndex == 0 && NameKids.SelectedIndex == 0 && ReviewReport.Text == "")
            {

                Response.Redirect("Default.aspx");
            }
            else
            {
                ManagerTeam.SelectedIndex = 0;
                NameKids.SelectedIndex = 0;
                ReviewReport.Text = "";
            }


        }

        protected void ManagerTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["AdminsName"] != null)
            {
                //רשימת ילדים
                NameKids.DataSource = Admins.viewNameKidsB(int.Parse(ManagerTeam.SelectedValue));
                NameKids.DataBind();
                NameKids.DataTextField = "NameKids";
                NameKids.DataValueField = "IDkids";
                NameKids.DataBind();
                NameKids.Items.Insert(0, new ListItem("שם ילד לדיווח", "0"));
            }
        }
    }
}
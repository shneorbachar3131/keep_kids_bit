﻿<%@ Page Title="שליחת תזכורות" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="SendReminders.aspx.cs" Inherits="KeepKids.Admin.SendReminders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/vendors-rtl.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css-rtl/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="../../../app-assets/css-rtl/core/colors/palette-gradient.css">
    <!-- END: Page CSS-->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">


    <div class="app-content content">
        <div class="content-wrapper">
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <h1 class="font-large-2 text-center mb-2">שליחת תזכורת</h1>
                        <div class="card-content collapse show">
                            <div class="card-body">

                                <div class="row justify-content-md-center">
                                    <div class="col-md-6">
                                        <input type="hidden" id="idcompani" runat="server" />
                                        <input type="hidden" id="MeeZe" runat="server" />
                                        <input type="hidden" id="IdUser" runat="server" />
                                        <input type="hidden" id="NameUser" runat="server" />
                                        <div class="form-group" id="manager">
                                            <select class="select2 form-control" id="programmatic-single">
                                                <option value="0">?את מי לתזכר</option>
                                                <option value="Admin">המנהל שלי</option>
                                                <option value="AllParent">כל ההורים</option>
                                                <option value="parentBYclass">כל הורי כיתה/גן ספציפית</option>
                                                <option value="PersonParent">הורה פרטני</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="admin">
                                            <select class="select2 form-control" id="programmatic-single-admins">
                                                <option value="0">?את מי לתזכר</option>
                                                <option value="AllManager">כל הגננות</option>
                                                <option value="PersonManager">גננת פרטני</option>
                                                <option value="parentBYkindergarden">כל הורי גן/כיתה ספציפים</option>
                                                <option value="PersonParent">הורה פרטני</option>
                                            </select>
                                        </div>
                                        <div class="form-group mt-1">
                                            <select class="select2 js-example-programmatic-multi form-control " id="programmatic-multiple" multiple="multiple">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Programmatic setting and clearing Select2 options">
                                            <button type="button" class="<%--js-programmatic-multi-set-val--%> atic-mul btn btn-outline-primary" onclick="MarkAll()">
                                                בחר הכל
                                           
                                            </button>
                                            <button type="button" class="atic-multi-cle btn btn-outline-primary" onclick="Clear()">
                                                נקה הכל
                                           
                                            </button>
                                        </div>
                                        <h1></h1>
                                        <div class="form-group">
                                            <input id="Subject" class="form-control" placeholder="נושא">
                                        </div>

                                        <div class="form-group">
                                            <textarea id="Description" rows="6" class="form-control border-primary" placeholder="תיאור"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-center">
                                <button type="button" class="btn btn-warning mr-1" onclick="Cancel()">
                                    <i class="ft-x"></i>בטל
                                               
                                </button>
                                <button type="button" class="btn btn-primary" onclick="SendReminder()">
                                    <i class="la la-check-square-o"></i>שלח
                               
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">
    <script src="js/SendReimend.js"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="../../../app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="../../../app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="../../../app-assets/js/scripts/forms/select/form-select2.js"></script>
    <!-- END: Page JS-->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>






﻿using KeepKids.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KeepKids.Admin
{
    public partial class SendReminders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["ManagerName"] != null)
                {
                    MeeZe.Value = "Manager";
                    Manager fn = (Manager)Session["ManagerName"];
                    IdUser.Value = fn.IDteamMember.ToString();
                    NameUser.Value = fn.FullName.ToString();
                }
                if (Session["AdminsName"] != null)
                {
                    MeeZe.Value = "Admin";
                    Admins fn = (Admins)Session["AdminsName"];
                    IdUser.Value = fn.IDadmin.ToString();
                    NameUser.Value = fn.NameAdmin + " " + fn.LastNameAdmin;
                    idcompani.Value = fn.IDcompany.ToString();
                }
            }
        }
    }
}

﻿<%@ Page Title="העלאת טפסים" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="UploadFiles.aspx.cs" Inherits="KeepKids.Admin.UploadFiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/app-email.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="css/custme.css" rel="stylesheet" />
    <!-- END: Page CSS-->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body ">
                <div class="card email-app-details">
                    <h1 class="font-large-2 text-center mb-2" id="HIDEboth1" runat="server">שליחת טפסים ומסמכים</h1>
                    <div class="card-content">
                        <div class="d-block d-lg-none mt-1 ml-1">
                        </div>

                        <div class="email-app-title card-body" id="conten" runat="server">
                            <h3 class="list-group-item-heading" id="TitleTag" runat="server"></h3>
                        </div>

                        <div class="media-list">

                            <div id="collapse1" runat="server">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:TextBox ID="TheSend" runat="server" CssClass="form-control" ReadOnly="true" />
                                            </div>
                                            <div class="col-md-6" id="HideSendToWho" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="SendToWho" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control" OnSelectedIndexChanged="SendToWho_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="0">בחר נמען לשליחת הטפסים</asp:ListItem>
                                                        <asp:ListItem Value="1">לכל הגננות</asp:ListItem>
                                                        <asp:ListItem Value="2">לגננת פרטני</asp:ListItem>
                                                        <asp:ListItem Value="3">לכל הורי גן ספציפי</asp:ListItem>
                                                        <asp:ListItem Value="4">להורה פרטני</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideKinderGarden" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="KinderGarden" runat="server" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideManagerTeam" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="ManagerTeam" runat="server" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="TxtManagerTeam" runat="server" ControlToValidate="ManagerTeam" ErrorMessage="אנא בחר מנהל צוות" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideKinderGarden1" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="KinderGarden1" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="TxtKinderGarden1" runat="server" ControlToValidate="KinderGarden1" ErrorMessage="אנא בחר שם גן" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideNameKids" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="NameKids" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="TxtNameKids" runat="server" ControlToValidate="NameKids" ErrorMessage="אנא בחר שם בן ההורה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-4">
                                                <div class="form-group"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:TextBox ID="TitleMessage" runat="server" CssClass="form-control" placeholder="כותרת ההודעה" /><asp:RequiredFieldValidator ID="TxtTitleMessage" runat="server" ControlToValidate="TitleMessage" ErrorMessage="אנא כתוב נושא להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <style>
                                                #containerCH {
                                                    text-align: center;
                                                }

                                                input[type="file"] {
                                                    display: none;
                                                }

                                                .custom-file-upload {
                                                    border: 1px solid #ccc;
                                                    display: inline-block;
                                                    padding: 6px 12px;
                                                    cursor: pointer;
                                                }
                                            </style>
                                            <div class="col-12 col-sm-12 col-md-12">
                                                <div id="containerCH">
                                                    <label class="custom-file-upload">
                                                        <asp:FileUpload runat="server" ID="PicUpload" /><asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="PicUpload" ErrorMessage="בחר קובץ להעלאה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <i class="fas fa-arrow-circle-up fa-3x pr-2"></i><b>העלה קובץ</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="ContentMessage" runat="server" CssClass="shneorbachar" TextMode="multiline" Columns="50" Rows="5" placeholder="תוכן ההודעה" /><asp:RequiredFieldValidator ID="TxtContentMessage" runat="server" ControlToValidate="ContentMessage" ErrorMessage="אנא כתוב תוכן להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <asp:Button ID="Send" OnClick="Send_Click" Text="שגר" runat="server" CssClass="btn btn-success round btn-min-width mr-1 mb-1 waves-effect waves-light" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                    </div>
                                </div>
                            </div>



                            <div id="collapse2" runat="server">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:TextBox ID="TheSendManager" runat="server" CssClass="form-control" ReadOnly="true" />
                                            </div>
                                            <div class="col-md-6" id="HideSendToWho1" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="SendToWho1" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control" OnSelectedIndexChanged="SendToWho1_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="0">בחר נמען לשליחת הקבצים</asp:ListItem>
                                                        <asp:ListItem Value="9">למנהל שלי</asp:ListItem>
                                                        <asp:ListItem Value="10">לכל ההורים</asp:ListItem>
                                                        <asp:ListItem Value="11">לכל הורי כיתה ספציפית</asp:ListItem>
                                                        <asp:ListItem Value="12">להורה פרטני</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideMyAdmins" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="MyAdmins" runat="server" DataTextField="NameAdmin" DataValueField="IDadmin" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideNameKidsMANAGER" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="NameKidsMANAGER" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideNameClass" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="NameClass" runat="server" DataTextField="NameClass" DataValueField="IDclass" CssClass="form-control" OnSelectedIndexChanged="NameClass_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NameClass" ErrorMessage="אנא בחר כיתה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideKidsClass" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="KidsClass" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control" OnSelectedIndexChanged="NameClass_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideNameKidsMANAGER1" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="NameKidsMANAGER1" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="NameKidsMANAGER1" ErrorMessage="אנא בחר בן הורה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-4">
                                                <div class="form-group"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:TextBox ID="TitleMessage1" runat="server" CssClass="form-control" placeholder="כותרת ההודעה" /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TitleMessage1" ErrorMessage="אנא כתוב נושא להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group"></div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <style>
                                                #containerCH1 {
                                                    text-align: center;
                                                }

                                                input[type="file"] {
                                                    display: none;
                                                }

                                                .custom-file-upload {
                                                    border: 1px solid #ccc;
                                                    display: inline-block;
                                                    padding: 6px 12px;
                                                    cursor: pointer;
                                                }
                                            </style>
                                            <div class="col-12 col-sm-12 col-md-12">
                                                <div id="containerCH1">
                                                    <label class="custom-file-upload">
                                                        <asp:FileUpload runat="server" ID="PicUpload1" /><asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="PicUpload" ErrorMessage="בחר קובץ להעלאה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <i class="fas fa-arrow-circle-up fa-3x pr-2"></i><b>העלה קובץ</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="ContentMessage1" runat="server" CssClass="shneorbachar" TextMode="multiline" Columns="50" Rows="5" placeholder="תוכן ההודעה" /><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ContentMessage1" ErrorMessage="אנא כתוב תוכן להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <asp:Button ID="Send1" OnClick="Send_Click" Text="שגר" runat="server" CssClass="btn btn-success round btn-min-width mr-1 mb-1 waves-effect waves-light" />
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                    </div>
                                </div>
                            </div>


                            <div id="collapse3" runat="server">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <asp:TextBox ID="TheSendParents" runat="server" CssClass="form-control" ReadOnly="true" />
                                            </div>
                                            <div class="col-md-6" id="HideSendToWho2" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="SendToWho2" runat="server" DataTextField="NameKindergarten" DataValueField="IDkindergarten" CssClass="form-control" OnSelectedIndexChanged="SendToWho2_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="0">בחר נמען לשליחת הקבצים </asp:ListItem>
                                                        <asp:ListItem Value="20">לגננת של הילד</asp:ListItem>
                                                        <asp:ListItem Value="21">לאחד מהורי ילדי הגן</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideIDteamMember" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="IDteamMember" runat="server" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="HideIDkids" runat="server">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="IDkids" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="IDkids" ErrorMessage="אנא בחר בן הורה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-4">
                                                <div class="form-group"></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:TextBox ID="TitleMessage2" runat="server" CssClass="form-control" placeholder="כותרת ההודעה" /><asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TitleMessage2" ErrorMessage="אנא כתוב נושא להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group"></div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <style>
                                                #containerCH2 {
                                                    text-align: center;
                                                }

                                                input[type="file"] {
                                                    display: none;
                                                }

                                                .custom-file-upload {
                                                    border: 1px solid #ccc;
                                                    display: inline-block;
                                                    padding: 6px 12px;
                                                    cursor: pointer;
                                                }
                                            </style>
                                            <div class="col-12 col-sm-12 col-md-12">
                                                <div id="containerCH2">
                                                    <label class="custom-file-upload">
                                                        <asp:FileUpload runat="server" ID="PicUpload2" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="PicUpload" ErrorMessage="בחר קובץ להעלאה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <i class="fas fa-arrow-circle-up fa-3x pr-2"></i><b>העלה קובץ</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="ContentMessage2" runat="server" CssClass="shneorbachar" TextMode="multiline" Columns="50" Rows="5" placeholder="תוכן ההודעה" /><asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ContentMessage2" ErrorMessage="אנא כתוב תוכן להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                        <asp:Button ID="Send2" OnClick="Send_Click" Text="שגר" runat="server" CssClass="btn btn-success round btn-min-width mr-1 mb-1 waves-effect waves-light" />
                                        <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                    </div>
                                </div>
                            </div>

                            <h3 class="list-group-item-heading" id="TitleTag1" runat="server"></h3>



                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">
    <script src="/app-assets/js/scripts/pages/app-email.js"></script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;
using System.Configuration;
using Twilio;

namespace KeepKids.Admin
{
    public partial class ViewEducationAct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ChackSource();
            }

        }
        protected void ChackSource()
        {

            if (Session["ParentsName"] != null)
            {
                HIDE1.Visible = false;
                HIDEtoParent.Visible = false;
                KinderGarden.Visible = false;
                TheNameClass.Visible = false;
                KinderGarden1.Visible = false;
                TheNameClass1.Visible = false;
                NameClass1.Visible = false;
                //על פי שם
                Parents Tmp = (Parents)Session["ParentsName"];
                NameKidsByName.DataSource = Parents.viewNameKidsById(Tmp.UserNameParents);
                NameKidsByName.DataBind();
                NameKidsByName.DataTextField = "NameKids";
                NameKidsByName.DataValueField = "IDkids";
                NameKidsByName.DataBind();
                NameKidsByName.Items.Insert(0, new ListItem("בחר שם הילד", "0"));

                NameKidsByName1.DataSource = Parents.viewNameKidsById(Tmp.UserNameParents);
                NameKidsByName1.DataBind();
                NameKidsByName1.DataTextField = "NameKids";
                NameKidsByName1.DataValueField = "IDkids";
                NameKidsByName1.DataBind();
                NameKidsByName1.Items.Insert(0, new ListItem("בחר שם הילד", "0"));

            }
            if (Session["ManagerName"] != null)
            {

                HIDE.Visible = false;
                HIDEtoManager.Visible = false;
                KinderGarden.Visible = false;
                TheNameClass.Visible = false;
                KinderGarden1.Visible = false;
                TheNameClass1.Visible = false;
                NameKidsByName1.Visible = false;
                //כיתת לימוד
                Manager Tmp = (Manager)Session["ManagerName"];
                NameClass.DataSource = ToReport.ShowClass(Tmp.IDteamMember);
                NameClass.DataBind();
                NameClass.DataTextField = "NameClass";
                NameClass.DataValueField = "IDclass";
                NameClass.DataBind();
                NameClass.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));

                //כיתת לימוד לתאריך
                NameClass1.DataSource = ToReport.ShowClass(Tmp.IDteamMember);
                NameClass1.DataBind();
                NameClass1.DataTextField = "NameClass";
                NameClass1.DataValueField = "IDclass";
                NameClass1.DataBind();
                NameClass1.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));
            }
            if (Session["AdminsName"] != null)
            {
                NameKidsByName1.Visible = false;
                NameKidsByName.Visible = false;
                NameClass.Visible = false;
                NameClass1.Visible = false;
                Admins Tmp1 = (Admins)Session["AdminsName"];
                //שם גן
                KinderGarden.DataSource = ToReport.NameKinderGardenMethod(Tmp1.IDcompany, Tmp1.IDadmin);
                KinderGarden.DataBind();
                KinderGarden.DataTextField = "NameKindergarten";
                KinderGarden.DataValueField = "IDkindergarten";
                KinderGarden.DataBind();
                KinderGarden.Items.Insert(0, new ListItem("בחר שם הגן", "0"));

                //שם על פי תאריך
                KinderGarden1.DataSource = ToReport.NameKinderGardenMethod(Tmp1.IDcompany, Tmp1.IDadmin);
                KinderGarden1.DataBind();
                KinderGarden1.DataTextField = "NameKindergarten";
                KinderGarden1.DataValueField = "IDkindergarten";
                KinderGarden1.DataBind();
                KinderGarden1.Items.Insert(0, new ListItem("בחר שם הגן", "0"));

                //כיתת לימוד
                TheNameClass.DataSource = ToReport.ShowClassAdmin(int.Parse(KinderGarden.SelectedValue));
                TheNameClass.DataBind();
                TheNameClass.DataTextField = "NameClass";
                TheNameClass.DataValueField = "IDclass";
                TheNameClass.DataBind();
                TheNameClass.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));

                //כיתת לימוד על פי תאריך
                TheNameClass1.DataSource = ToReport.ShowClassAdmin(int.Parse(KinderGarden.SelectedValue));
                TheNameClass1.DataBind();
                TheNameClass1.DataTextField = "NameClass";
                TheNameClass1.DataValueField = "IDclass";
                TheNameClass1.DataBind();
                TheNameClass1.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));
            }
        }

        protected void SHOWETable1()
        {
            Mytable1.Text = "<div class='row bgtable1'><div class='col-md-12'><div class='table-responsive'><table class='table table-bordered'><thead><tr><th style='width: 20%;'>תאריך</th><th>תיאור</th><th style='width: 20%;' class='HideParents'>תאריך</th></tr></thead></table></div></div></div>";

        }
        protected void SHOWETable2()
        {
            Mytable2.Text = "<div class='row bgtable1'><div class='col-md-12'><div class='table-responsive'><table class='table table-bordered'><thead><tr><th style='width: 20%;'>תאריך</th><th>תיאור</th></tr></thead></table></div></div></div>";

        }

        protected void ShowEducationActByMonth_Click(object sender, EventArgs e)
        {
            ShowEducationActByMonth.CssClass = "btn btn-danger  btn-block";
            if (Session["ParentsName"] != null)
            {
                if (Parents.viewEducationActByMonth(int.Parse(NameKidsByName.SelectedValue)).Count == 0)
                {
                    ShowEducationActByMonth.Text = "אין תוצאה";
                    Mytable1.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                    //RptName.DataSource = Parents.viewEducationActByMonth(int.Parse(NameKidsByName.SelectedValue));
                    //RptName.DataBind();
                }
                else
                {
                    ShowEducationActByMonth.Text = "הצגת אירועים חינוכיים";
                    SHOWETable1();
                    RptName.DataSource = Parents.viewEducationActByMonth(int.Parse(NameKidsByName.SelectedValue));
                    RptName.DataBind();
                }
            }
            if (Session["ManagerName"] != null)
            {
                if (Manager.viewEducationActByMonth(int.Parse(NameClass.SelectedValue)).Count == 0)
                {
                    ShowEducationActByMonth.Text = "אין תוצאה";
                    Mytable1.Text = "<b class='text-danger'>לא נמצאו נתונים על הכיתה</b>";
                    //RptName.DataSource = Manager.viewEducationActByMonth(int.Parse(NameClass.SelectedValue));
                    //RptName.DataBind();
                }
                else
                {
                    ShowEducationActByMonth.Text = "הצגת אירועים חינוכיים";
                    SHOWETable1();
                    RptName.DataSource = Manager.viewEducationActByMonth(int.Parse(NameClass.SelectedValue));
                    RptName.DataBind();
                }

            }
            if (Session["AdminsName"] != null)
            {
                if (Manager.viewEducationActByMonth(int.Parse(TheNameClass.SelectedValue)).Count == 0)
                {
                    ShowEducationActByMonth.Text = "אין תוצאה";
                    Mytable1.Text = "<b class='text-danger'>לא נמצאו נתונים על הכיתה</b>";
                    //RptName.DataSource = Manager.viewEducationActByMonth(int.Parse(TheNameClass.SelectedValue));
                    //RptName.DataBind();
                }
                else
                {
                    ShowEducationActByMonth.Text = "הצגת אירועים חינוכיים";
                    SHOWETable1();
                    RptName.DataSource = Manager.viewEducationActByMonth(int.Parse(TheNameClass.SelectedValue));
                    RptName.DataBind();
                }

            }
        }

        //כאשר שם גן משתנה בחודשי
        protected void KinderGarden_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["AdminsName"] != null)
            {
                TheNameClass.DataSource = ToReport.ShowClassAdmin(int.Parse(KinderGarden.SelectedValue));
                TheNameClass.DataBind();
                TheNameClass.DataTextField = "NameClass";
                TheNameClass.DataValueField = "IDclass";
                TheNameClass.DataBind();
                TheNameClass.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));
            }
        }

        //כאשר שם גן משתנה בבחירת תאריך
        protected void KinderGarden1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //הרצה של סקריפט
            string script = "$(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) });";
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(SomeClass), Guid.NewGuid().ToString(), script, true);

            if (Session["AdminsName"] != null)
            {
                TheNameClass1.DataSource = ToReport.ShowClassAdmin(int.Parse(KinderGarden1.SelectedValue));
                TheNameClass1.DataBind();
                TheNameClass1.DataTextField = "NameClass";
                TheNameClass1.DataValueField = "IDclass";
                TheNameClass1.DataBind();
                TheNameClass1.Items.Insert(0, new ListItem("בחר כיתת לימוד", "0"));
            }
        }

        //חודש
        protected void NameKidsByName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowEducationActByMonth.CssClass = "btn soldeen  btn-block";
            ShowEducationActByMonth.Text = "לחץ להצגה";
            Mytable1.Text = "";
            RptName.DataSource = "";
            RptName.DataBind();
        }


        //תאריכי
        protected void NameKidsByName1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //הרצה של סקריפט
            string script = "$(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) });";
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(SomeClass), Guid.NewGuid().ToString(), script, true);


            ShowEducationActByDate.CssClass = "btn soldeen  btn-block";
            ShowEducationActByDate.Text = "לחץ להצגה";
            Mytable2.Text = "";
            RptName1.DataSource = "";
            RptName1.DataBind();
        }

        //חודדש
        protected void NameClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowEducationActByMonth.CssClass = "btn soldeen  btn-block";
            ShowEducationActByMonth.Text = "לחץ להצגה";
            Mytable1.Text = "";
            RptName.DataSource = "";
            RptName.DataBind();

        }
        //תאריכי
        protected void NameClass1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //הרצה של סקריפט
            string script = "$(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) });";
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(SomeClass), Guid.NewGuid().ToString(), script, true);

            ShowEducationActByDate.CssClass = "btn soldeen  btn-block";
            ShowEducationActByDate.Text = "לחץ להצגה";
            Mytable2.Text = "";
            RptName1.DataSource = "";
            RptName1.DataBind();
        }

        //חודש
        protected void TheNameClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowEducationActByMonth.CssClass = "btn soldeen  btn-block";
            ShowEducationActByMonth.Text = "לחץ להצגה";
            Mytable1.Text = "";
            RptName.DataSource = "";
            RptName.DataBind();
        }
        //תאריכי
        protected void TheNameClass1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowEducationActByDate.CssClass = "btn soldeen  btn-block";
            ShowEducationActByDate.Text = "לחץ להצגה";
            Mytable2.Text = "";
            RptName1.DataSource = "";
            RptName1.DataBind();
        }







        protected void ShowEducationActByDate_Click(object sender, EventArgs e)
        {
            //הרצה של סקריפט
            string script = "$(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) });";
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(SomeClass), Guid.NewGuid().ToString(), script, true);

            ShowEducationActByDate.CssClass = "btn btn-danger  btn-block";
            if (Session["ParentsName"] != null)
            {
                if (Parents.viewEducationActByDate(int.Parse(NameKidsByName1.SelectedValue), maxYear.Text, dropTextColor.Text).Count == 0)
                {
                    ShowEducationActByDate.Text = "אין תוצאה";
                    Mytable2.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                }
                else
                {
                    ShowEducationActByDate.Text = "הצגת אירועים חינוכיים";
                    SHOWETable2();
                    RptName1.DataSource = Parents.viewEducationActByDate(int.Parse(NameKidsByName1.SelectedValue), maxYear.Text, dropTextColor.Text);
                    RptName1.DataBind();
                }
            }
            if (Session["ManagerName"] != null)
            {
                if (Admins.viewEducationActByDate(int.Parse(NameClass1.SelectedValue), maxYear.Text, dropTextColor.Text).Count == 0)
                {
                    ShowEducationActByDate.Text = "אין תוצאה";
                    Mytable2.Text = "<b class='text-danger'>לא נמצאו נתונים על הכיתה</b>";
                }
                else
                {
                    ShowEducationActByDate.Text = "הצגת אירועים חינוכיים";
                    SHOWETable2();
                    RptName1.DataSource = Admins.viewEducationActByDate(int.Parse(NameClass1.SelectedValue), maxYear.Text, dropTextColor.Text);
                    RptName1.DataBind();
                }
            }
            if (Session["AdminsName"] != null)
            {
                if (Admins.viewEducationActByDate(int.Parse(TheNameClass1.SelectedValue), maxYear.Text, dropTextColor.Text).Count == 0)
                {
                    ShowEducationActByDate.Text = "אין תוצאה";
                    Mytable2.Text = "<b class='text-danger'>לא נמצאו נתונים על הכיתה</b>";
                }
                else
                {
                    ShowEducationActByDate.Text = "הצגת אירועים חינוכיים";
                    SHOWETable2();
                    RptName1.DataSource = Admins.viewEducationActByDate(int.Parse(TheNameClass1.SelectedValue), maxYear.Text, dropTextColor.Text);
                    RptName1.DataBind();
                }

            }

        }

        private class SomeClass
        {
        }



    }

}




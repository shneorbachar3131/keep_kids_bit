﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;

namespace KeepKids.Admin
{
    public partial class ViewEventUnusual : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ChackSource();

            }
        }
        protected void ChackSource()
        {

            if (Session["ParentsName"] != null)
            {
                ManagerTeam.Visible = false;
                NameKids.Visible = false;
                ManagerTeam1.Visible = false;
                NameKids1.Visible = false;
                //על פי שם
                Parents Tmp = (Parents)Session["ParentsName"];
                NameKidsByName.DataSource = Parents.viewNameKidsById(Tmp.UserNameParents);
                NameKidsByName.DataBind();
                NameKidsByName.DataTextField = "NameKids";
                NameKidsByName.DataValueField = "IDkids";
                NameKidsByName.DataBind();




                //על פי תאריך
                NameKidsByDate.DataSource = Parents.viewNameKidsById(Tmp.UserNameParents);
                NameKidsByDate.DataBind();
                NameKidsByDate.DataTextField = "NameKids";
                NameKidsByDate.DataValueField = "IDkids";
                NameKidsByDate.DataBind();
                NameKidsByDate.SelectedIndex = 0;
            }

            if (Session["ManagerName"] != null)
            {
                ManagerTeam.Visible = false;
                NameKids.Visible = false;
                ManagerTeam1.Visible = false;
                NameKids1.Visible = false;
                Manager Tmp1 = (Manager)Session["ManagerName"];
                //על פי שם
                NameKidsByName.DataSource = Manager.viewNameKids(Tmp1.IDteamMember);
                NameKidsByName.DataBind();
                NameKidsByName.DataTextField = "NameKids";
                NameKidsByName.DataValueField = "IDkids";
                NameKidsByName.DataBind();
                NameKidsByName.Items.Insert(0, new ListItem("שם הילד לצפייה", "0"));

                //על פי תאריך
                NameKidsByDate.DataSource = Manager.viewNameKids(Tmp1.IDteamMember);
                NameKidsByDate.DataBind();
                NameKidsByDate.DataTextField = "NameKids";
                NameKidsByDate.DataValueField = "IDkids";
                NameKidsByDate.DataBind();
                NameKidsByDate.Items.Insert(0, new ListItem("שם הילד לצפייה", "0"));

            }
            if (Session["AdminsName"] != null)
            {
                HIDEADMIN.Visible = false;
                HIDEADMIN1.Visible = false;
                Admins Tmp2 = (Admins)Session["AdminsName"];
                //לפי שם
                ManagerTeam.DataSource = Admins.viewByAdmin(Tmp2.IDcompany);
                ManagerTeam.DataBind();
                ManagerTeam.DataTextField = "FullName";
                ManagerTeam.DataValueField = "IDteamMember";
                ManagerTeam.DataBind();
                ManagerTeam.Items.Insert(0, new ListItem("שם מנהל הצוות", "0"));
                ManagerTeam.SelectedIndex = 0;

                //רשימת ילדים לפי שם
                NameKids.DataSource = Admins.viewNameKids(int.Parse(ManagerTeam.SelectedValue));
                NameKids.DataBind();
                NameKids.DataTextField = "NameKids";
                NameKids.DataValueField = "IDkids";
                NameKids.DataBind();
                NameKids.Items.Insert(0, new ListItem("שם הילד", "0"));
                NameKids.SelectedIndex = 0;

                //תאריכי
                ManagerTeam1.DataSource = Admins.viewByAdmin(Tmp2.IDcompany);
                ManagerTeam1.DataBind();
                ManagerTeam1.DataTextField = "FullName";
                ManagerTeam1.DataValueField = "IDteamMember";
                ManagerTeam1.DataBind();
                ManagerTeam1.Items.Insert(0, new ListItem("שם מנהל הצוות", "0"));
                ManagerTeam1.SelectedIndex = 0;


                //רשימת ילדים לפי תאריך
                NameKids1.DataSource = Admins.viewNameKids(int.Parse(ManagerTeam.SelectedValue));
                NameKids1.DataBind();
                NameKids1.DataTextField = "NameKids";
                NameKids1.DataValueField = "IDkids";
                NameKids1.DataBind();
                NameKids1.Items.Insert(0, new ListItem("שם הילד", "0"));
                NameKids1.SelectedIndex = 0;

            }
        }
        protected void ManagerTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["AdminsName"] != null)
            {
                //רשימת ילדים
                NameKids.DataSource = Admins.viewNameKidsB(int.Parse(ManagerTeam.SelectedValue));
                NameKids.DataBind();
                NameKids.DataTextField = "NameKids";
                NameKids.DataValueField = "IDkids";
                NameKids.DataBind();
                NameKids.Items.Insert(0, new ListItem("שם הילד", "0"));
            }
        }

        protected void SHOWETable1()
        {
            Mytable1.Text = "<div class='row bgtable'><div class='col-md-12'><div class='table-responsive'><table class='table table-bordered'><thead><tr><th style='width: 20%;'>תאריך</th><th>תיאור</th><th style='width: 20%;' class='HideParents'>עריכה</th></tr></thead></table></div></div></div>";

        }
        protected void SHOWETable2()
        {
            Mytable2.Text = "<div class='row bgtable'><div class='col-md-12'><div class='table-responsive'><table class='table table-bordered'><thead><tr><th style='width: 20%;'>תאריך</th><th>תיאור</th></tr></thead></table></div></div></div>";

        }

        protected void ShowEventUnusualByName_Click(object sender, EventArgs e)
        {
            ShowEventUnusualByName.CssClass = "btn Class2 btn-block";
            if (Session["ParentsName"] != null)
            {
                int IDkids = int.Parse(NameKidsByName.SelectedValue);
                if (Parents.viewDescriptionEvent(IDkids).Count == 0)
                {
                    Mytable1.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                    List<Parents> Lstn = (List<Parents>)Session["DescriptionEvent"];
                    RptName.DataSource = Parents.viewDescriptionEvent(IDkids);
                    RptName.DataBind();
                }
                else
                {

                    SHOWETable1();
                    List<Parents> Lstn = (List<Parents>)Session["DescriptionEvent"];
                    RptName.DataSource = Parents.viewDescriptionEvent(IDkids);
                    RptName.DataBind();
                }

            }
            if (Session["ManagerName"] != null)
            {
                int IDkids = int.Parse(NameKidsByName.SelectedValue);
                if (Manager.viewDescriptionEvent(IDkids).Count == 0)
                {
                    Mytable1.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                    List<Manager> Lstn = (List<Manager>)Session["DescriptionEvent"];
                    RptName.DataSource = Manager.viewDescriptionEvent(IDkids);
                    RptName.DataBind();
                }
                else
                {
                    SHOWETable1();
                    List<Manager> Lstn = (List<Manager>)Session["DescriptionEvent"];
                    RptName.DataSource = Manager.viewDescriptionEvent(IDkids);
                    RptName.DataBind();
                }

            }
            if (Session["AdminsName"] != null)
            {
                if (Manager.viewDescriptionEvent(int.Parse(NameKids.SelectedValue)).Count == 0)
                {
                    Mytable1.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                    RptName.DataBind();
                }
                else
                {
                    SHOWETable1();
                    RptName.DataSource = Manager.viewDescriptionEvent(int.Parse(NameKids.SelectedValue));
                    RptName.DataBind();
                }
            }


        }

        protected void ShowEventUnusualByDate_Click(object sender, EventArgs e)
        {
            //הרצה של סקריפט
            string script = "$(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) });";
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(SomeClass), Guid.NewGuid().ToString(), script, true);


            ShowEventUnusualByDate.CssClass = "btn Class2 btn-block";
            string From = maxYear.Text;
            string To = dropTextColor.Text;

            if (Session["ParentsName"] != null)
            {
                int ID = int.Parse(NameKidsByDate.SelectedValue);
                if (Parents.viewBetweenDate(ID, From, To).Count == 0)
                {
                    Mytable2.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                    List<Parents> Lstn = (List<Parents>)Session["DescriptionEvent"];
                    RptDate.DataSource = Parents.viewBetweenDate(ID, From, To);
                    RptDate.DataBind();
                }
                else
                {
                    SHOWETable2();
                    List<Parents> Lstn = (List<Parents>)Session["DescriptionEvent"];
                    RptDate.DataSource = Parents.viewBetweenDate(ID, From, To);
                    RptDate.DataBind();
                }

            }
            if (Session["ManagerName"] != null)
            {
                int ID = int.Parse(NameKidsByDate.SelectedValue);
                if (Manager.viewBetweenDate(ID, From, To).Count == 0)
                {
                    Mytable2.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                    RptDate.DataBind();
                }
                else
                {
                    SHOWETable2();
                    RptDate.DataSource = Manager.viewBetweenDate(ID, From, To);
                    RptDate.DataBind();
                }

            }
            if (Session["AdminsName"] != null)
            {
                if (Manager.viewBetweenDate(int.Parse(NameKids1.SelectedValue), From, To).Count == 0)
                {
                    Mytable2.Text = "<b class='text-danger'>לא נמצאו נתונים על התלמיד</b>";
                    RptDate.DataBind();
                }
                else
                {
                    SHOWETable2();
                    RptDate.DataSource = Manager.viewBetweenDate(int.Parse(NameKids1.SelectedValue), From, To);
                    RptDate.DataBind();
                }
            }


        }

        protected void ManagerTeam1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //הרצה של סקריפט
            string script = "$(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) });";
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(SomeClass), Guid.NewGuid().ToString(), script, true);

            if (Session["AdminsName"] != null)
            {
                //רשימת ילדים
                NameKids1.DataSource = Admins.viewNameKidsB(int.Parse(ManagerTeam1.SelectedValue));
                NameKids1.DataBind();
                NameKids1.DataTextField = "NameKids";
                NameKids1.DataValueField = "IDkids";
                NameKids1.DataBind();
                NameKids1.Items.Insert(0, new ListItem("שם הילד", "0"));
            }
        }

        private class SomeClass
        {
        }

        protected void NameKidsByDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //הרצה של סקריפט
            string script = "$(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) });";
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(SomeClass), Guid.NewGuid().ToString(), script, true);

        }
    }
}



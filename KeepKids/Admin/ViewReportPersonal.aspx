﻿<%@ Page Title="צפייה בדוח אישי" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ViewReportPersonal.aspx.cs" Inherits="KeepKids.Admin.ViewReportPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/selects/selectivity-full.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/plugins/forms/selectivity/selectivity.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/material-vendors-rtl.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/ui/jquery-ui.min.css">
    <%--    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/material-extended.css">--%>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/custom-rtl.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/material-vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/plugins/ui/jqueryui.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css">

    <link href="css/custme.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css">
    <!-- END: Vendor CSS-->
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/app-contacts.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/datedropper.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/timedropper.min.css">
    <script src="js/jquery-3.4.1.js"></script>
    <link href="css/custme.css" rel="stylesheet" />
    <style>
        .Class2 {
            animation: background-fade 0.3s forwards;
        }


        @keyframes background-fade {
            99.9% {
                background: #ef5757;
            }

            100% {
                background: #28D094;
            }
        }
    </style>
    <style>
        .form-control1 {
            display: block;
            width: 100%;
            height: calc(1.25em + 1.5rem + 2px);
            padding: 0.75rem 1rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.25;
            color: #4E5154;
            background-color: transparent;
            background-clip: padding-box;
            border: 1px solid #BABFC7;
            border-radius: 0.25rem;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            -moz-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <div class="app-content content">
        <div class="content-wrapper Hide1">
            <div class="content-detached content-right ">
                <div class="content-body">
                    <div class="card italya">
                        <div class="card-body ">
                            <div class="form-body pb-1">

                                <h4 class="form-section"><i class="la la-drupal"></i>צפייה בדיווח אישי לפי שם בלבד</h4>
                                <asp:Literal ID="Worng" runat="server" />
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ManagerTeam" runat="server" OnSelectedIndexChanged="ManagerTeam_SelectedIndexChanged" AutoPostBack="true" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control1">
                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <asp:DropDownList ID="NameKids" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="single-input-with-labels">
                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" id="HIDEADMIN" runat="server">
                                        <asp:DropDownList ID="NameKidsByName" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="single-input-with-labels">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:LinkButton runat="server" ID="ShowEventUnusualByName" OnClick="ShowEventUnusualByName_Click" CssClass="btn btn-success btn-block"><i class="la la-check-square-o"></i>חפש</asp:LinkButton>
                                    </div>
                                </div>

                            </div>

                            <asp:Literal ID="Mytable1" runat="server" />
                            <asp:Repeater ID="RptName" runat="server">
                                <ItemTemplate>
                                    <div class="row bgtable">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">

                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 20%;">
                                                                <b class="Mycode">
                                                                    <%#Eval("DateReportPersonal") %>
                                                                </b>
                                                            </td>
                                                            <td>
                                                                <%#Eval("Report") %>
                                                            </td>
                                                            <td style="width: 20%;" class="HideParents">
                                                                <button type="button" onclick="LoadReportPersonal(<%#Eval("IDreportPersonal") %>)" data-toggle="modal" data-target="#inlineForm100" class="btn primary edit ">
                                                                    <i class="la la-pencil"></i>
                                                                </button>
                                                                <div class="modal fade text-left" id="inlineForm100" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3333" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <label class="modal-title text-text-bold-600" id="myModalLabel3333">ערוך פרטים</label>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="row text-center">
                                                                                    <div class="col-3"></div>
                                                                                    <div class="col-6">
                                                                                        <label>תאריך: </label>
                                                                                        <div class="form-group">
                                                                                            <input type="text" id="Main_dropBackgroundColor" class="form-control"><input type="hidden" id="IDreportPersonal">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-3"></div>
                                                                                </div>
                                                                                <div class="row text-center">
                                                                                    <div class="col-12">

                                                                                        <label>תיאור: </label>
                                                                                        <div class="form-group">
                                                                                            <textarea id="Report" name="Report" cols="40" rows="5" class="form-control"></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="בטל">
                                                                                <button type="button" id="custom-icon" onclick="UpdateReportPersonal()" class="btn btn-outline-primary btn-lg Alertu">עדכן</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <button type="button" onclick="Del(<%#Eval("IDreportPersonal")%>)" class="btn danger m-md-0 p-md-0" id="cancel-button">
                                                                    <i class="la la-trash-o"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>


                        </div>
                    </div>
                </div>
            </div>





            <div class="content-detached content-right ">
                <div class="content-body">
                    <div class="card bj">
                        <div class="card-body ">
                            <div class="form-body ">
                                <div class="card-img-top img-fluid bg-cover height-300" style="background: url('../pics/nik.jpg') 50%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>






            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="content-detached content-right ">
                        <div class="content-body">
                            <div class="card italya">
                                <div class="card-body ">
                                    <div class="form-body pb-1">
                                        <h4 class="form-section"><i class="la la-drupal"></i>צפייה בדיווח אישי לפי שם ותאריך</h4>
                                        <asp:Literal ID="Literal1" runat="server" />
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="ManagerTeam1" runat="server" OnSelectedIndexChanged="ManagerTeam1_SelectedIndexChanged" AutoPostBack="true" DataTextField="FullName" DataValueField="IDteamMember" CssClass="form-control round">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="NameKids1" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass="form-control round">
                                                        <asp:ListItem></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <%--  צוות הורים--%>
                                        <div class="row">
                                            <div class="col-md-4" id="HIDEADMIN1" runat="server">
                                                <asp:DropDownList ID="NameKidsByDate" runat="server" DataTextField="NameKids" DataValueField="IDkids" CssClass=" form-control" OnSelectedIndexChanged="NameKidsByDate_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="maxYear" CssClass="form-control bj" runat="server" placeholder="מתאריך" onmouseover="Del()"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3">

                                                <asp:TextBox ID="dropTextColor" CssClass="form-control bj" runat="server" placeholder="עד תאריך"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2">

                                                <asp:LinkButton ID="ShowEventUnusualByDate" OnClick="ShowEventUnusualByDate_Click" CssClass="btn btn-success btn-block" runat="server"><i class="la la-check-square-o"></i>חפש</asp:LinkButton>

                                            </div>

                                        </div>
                                    </div>
                                    <asp:Literal ID="Mytable2" runat="server" />
                                    <asp:Repeater ID="RptDate" runat="server">
                                        <ItemTemplate>
                                            <div class="row bgtable">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">

                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 20%;">
                                                                        <b class="Mycode">
                                                                            <%#Eval("DateReportPersonal") %>
                                                                        </b>
                                                                    </td>
                                                                    <td>
                                                                        <%#Eval("Report") %>
                                                                    </td>
                                                                </tr>
                                                            </tbody>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ShowEventUnusualByDate" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">
    <script src="js/MyJavaScript2.js"></script>
    <script src="/app-assets/vendors/js/tables/jquery.dataTables.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/jquery.raty.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="/app-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js"></script>
    <script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/datedropper.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/timedropper.min.js"></script>
    <script src="/app-assets/js/scripts/pages/app-contacts.js"></script>
    <script src="/app-assets/js/scripts/extensions/date-time-dropper.js"></script>
    <script src="/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
    <script src="/app-assets/vendors/js/forms/select/selectivity-full.min.js"></script>
    <script src="/app-assets/js/scripts/forms/select/form-selectivity.js"></script>
    <%--        <script>function Del() { $(document).ready(function () { $('#animate').dateDropper({ dropWidth: 200 }), $('#init_animation').dateDropper({ dropWidth: 200, init_animation: 'bounce' }), $('#format').dateDropper({ dropWidth: 200, format: 'j l, F, Y' }), $('#lang').dateDropper({ dropWidth: 200, lang: 'ar' }), $('#lock').dateDropper({ dropWidth: 200, lock: 'from' }), $('#Main_maxYear').dateDropper({ dropWidth: 200, Main_maxYear: '2020' }), $('#minYear').dateDropper({ dropWidth: 200, minYear: '2001' }), $('#yearsRange').dateDropper({ dropWidth: 200, yearsRange: '5' }), $('#dropPrimaryColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#F6BB42', dropBorder: '1px solid #F6BB42' }), $('#Main_dropTextColor').dateDropper({ dropWidth: 200, dropPrimaryColor: '#10617E', dropBorder: '1px solid #10617E', Main_dropBackgroundColor: '#23b1e3', Main_dropTextColor: '#FFF' }), $('#Main_dropBackgroundColor').dateDropper({ dropWidth: 200, Main_dropBackgroundColor: '#ACDAEC' }), $('#dropBorder').dateDropper({ dropWidth: 200, dropPrimaryColor: '#2fb594', dropBorder: '1px solid #2dad8d' }), $('#dropBorderRadius').dateDropper({ dropWidth: 200, dropPrimaryColor: '#e8273a', dropBorder: '1px solid #e71e32', dropBorderRadius: '0' }), $('#dropShadow').dateDropper({ dropWidth: 200, dropPrimaryColor: '#fa4420', dropBorder: '1px solid #fa4420', dropBorderRadius: '20', dropShadow: '0 0 10px 0 rgba(250, 68, 32, 0.6)' }), $('#dropWidth').dateDropper({ dropWidth: 250 }), $('#Main_dropTextWeight').dateDropper({ dropWidth: 400, Main_dropTextWeight: 'normal' }) }); }</script>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>



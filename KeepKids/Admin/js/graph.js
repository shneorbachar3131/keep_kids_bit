﻿/*=========================================================================================
    File Name: doughnut.js
    Description: Chartjs simple doughnut chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
   Version: 3.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Doughnut chart
// ------------------------------
$(window).on("load", function () {

    //Get the context of the Chart canvas element we want to select
    var ctx = $("#simple-doughnut-chart");

    // Chart Options
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        responsiveAnimationDuration: 500,
    };
    var admin = $('#Main_Admin').val();
    var team = $('#Main_Team').val();
    var parents = $('#Main_Parent').val();
    var kids = $('#Main_Kids').val();

    // Chart Data
    var chartData = {
        labels: ["מנהלים", "חברי צוות", "הורים", "תלמידים"],
        datasets: [{
            label: "use",
            data: [admin, team, parents, kids],
            //backgroundColor: ['#00A5A8', '#626E82', '#FF7D4D', '#FF4558'],
            backgroundColor: ['#4cff00', '#FF7D4D', '#b200ff', '#FF4558'],
            
        }]
    };

    var config = {
        //type: 'doughnut',
        //type: 'line',
        type: 'bar',
        //type: 'pie',
        //type: 'polarArea',

        // Chart Options
        options: chartOptions,

        data: chartData
    };

    // Create the chart
    var doughnutSimpleChart = new Chart(ctx, config);

});


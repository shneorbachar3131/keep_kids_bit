﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="KeepKids.Admin.loginA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>התחברות</title>
    <!--commit to fun-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard." />
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard" />
    <meta name="author" content="PIXINVENT" />
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png" />
    <link rel="shortcut icon" type="image/x-icon" href="../pics/‏‏MYKIND.png" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet" />

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors-rtl.min.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap-extended.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/colors.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/components.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/custom-rtl.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/loaders/loaders.min.css" />

    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu-modern.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/login-register.css" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css" />
    <link href="css/login.css" rel="stylesheet" />
    <!-- END: Custom CSS-->
</head>
<body class="vertical-layout vertical-menu-modern 1-column  bg-full-screen-image blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

    <form id="form1" runat="server" class="form-horizontal ">
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row mb-1 ">
                </div>
                <div class="content-body ">
                    <section class="flexbox-container ">
                        <div class="col-12 d-flex align-items-center justify-content-center ">
                            <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0 ">
                                <div class="card border-grey border-lighten-3 px-1 py-1 m-0 classLogin">
                                    <div class="card-header border-0 pb-0 classLogin">
                                        <div class="card-title text-center ">
                                            <img src="../pics/‏‏MYKIND.png" alt="img logo" class="something" />
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                            <asp:Panel DefaultButton="btnlogin" runat="server">
                                                <%--<fieldset class="form-group floating-label-form-group">
                                               <asp:TextBox ID="FullName" runat="server"   CssClass="form-control"  placeholder="שם מלא"/>
                                            </fieldset>--%>
                                                <fieldset class="form-group floating-label-form-group">
                                                    <asp:TextBox ID="UserName" runat="server" TextMode="Email" CssClass="form-control form-control-lg input-lg" placeholder="שם משתמש-מייל" /><asp:RequiredFieldValidator ID="TxtUserName" runat="server" ControlToValidate="UserName" ErrorMessage="אנא הזן כתובת מייל" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtUserName" runat="server" ControlToValidate="UserName" ErrorMessage="הזן מייל חוקי" Text="**" Font-Bold="True" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                </fieldset>
                                                <fieldset class="form-group floating-label-form-group mb-1">
                                                    <asp:TextBox ID="UserPassword" runat="server" CssClass="form-control form-control-lg input-lg" TextMode="Password" placeholder="סיסמא" /><asp:RequiredFieldValidator ID="TxtPassword" runat="server" ControlToValidate="UserPassword" ErrorMessage="אנא הזן סיסמא" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </fieldset>
                                                <asp:Literal ID="Worng" runat="server" />


                                            </asp:Panel>

                                        </div>
                                        <div class="card-body">
                                            <asp:LinkButton ID="btnlogin" OnClick="btnlogin_Click" CssClass="btn btn-outline-success btn-block box-shadow-3 " runat="server"><i class="ft-unlock"></i> התחבר</asp:LinkButton>
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                        </div>
                                        <div class="form-group row float-right pr-2">

                                            <div class="row">
                                                <a href="ForgetPassword.aspx" class="btn btn-outline-danger btn-min-width box-shadow-5 mr-1 mb-1">שכחת סיסמא?</a>
                                                <div class="btn-group mr-1 mb-1">
                                                    <button type="button" class="btn btn-outline-info box-shadow-5">הרשמה</button>
                                                    <button type="button" class="btn btn-outline-info box-shadow-5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="RegisterAdmins.aspx">הרשמה מנהלים</a>
                                                        <a class="dropdown-item" href="register.aspx">הרשמה צוות</a>
                                                        <a class="dropdown-item" href="RegisterParents.aspx">הרשמה הורים</a>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <!-- BEGIN: Vendor JS-->
        <script src="/app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="/app-assets/js/core/app-menu.js"></script>
        <script src="/app-assets/js/core/app.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="/app-assets/js/scripts/forms/form-login-register.js"></script>
        <!-- END: Page JS-->

    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeepKids.BLL;

namespace KeepKids
{
    namespace Admin
    {


        public partial class loginA : System.Web.UI.Page
        {
            protected void Page_Load(object sender, EventArgs e)
            {

                if (!IsPostBack)
                {
                    if (Session["ParentsName"] != null)
                    {
                        Parents Tmp1 = (Parents)Session["ParentsName"];
                        UserName.Text = Tmp1.UserNameParents;

                    }
                    if (Session["ManagerName"] != null)
                    {
                        Manager Tmp1 = (Manager)Session["ManagerName"];
                        UserName.Text = Tmp1.UserNameTeamMember;

                    }
                    if (Session["AdminsName"] != null)
                    {
                        Admins Tmp1 = (Admins)Session["AdminsName"];
                        UserName.Text = Tmp1.UserNameAdmin;

                    }
                    Response.Cache.SetNoStore();
                }

            }

            protected void btnlogin_Click(object sender, EventArgs e)
            {
                Manager Tmp = new Manager()
                {
                    UserNameTeamMember = UserName.Text,
                    PasswordTeamMember = UserPassword.Text

                };
                Parents Tmp1 = new Parents()
                {
                    UserNameParents = UserName.Text,
                    PasswordParents = UserPassword.Text

                };
                Admins Tmp2 = new Admins()
                {
                    UserNameAdmin = UserName.Text,
                    PasswordAdmin = UserPassword.Text

                };
                if (Tmp.CheckLogin())
                {

                    Session["ManagerName"] = Tmp;
                    Response.Redirect("Default.aspx");
                }
                if (Tmp1.CheckLogin())
                {

                    Session["ParentsName"] = Tmp1;
                    Response.Redirect("Default.aspx");
                }
                if (Tmp2.CheckLogin())
                {

                    Session["AdminsName"] = Tmp2;
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    Worng.Text = "<b class='text-danger'>*שם או סיסמא אינם תקינים </b>";
                }
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="KeepKids.Admin.registr" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<%--<head runat="server">
    <title>הרשמה</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard." />
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard" />
    <meta name="author" content="PIXINVENT" />
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png" />
    <link rel="shortcut icon" type="image/x-icon" href="../pics/‏‏MYKIND.png" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />


    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/material-vendors-rtl.min.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/material.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/components.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap-extended.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/material-extended.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/material-colors.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/custom-rtl.css" />
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/material-vertical-menu.css" />
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/login-register.css" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css" />
    <!-- END: Custom CSS-->
    <link href="../css/custme.css" rel="stylesheet" />


</head>--%>
    <head runat="server">
    <title>הרשמה</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard." />
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard" />
    <meta name="author" content="PIXINVENT" />
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png" />
    <link rel="shortcut icon" type="image/x-icon" href="../pics/‏‏MYKIND.png" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors-rtl.min.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/icheck.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/icheck/custom.css"/>
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/bootstrap-extended.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/colors.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/components.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/custom-rtl.css"/>
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/menu/menu-types/vertical-menu-modern.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/core/colors/palette-gradient.css"/>
    <link rel="stylesheet" type="text/css" href="/app-assets/css-rtl/pages/login-register.css"/>
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style-rtl.css"/>
    <!-- END: Custom CSS-->
        <link href="css/custme1.css" rel="stylesheet" />
</head>
<body class="vertical-layout vertical-menu-modern 1-column  bg-cyan bg-lighten-2 fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

    <form id="form1" runat="server" class="form-horizontal">
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-header row mb-1">
                </div>
                <div class="content-body">
                    <section class="flexbox-container">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                                <div class="card border-grey border-lighten-3 m-0">
                                    <div class="card-header border-0 pb-0">
                                        <div class="card-title text-center">
                                            <img src="../pics/‏‏MYKIND.png" alt="branding logo" />
                                        </div>
                                        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>הרשמה דרך מייל</span></h6>
                                    </div>
                                    <div class="card-content">
                                        <div class="card-body">
                                        
                                            <asp:Panel DefaultButton="btnRegister" runat="server">
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtFirstName" runat="server" ControlToValidate="FirstName" ErrorMessage="אנא הזן שם פרטי" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="FirstName" runat="server" CssClass="form-control" placeholder="שם פרטי" />
                                                            <div class="form-control-position">
                                                                <i class="ft-user"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtLastName" runat="server" ControlToValidate="LastName" ErrorMessage="אנא הזן שם משפחה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="LastName" runat="server" CssClass="form-control" placeholder="שם משפחה" />
                                                            <div class="form-control-position">
                                                                <i class="ft-user"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtPhone" runat="server" ControlToValidate="Phone" ErrorMessage="אנא הזן מספר נייד" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtPhone" runat="server" ControlToValidate="Phone" ErrorMessage="הזן מספר נייד תקין בעל 10 ספרות" ValidationExpression="[0-9]{10}" Text="**" Font-Bold="True" ForeColor="Red"></asp:RegularExpressionValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="Phone" runat="server" CssClass="form-control" placeholder="מספר נייד" />
                                                            <div class="form-control-position">
                                                                <i class="la la-phone"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtEmail" runat="server" ControlToValidate="Email" ErrorMessage="אנא הזן כתובת מייל" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtEmail" runat="server" ControlToValidate="Email" ErrorMessage="הזן מייל חוקי" Text="**" Font-Bold="True" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="Email" runat="server" TextMode="Email" CssClass="form-control" placeholder="אימייל" />
                                                            <div class="form-control-position">
                                                                <i class="ft-mail"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtPassword" runat="server" ControlToValidate="Password" ErrorMessage="אנא הזן סיסמא" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtPassword" runat="server" ControlToValidate="Password" ErrorMessage="לפחות 8 תווים אות אחת קטנה, אות אחת גדולה, ובאנגלית" Text="**" Font-Bold="True" ForeColor="Red" ValidationExpression="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$"></asp:RegularExpressionValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control" placeholder="סיסמא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-key"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:CompareValidator ID="COMTxtRealPassword" runat="server" ErrorMessage="הסיסמא אינה תואמת" Font-Bold="True" Text="***" ControlToCompare="RealPassword" ControlToValidate="Password" ForeColor="Red"></asp:CompareValidator>
                                                        <fieldset class="form-group position-relative has-icon-left">
                                                            <asp:TextBox ID="RealPassword" runat="server" TextMode="Password" CssClass="form-control" placeholder="אימות סיסמא" />
                                                            <div class="form-control-position">
                                                                <i class="la la-key"></i>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <%--<fieldset class="form-group position-relative has-icon-left">
                                                <asp:TextBox ID="a" runat="server" TextMode="Password" CssClass="form-control" placeholder="סיסמא" />
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                                <div class="help-block font-small-3"></div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <asp:TextBox ID="as" runat="server" TextMode="Password" CssClass="form-control" placeholder="אימות סיסמא" />
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                                <div class="help-block font-small-3"></div>
                                            </fieldset>--%>

                                                <div class="row">
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="SelectAreYou" runat="server" ControlToValidate="AreYou" ErrorMessage="אנא בחר סוג עבודה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="-1" ></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="AreYou" runat="server" class="form-control form-group position-relative has-icon-left">
                                                            <asp:ListItem Value="-1" Selected="True">האם את גננת?</asp:ListItem>
                                                            <asp:ListItem Value="1">כן</asp:ListItem>
                                                            <asp:ListItem Value="2">לא</asp:ListItem>
                                                            <asp:ListItem Value="3">אחר</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <%-- <div class="col-12 col-sm-6 col-md-6">
                                                    <asp:DropDownList ID="company" runat="server" class="form-control form-group position-relative has-icon-left">
                                                        <asp:ListItem Value="-1" Selected="True">שם החברה בו הינך עובדת</asp:ListItem>
                                                        <asp:ListItem Value="1000">אגמים</asp:ListItem>
                                                        <asp:ListItem Value="1006">מרבדי החיים</asp:ListItem>
                                                        <asp:ListItem Value="1012">רומא ישראל</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>
                                                    <div class="col-12 col-sm-6 col-md-6">
                                                        <asp:RequiredFieldValidator ID="TxtCompany" runat="server" ControlToValidate="nameID" ErrorMessage="אנא שייך עצמך לחברה" Font-Bold="True" Text="*" ForeColor="Red" InitialValue="0"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="nameID" runat="server" DataTextField="NameCompany" DataValueField="IDcompany" class="form-control form-group position-relative has-icon-left">
                                                            <asp:ListItem></asp:ListItem>  
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                <div class="col-12 col-sm-12 col-md-12 ">
                                                    <div id="containerCH">
                                                    <b>העלאת תמונה</b>

                                                    <asp:FileUpload runat="server" ID="PicUpload" CssClass="btn btn-sm xor col-8 col-sm-12 col-md-12 "/><asp:RequiredFieldValidator ID="TxtPicUpload" runat="server" ControlToValidate="PicUpload" ErrorMessage="בחר תמונה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                              
                                                <style>
                                                    .xor {
                                                    background-color: #0026ff;
                                                    border: none;
                                                    color: #FFFFFF;
                                                    padding: 15px 32px;
                                                    text-align: center;
                                                    -webkit-transition-duration: 0.4s;
                                                    transition-duration: 0.4s;
                                                    text-decoration: none;
                                                    font-size: 14px;
                                                    cursor: pointer;
                                                    display:grid;
                                                    width:250px;
                                                    margin:auto;
                                                    border: 2px dotted ;
                                                    
                                                    }
                                                    #containerCH{
                                                      text-align:center;
                                                    }
                                                    
                                                </style>
                                                <script>
                                                    var input = document.querySelector("#PicUpload");

                                                    input.addEventListener("change", preview);
                                                    function preview() {
                                                        var fileObject = this.files[0];
                                                        var fileReader = new FileReader();
                                                        fileReader.readAsDataURL(fileObject);

                                                    }
                                                </script>
                                            </div>
                                               <asp:Literal ID="Worng" runat="server" />
                                                <%--<div class="row mb-1">
                                                <div class="col-4 col-sm-3 col-md-3">
                                                    <fieldset>
                                                        <input type="checkbox" id="remember-me" class="chk-remember">
                                                        <label for="remember-me"> I Agree</label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-8 col-sm-9 col-md-9">
                                                    <p class="font-small-3">By clicking Register, you agree to the <a href="#" data-toggle="modal" data-target="#t_and_c_m">Terms and Conditions</a> set out by this site, including our Cookie Use.</p>
                                                </div>
                                            </div>--%>
                                                <div class="card-body">
                                                    <asp:LinkButton ID="btnRegister" OnClick="btnRegister_Click" CssClass="btn btn-outline-info btn-block" runat="server"><i class="ft-user"></i>הרשמה</asp:LinkButton>
                                                    <a href="login.aspx" class="btn btn-outline-danger btn-block"><i class="ft-unlock"></i>התחברות</a>
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                                </div>
                                            </asp:Panel>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>

        <script src="/app-assets/vendors/js/vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->
        <!-- BEGIN: Page Vendor JS-->
        <script src="/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="/app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
        <!-- END: Page Vendor JS-->
        <!-- BEGIN: Theme JS-->
        <script src="/app-assets/js/core/app-menu.js"></script>
        <script src="/app-assets/js/core/app.js"></script>
        <!-- END: Theme JS-->
        <!-- BEGIN: Page JS-->
        <script src="/app-assets/js/scripts/forms/form-login-register.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery-3.4.1.js"></script>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using KeepKids.DAL;


namespace KeepKids
{
    namespace BLL
    {
        public class Admins
        {

            public int IDadmin { get; set; }
            public int IDcompany { get; set; }
            public string NameAdmin { get; set; }
            public string LastNameAdmin { get; set; }
            public string PicAdmin { get; set; }
            public string AddressAdmin { get; set; }
            public string PhoneAdmin { get; set; }
            public string UserNameAdmin { set; get; }
            public string PasswordAdmin { get; set; }
            public string PasswordAdminRecovery { get; set; }
            //טבלת חברות
            public string NameCompany { get; set; }
            public string AddressCompany { get; set; }
            public string MailCompany { get; set; }
            public int CountClass { get; set; }
            public int Price { get; set; }

            //טבלת ילדים
            public int IDkids { get; set; }
            public string NameKids { get; set; }
            public string NameFather { get; set; }
            public string NameMother { get; set; }
            public string PicKids { get; set; }
            public string PhoneFather { get; set; }
            public string PhoneMother { get; set; }
            public string UserNameTeamMember { get; set; }
            public string UserNameParents { get; set; }

            //טבלת צוות
            public int IDteamMember { get; set; }
            public string FullName { get; set; }
            public string PicManager { get; set; }
            public string nameTeamMember { get; set; }
            //טבלת כיתות
            public int IDclass { get; set; }
            public string NameClass { get; set; }
            //טבלת אירועים חינוכיים
            public string DateEducationAct { get; set; }
            public string Action { get; set; }
            //טבלת הודעות
            public int IDmessage { get; set; }
            public int IDsender { get; set; }
            public string Picsender { get; set; }
            public int StatusSender { get; set; }
            public int IDgetNotice { get; set; }
            public int StatusGetNotice { get; set; }
            public string SubjectMessage { get; set; }
            public string ContentMessage { get; set; }
            public string DateSendMessage { get; set; }
            public string NaneSender { get; set; }
            public string PicGetNotice { get; set; }
            public string NameGetNotice { get; set; }
            public string NameFile { get; set; }
            //טבלת דיווח נוכחות
            public string Status { get; set; }
            public string Remarks { get; set; }
            public int IDpresenceReport { set; get; }


            public Admins() { }


            public static List<Admins> GetAll()
            {
                DataTable Dt = AdminsDAL.GetAll();
                List<Admins> Mess = new List<Admins>();
                Admins list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Admins();
                    list.IDadmin = (int)Dt.Rows[i]["IDadmin"];
                    list.IDcompany = (int)Dt.Rows[i]["IDcompany"];
                    list.NameAdmin = (string)Dt.Rows[i]["NameAdmin"];
                    list.LastNameAdmin = (string)Dt.Rows[i]["LastNameAdmin"];
                    list.PicAdmin = (string)Dt.Rows[i]["PicAdmin"];
                    list.AddressAdmin = (string)Dt.Rows[i]["AddressAdmin"];
                    list.PhoneAdmin = (string)Dt.Rows[i]["PhoneAdmin"];
                    list.UserNameAdmin = (string)Dt.Rows[i]["UserNameAdmin"];
                    list.PasswordAdmin = (string)Dt.Rows[i]["PasswordAdmin"];

                    Mess.Add(list);

                }
                return Mess;

            }



            public static List<Admins> GetById(int ID)
            {
                DataTable Dt = AdminsDAL.GetById(ID);
                List<Admins> Mess = new List<Admins>();
                Admins list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Admins();
                    list.IDadmin = (int)Dt.Rows[i]["IDadmin"];
                    list.IDcompany = (int)Dt.Rows[i]["IDcompany"];
                    list.NameAdmin = (string)Dt.Rows[i]["NameAdmin"];
                    list.LastNameAdmin = (string)Dt.Rows[i]["LastNameAdmin"];
                    list.PicAdmin = (string)Dt.Rows[i]["PicAdmin"];
                    list.AddressAdmin = (string)Dt.Rows[i]["AddressAdmin"];
                    list.PhoneAdmin = (string)Dt.Rows[i]["PhoneAdmin"];
                    list.UserNameAdmin = (string)Dt.Rows[i]["UserNameAdmin"];
                    list.PasswordAdmin = (string)Dt.Rows[i]["PasswordAdmin"];

                    Mess.Add(list);

                }
                return Mess;

            }




            public static List<Admins> Chats(int ID)
            {
                DataTable Dt = AdminsDAL.Chats(ID);
                List<Admins> Lst = new List<Admins>();
                Admins name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Admins();
                    name.PicKids = (string)Dt.Rows[i]["PicKids"];
                    name.NameFather = (string)Dt.Rows[i]["NameFather"];
                    name.NameMother = (string)Dt.Rows[i]["NameMother"];
                    name.IDkids = (int)Dt.Rows[i]["IDkidsONadmin"];
                    Lst.Add(name);

                }
                return Lst;
            }

            public bool CheckRegister()
            {
                return AdminsDAL.CheckRegister(this);
            }


            public bool CheckUpdate()
            {
                return AdminsDAL.CheckUpdate(this);
            }


            public bool CheckLogin()
            {
                return AdminsDAL.CheckLogin(this);
            }


            public bool CheckMail()
            {
                return AdminsDAL.CheckMail(this);
            }



            public bool InsertPass()
            {
                return AdminsDAL.InsertPass(this);
            }

            public static List<Admins> viewNameKidsByIDcompany(int IDcompany)
            {
                DataTable Dt = AdminsDAL.viewNameKidsByIDcompany(IDcompany);
                List<Admins> Lst = new List<Admins>();
                Admins name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Admins();
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    name.NameKids = (string)Dt.Rows[i]["NameKids"] + " " + (string)Dt.Rows[i]["LastNameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }




            public static List<Admins> viewByAdmin(int IDadmin)
            {
                DataTable Dt = AdminsDAL.viewByAdmin(IDadmin);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    ID.FullName = (string)Dt.Rows[i]["FullName"];
                    Lst.Add(ID);

                }
                return Lst;
            }



            public static List<Admins> viewNameKids(int copany)
            {
                DataTable Dt = AdminsDAL.viewNameKids(copany);
                List<Admins> Lst = new List<Admins>();
                Admins name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Admins();
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    name.NameKids = (string)Dt.Rows[i]["LastNameKids"] + " " + (string)Dt.Rows[i]["NameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }



            public static List<Admins> viewNameKidsB(int idtm)
            {
                DataTable Dt = AdminsDAL.viewNameKidsB(idtm);
                List<Admins> Lst = new List<Admins>();
                Admins name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Admins();
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    name.NameKids = (string)Dt.Rows[i]["NameKids"] + " " + (string)Dt.Rows[i]["LastNameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }


            public static List<Admins> viewNameTeamMember(int idcopany)
            {
                DataTable Dt = AdminsDAL.viewNameTeamMember(idcopany);
                List<Admins> Lst = new List<Admins>();
                Admins name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Admins();
                    name.FullName = (string)Dt.Rows[i]["FullName"];
                    name.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    Lst.Add(name);

                }
                return Lst;
            }




            public static List<Admins> ShowClass(int ID)
            {
                DataTable Dt = AdminsDAL.ShowClass(ID);
                List<Admins> Lst = new List<Admins>();
                Admins Class;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Class = new Admins();
                    Class.NameClass = (string)Dt.Rows[i]["NameClass"];
                    Class.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(Class);

                }
                return Lst;
            }


            public static List<Admins> ListKidsOPTION(int IDclass, int IDteamMember)
            {
                DataTable Dt = AdminsDAL.ListKidsOPTION(IDclass, IDteamMember);
                List<Admins> Mess = new List<Admins>();
                Admins list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Admins();
                    list.IDkids = (int)Dt.Rows[i]["IDkids"];
                    list.NameKids = (string)Dt.Rows[i]["FullName"];
                    list.NameFather = (string)Dt.Rows[i]["NameFather"];
                    list.PhoneFather = (string)Dt.Rows[i]["PhoneFather"];
                    list.NameMother = (string)Dt.Rows[i]["NameMother"];
                    list.PhoneMother = (string)Dt.Rows[i]["PhoneMother"];

                    Mess.Add(list);

                }
                return Mess;
            }

            public static string TakePicture(int idtm)
            {
                return AdminsDAL.TakePicture(idtm);
            }

            public static string ShowPhone(int idtm)
            {
                return AdminsDAL.ShowPhone(idtm);
            }

            public static List<Admins> NewKinderGarden(int IDcompany)
            {
                DataTable Dt = AdminsDAL.NewKinderGarden(IDcompany);
                List<Admins> Lst = new List<Admins>();
                Admins name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Admins();
                    name.FullName = (string)Dt.Rows[i]["FullName"];
                    name.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    Lst.Add(name);

                }
                return Lst;
            }




            public static List<Admins> IDkindergarten(int comany)
            {
                DataTable Dt = AdminsDAL.IDkindergarten(comany);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    ID.FullName = (string)Dt.Rows[i]["FullName"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public static bool InserstEducationAct(int idclass, string date, string action)
            {
                return AdminsDAL.InserstEducationAct(idclass, date, action);
            }



            public static List<Admins> viewEducationActByDate(int idclass, string from, string to)
            {
                DataTable Dt = AdminsDAL.viewEducationActByDate(idclass, from, to);
                List<Admins> Lst = new List<Admins>();
                Admins Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Admins();
                    Name.DateEducationAct = Dt.Rows[i]["DateEducationAct"].ToString();
                    Name.Action = (string)Dt.Rows[i]["Action"];
                    Lst.Add(Name);

                }
                return Lst;
            }


            public static List<Admins> ParentsByKinderGarden(int idkindergarten)
            {
                DataTable Dt = AdminsDAL.ParentsByKinderGarden(idkindergarten);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDkids = (int)Dt.Rows[i]["IDkids"];
                    Lst.Add(ID);

                }
                return Lst;
            }








            public static List<Admins> ShowMeMYNewMessage(int IDget, int StatusGet)
            {
                DataTable Dt = AdminsDAL.ShowMeMYNewMessage(IDget, StatusGet);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }



            public static string OnlyPicManager(int idTm)
            {
                return AdminsDAL.OnlyPicManager(idTm);
            }


            public static string OnlyNameManager(int Idkids)
            {
                return AdminsDAL.OnlyNameManager(Idkids);

            }
            public static string OnlyPicKids(int Idkids)
            {
                return AdminsDAL.OnlyPicKids(Idkids);
            }


            public static string OnlyNameKids(int Idkids)
            {
                return AdminsDAL.OnlyNameKids(Idkids);
            }


            public static void UpdateMarkTORead(int idmassage)
            {
                AdminsDAL.UpdateMarkTORead(idmassage);
            }


            public static List<Admins> MessageReaded(int IDget, int StatusGet)
            {
                DataTable Dt = AdminsDAL.MessageReaded(IDget, StatusGet);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }



            public static void UpdateToTheTrash(int idmassage)
            {
                AdminsDAL.UpdateToTheTrash(idmassage);
            }


            public static void UpdateToTheTrash1(int idmassage)
            {
                AdminsDAL.UpdateToTheTrash1(idmassage);
            }

            public static void UpdateToTheTrash2(int idmassage)
            {
                AdminsDAL.UpdateToTheTrash2(idmassage);
            }




            public static List<Admins> ShowMeMYTrashMessage(int IDget, int StatusGet)
            {
                DataTable Dt = AdminsDAL.ShowMeMYTrashMessage(IDget, StatusGet);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }




            public static List<Admins> ShowMeMYTrashMessage1(int IDSENDER, int StatusSender)
            {
                DataTable Dt = AdminsDAL.ShowMeMYTrashMessage1(IDSENDER, StatusSender);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public static List<Admins> MyItems(int Myid)
            {
                DataTable Dt = AdminsDAL.MyItems(Myid);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static void TrashForever(int idmassage)
            {
                AdminsDAL.TrashForever(idmassage);
            }


            public static List<Admins> viewManagerByAdmin(int IDadmin)
            {
                DataTable Dt = AdminsDAL.viewManagerByAdmin(IDadmin);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.UserNameTeamMember = (string)Dt.Rows[i]["UserNameTeamMember"];
                    ID.FullName = (string)Dt.Rows[i]["FullName"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public static List<Admins> ParentIndividual(int idadmin)
            {
                DataTable Dt = AdminsDAL.ParentIndividual(idadmin);
                List<Admins> Lst = new List<Admins>();
                Admins name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Admins();
                    name.UserNameParents = (string)Dt.Rows[i]["UserNameParents"];
                    name.NameKids = (string)Dt.Rows[i]["LastNameKids"] + " " + (string)Dt.Rows[i]["NameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }

            public static List<Admins> UserParents(int Class)
            {
                DataTable Dt = AdminsDAL.UserParents(Class);
                List<Admins> Lst = new List<Admins>();
                Admins ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Admins();
                    ID.UserNameParents = (string)Dt.Rows[i]["UserNameParents"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public bool InsertCompany()
            {
                return AdminsDAL.InsertCompany(this);
            }

            public static int[] GetCountClassByCompany(int IDcom)
            {
                return AdminsDAL.GetCountClassByCompany(IDcom);
            }

            public static int HowAdmin()
            {
                Database Db = new Database();
                string sql = "select count(*) as 'total' from Admin_2";
                DataTable Dt = Db.Execute(sql);
                int tot = (int)Dt.Rows[0]["total"];
                return tot;
            }

            public bool CheckClass(string nameclas, int idkindergarten)
            {
                return AdminsDAL.CheckClass(nameclas, idkindergarten);
            }

            public static List<Admins> GetPresenceReport(int ID)
            {
                DataTable Dt = AdminsDAL.GetPresenceReport(ID);
                List<Admins> Mess = new List<Admins>();
                Admins list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Admins();
                    list.IDpresenceReport = (int)Dt.Rows[i]["IDpresenceReport"];
                    list.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    list.Status = (string)Dt.Rows[i]["Status"];
                    list.Remarks = (string)Dt.Rows[i]["Remarks"];

                    Mess.Add(list);

                }
                return Mess;

            }


            public bool CheckUpdateSpecific()
            {
                return AdminsDAL.CheckUpdateSpecific(this);
            }
        }
    }

}


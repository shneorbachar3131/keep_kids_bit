﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using KeepKids.DAL;

namespace KeepKids
{
    namespace BLL
    {
        public class Manager
        {         //טבלת צוות

            public int IDteamMember { get; set; }
            public int IDcompany { get; set; }
            public string IDjob { get; set; }
            public string FullName { get; set; }
            public string nameTeamMember { get; set; }
            public string FamilyTeamMember { set; get; }
            public string PhoneTeamMember { get; set; }
            public string UserNameTeamMember { get; set; }
            public string PasswordTeamMember { get; set; }
            public string PasswordTMrecovery { get; set; }
            public string PicManager { get; set; }

            //טבלת חברות
            public string NameCompany { get; set; }
            //טבלת אירועים חריג
            public string DateEventUnusual { get; set; }
            public string DescriptionEvent { get; set; }
            public int IDeventUnusual { get; set; }
            //טבלת הורים
            public int IDkids { get; set; }
            public string NameKids { get; set; }
            public string PicKids { get; set; }
            public string NameFather { get; set; }
            public string NameMother { get; set; }
            public string PhoneFather { get; set; }
            public string PhoneMother { get; set; }
            public string UserNameParents { get; set; }
            //טבלת מנהלים
            public int IDadmin { get; set; }
            public string NameAdmin { get; set; }
            public string UserNameAdmin { get; set; }


            //טבלת ילדים 
            public string Report { get; set; }
            public string DateReportPersonal { get; set; }
            //טבלת גנים
            public string NameKindergarten { get; set; }
            //טבלת אירועים חינוכיים
            public string DateEducationAct { get; set; }
            public string Action { get; set; }
            public int IDeducationAct { get; set; }
            //טבלת הודעות
            public int IDmessage { get; set; }
            public int IDsender { get; set; }
            public string Picsender { get; set; }
            public int StatusSender { get; set; }
            public int IDgetNotice { get; set; }
            public int StatusGetNotice { get; set; }
            public string SubjectMessage { get; set; }
            public string ContentMessage { get; set; }
            public string NameFile { get; set; }
            public string DateSendMessage { get; set; }
            public string NaneSender { get; set; }
            public string PicGetNotice { get; set; }
            public string NameGetNotice { get; set; }
            //טבלת נוכחות
            public int IDpresenceReport { get; set; }
            public string DatePresenceReport { get; set; }
            public string Status { get; set; }
            public string Remarks { get; set; }
            //טבלת דוח אישי
            public int IDreportPersonal { get; set; }

            //קונסטרקטור כללי
            public Manager() { }
            //קונטרקטורים ייחודים
            public Manager(string UserNameTeamMember, string PasswordTeamMember)
            {

                this.UserNameTeamMember = UserNameTeamMember;
                this.PasswordTeamMember = PasswordTeamMember;

            }
            public Manager(string nameTeamMember, string UserNameTeamMember, string PasswordTeamMember, string PicManager)
            {
                this.nameTeamMember = nameTeamMember;
                this.UserNameTeamMember = UserNameTeamMember;
                this.PasswordTeamMember = PasswordTeamMember;
                this.PicManager = PicManager;
            }


            public static List<Manager> GetAll()
            {
                DataTable Dt = ManagerDAL.GetAll();
                List<Manager> Mess = new List<Manager>();
                Manager list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Manager();
                    list.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    list.IDcompany = (int)Dt.Rows[i]["IDcompany"];
                    list.IDjob = Dt.Rows[i]["IDjob"].ToString();
                    list.FullName = (string)Dt.Rows[i]["FullName"];
                    list.nameTeamMember = (string)Dt.Rows[i]["nameTeamMember"];
                    list.FamilyTeamMember = (string)Dt.Rows[i]["FamilyTeamMember"];
                    list.PhoneTeamMember = (string)Dt.Rows[i]["PhoneTeamMember"];
                    list.UserNameTeamMember = (string)Dt.Rows[i]["UserNameTeamMember"];
                    list.PasswordTeamMember = (string)Dt.Rows[i]["PasswordTeamMember"];
                    list.PicManager = (string)Dt.Rows[i]["PicManager"];

                    Mess.Add(list);

                }
                return Mess;

            }


            public static List<Manager> GetById(int ID)
            {
                DataTable Dt = ManagerDAL.GetById(ID);
                List<Manager> Mess = new List<Manager>();
                Manager list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Manager();
                    list.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    list.IDcompany = (int)Dt.Rows[i]["IDcompany"];
                    list.IDjob = Dt.Rows[i]["IDjob"].ToString();
                    list.FullName = (string)Dt.Rows[i]["FullName"];
                    list.nameTeamMember = (string)Dt.Rows[i]["nameTeamMember"];
                    list.FamilyTeamMember = (string)Dt.Rows[i]["FamilyTeamMember"];
                    list.PhoneTeamMember = (string)Dt.Rows[i]["PhoneTeamMember"];
                    list.UserNameTeamMember = (string)Dt.Rows[i]["UserNameTeamMember"];
                    list.PasswordTeamMember = (string)Dt.Rows[i]["PasswordTeamMember"];
                    list.PicManager = (string)Dt.Rows[i]["PicManager"];

                    Mess.Add(list);

                }
                return Mess;

            }



            public static string NameKinderGarden(int Tmp)
            {
                return ManagerDAL.NameKinderGarden(Tmp);
            }


            public static string NameKinderGarden2(int Tmp)
            {
                return ManagerDAL.NameKinderGarden2(Tmp);
            }


            public static List<Manager> ListKids(int ID, int Class)
            {
                DataTable Dt = ManagerDAL.ListKids(ID, Class);
                List<Manager> Mess = new List<Manager>();
                Manager list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Manager();

                    list.IDkids = (int)Dt.Rows[i]["IDkids"];
                    list.NameKids = (string)Dt.Rows[i]["FullName"];
                    list.NameFather = (string)Dt.Rows[i]["NameFather"];
                    list.PhoneFather = (string)Dt.Rows[i]["PhoneFather"];
                    list.NameMother = (string)Dt.Rows[i]["NameMother"];
                    list.PhoneMother = (string)Dt.Rows[i]["PhoneMother"];

                    Mess.Add(list);

                }
                return Mess;
            }


            public static string ShowPic(int ID)
            {
                Database Db = new Database();
                {
                    Manager pic = new Manager();
                    string sql = "select PicKids from Kids_5  where IDkids = '" + ID + "'";
                    DataTable Dt = Db.Execute(sql);
                    if (Dt.Rows.Count > 0)
                    {
                        pic.PicKids = (string)Dt.Rows[0]["PicKids"];
                    }

                    return pic.PicKids;
                }

            }

            public static List<Manager> ShowMessage(int ID)
            {
                DataTable Dt = ManagerDAL.ShowMessage(ID);
                List<Manager> Mess = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    Mess.Add(Name);

                }
                return Mess;
            }






            public bool CheckUpdate()
            {
                return ManagerDAL.CheckUpdate(this);
            }

            public static bool Delete(int ID)
            {
                return ManagerDAL.Delete(ID);
            }




            public bool CheckLogin()
            {
                return ManagerDAL.CheckLogin(this);
            }


            public static List<Manager> view()
            {
                DataTable Dt = ManagerDAL.view();
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDcompany = (int)Dt.Rows[i]["IDcompany"];
                    ID.NameCompany = (string)Dt.Rows[i]["NameCompany"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static List<Manager> ViewMail(int Tmp)
            {
                DataTable Dt = ManagerDAL.ViewMail(Tmp);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.UserNameTeamMember = (string)Dt.Rows[i]["UserNameTeamMember"];
                    ID.PhoneTeamMember = (string)Dt.Rows[i]["PhoneTeamMember"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static List<Manager> viewAll()
            {
                DataTable Dt = ManagerDAL.viewAll();
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    ID.FullName = (string)Dt.Rows[i]["FullName"];
                    Lst.Add(ID);

                }
                return Lst;
            }



            public static List<Manager> viewOne(int idtm)
            {
                DataTable Dt = ManagerDAL.viewOne(idtm);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    ID.FullName = (string)Dt.Rows[i]["FullName"];
                    Lst.Add(ID);

                }
                return Lst;
            }



            public static List<Manager> viewByid(int IDtm)
            {
                DataTable Dt = ManagerDAL.viewByid(IDtm);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    ID.FullName = (string)Dt.Rows[i]["FullName"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static List<Manager> viewNameKids(int ID)
            {
                DataTable Dt = ManagerDAL.viewNameKids(ID);
                List<Manager> Lst = new List<Manager>();
                Manager name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Manager();
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    name.NameKids = (string)Dt.Rows[i]["NameKids"] + " " + (string)Dt.Rows[i]["LastNameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }




            public static List<Manager> Chats(int ID)
            {
                DataTable Dt = ManagerDAL.Chats(ID);
                List<Manager> Lst = new List<Manager>();
                Manager name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Manager();
                    name.PicKids = (string)Dt.Rows[i]["PicKids"];
                    name.NameFather = (string)Dt.Rows[i]["NameFather"];
                    name.NameMother = (string)Dt.Rows[i]["NameMother"];
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    Lst.Add(name);

                }
                return Lst;
            }



            public bool CheckRegister()
            {
                return ManagerDAL.CheckRegister(this);
            }

            public static List<Manager> viewDescriptionEvent(int ID)
            {
                DataTable Dt = ManagerDAL.viewDescriptionEvent(ID);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDeventUnusual = (int)Dt.Rows[i]["IDeventUnusual"];
                    Name.DescriptionEvent = (string)Dt.Rows[i]["DescriptionEvent"];
                    Name.DateEventUnusual = Dt.Rows[i]["DateEventUnusual1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }


            public static List<Manager> viewReportPersonal(int ID)
            {
                DataTable Dt = ManagerDAL.viewReportPersonal(ID);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDreportPersonal = (int)Dt.Rows[i]["IDreportPersonal"];
                    Name.Report = (string)Dt.Rows[i]["Report"];
                    Name.DateReportPersonal = Dt.Rows[i]["DateReportPersonal1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }


            public static List<Manager> viewBetweenDate(int ID, string From, string To)
            {
                DataTable Dt = ParentsDAL.viewBetweenDate(ID, From, To);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.DescriptionEvent = (string)Dt.Rows[i]["DescriptionEvent"];
                    Name.DateEventUnusual = Dt.Rows[i]["DateEventUnusual1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }



            public static List<Manager> viewBetweenDateReportPersonal(int ID, string From, string To)
            {
                DataTable Dt = ManagerDAL.viewBetweenDateReportPersonal(ID, From, To);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.Report = (string)Dt.Rows[i]["Report"];
                    Name.DateReportPersonal = Dt.Rows[i]["DateReportPersonal1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }



            public bool CheckMail()
            {
                return ManagerDAL.CheckMail(this);
            }



            public bool InsertPass()
            {
                return ManagerDAL.InsertPass(this);
            }

            public static bool AddClass(int idkg, string nc, string ga)
            {
                return ManagerDAL.AddClass(idkg, nc, ga);
            }


            public static int IDkinderGarden(int Tmp)
            {
                return ManagerDAL.IDkinderGarden(Tmp);
            }


            public static bool InserstEducationAct(int idclass, string date, string action)
            {
                return ManagerDAL.InserstEducationAct(idclass, date, action);
            }

            public static List<Manager> viewEducationActByMonth(int idclass)
            {
                DataTable Dt = ManagerDAL.viewEducationActByMonth(idclass);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDeducationAct = (int)Dt.Rows[i]["IDeducationAct"];
                    Name.DateEducationAct = Dt.Rows[i]["DateEducationAct"].ToString();
                    Name.Action = (string)Dt.Rows[i]["Action"];
                    Lst.Add(Name);

                }
                return Lst;
            }



            public static string OnlyPicKids(int Idkids)
            {
                return ManagerDAL.OnlyPicKids(Idkids);
            }


            public static string OnlyNameKids(int Idkids)
            {
                return ManagerDAL.OnlyNameKids(Idkids);
            }

            public static List<Manager> ShowMeMYNewMessage(int IDget, int StatusGet)
            {
                DataTable Dt = ManagerDAL.ShowMeMYNewMessage(IDget, StatusGet);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static List<Manager> MessageReaded(int IDget, int StatusGet)
            {
                DataTable Dt = ManagerDAL.MessageReaded(IDget, StatusGet);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public static List<Manager> MyItems(int Myid)
            {
                DataTable Dt = ManagerDAL.MyItems(Myid);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public static List<Manager> ShowMeMYTrashMessage(int IDget, int StatusGet)
            {
                DataTable Dt = ManagerDAL.ShowMeMYTrashMessage(IDget, StatusGet);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }




            public static List<Manager> ShowMeMYTrashMessage1(int IDSENDER, int StatusSender)
            {
                DataTable Dt = ManagerDAL.ShowMeMYTrashMessage1(IDSENDER, StatusSender);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Manager();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }




            public static void UpdateToTheTrash(int idmassage)
            {
                ManagerDAL.UpdateToTheTrash(idmassage);
            }


            public static void UpdateToTheTrash1(int idmassage)
            {
                ManagerDAL.UpdateToTheTrash1(idmassage);
            }

            public static void UpdateToTheTrash2(int idmassage)
            {
                ManagerDAL.UpdateToTheTrash2(idmassage);
            }

            public static void UpdateMarkTORead(int idmassage)
            {
                ManagerDAL.UpdateMarkTORead(idmassage);
            }



            public static List<Manager> MyAdmins(int idtm)
            {
                DataTable Dt = ManagerDAL.MyAdmins(idtm);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    ID = new Manager();
                    ID.IDadmin = (int)Dt.Rows[i]["IDadmin"];
                    ID.NameAdmin = (string)Dt.Rows[i]["NameAdmin"];
                    Lst.Add(ID);
                }
                return Lst;
            }

            public static string OnlypicAdmins(int IDTM)
            {
                return ManagerDAL.OnlypicAdmins(IDTM);
            }


            public static string OnlynameAdmins(int IDTM)
            {
                return ManagerDAL.OnlynameAdmins(IDTM);
            }


            public static bool DeleteBeforeInsert(int IDtm, int idcls, string date)
            {
                return ManagerDAL.DeleteBeforeInsert(IDtm, idcls, date);
            }


            public static List<Manager> viewReport(int IDkids)
            {
                DataTable Dt = ManagerDAL.viewReport(IDkids);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDpresenceReport = (int)Dt.Rows[i]["IDpresenceReport"];
                    Name.DatePresenceReport = (string)Dt.Rows[i]["DatePresenceReport"];
                    Name.Status = (string)Dt.Rows[i]["Status"];
                    Name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    if (Name.Status == "1")
                    {
                        Name.Status = "הגיע";

                    }
                    else
                    {
                        Name.Status = "לא הגיע";
                    }
                    Name.Remarks = (string)Dt.Rows[i]["Remarks"];
                    Lst.Add(Name);
                }
                return Lst;
            }





            public static List<Manager> viewBetweenDateReport(int ID, string From, string To)
            {
                DataTable Dt = ManagerDAL.viewBetweenDateReport(ID, From, To);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDpresenceReport = (int)Dt.Rows[i]["IDpresenceReport"];
                    Name.DatePresenceReport = (string)Dt.Rows[i]["DatePresenceReport"];
                    Name.Status = (string)Dt.Rows[i]["Status"];
                    if (Name.Status == "1")
                    {
                        Name.Status = "הגיע";

                    }
                    else
                    {
                        Name.Status = "לא הגיע";
                    }
                    Name.Remarks = (string)Dt.Rows[i]["Remarks"];
                    Lst.Add(Name);

                }
                return Lst;
            }

            public static void TrashForever(int idmassage)
            {
                ManagerDAL.TrashForever(idmassage);
            }



            public static List<Manager> UserAdmins(int idtm)
            {
                DataTable Dt = ManagerDAL.UserAdmins(idtm);
                List<Manager> Lst = new List<Manager>();
                Manager ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    ID = new Manager();
                    ID.UserNameAdmin = (string)Dt.Rows[i]["UserNameAdmin"];
                    ID.NameAdmin = (string)Dt.Rows[i]["NameAdmin"];
                    Lst.Add(ID);
                }
                return Lst;
            }

            public static List<Manager> UserNameKids(int ID)
            {
                DataTable Dt = ManagerDAL.UserNameKids(ID);
                List<Manager> Lst = new List<Manager>();
                Manager name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Manager();
                    name.UserNameParents = (string)Dt.Rows[i]["UserNameParents"];
                    name.NameKids = (string)Dt.Rows[i]["NameKids"] + " " + (string)Dt.Rows[i]["LastNameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }

            public static string HowTeam()
            {
                Database Db = new Database();
                string sql = "select count(*) as 'total' from TeamMember_3";
                DataTable Dt = Db.Execute(sql);
                int tot = (int)Dt.Rows[0]["total"];
                return tot.ToString();
            }

            public bool CheckClass(string nameclas, int idkindergarten)
            {
                return ManagerDAL.CheckClass(nameclas, idkindergarten);
            }

            public static int UnreadMessages(int idget)
            {
                return ManagerDAL.UnreadMessages(idget);
            }


            public static List<Manager> GetReportPersonal(int ID)
            {
                DataTable Dt = ManagerDAL.GetReportPersonal(ID);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDreportPersonal = (int)Dt.Rows[i]["IDreportPersonal"];
                    Name.Report = (string)Dt.Rows[i]["Report"];
                    Name.DateReportPersonal = Dt.Rows[i]["DateReportPersonal1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }

            public bool CheckUpdateSpecific()
            {
                return ManagerDAL.CheckUpdateSpecific(this);
            }

            public bool CheckUpdateSpecific1()
            {
                return ManagerDAL.CheckUpdateSpecific1(this);
            }
            public bool CheckUpdateSpecificEvent()
            {
                return ManagerDAL.CheckUpdateSpecificEvent(this);
            }
            public bool CheckUpdateSpecificEvent1()
            {
                return ManagerDAL.CheckUpdateSpecificEvent1(this);
            }

            public bool CheckUpdateSpecificAct()
            {
                return ManagerDAL.CheckUpdateSpecificAct(this);
            }
            public bool CheckUpdateSpecificAct1()
            {
                return ManagerDAL.CheckUpdateSpecificAct1(this);
            }

            public static List<Manager> GetEventUnusual(int ID)
            {
                DataTable Dt = ManagerDAL.GetEventUnusual(ID);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDeventUnusual = (int)Dt.Rows[i]["IDeventUnusual"];
                    Name.DateEventUnusual = (string)Dt.Rows[i]["DateEventUnusual"];
                    Name.DescriptionEvent = (string)Dt.Rows[i]["DescriptionEvent"];
                    Lst.Add(Name);

                }
                return Lst;
            }

            public static List<Manager> GetAct(int ID)
            {
                DataTable Dt = ManagerDAL.GetAct(ID);
                List<Manager> Lst = new List<Manager>();
                Manager Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Manager();
                    Name.IDeducationAct = (int)Dt.Rows[i]["IDeducationAct"];
                    Name.DateEducationAct = (string)Dt.Rows[i]["DateEducationAct"];
                    Name.Action = (string)Dt.Rows[i]["Action"];
                    Lst.Add(Name);

                }
                return Lst;
            }
        }
    }
}
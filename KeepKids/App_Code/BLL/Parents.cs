﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using KeepKids.DAL;


namespace KeepKids
{
    namespace BLL
    {
        public class Parents
        {
            //טבלת ילדים
            public int IDkids { get; set; }
            public int IDteamMember { get; set; }
            public string NameKids { get; set; }
            public string LastNameKids { get; set; }
            public string PicKids { get; set; }
            public string NameFather { set; get; }
            public string NameMother { get; set; }
            public string UserNameParents { get; set; }
            public string PasswordParents { get; set; }
            public string PasswordParentsRecovery { get; set; }
            public string PhoneFather { get; set; }
            public string PhoneMother { get; set; }
            //טבלת חברי צוות
            public string FullName { get; set; }
            public string PicManager { get; set; }
            public string UserNameTeamMember { get; set; }
            public string PhoneTeamMember { get; set; }


            //טבלת ילדים 
            public string Report { get; set; }
            public string DateReportPersonal { get; set; }
            //טבלת אירועים חריג
            public string DateEventUnusual { get; set; }
            public string DescriptionEvent { get; set; }

            //טבלת כיתות
            public string NameClass { get; set; }
            public int IDclass { get; set; }
            //טבלת אירועים חינוכיים
            public string DateEducationAct { get; set; }
            public string Action { get; set; }
            public int IDeducationAct { get; set; }
            //טבלת הודעות
            public int IDmessage { get; set; }
            public int IDsender { get; set; }
            public string Picsender { get; set; }
            public int StatusSender { get; set; }
            public int IDgetNotice { get; set; }
            public int StatusGetNotice { get; set; }
            public string SubjectMessage { get; set; }
            public string ContentMessage { get; set; }
            public string DateSendMessage { get; set; }
            public string NaneSender { get; set; }
            public string PicGetNotice { get; set; }
            public string NameGetNotice { get; set; }
            public string NameFile { get; set; }
            //טבלת נוכחות
            public int IDpresenceReport { get; set; }
            public string DatePresenceReport { get; set; }
            public string Status { get; set; }
            public string Remarks { get; set; }
            //טבלת מנהלים
            public string NameAdmin { get; set; }
            public int IDadmin { get; set; }
            //טבלת דיווח אישי
            public int IDreportPersonal { get; set; }
            

            public Parents() { }




            public static List<Parents> viewAllClass(int tmID)
            {
                DataTable Dt = ParentsDAL.viewAllClass(tmID);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.NameClass = (string)Dt.Rows[i]["NameClass"];
                    ID.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(ID);

                }
                return Lst;
            }



            public static List<Parents> viewClassByKids(int Class, int idtm)
            {
                DataTable Dt = ParentsDAL.viewClassByKids(Class, idtm);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.NameClass = (string)Dt.Rows[i]["NameClass"];
                    ID.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static List<Parents> ShowMessage(int ID)
            {
                DataTable Dt = ParentsDAL.ShowMessage(ID);
                List<Parents> Mess = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    Mess.Add(Name);

                }
                return Mess;
            }


            public static string ShowPic(int ID)
            {
                Database Db = new Database();
                {
                    Parents pic = new Parents();
                    string sql = "select distinct PicManager from TeamMember_3 t3 join Kids_5 k5 on t3.IDteamMember=k5.IDteamMember where t3.IDteamMember = '" + ID + "'";
                    DataTable Dt = Db.Execute(sql);
                    if (Dt.Rows.Count > 0)
                    {
                        pic.PicManager = (string)Dt.Rows[0]["PicManager"];
                    }

                    return pic.PicManager;
                }

            }

            public static List<Parents> Chats(int ID)
            {
                DataTable Dt = ParentsDAL.Chats(ID);
                List<Parents> Lst = new List<Parents>();
                Parents name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Parents();
                    name.FullName = (string)Dt.Rows[i]["FullName"];
                    name.PicManager = (string)Dt.Rows[i]["PicManager"];
                    name.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    Lst.Add(name);

                }
                return Lst;
            }


            public static List<Parents> viewDescriptionEvent(int ID)
            {
                DataTable Dt = ParentsDAL.viewDescriptionEvent(ID);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.DescriptionEvent = (string)Dt.Rows[i]["DescriptionEvent"];
                    Name.DateEventUnusual = (string)Dt.Rows[i]["DateEventUnusual1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }



            public static List<Parents> viewReportPersonal(int ID)
            {
                DataTable Dt = ParentsDAL.viewReportPersonal(ID);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents(); 
                    Name.IDreportPersonal = (int)Dt.Rows[i]["IDreportPersonal"];
                    Name.Report = (string)Dt.Rows[i]["Report"];
                    Name.DateReportPersonal = (string)Dt.Rows[i]["DateReportPersonal1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }


            public static List<Parents> viewBetweenDate(int ID, string From, string To)
            {
                DataTable Dt = ParentsDAL.viewBetweenDate(ID, From, To);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.DescriptionEvent = (string)Dt.Rows[i]["DescriptionEvent"];
                    Name.DateEventUnusual = (string)Dt.Rows[i]["DateEventUnusual1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }




            public static List<Parents> viewBetweenDateReportPersonal(int ID, string From, string To)
            {
                DataTable Dt = ParentsDAL.viewBetweenDateReportPersonal(ID, From, To);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.Report = (string)Dt.Rows[i]["Report"];
                    Name.DateReportPersonal = Dt.Rows[i]["DateReportPersonal1"].ToString();
                    Lst.Add(Name);

                }
                return Lst;
            }


            public static List<Parents> viewNameKids()
            {
                DataTable Dt = ParentsDAL.viewNameKids();
                List<Parents> Lst = new List<Parents>();
                Parents name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Parents();
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    name.NameKids = (string)Dt.Rows[i]["NameKids"] + " " + (string)Dt.Rows[i]["LastNameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }


            public static List<Parents> viewNameKidsById(string Email)
            {
                DataTable Dt = ParentsDAL.viewNameKidsById(Email);
                List<Parents> Lst = new List<Parents>();
                Parents name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Parents();
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    name.NameKids = (string)Dt.Rows[i]["NameKids"] + " " + (string)Dt.Rows[i]["LastNameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }


            public static List<Parents> viewNameWithoutFamili(string Email)
            {
                DataTable Dt = ParentsDAL.viewNameWithoutFamili(Email);
                List<Parents> Lst = new List<Parents>();
                Parents name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    name = new Parents();
                    name.IDkids = (int)Dt.Rows[i]["IDkids"];
                    name.NameKids = (string)Dt.Rows[i]["NameKids"];
                    Lst.Add(name);

                }
                return Lst;
            }

            public bool CheckRegister()
            {
                return ParentsDAL.CheckRegister(this);
            }

            public bool CheckUpdate()
            {
                return ParentsDAL.CheckUpdate(this);
            }

            public bool CheckUpdateSpecific()
            {
                return ParentsDAL.CheckUpdateSpecific(this);
            }

            public bool CheckReport()
            {
                return ParentsDAL.CheckReport(this);
            }

            public bool CheckEventUnusual()
            {
                return ParentsDAL.CheckEventUnusual(this);
            }

            public bool CheckLogin()
            {
                return ParentsDAL.CheckLogin(this);
            }

            public bool CheckMail()
            {
                return ParentsDAL.CheckMail(this);
            }

            public bool CheckPassword()
            {
                return ParentsDAL.CheckPassword(this);
            }



            public bool InsertPass()
            {
                return ParentsDAL.InsertPass(this);
            }




            public static List<Parents> GetAll()
            {
                DataTable Dt = ParentsDAL.GetAll();
                List<Parents> Mess = new List<Parents>();
                Parents list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Parents();
                    list.IDkids = (int)Dt.Rows[i]["IDkids"];
                    list.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    list.NameKids = (string)Dt.Rows[i]["NameKids"];
                    list.LastNameKids = (string)Dt.Rows[i]["LastNameKids"];
                    list.PicKids = (string)Dt.Rows[i]["PicKids"];
                    list.NameFather = (string)Dt.Rows[i]["NameFather"];
                    list.NameMother = (string)Dt.Rows[i]["NameMother"];
                    list.UserNameParents = (string)Dt.Rows[i]["UserNameParents"];
                    list.PasswordParents = (string)Dt.Rows[i]["PasswordParents"];
                    list.PhoneFather = (string)Dt.Rows[i]["PhoneFather"];
                    list.PhoneMother = (string)Dt.Rows[i]["PhoneMother"];

                    Mess.Add(list);

                }
                return Mess;

            }


            public static List<Parents> GetById(int ID)
            {
                DataTable Dt = ParentsDAL.GetById(ID);
                List<Parents> Mess = new List<Parents>();
                Parents list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Parents();
                    list.IDkids = (int)Dt.Rows[i]["IDkids"];
                    list.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    list.NameKids = (string)Dt.Rows[i]["NameKids"];
                    list.LastNameKids = (string)Dt.Rows[i]["LastNameKids"];
                    list.PicKids = (string)Dt.Rows[i]["PicKids"];
                    list.NameFather = (string)Dt.Rows[i]["NameFather"];
                    list.NameMother = (string)Dt.Rows[i]["NameMother"];
                    list.UserNameParents = (string)Dt.Rows[i]["UserNameParents"];
                    list.PasswordParents = (string)Dt.Rows[i]["PasswordParents"];
                    list.PhoneFather = (string)Dt.Rows[i]["PhoneFather"];
                    list.PhoneMother = (string)Dt.Rows[i]["PhoneMother"];

                    Mess.Add(list);

                }
                return Mess;

            }



            public static List<Parents> ShowClass(int ID)
            {
                DataTable Dt = ParentsDAL.ShowClass(ID);
                List<Parents> Lst = new List<Parents>();
                Parents Class;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Class = new Parents();
                    Class.NameClass = (string)Dt.Rows[i]["NameClass"];
                    Class.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(Class);

                }
                return Lst;
            }


            public static List<Parents> ListKids(int Class)
            {
                DataTable Dt = ParentsDAL.ListKids(Class);
                List<Parents> Mess = new List<Parents>();
                Parents list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Parents();

                    list.IDkids = (int)Dt.Rows[i]["IDkids"];
                    list.NameKids = (string)Dt.Rows[i]["FullName"];
                    list.NameFather = (string)Dt.Rows[i]["NameFather"];
                    list.PhoneFather = (string)Dt.Rows[i]["PhoneFather"];
                    list.NameMother = (string)Dt.Rows[i]["NameMother"];
                    list.PhoneMother = (string)Dt.Rows[i]["PhoneMother"];

                    Mess.Add(list);

                }
                return Mess;
            }



            public static List<Parents> ListKidsOPTION(int IDKIDS)
            {
                DataTable Dt = ParentsDAL.ListKidsOPTION(IDKIDS);
                List<Parents> Mess = new List<Parents>();
                Parents list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Parents();
                    list.NameKids = (string)Dt.Rows[i]["FullName"];
                    list.NameFather = (string)Dt.Rows[i]["NameFather"];
                    list.PhoneFather = (string)Dt.Rows[i]["PhoneFather"];
                    list.NameMother = (string)Dt.Rows[i]["NameMother"];
                    list.PhoneMother = (string)Dt.Rows[i]["PhoneMother"];

                    Mess.Add(list);

                }
                return Mess;
            }



            public static string FullNameTM(int IDteamMember)
            {
                return ParentsDAL.FullNameTM(IDteamMember);
            }


            public static string FullNameTM2(int IDteamMember)
            {
                return ParentsDAL.FullNameTM2(IDteamMember);
            }

            public static string TakePicture(int idk)
            {
                return ParentsDAL.TakePicture(idk);
            }

            public static string ShowPhone(int idkids)
            {
                return ParentsDAL.ShowPhone(idkids);
            }


            public static List<Parents> ViewMail(int idkids)
            {
                DataTable Dt = ParentsDAL.ViewMail(idkids);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.UserNameTeamMember = (string)Dt.Rows[i]["UserNameTeamMember"];
                    ID.PhoneTeamMember = (string)Dt.Rows[i]["PhoneTeamMember"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public static string ViewNameKids(int IDkids)
            {
                return ParentsDAL.ViewNameKids(IDkids);
            }




            public static List<Parents> viewEducationActByMonth(int IDkids)
            {
                DataTable Dt = ParentsDAL.viewEducationActByMonth(IDkids);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.IDeducationAct = (int)Dt.Rows[i]["IDeducationAct"];
                    Name.DateEducationAct = Dt.Rows[i]["DateEducationAct"].ToString();
                    Name.Action = (string)Dt.Rows[i]["Action"];
                    Lst.Add(Name);

                }
                return Lst;
            }





            public static List<Parents> viewEducationActByDate(int IDkids, string from, string to)
            {
                DataTable Dt = ParentsDAL.viewEducationActByDate(IDkids, from, to);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.DateEducationAct = Dt.Rows[i]["DateEducationAct"].ToString();
                    Name.Action = (string)Dt.Rows[i]["Action"];
                    Lst.Add(Name);

                }
                return Lst;
            }


            public static List<Parents> KindergartenOfTeacher(string UserNameParents)
            {
                DataTable Dt = ParentsDAL.KindergartenOfTeacher(UserNameParents);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    Name.FullName = (string)Dt.Rows[i]["FullName"];
                    Lst.Add(Name);

                }
                return Lst;
            }


            public static string OnlyPicManager(int idtm)
            {
                return ParentsDAL.OnlyPicManager(idtm);
            }


            public static string OnlyNameManager(int idtm)
            {
                return ParentsDAL.OnlyNameManager(idtm);
            }




            public static List<Parents> ListKidsToMessage(string UserNameParents)
            {
                DataTable Dt = ParentsDAL.ListKidsToMessage(UserNameParents);
                List<Parents> Mess = new List<Parents>();
                Parents list;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    list = new Parents();

                    list.IDkids = (int)Dt.Rows[i]["IDkids"];
                    list.NameKids = (string)Dt.Rows[i]["FullName"];
                    list.NameFather = (string)Dt.Rows[i]["NameFather"];
                    list.PhoneFather = (string)Dt.Rows[i]["PhoneFather"];
                    list.NameMother = (string)Dt.Rows[i]["NameMother"];
                    list.PhoneMother = (string)Dt.Rows[i]["PhoneMother"];

                    Mess.Add(list);

                }
                return Mess;
            }





            public static List<Parents> ShowMeMYNewMessage(string IDget, int StatusGet)
            {
                DataTable Dt = ParentsDAL.ShowMeMYNewMessage(IDget, StatusGet);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static void UpdateMarkTORead(int idmassage)
            {
                ParentsDAL.UpdateMarkTORead(idmassage);
            }



            public static void UpdateToTheTrash(int idmassage)
            {
                ParentsDAL.UpdateToTheTrash(idmassage);
            }


            public static List<Parents> MyItems(string Myid)
            {
                DataTable Dt = ParentsDAL.MyItems(Myid);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static List<Parents> MessageReaded(string IDget, int StatusGet)
            {
                DataTable Dt = ParentsDAL.MessageReaded(IDget, StatusGet);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }


            public static void UpdateToTheTrash1(int idmassage)
            {
                ParentsDAL.UpdateToTheTrash1(idmassage);
            }


            public static void UpdateToTheTrash2(int idmassage)
            {
                ParentsDAL.UpdateToTheTrash2(idmassage);
            }

            public static List<Parents> ShowMeMYTrashMessage(string user, int StatusGet)
            {
                DataTable Dt = ParentsDAL.ShowMeMYTrashMessage(user, StatusGet);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }

            public static List<Parents> ShowMeMYTrashMessage1(string user, int StatusSender)
            {
                DataTable Dt = ParentsDAL.ShowMeMYTrashMessage1(user, StatusSender);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.IDmessage = (int)Dt.Rows[i]["IDmessage"];
                    ID.Picsender = (string)Dt.Rows[i]["Picsender"];
                    ID.NaneSender = (string)Dt.Rows[i]["NaneSender"];
                    ID.IDsender = (int)Dt.Rows[i]["IDsender"];
                    ID.StatusSender = (int)Dt.Rows[i]["StatusSender"];
                    ID.IDgetNotice = (int)Dt.Rows[i]["IDgetNotice"];
                    ID.PicGetNotice = (string)Dt.Rows[i]["PicGetNotice"];
                    ID.NameGetNotice = (string)Dt.Rows[i]["NameGetNotice"];
                    ID.StatusGetNotice = (int)Dt.Rows[i]["StatusGetNotice"];
                    ID.SubjectMessage = (string)Dt.Rows[i]["SubjectMessage"];
                    ID.ContentMessage = (string)Dt.Rows[i]["ContentMessage"];
                    ID.NameFile = (string)Dt.Rows[i]["NameFile"];
                    ID.DateSendMessage = (string)Dt.Rows[i]["DateSendMessage"];
                    Lst.Add(ID);

                }
                return Lst;
            }




            public static List<Parents> viewReport(int IDkids)
            {
                DataTable Dt = ParentsDAL.viewReport(IDkids);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.IDpresenceReport = (int)Dt.Rows[i]["IDpresenceReport"];
                    Name.DatePresenceReport = (string)Dt.Rows[i]["DatePresenceReport"];
                    Name.Status = (string)Dt.Rows[i]["Status"];
                    if (Name.Status == "1")
                    {
                        Name.Status = "הגיע";

                    }
                    else
                    {
                        Name.Status = "לא הגיע";
                    }
                    Name.Remarks = (string)Dt.Rows[i]["Remarks"];
                    Lst.Add(Name);
                }
                return Lst;
            }



            public static List<Parents> viewBetweenDateReport(int ID, string From, string To)
            {
                DataTable Dt = ParentsDAL.viewBetweenDateReport(ID, From, To);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.IDpresenceReport = (int)Dt.Rows[i]["IDpresenceReport"];
                    Name.DatePresenceReport = (string)Dt.Rows[i]["DatePresenceReport"];
                    Name.Status = (string)Dt.Rows[i]["Status"];
                    if (Name.Status == "1")
                    {
                        Name.Status = "הגיע";

                    }
                    else
                    {
                        Name.Status = "לא הגיע";
                    }
                    Name.Remarks = (string)Dt.Rows[i]["Remarks"];
                    Lst.Add(Name);

                }
                return Lst;
            }





            public static List<Parents> AllAdmindByParents(string UserNameParents)
            {
                DataTable Dt = ParentsDAL.AllAdmindByParents(UserNameParents);
                List<Parents> Lst = new List<Parents>();
                Parents Name;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Name = new Parents();
                    Name.IDadmin = (int)Dt.Rows[i]["IDadmin"];
                    Name.NameAdmin = (string)Dt.Rows[i]["NameAdmin"];

                    Lst.Add(Name);

                }
                return Lst;
            }

            public static void TrashForever(int idmassage)
            {
                ParentsDAL.TrashForever(idmassage);
            }



            public static List<Parents> UserParents(int Class)
            {
                DataTable Dt = ParentsDAL.UserParents(Class);
                List<Parents> Lst = new List<Parents>();
                Parents ID;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    ID = new Parents();
                    ID.UserNameParents = (string)Dt.Rows[i]["UserNameParents"];
                    Lst.Add(ID);

                }
                return Lst;
            }
            public static string HowParents()
            {
                Database Db = new Database();
                string sql = "select count(*)*2 as 'total' from Kids_5";
                DataTable Dt = Db.Execute(sql);
                int tot = (int)Dt.Rows[0]["total"];
                return tot.ToString();
            }
            public static string HowKids()
            {
                Database Db = new Database();
                string sql = "select count(*) as 'total' from Kids_5";
                DataTable Dt = Db.Execute(sql);
                int tot = (int)Dt.Rows[0]["total"];
                return tot.ToString();
            }


            public static int UnreadMessages(string user)
            {
                return ParentsDAL.UnreadMessages(user);
            }

        }
    }
}
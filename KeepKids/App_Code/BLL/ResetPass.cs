﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeepKids.DAL;

namespace KeepKids
{
    namespace BLL
    {

        public class ResetPass
        {
            //טבלת חברי צוות
            public int IDteamMember { get; set; }
            public string FullName { get; set; }
            public string nameTeamMember { get; set; }
            public string FamilyTeamMember { set; get; }
            public string UserNameTeamMember { get; set; }
            public string PasswordTeamMember { get; set; }
            public string PasswordTMrecovery { get; set; }
            public string PicManager { get; set; }
            //טבלת מנהלים
            public int IDadmin { get; set; }
            public string NameAdmin { get; set; }
            public string UserNameAdmin { get; set; }
            public string PasswordAdmin { get; set; }
            public string PasswordAdminRecovery { get; set; }
            //טבלת ילדים
            public int IDkids { get; set; }
            public string NameFather { get; set; }
            public string NameMother { get; set; }
            public string UserNameParents { get; set; }
            public string PasswordParents { get; set; }
            public string PasswordParentsRecovery { get; set; }









            public bool CheckMail()
            {
                return ResetPassDAL.CheckMail(this);
            }



            public bool InsertPass()
            {
                return ResetPassDAL.InsertPass(this);
            }


        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KeepKids.DAL;
using System.Data;


namespace KeepKids
{
    namespace BLL
    {
        public class ToReport
        {


            public int IDteamMember { get; set; }
            public string NameKids { get; set; }
            public string LastNameKids { get; set; }
            public string UserNameParents { get; set; }
            public string PhoneFather { get; set; }
            public string PicKids { get; set; }
            //טבלת כיתות
            public string NameClass { get; set; }
            public int IDclass { get; set; }
            public int IDkids { get; set; }
            //טבלת גנים
            public string NameKindergarten { get; set; }
            public int IDkindergarten { get; set; }

            public ToReport() { }



            public static List<ToReport> ShowKidsBYclass(int ID, int Class)
            {
                DataTable Dt = ToReportDAL.ShowKidsBYclass(ID, Class);
                List<ToReport> Lst = new List<ToReport>();
                ToReport RE;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    RE = new ToReport();
                    RE.IDkids = (int)Dt.Rows[i]["IDkids"];
                    RE.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    RE.NameKids = (string)Dt.Rows[i]["NameKids"];
                    RE.PicKids = (string)Dt.Rows[i]["PicKids"];
                    RE.LastNameKids = (string)Dt.Rows[i]["LastNameKids"];
                    RE.NameClass = (string)Dt.Rows[i]["NameClass"];
                    RE.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(RE);

                }
                return Lst;
            }



            public static List<ToReport> ShowKidsBYclassAdmin(int ID, int Class)
            {
                DataTable Dt = ToReportDAL.ShowKidsBYclassAdmin(ID, Class);
                List<ToReport> Lst = new List<ToReport>();
                ToReport RE;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    RE = new ToReport();
                    RE.IDkids = (int)Dt.Rows[i]["IDkids"];
                    RE.IDteamMember = (int)Dt.Rows[i]["IDteamMember"];
                    RE.NameKids = (string)Dt.Rows[i]["NameKids"];
                    RE.PicKids = (string)Dt.Rows[i]["PicKids"];
                    RE.LastNameKids = (string)Dt.Rows[i]["LastNameKids"];
                    RE.NameClass = (string)Dt.Rows[i]["NameClass"];
                    RE.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(RE);

                }
                return Lst;
            }


            public static List<ToReport> ShowClass(int ID)
            {
                DataTable Dt = ToReportDAL.ShowClass(ID);
                List<ToReport> Lst = new List<ToReport>();
                ToReport Class;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Class = new ToReport();
                    Class.NameClass = (string)Dt.Rows[i]["NameClass"];
                    Class.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(Class);

                }
                return Lst;
            }


            public static List<ToReport> ShowClassAdmin(int ID)
            {
                DataTable Dt = ToReportDAL.ShowClassAdmin(ID);
                List<ToReport> Lst = new List<ToReport>();
                ToReport Class;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Class = new ToReport();
                    Class.NameClass = (string)Dt.Rows[i]["NameClass"];
                    Class.IDclass = (int)Dt.Rows[i]["IDclass"];
                    Lst.Add(Class);

                }
                return Lst;
            }

            public static List<ToReport> NameKinderGardenMethod(int idcom, int idad)
            {
                DataTable Dt = ToReportDAL.NameKinderGardenMethod(idcom, idad);
                List<ToReport> Lst = new List<ToReport>();
                ToReport Class;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Class = new ToReport();
                    Class.NameKindergarten = (string)Dt.Rows[i]["NameKindergarten"];
                    Class.IDkindergarten = (int)Dt.Rows[i]["IDkindergarten"];
                    Lst.Add(Class);

                }
                return Lst;
            }

            public static bool InsertPresenceReport(int idtm, int idclass, string date, int idk, string status, string remarks)
            {
                return ToReportDAL.InsertPresenceReport(idtm, idclass, date, idk, status, remarks);
            }



            public static List<ToReport> allParentsKinderGarden(int idad)
            {
                DataTable Dt = ToReportDAL.allParentsKinderGarden(idad);
                List<ToReport> Lst = new List<ToReport>();
                ToReport Class;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    Class = new ToReport();
                    Class.NameKindergarten = (string)Dt.Rows[i]["NameKindergarten"];
                    Class.IDkindergarten = (int)Dt.Rows[i]["IDkindergarten"];
                    Lst.Add(Class);

                }
                return Lst;
            }









        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using KeepKids.BLL;



namespace KeepKids
{
    namespace DAL
    {
        public class AdminsDAL
        {
            public static DataTable GetAll()
            {
                Database Db = new Database();
                string sql = "select * from Admin_2";
                return Db.Execute(sql);

            }

            public static DataTable GetById(int ID)
            {
                Database Db = new Database();
                string sql = "select * from Admin_2 where IDadmin='" + ID + "'";
                return Db.Execute(sql);
            }

            public static DataTable Chats(int ID)
            {
                Database Db = new Database();
                string sql = "select NameFather, NameMother, PicKids, IDkids as 'IDkidsONadmin' from Admin_2 a2 join TeamMember_3 tm3 on a2.IDcompany=tm3.IDcompany join Kids_5 k5 on tm3.IDteamMember= k5.IDteamMember where IDadmin= '" + ID + "'";
                return Db.Execute(sql);
            }


            public static bool CheckLogin(Admins Tmp)
            {
                Database Db = new Database();
                string sql = "select * from Admin_2 where UserNameAdmin = '" + Tmp.UserNameAdmin + "' and PasswordAdmin = '" + Tmp.PasswordAdmin + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                //בדיקה האם המשתמש והסיסמא זהים ומוכרים
                {
                    Tmp.IDadmin = (int)Dt.Rows[0]["IDadmin"];
                    Tmp.IDcompany = (int)Dt.Rows[0]["IDcompany"];
                    Tmp.NameAdmin = (string)Dt.Rows[0]["NameAdmin"];
                    Tmp.LastNameAdmin = (string)Dt.Rows[0]["LastNameAdmin"];
                    Tmp.PicAdmin = (string)Dt.Rows[0]["PicAdmin"];
                    Tmp.AddressAdmin = (string)Dt.Rows[0]["AddressAdmin"];
                    Tmp.PhoneAdmin = (string)Dt.Rows[0]["PhoneAdmin"];
                    Tmp.UserNameAdmin = (string)Dt.Rows[0]["UserNameAdmin"];
                    Tmp.PasswordAdmin = (string)Dt.Rows[0]["PasswordAdmin"];
                    return true;


                }
                return false;
            }


            public static bool CheckRegister(Admins Tmp1)
            {

                Database Db = new Database();

                {
                    string sql = "INSERT INTO Admin_2 ([IDcompany],[NameAdmin],[LastNameAdmin], [PicAdmin], [AddressAdmin], [PhoneAdmin], [UserNameAdmin], [PasswordAdmin]) VALUES('" + Tmp1.IDcompany + "','" + Tmp1.NameAdmin + "','" + Tmp1.LastNameAdmin + "', '" + Tmp1.PicAdmin + "','" + Tmp1.AddressAdmin + "','" + Tmp1.PhoneAdmin + "','" + Tmp1.UserNameAdmin + "','" + Tmp1.PasswordAdmin + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }

            }


            public static bool CheckMail(Admins Tmp)
            {
                Database Db = new Database();
                //string sql = "select UserNameTeamMember from TeamMember_3 where UserNameTeamMember = '" + Tmp.UserNameTeamMember + "'";
                string sql1 = "SELECT UserNameAdmin, NameAdmin FROM Admin_2 where UserNameAdmin = '" + Tmp.UserNameAdmin + "'";
                //string sql2 = "SELECT UserNameParents FROM Kids_5 where UserNameParents = '" + Tmp.UserNameParents + "'";
                DataTable Dt = Db.Execute(sql1);
                //DataTable Dt1 = Db.Execute(sql1);
                //DataTable Dt2 = Db.Execute(sql2);

                if (Dt.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {
                    Tmp.NameAdmin = (string)Dt.Rows[0]["NameAdmin"];
                    return true;


                }
                //if (Dt1.Rows.Count > 0)
                ////בדיקה האם המייל זהה ומוכר
                //{

                //    return true;


                //}
                //if (Dt2.Rows.Count > 0)
                ////בדיקה האם המייל זהה ומוכר
                //{

                //    return true;


                //}
                return false;
            }

            public static bool CheckUpdate(Admins Tmp1)
            {

                Database Db = new Database();

                {
                    string sql =
                    "UPDATE Admin_2 SET IDcompany = '" + Tmp1.IDcompany + "', NameAdmin='" + Tmp1.NameAdmin + "',LastNameAdmin ='" + Tmp1.LastNameAdmin + "',PicAdmin='" + Tmp1.PicAdmin + "',AddressAdmin='" + Tmp1.AddressAdmin + "',PhoneAdmin='" + Tmp1.PhoneAdmin + "',UserNameAdmin='" + Tmp1.UserNameAdmin + "',PasswordAdmin='" + Tmp1.PasswordAdmin + "' WHERE IDadmin='" + Tmp1.IDadmin + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }

            }


            public static bool InsertPass(Admins Tmp1)
            {

                Database Db = new Database();
                {
                    string sql = "UPDATE Admin_2 SET PasswordAdmin = '" + Tmp1.PasswordAdminRecovery + "' where IDadmin = '" + Tmp1.IDadmin + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }


            }

            public static DataTable viewNameKidsByIDcompany(int IDcompany)
            {
                Database Db = new Database();
                string sql = " select IDkids, NameKids, LastNameKids from Kids_5 join TeamMember_3 on Kids_5.IDteamMember=TeamMember_3.IDteamMember join Admin_2  on Admin_2.IDcompany=TeamMember_3.IDcompany where Admin_2.IDcompany='" + IDcompany + "'";
                return Db.Execute(sql);
            }



            public static DataTable viewByAdmin(int IDadmin)
            {
                Database Db = new Database();
                string sql = "select IDteamMember, FullName, a2.IDcompany from  TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany WHERE a2.IDcompany='" + IDadmin + "' order by FullName asc";
                return Db.Execute(sql);
            }



            public static DataTable viewNameKids(int company)
            {
                Database Db = new Database();
                string sql = "select IDkids, LastNameKids, NameKids from TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany join Kids_5 k5 on k5.IDteamMember= tm3.IDteamMember WHERE a2.IDcompany= '" + company + "' order by LastNameKids asc";
                return Db.Execute(sql);
            }

            public static DataTable viewNameKidsB(int idtm)
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids, LastNameKids from TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany join Kids_5 k5 on k5.IDteamMember= tm3.IDteamMember WHERE tm3.IDteamMember= '" + idtm + "' order by FullName asc";
                return Db.Execute(sql);
            }


            public static DataTable viewNameTeamMember(int idcompany)
            {
                Database Db = new Database();
                string sql = "select FullName , IDteamMember from TeamMember_3  where IDcompany='" + idcompany + "' order by nameTeamMember asc";
                return Db.Execute(sql);
            }

            public static DataTable ShowClass(int ID)
            {
                Database Db = new Database();
                string sql = "select c7.NameClass, c7.IDclass from Class_7 c7 join Kindergarten_6 kg6 on c7.IDkindergarten=kg6.IDkindergarten join  TeamMember_3 tm3 on kg6.IDteamMember=tm3.IDteamMember where  tm3.IDteamMember='" + ID + "'";
                return Db.Execute(sql);
            }


            public static DataTable ListKidsOPTION(int IDclass, int IDteamMember)
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids + ' ' + LastNameKids FullName, NameFather, PhoneFather, NameMother, PhoneMother from Kids_5 k5 join Class_7 c7 on k5.IDclass = c7.IDclass WHERE k5.IDclass IN(SELECT k5.IDclass FROM Kids_5 k5 WHERE IDclass = '" + IDclass + "') and IDteamMember IN(SELECT IDteamMember FROM Kids_5 k5 WHERE IDteamMember = '" + IDteamMember + "')";
                return Db.Execute(sql);
            }

            public static string TakePicture(int idtm)
            {
                string tot = "";
                Database Db = new Database();
                string sql = "select PicManager from TeamMember_3 where IDteamMember='" + idtm + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    tot = (string)Dt.Rows[0]["PicManager"];
                }
                return tot;
            }



            public static string ShowPhone(int idtm)
            {
                string phone = "";
                Database Db = new Database();
                string sql = "select PhoneTeamMember from TeamMember_3 where IDteamMember='" + idtm + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    phone = (string)Dt.Rows[0]["PhoneTeamMember"];
                }
                return phone;
            }

            public static DataTable NewKinderGarden(int IDcompany)
            {
                Database Db = new Database();
                string sql = "SELECT * FROM TeamMember_3 tm3  " +
                            "FULL JOIN Kindergarten_6 kg6 " +
                            "ON tm3.IDteamMember=kg6.IDteamMember " +
                            "WHERE kg6.IDteamMember IS null " +
                            "AND IDkindergarten IS null " +
                            "AND NameKindergarten IS null " +
                            "AND AddressKindergarten IS null " +
                            "AND IDcompany ='" + IDcompany + "'";
                return Db.Execute(sql);
            }



            public static bool InsertNewKinder(int idtm, string nkg, string akg)
            {
                Database Db = new Database();
                {
                    string sql = "INSERT INTO Kindergarten_6 ([IDteamMember],[NameKindergarten], [AddressKindergarten]) VALUES('" + idtm + "', '" + nkg + "', '" + akg + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }
            }



            public static DataTable IDkindergarten(int comany)
            {
                Database Db = new Database();
                string sql = "select FullName, IDkindergarten as 'IDteamMember' from TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany join Kindergarten_6 kg6 on tm3.IDteamMember=kg6.IDteamMember where tm3.IDcompany='" + comany + "'";
                return Db.Execute(sql);
            }




            public static bool InserstEducationAct(int idclass, string date, string action)
            {
                Database Db = new Database();
                {
                    string sql = "INSERT INTO EducationAct_13([IDclass],[DateEducationAct],[Action]) VALUES('" + idclass + "','" + date + "', '" + action + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }

            }


            public static DataTable viewEducationActByDate(int idclass, string from, string to)
            {
                Database Db = new Database();
                string sql = "select CAST (datepart(DAY, DateEducationAct) as varchar) +'-'+ " +
                                    "CAST(datepart(MONTH, DateEducationAct)as varchar) +'-'+ " +
                                    "CAST(datepart(YEAR, DateEducationAct)as varchar) DateEducationAct , " +
                                    "Action " +
                             "from EducationAct_13 " +
                             "where IDclass='" + idclass + "' " +
                             "and DateEducationAct between '" + from + "' " +
                             "and '" + to + "' ORDER BY cast(DateEducationAct as date) ASC";

                return Db.Execute(sql);
            }



            public static bool InsertNewMessage(int idsender, string pic, string name, int statussender, int idgetnotice, string picnotice, string namenotice, int statusgetnotice, string subjectmessage, string contentmessage, string NameFile, string datesendmessage)
            {
                Database Db = new Database();
                {
                    string sql = "INSERT INTO MyMessageBox_16 ([IDsender],[Picsender], [NaneSender],[StatusSender],[IDgetNotice],[PicGetNotice],[NameGetNotice], [StatusGetNotice], [SubjectMessage], [ContentMessage], [NameFile], [DateSendMessage]) " +
                    "VALUES('" + idsender + "', '" + pic + "','" + name + "','" + statussender + "','" + idgetnotice + "','" + picnotice + "','" + namenotice + "', '" + statusgetnotice + "','" + subjectmessage + "','" + contentmessage + "','" + NameFile + "', '" + datesendmessage + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }
            }



            public static DataTable ParentsByKinderGarden(int idkindergarten)
            {
                Database Db = new Database();
                string sql = "select IDkids from Kids_5 k5 " +
                                 "join Kindergarten_6 kg6 on k5.IDteamMember=kg6.IDteamMember " +
                                 "where IDkindergarten='" + idkindergarten + "'";
                return Db.Execute(sql);
            }



            public static DataTable ShowMeMYNewMessage(int IDget, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 " +
                             "where IDgetNotice='" + IDget + "' and StatusGetNotice='" + StatusGet + "' ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }





            public static string OnlyPicManager(int IdTm)
            {
                string pic = "";
                Database Db = new Database();
                string sql = "select PicManager from TeamMember_3 " +
                             "where IDteamMember='" + IdTm + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    pic = (string)Dt.Rows[0]["PicManager"];
                }
                return pic;
            }
            public static string OnlyNameManager(int IdTm)
            {
                string Name = "";
                Database Db = new Database();
                string sql = "select FullName from TeamMember_3 " +
                             "where IDteamMember='" + IdTm + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    Name = (string)Dt.Rows[0]["FullName"];
                }
                return Name;
            }

            public static string OnlyPicKids(int Idkids)
            {
                string pic = "";
                Database Db = new Database();
                string sql = "select PicKids from Kids_5 " +
                             "where IDkids='" + Idkids + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    pic = (string)Dt.Rows[0]["PicKids"];
                }
                return pic;
            }

            public static string OnlyNameKids(int Idkids)
            {
                string name = "";
                Database Db = new Database();
                string sql = "select NameKids + ' ' + LastNameKids as NameKids  from Kids_5 " +
                             "where IDkids='" + Idkids + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    name = (string)Dt.Rows[0]["NameKids"];
                }
                return name;
            }



            public static void UpdateMarkTORead(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 2 WHERE StatusGetNotice =1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }

            public static DataTable MessageReaded(int IDget, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDgetNotice='" + IDget + "' and StatusGetNotice='" + StatusGet + "' " +
                             "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }





            public static void UpdateToTheTrash(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 3 WHERE StatusGetNotice =1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }

            public static void UpdateToTheTrash1(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 3 WHERE StatusGetNotice =2 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }

            public static void UpdateToTheTrash2(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusSender= 3 WHERE StatusSender= 1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }



            public static DataTable ShowMeMYTrashMessage(int IDget, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 " +
                            "where IDgetNotice='" + IDget + "' and StatusGetNotice='" + StatusGet + "' " +
                            "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static DataTable ShowMeMYTrashMessage1(int IDSENDER, int StatusSender)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDsender='" + IDSENDER + "' AND StatusSender='" + StatusSender + "' " +
                    "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }







            public static DataTable MyItems(int Myid)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDsender='" + Myid + "' and StatusSender=1 " +
                             "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static void TrashForever(int idmassege)
            {
                Database Db = new Database();
                string sql = "DELETE from MyMessageBox_16 WHERE IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }



            public static DataTable viewManagerByAdmin(int IDadmin)
            {
                Database Db = new Database();
                string sql = "select UserNameTeamMember, FullName, a2.IDcompany from  TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany WHERE a2.IDadmin='" + IDadmin + "' order by FullName asc";
                return Db.Execute(sql);
            }



            public static DataTable ParentIndividual(int idadmin)
            {
                Database Db = new Database();
                string sql = "select UserNameParents, LastNameKids, NameKids from TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany join Kids_5 k5 on k5.IDteamMember= tm3.IDteamMember WHERE a2.IDadmin= '" + idadmin + "' order by LastNameKids asc";
                return Db.Execute(sql);
            }





            public static DataTable UserParents(int KINDERGARDEN)
            {
                Database Db = new Database();
                string sql = "  select UserNameParents from Kids_5 join TeamMember_3 tm3 on Kids_5.IDteamMember=tm3.IDteamMember join Kindergarten_6 on tm3.IDteamMember=Kindergarten_6.IDteamMember where IDkindergarten= '" + KINDERGARDEN + "'";
                return Db.Execute(sql);
            }


            public static bool InsertCompany(Admins Tmp1)
            {
                Database Db = new Database();
                {
                    string sql = "INSERT INTO Company_1 ([NameCompany],[AddressCompany],[MailCompany],[CountClass],[Price]) VALUES('" + Tmp1.NameCompany + "','" + Tmp1.AddressCompany + "','" + Tmp1.MailCompany + "', '" + Tmp1.CountClass + "','" + Tmp1.Price + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }
            }


            public static int[] GetCountClassByCompany(int IDcom)
            {
                Database Db = new Database();
                string sql = "select COUNT(IDclass) as 'IDclass', CountClass from Class_7 C7 JOIN Kindergarten_6 KG6 ON KG6.IDkindergarten=C7.IDkindergarten JOIN TeamMember_3 TM3 ON KG6.IDteamMember=TM3.IDteamMember JOIN Company_1 C1 ON TM3.IDcompany=C1.IDcompany WHERE C1.IDcompany='" + IDcom + "' GROUP BY C1.IDcompany, CountClass";
                DataTable Dt = Db.Execute(sql);
                int tot = (int)Dt.Rows[0]["CountClass"];
                int RightNow = (int)Dt.Rows[0]["IDclass"];
                int[] marks = new int[2];
                marks[0] = tot;
                marks[1] = RightNow;
                return marks;
            }

            public static bool CheckClass(string nameclas, int idkinergarten)
            {
                Database Db = new Database();
                string sql = "select NameClass from Class_7 c7 where NameClass='" + nameclas + "' and c7.IDkindergarten='" + idkinergarten + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {
                    return true;
                }
                return false;
            }



            public static DataTable GetPresenceReport(int ID)
            {
                Database Db = new Database();
                string sql = "select * from PresenceReport_10 where IDpresenceReport='" + ID + "'";
                return Db.Execute(sql);
            }



            public static bool CheckUpdateSpecific(Admins Tmp1)
            {

                Database Db1 = new Database();
                string sql1 = "UPDATE PresenceReport_10 SET Status='" + Tmp1.Status + "', Remarks='" + Tmp1.Remarks + "' WHERE IDpresenceReport='" + Tmp1.IDpresenceReport + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }

            public static DataTable Delete(int IDpresenceReport)
            {
                Database Db = new Database();
                string sql = "DELETE FROM PresenceReport_10 " +
                             "where IDpresenceReport='" + IDpresenceReport + "'";
                return Db.Execute(sql);
            }
        }

    }

}



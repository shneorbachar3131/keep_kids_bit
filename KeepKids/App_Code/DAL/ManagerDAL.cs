﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using KeepKids.BLL;


namespace KeepKids
{
    namespace DAL
    {
        public class ManagerDAL
        {

            public static DataTable GetAll()
            {
                Database Db = new Database();
                string sql = "select * from TeamMember_3";
                return Db.Execute(sql);

            }

            public static DataTable GetById(int ID)
            {
                Database Db = new Database();
                string sql = "select * from TeamMember_3 where IDteamMember='" + ID + "'";
                return Db.Execute(sql);
            }



            public static string NameKinderGarden(int Tmp)
            {
                string tot = "";
                Database Db = new Database();
                string sql = "select NameKindergarten from Kindergarten_6 kg6 join TeamMember_3 tm3 on kg6.IDteamMember= tm3.IDteamMember where tm3.IDteamMember= '" + Tmp + "'";
                DataTable Dt = Db.Execute(sql);
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    string Name = (string)Dt.Rows[i]["NameKindergarten"];
                    tot += " " + Name;



                }
                return tot;


            }



            public static string NameKinderGarden2(int Tmp)
            {
                string tot = "";
                Database Db = new Database();
                string sql = "select distinct NameKindergarten from Kids_5 k5 " +
                            "join Class_7 c7 on k5.IDclass=c7.IDclass " +
                            "join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember " +
                            "join Kindergarten_6 kg6 on tm3.IDteamMember=kg6.IDteamMember " +
                            "WHERE k5.IDclass IN (SELECT k5.IDclass FROM Kids_5 k5 WHERE IDkids ='" + Tmp + "') " +
                            "and k5.IDteamMember IN (SELECT IDteamMember FROM Kids_5 k5 WHERE IDkids ='" + Tmp + "')";
                DataTable Dt = Db.Execute(sql);
                for (int i = 0; i < Dt.Rows.Count; i++)
                {

                    string Name = (string)Dt.Rows[i]["NameKindergarten"];
                    tot += " " + Name;



                }
                return tot;


            }





            public static DataTable ListKids(int ID, int Class)
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids + ' ' + LastNameKids FullName, NameFather, PhoneFather, " +
                    "NameMother, PhoneMother from Kids_5 where IDteamMember='" + ID + "' and IDclass='" + Class + "'";
                return Db.Execute(sql);
            }




            public static DataTable ShowMessage(int ID)
            {
                Database Db = new Database();
                string sql = "select ContentMessage from Message_16 where IDsend = '" + ID + "'";
                return Db.Execute(sql);
            }



            public static bool CheckLogin(Manager Tmp)
            {
                Database Db = new Database();
                string sql = "select * from TeamMember_3 where UserNameTeamMember = '" + Tmp.UserNameTeamMember + "' and PasswordTeamMember = '" + Tmp.PasswordTeamMember + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                //בדיקה האם המשתמש והסיסמא זהים ומוכרים
                {
                    Tmp.IDteamMember = (int)Dt.Rows[0]["IDteamMember"];
                    Tmp.nameTeamMember = (string)Dt.Rows[0]["nameTeamMember"];
                    Tmp.FamilyTeamMember = (string)Dt.Rows[0]["FamilyTeamMember"];
                    Tmp.PhoneTeamMember = (string)Dt.Rows[0]["PhoneTeamMember"];
                    Tmp.PicManager = (string)Dt.Rows[0]["PicManager"];
                    Tmp.IDcompany = (int)Dt.Rows[0]["IDcompany"];
                    Tmp.IDjob = Dt.Rows[0]["IDjob"].ToString();
                    Tmp.FullName = (string)Dt.Rows[0]["FullName"];
                    return true;


                }
                return false;
            }

            public static DataTable viewDescriptionEvent(int ID)
            {
                Database Db = new Database();
                string sql = "select IDeventUnusual,CAST(datepart(DAY, DateEventUnusual) as varchar) +'-'+ CAST(datepart(MONTH, DateEventUnusual)as varchar) +'-'+ CAST(datepart(YEAR, DateEventUnusual)as varchar) DateEventUnusual1,DescriptionEvent from EventUnusual_12 where IDkids= '" + ID + "'order by DateEventUnusual asc";
                return Db.Execute(sql);
            }


            public static DataTable viewReportPersonal(int ID)
            {
                Database Db = new Database();
                string sql = "select IDreportPersonal, CAST(datepart(DAY, DateReportPersonal) as varchar) +'-'+ CAST(datepart(MONTH, DateReportPersonal)as varchar) +'-'+ CAST(datepart(YEAR, DateReportPersonal)as varchar) DateReportPersonal1, Report from ReportPersonal_15  where IDkids= '" + ID + "'order by DateReportPersonal asc";
                return Db.Execute(sql);
            }

            public static DataTable viewBetweenDate(int ID, string From, string To)
            {
                Database Db = new Database();
                string sql = "select CAST(datepart(DAY, DateEventUnusual) as varchar) +'-'+ CAST(datepart(MONTH, DateEventUnusual)as varchar) +'-'+ CAST(datepart(YEAR, DateEventUnusual)as varchar)  DateEventUnusual1,DescriptionEvent from EventUnusual_12  where IDkids= '" + ID + "' and DateEventUnusual between CONVERT(datetime,'" + From + "') AND CONVERT(datetime,'" + To + "') order by DateEventUnusual asc";
                return Db.Execute(sql);
            }


            public static DataTable viewBetweenDateReportPersonal(int ID, string From, string To)
            {
                Database Db = new Database();
                string sql = "select CAST(datepart(DAY, DateReportPersonal) as varchar) +'-'+ CAST(datepart(MONTH, DateReportPersonal)as varchar) +'-'+ CAST(datepart(YEAR, DateReportPersonal)as varchar)  DateReportPersonal1,Report from ReportPersonal_15  where IDkids= '" + ID + "' and DateReportPersonal between CONVERT(datetime,'" + From + "') AND CONVERT(datetime,'" + To + "') order by DateReportPersonal asc";
                return Db.Execute(sql);
            }


            public static DataTable view()
            {
                Database Db = new Database();
                string sql = "select IDcompany, NameCompany from Company_1";
                return Db.Execute(sql);
            }

            public static DataTable ViewMail(int Tmp)
            {
                Database Db = new Database();
                string sql = "select UserNameTeamMember, PhoneTeamMember from TeamMember_3 where IDteamMember='" + Tmp + "'";
                return Db.Execute(sql);
            }

            public static DataTable viewByid(int IDtm)
            {
                Database Db = new Database();
                string sql = "select IDteamMember, FullName from TeamMember_3 WHERE IDteamMember='" + IDtm + "' order by FullName asc";
                return Db.Execute(sql);
            }




            public static DataTable viewAll()
            {
                Database Db = new Database();
                string sql = "select IDteamMember, FullName from TeamMember_3 order by FullName asc";
                return Db.Execute(sql);
            }


            public static DataTable viewOne(int idtm)
            {
                Database Db = new Database();
                string sql = "select IDteamMember, FullName from TeamMember_3 where IDteamMember='" + idtm + "'";
                return Db.Execute(sql);
            }


            public static DataTable viewNameKids(int ID)
            {
                Database Db = new Database();
                string sql = "select k5.IDkids, NameKids, LastNameKids from Kids_5 k5  join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember  where tm3.IDteamMember='" + ID + "' order by LastNameKids asc";
                return Db.Execute(sql);
            }



            public static DataTable Chats(int ID)
            {
                Database Db = new Database();
                string sql = "select NameFather, NameMother, PicKids, IDkids from Kids_5 where IDteamMember='" + ID + "'";
                return Db.Execute(sql);
            }



            public static bool CheckRegister(Manager Tmp1)
            {

                Database Db = new Database();

                {
                    string sql = "INSERT INTO TeamMember_3 ([IDcompany],[IDjob],[FullName], [nameTeamMember], [FamilyTeamMember], [PhoneTeamMember], [UserNameTeamMember], [PasswordTeamMember], [PicManager]) VALUES('" + Tmp1.IDcompany + "','" + Tmp1.IDjob + "','" + Tmp1.nameTeamMember + " " + Tmp1.FamilyTeamMember + "','" + Tmp1.nameTeamMember + "','" + Tmp1.FamilyTeamMember + "','" + Tmp1.PhoneTeamMember + "','" + Tmp1.UserNameTeamMember + "','" + Tmp1.PasswordTeamMember + "', '" + Tmp1.PicManager + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }


            }


            public static bool CheckMail(Manager Tmp)
            {
                Database Db = new Database();
                string sql = "select UserNameTeamMember, nameTeamMember from TeamMember_3 where UserNameTeamMember = '" + Tmp.UserNameTeamMember + "'";
                //string sql1 = "SELECT UserNameAdmin FROM Admin_2 where UserNameAdmin = '" + Tmp.UserNameAdmin + "'";
                //string sql2 = "SELECT UserNameParents FROM Kids_5 where UserNameParents = '" + Tmp.UserNameParents + "'";
                DataTable Dt = Db.Execute(sql);
                //DataTable Dt1 = Db.Execute(sql1);
                //DataTable Dt2 = Db.Execute(sql2);

                if (Dt.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {

                    Tmp.nameTeamMember = (string)Dt.Rows[0]["nameTeamMember"];
                    return true;


                }
                //if (Dt1.Rows.Count > 0)
                ////בדיקה האם המייל זהה ומוכר
                //{

                //    return true;


                //}
                //if (Dt2.Rows.Count > 0)
                ////בדיקה האם המייל זהה ומוכר
                //{

                //    return true;


                //}
                return false;
            }



            public static bool CheckUpdate(Manager Tmp1)
            {

                Database Db = new Database();

                {
                    string sql =
                    "UPDATE TeamMember_3 SET IDcompany = '" + Tmp1.IDcompany + "'," +
                    "IDjob='" + Tmp1.IDjob + "'," +
                    "nameTeamMember='" + Tmp1.nameTeamMember + "'," +
                    "FamilyTeamMember='" + Tmp1.FamilyTeamMember + "'," +
                    "PhoneTeamMember='" + Tmp1.PhoneTeamMember + "'," +
                    "UserNameTeamMember='" + Tmp1.UserNameTeamMember + "'," +
                    "PasswordTeamMember='" + Tmp1.PasswordTeamMember + "'," +
                    "PicManager='" + Tmp1.PicManager + "'," +
                    "FullName='" + Tmp1.FullName + "' WHERE IDteamMember='" + Tmp1.IDteamMember + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }

            }



            public static bool InsertPass(Manager Tmp1)
            {

                Database Db = new Database();
                {
                    string sql = "UPDATE TeamMember_3 SET PasswordTeamMember = '" + Tmp1.PasswordTMrecovery + "' where IDteamMember = '" + Tmp1.IDteamMember + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }


            }



            public static bool Delete(int ID)
            {

                Database Db = new Database();
                {
                    string sql = "DELETE FROM TeamMember_3 where IDteamMember = '" + ID + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }


            }





            public static bool InsertMessage(int meesend, string ContentMessage)
            {

                Database Db = new Database();

                {
                    string sql = "INSERT INTO Message_16([IDsend],[ContentMessage]) VALUES('" + meesend + "', '" + ContentMessage + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }


            }

            public static bool AddClass(int idkg, string nc, string ga)
            {

                Database Db = new Database();

                {
                    string sql = "INSERT INTO Class_7 ([IDkindergarten],[NameClass],[GroupAge]) " +
                                 "VALUES('" + idkg + "','" + nc + "','" + ga + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }


            }


            public static int IDkinderGarden(int Tmp)
            {
                int ID = 0;
                Database Db = new Database();
                string sql = "select IDkindergarten from Kindergarten_6 kg6 join TeamMember_3 tm3 on kg6.IDteamMember= tm3.IDteamMember where tm3.IDteamMember= '" + Tmp + "'";
                DataTable Dt = Db.Execute(sql);
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    ID = (int)Dt.Rows[i]["IDkindergarten"];

                }
                return ID;

            }


            public static bool InserstEducationAct(int idclass, string date, string action)
            {
                Database Db = new Database();
                {
                    string sql = "INSERT INTO EducationAct_13([IDclass],[DateEducationAct],[Action]) VALUES('" + idclass + "','" + date + "', '" + action + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }

            }


            public static DataTable viewEducationActByMonth(int idclass)
            {
                Database Db = new Database();
                string sql = "select IDeducationAct, CAST (datepart(DAY, DateEducationAct) as varchar) +'-'+ " +
                                    "CAST(datepart(MONTH, DateEducationAct)as varchar) +'-'+ " +
                                    "CAST(datepart(YEAR, DateEducationAct)as varchar) DateEducationAct , Action from EducationAct_13 " +
                              "where IDclass='" + idclass + "' and DateEducationAct between dateadd(month,datediff(month,0,getdate()),0) " +
                                               "and dateadd(day,-1,dateadd(month,datediff(month,-1,getdate()),0)) " +
                              "ORDER BY cast(DateEducationAct as date) ASC";

                return Db.Execute(sql);
            }

            public static string OnlyPicKids(int Idkids)
            {
                string pic = "";
                Database Db = new Database();
                string sql = "select PicKids from Kids_5 " +
                             "where IDkids='" + Idkids + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    pic = (string)Dt.Rows[0]["PicKids"];
                }
                return pic;
            }

            public static string OnlyNameKids(int Idkids)
            {
                string name = "";
                Database Db = new Database();
                string sql = "select NameKids + ' ' + LastNameKids as NameKids  from Kids_5 " +
                             "where IDkids='" + Idkids + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    name = (string)Dt.Rows[0]["NameKids"];
                }
                return name;
            }



            public static bool InsertNewMessage(int idsender, string pic, string name, int statussender, int idgetnotice, string picnotice, string namenotice, int statusgetnotice, string subjectmessage, string contentmessage, string NameFile, string datesendmessage)
            {
                Database Db = new Database();
                {
                    string sql = "INSERT INTO MyMessageBox_16 ([IDsender],[Picsender], [NaneSender],[StatusSender],[IDgetNotice], [PicGetNotice],[NameGetNotice], [StatusGetNotice], [SubjectMessage], [ContentMessage],[NameFile], [DateSendMessage]) " +
                    "VALUES('" + idsender + "', '" + pic + "','" + name + "','" + statussender + "','" + idgetnotice + "','" + picnotice + "','" + namenotice + "', '" + statusgetnotice + "','" + subjectmessage + "','" + contentmessage + "','" + NameFile + "','" + datesendmessage + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }
            }

            public static DataTable ShowMeMYNewMessage(int IDget, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 " +
                             "where IDgetNotice='" + IDget + "' and StatusGetNotice='" + StatusGet + "' ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static DataTable MessageReaded(int IDget, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDgetNotice='" + IDget + "' and StatusGetNotice='" + StatusGet + "' " +
                             "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static DataTable MyItems(int Myid)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDsender='" + Myid + "' and StatusSender=1 " +
                             "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static DataTable ShowMeMYTrashMessage(int IDget, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDgetNotice='" + IDget + "' and StatusGetNotice='" + StatusGet + "' ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }
            public static DataTable ShowMeMYTrashMessage1(int IDSENDER, int StatusSender)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDsender='" + IDSENDER + "' AND StatusSender='" + StatusSender + "' " +
                    "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static void UpdateToTheTrash(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 3 WHERE StatusGetNotice =1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }

            public static void UpdateToTheTrash1(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 3 WHERE StatusGetNotice =2 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }

            public static void UpdateToTheTrash2(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusSender= 3 WHERE StatusSender= 1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }

            public static void UpdateMarkTORead(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 2 WHERE StatusGetNotice =1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }


            public static DataTable MyAdmins(int idtm)
            {
                Database Db = new Database();
                string sql = "select NameAdmin  + ' '  + LastNameAdmin NameAdmin,IDadmin from TeamMember_3 tm3 " +
                            "join Admin_2 a2 on tm3.IDcompany=a2.IDcompany " +
                            "where IDteamMember='" + idtm + "'";

                return Db.Execute(sql);
            }


            public static string OnlypicAdmins(int IDTM)
            {
                string pic = "";
                Database Db = new Database();
                string sql = "select PicAdmin from TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany " +
                              "where IDteamMember='" + IDTM + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    pic = (string)Dt.Rows[0]["PicAdmin"];
                }
                return pic;
            }

            public static string OnlynameAdmins(int IDTM)
            {
                string name = "";
                Database Db = new Database();
                string sql = "select NameAdmin + ' ' + LastNameAdmin as NameAdmin from TeamMember_3 tm3 join Admin_2 a2 on tm3.IDcompany=a2.IDcompany " +
                             "where IDteamMember='" + IDTM + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    name = (string)Dt.Rows[0]["NameAdmin"];
                }
                return name;
            }



            public static bool DeleteBeforeInsert(int IDtm, int idcls, string date)
            {

                Database Db = new Database();
                {
                    string sql = "delete from PresenceReport_10 " +
                                    "where IDteamMember='" + IDtm + "' " +
                                    "and IDclass='" + idcls + "' " +
                                    "and DatePresenceReport='" + date + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }


            }



            public static DataTable viewReport(int IDkids)
            {
                Database Db = new Database();
                string sql = "select IDpresenceReport,IDteamMember, IDclass, CAST (datepart(DAY, DatePresenceReport) as varchar) +'-'+ " +
                                    "CAST(datepart(MONTH, DatePresenceReport)as varchar) +'-'+ " +
                                    "CAST(datepart(YEAR, DatePresenceReport)as varchar) DatePresenceReport,  " +
                    "IDkids, Status, Remarks from PresenceReport_10  pr10 " +
                    "where IDkids='" + IDkids + "' " +
                    "and DatePresenceReport between dateadd(month,datediff(month,0,getdate()),0) " +
                    "and dateadd(day,-1,dateadd(month,datediff(month,-1,getdate()),0)) " +
                    "ORDER BY cast(DatePresenceReport as date) ASC";
                return Db.Execute(sql);
            }

            public static DataTable viewBetweenDateReport(int ID, string From, string To)
            {
                Database Db = new Database();
                string sql = "select IDpresenceReport,IDteamMember,IDclass, " +
                    "CAST(datepart(DAY, DatePresenceReport) as varchar) +'-'+  " +
                    "CAST(datepart(MONTH, DatePresenceReport)as varchar) +'-'+ " +
                    "CAST(datepart(YEAR, DatePresenceReport)as varchar) DatePresenceReport, " +
                    "IDkids,Status,Remarks from PresenceReport_10  " +
                    "where IDkids= '" + ID + "' " +
                    "and DatePresenceReport BETWEEN CONVERT(datetime,'" + From + "') AND CONVERT(datetime,'" + To + "') " +
                    "order by DatePresenceReport asc";
                return Db.Execute(sql);
            }

            public static void TrashForever(int idmassege)
            {
                Database Db = new Database();
                string sql = "DELETE from MyMessageBox_16 WHERE IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }


            public static DataTable UserAdmins(int idtm)
            {
                Database Db = new Database();
                string sql = "select NameAdmin  + ' '  + LastNameAdmin NameAdmin,UserNameAdmin from TeamMember_3 tm3 " +
                            "join Admin_2 a2 on tm3.IDcompany=a2.IDcompany " +
                            "where IDteamMember='" + idtm + "'";

                return Db.Execute(sql);
            }

            public static DataTable UserNameKids(int ID)
            {
                Database Db = new Database();
                string sql = "select UserNameParents, NameKids, LastNameKids from Kids_5 k5  join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember  where tm3.IDteamMember='" + ID + "' order by LastNameKids asc";
                return Db.Execute(sql);
            }

            public static bool CheckClass(string nameclas, int idkinergarten)
            {
                Database Db = new Database();
                string sql = "select NameClass from Class_7 c7 join Kindergarten_6 kg6 " +
                    "on c7.IDkindergarten=kg6.IDkindergarten join TeamMember_3 tm3 " +
                    "on kg6.IDteamMember=tm3.IDteamMember where NameClass='" + nameclas + "' and tm3.IDteamMember='" + idkinergarten + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {

                    Manager tmp = new Manager();
                    tmp.nameTeamMember = (string)Dt.Rows[0]["NameClass"];
                    return true;


                }
                return false;
            }


            public static int UnreadMessages(int idget)
            {
                int name = 0;
                Database Db = new Database();
                string sql = "select COUNT(IDmessage) as 'count' from MyMessageBox_16 where IDgetNotice='" + idget + "' and StatusGetNotice=1 group by IDgetNotice ";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    name = (int)Dt.Rows[0]["count"];
                }
                return name;

            }

            public static DataTable GetReportPersonal(int ID)
            {
                Database Db = new Database();
                string sql = "select IDreportPersonal, CAST(datepart(DAY, DateReportPersonal) as varchar) +'-'+ CAST(datepart(MONTH, DateReportPersonal)as varchar) +'-'+ CAST(datepart(YEAR, DateReportPersonal)as varchar) DateReportPersonal1, Report from ReportPersonal_15  where IDreportPersonal= '" + ID + "'";
                return Db.Execute(sql);
            }

            public static bool CheckUpdateSpecific(Manager Tmp1)
            {

                Database Db1 = new Database();
                string sql1 = "UPDATE ReportPersonal_15 SET DateReportPersonal='" + Tmp1.DateReportPersonal + "', Report='" + Tmp1.Report + "' WHERE IDreportPersonal='" + Tmp1.IDreportPersonal + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }



            public static bool CheckUpdateSpecific1(Manager Tmp1)
            {

                Database Db1 = new Database();
                string sql1 = "UPDATE ReportPersonal_15 SET DateReportPersonal=convert(date, '" + Tmp1.DateReportPersonal + "', 103), Report='" + Tmp1.Report + "' WHERE IDreportPersonal='" + Tmp1.IDreportPersonal + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }



            public static bool CheckUpdateSpecificEvent(Manager Tmp1)
            {

                Database Db1 = new Database();
                string sql1 = "UPDATE EventUnusual_12 SET DateEventUnusual='" + Tmp1.DateEventUnusual + "', DescriptionEvent='" + Tmp1.DescriptionEvent + "' WHERE IDeventUnusual='" + Tmp1.IDeventUnusual + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }
            public static bool CheckUpdateSpecificEvent1(Manager Tmp1)
            {
                Database Db1 = new Database();
                string sql1 = "UPDATE EventUnusual_12 SET DateEventUnusual= convert(date, '" + Tmp1.DateEventUnusual + "', 103), DescriptionEvent='" + Tmp1.DescriptionEvent + "' WHERE IDeventUnusual='" + Tmp1.IDeventUnusual + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }

            public static DataTable DeleteReportPersonal(int IDreportPersonal)
            {
                Database Db = new Database();
                string sql = "DELETE FROM ReportPersonal_15 " +
                             "where IDreportPersonal='" + IDreportPersonal + "'";
                return Db.Execute(sql);
            }

            public static DataTable GetEventUnusual(int ID)
            {
                Database Db = new Database();
                string sql = "select IDeventUnusual, CAST(datepart(DAY, DateEventUnusual) as varchar) +'-'+  " +
                                                    "CAST(datepart(MONTH, DateEventUnusual)as varchar) +'-'+ " +
                                                    "CAST(datepart(YEAR, DateEventUnusual)as varchar) DateEventUnusual, DescriptionEvent from EventUnusual_12  " +
                                                    "where IDeventUnusual= '" + ID + "'";
                return Db.Execute(sql);
            }

            public static DataTable DeleteEventUnusual(int IDeventUnusual)
            {
                Database Db = new Database();
                string sql = "DELETE FROM EventUnusual_12 " +
                             "where IDeventUnusual='" + IDeventUnusual + "'";
                return Db.Execute(sql);
            }
            public static DataTable GetAct(int ID)
            {
                Database Db = new Database();
                string sql = "select IDeducationAct, CAST(datepart(DAY, DateEducationAct) as varchar) +'-'+  " +
                                                    "CAST(datepart(MONTH, DateEducationAct)as varchar) +'-'+ " +
                                                    "CAST(datepart(YEAR, DateEducationAct)as varchar) DateEducationAct, Action from EducationAct_13  " +
                                                    "where IDeducationAct= '" + ID + "'";
                return Db.Execute(sql);
            }
            public static bool CheckUpdateSpecificAct(Manager Tmp1)
            {

                Database Db1 = new Database();
                string sql1 = "UPDATE EducationAct_13 SET DateEducationAct='" + Tmp1.DateEducationAct + "', " +
                    "Action='" + Tmp1.Action + "' " +
                    "WHERE IDeducationAct='" + Tmp1.IDeducationAct + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }

            public static bool CheckUpdateSpecificAct1(Manager Tmp1)
            {

                Database Db1 = new Database();
                string sql1 = "UPDATE EducationAct_13 SET DateEducationAct=convert(date, '" + Tmp1.DateEducationAct + "', 103), " +
                    "Action='" + Tmp1.Action + "' " +
                    "WHERE IDeducationAct='" + Tmp1.IDeducationAct + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }


            public static DataTable DeleteEducationAct(int IDeducationAct)
            {
                Database Db = new Database();
                string sql = "DELETE FROM EducationAct_13 " +
                             "where IDeducationAct='" + IDeducationAct + "'";
                return Db.Execute(sql);
            }
        }
    }
}



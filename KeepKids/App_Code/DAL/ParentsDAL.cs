﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using KeepKids.BLL;

namespace KeepKids
{
    namespace DAL
    {
        public class ParentsDAL
        {




            public static DataTable viewAllClass(int tmID)
            {
                Database Db = new Database();
                string sql = "select NameClass, c7.IDclass from Class_7 c7 join Kindergarten_6 kg6 on c7.IDkindergarten=kg6.IDkindergarten join TeamMember_3 tm3 on kg6.IDteamMember=tm3.IDteamMember where tm3.IDteamMember='" + tmID + "'";
                return Db.Execute(sql);
            }


            public static DataTable viewClassByKids(int idkids, int Class)
            {
                Database Db = new Database();
                string sql = "select DISTINCT NameClass, c7.IDclass from TeamMember_3 t3 " +
                             "join Kids_5 k5 on t3.IDteamMember = k5.IDteamMember " +
                             "join Kindergarten_6 k6 on t3.IDteamMember=k6.IDteamMember " +
                             "join Class_7 c7 on k5.IDclass=c7.IDclass " +
                             "where k5.IDkids = '" + idkids + "' and c7.IDclass='" + Class + "'" +
                             "order by NameClass asc ";
                return Db.Execute(sql);
            }



            public static DataTable GetAll()
            {
                Database Db = new Database();
                string sql = "select * from Kids_5";
                return Db.Execute(sql);

            }

            public static DataTable GetById(int ID)
            {
                Database Db = new Database();
                string sql = "select * from Kids_5 where IDkids='" + ID + "'";
                return Db.Execute(sql);
            }



            public static DataTable Delete(int ID)
            {
                Database Db = new Database();
                string sql = "DELETE FROM Kids_5 " +
                             "where IDkids='" + ID + "'";
                return Db.Execute(sql);
            }





            public static DataTable ShowMessage(int ID)
            {
                Database Db = new Database();
                string sql = "select ContentMessage from Message_16 where IDsend = '" + ID + "'";
                return Db.Execute(sql);
            }



            public static DataTable Chats(int ID)
            {
                Database Db = new Database();
                string sql = "select FullName, PicManager, IDteamMember from TeamMember_3 where IDteamMember='" + ID + "'";
                return Db.Execute(sql);
            }




            public static bool CheckLogin(Parents Tmp)
            {
                Database Db = new Database();
                string sql = "select * from Kids_5 where UserNameParents = '" + Tmp.UserNameParents + "' and PasswordParents = '" + Tmp.PasswordParents + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                //בדיקה האם המשתמש והסיסמא זהים ומוכרים
                {
                    Tmp.IDteamMember = (int)Dt.Rows[0]["IDteamMember"];
                    Tmp.IDclass = (int)Dt.Rows[0]["IDclass"];
                    Tmp.NameKids = (string)Dt.Rows[0]["NameKids"];
                    Tmp.LastNameKids = (string)Dt.Rows[0]["LastNameKids"];
                    Tmp.NameFather = (string)Dt.Rows[0]["NameFather"];
                    Tmp.NameMother = (string)Dt.Rows[0]["NameMother"];
                    Tmp.PhoneFather = (string)Dt.Rows[0]["PhoneFather"];
                    Tmp.PhoneMother = (string)Dt.Rows[0]["PhoneMother"];
                    Tmp.PicKids = (string)Dt.Rows[0]["PicKids"];
                    Tmp.IDkids = (int)Dt.Rows[0]["IDkids"];
                    return true;


                }
                return false;
            }



            public static bool CheckMail(Parents Tmp)
            {
                Database Db = new Database();
                string sql = "SELECT UserNameParents,NameFather, NameMother, PasswordParents  FROM Kids_5 where UserNameParents = '" + Tmp.UserNameParents + "'";
                DataTable Dt = Db.Execute(sql);

                if (Dt.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {
                    Tmp.NameFather = (string)Dt.Rows[0]["NameFather"];
                    Tmp.NameMother = (string)Dt.Rows[0]["NameMother"];
                    return true;


                }

                return false;
            }

            public static bool CheckPassword(Parents Tmp)
            {
                Database Db = new Database();
                string sql = "SELECT UserNameParents,NameFather, NameMother, PasswordParents  FROM Kids_5 where UserNameParents = '" + Tmp.UserNameParents + "' and PasswordParents= '" + Tmp.PasswordParents + "'";
                DataTable Dt = Db.Execute(sql);

                if (Dt.Rows.Count > 0)
                //בדיקה האם המייל זהה ומוכר
                {
                    Tmp.NameFather = (string)Dt.Rows[0]["NameFather"];
                    Tmp.NameMother = (string)Dt.Rows[0]["NameMother"];
                    return true;


                }

                return false;
            }




            public static DataTable viewDescriptionEvent(int ID)
            {
                Database Db = new Database();
                string sql = "select CAST(datepart(DAY, DateEventUnusual) as varchar) +'-'+ " +
                                    "CAST(datepart(MONTH, DateEventUnusual)as varchar) +'-'+ " +
                                    "CAST(datepart(YEAR, DateEventUnusual)as varchar) DateEventUnusual1,DescriptionEvent " +
                             "from EventUnusual_12 " +
                             "WHERE IDkids= '" + ID + "' " +
                             "and DateEventUnusual between dateadd(month,datediff(month,0,getdate()),0) and " +
                                                          "dateadd(day,-1,dateadd(month,datediff(month,-1,getdate()),0)) " +
                             "ORDER BY cast(DateEventUnusual as date)";
                return Db.Execute(sql);
            }

            public static DataTable viewReportPersonal(int ID)
            {
                Database Db = new Database();
                string sql = "select IDreportPersonal, CAST(datepart(DAY, DateReportPersonal) as varchar) + '-' + CAST(datepart(MONTH, DateReportPersonal) as varchar) + '-' + CAST(datepart(YEAR, DateReportPersonal) as varchar) DateReportPersonal1,Report from ReportPersonal_15 WHERE IDkids = '" + ID + "' order by DateReportPersonal asc";
                return Db.Execute(sql);
            }



            public static DataTable viewBetweenDate(int ID, string From, string To)
            {
                Database Db = new Database();
                string sql = "select CAST(datepart(DAY, DateEventUnusual) as varchar) +'-'+ CAST(datepart(MONTH, DateEventUnusual)as varchar) +'-'+ CAST(datepart(YEAR, DateEventUnusual)as varchar)  DateEventUnusual1,DescriptionEvent from EventUnusual_12  WHERE IDkids= '" + ID + "' and DateEventUnusual BETWEEN CONVERT(datetime,'" + From + "') AND CONVERT(datetime,'" + To + "') order by DateEventUnusual asc";
                return Db.Execute(sql);
            }



            public static DataTable viewBetweenDateReportPersonal(int ID, string From, string To)
            {
                Database Db = new Database();
                string sql = "select CAST(datepart(DAY, DateReportPersonal) as varchar) +'-'+ CAST(datepart(MONTH, DateReportPersonal)as varchar) +'-'+ CAST(datepart(YEAR, DateReportPersonal)as varchar)  DateReportPersonal1,Report from ReportPersonal_15  where IDkids= '" + ID + "' and DateReportPersonal BETWEEN CONVERT(datetime,'" + From + "') AND CONVERT(datetime,'" + To + "') order by DateReportPersonal asc";
                return Db.Execute(sql);
            }


            public static DataTable viewNameKids()
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids, LastNameKids from Kids_5 order by LastNameKids asc";
                return Db.Execute(sql);
            }

            public static DataTable viewNameKidsById(string Email)
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids, LastNameKids from Kids_5 WHERE UserNameParents='" + Email + "' order by LastNameKids asc";
                return Db.Execute(sql);
            }


            public static DataTable viewNameWithoutFamili(string Email)
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids from Kids_5 WHERE UserNameParents='" + Email + "' order by LastNameKids asc";
                return Db.Execute(sql);
            }

            public static bool CheckRegister(Parents Tmp1)
            {

                Database Db = new Database();

                {
                    string sql = "INSERT INTO Kids_5 ([IDteamMember],[IDclass],[NameKids],[LastNameKids], [PicKids], [NameFather], [NameMother], [UserNameParents], [PasswordParents], [PhoneFather], [PhoneMother]) VALUES('" + Tmp1.IDteamMember + "','" + Tmp1.IDclass + "','" + Tmp1.NameKids + "','" + Tmp1.LastNameKids + "', '" + Tmp1.PicKids + "','" + Tmp1.NameFather + "','" + Tmp1.NameMother + "','" + Tmp1.UserNameParents + "','" + Tmp1.PasswordParents + "','" + Tmp1.PhoneFather + "', '" + Tmp1.PhoneMother + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }

            }


            public static bool CheckUpdate(Parents Tmp1)
            {

                Database Db = new Database();

                {
                    string sql = "UPDATE Kids_5 SET IDteamMember = '" + Tmp1.IDteamMember + "', " +
                                                    "IDclass= '" + Tmp1.IDclass + "'," +
                                                    "NameKids='" + Tmp1.NameKids + "'," +
                                                    "LastNameKids='" + Tmp1.LastNameKids + "'," +
                                                    "PicKids='" + Tmp1.PicKids + "'," +
                                                    "NameFather='" + Tmp1.NameFather + "'," +
                                                    "NameMother='" + Tmp1.NameMother + "'," +
                                                    "UserNameParents='" + Tmp1.UserNameParents + "'," +
                                                    "PasswordParents='" + Tmp1.PasswordParents + "'," +
                                                    "PhoneFather='" + Tmp1.PhoneFather + "'," +
                                                    "PhoneMother='" + Tmp1.PhoneMother + "' WHERE IDkids='" + Tmp1.IDkids + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }

            }

            public static bool CheckUpdateSpecific(Parents Tmp1)
            {
                if (Tmp1.NameFather is null)
                {

                    Database Db = new Database();
                    {
                        string sql = "UPDATE Kids_5 SET NameKids='" + Tmp1.NameKids + "', " +
                                                       "LastNameKids='" + Tmp1.LastNameKids + "' " +
                                                       "WHERE IDkids='" + Tmp1.IDkids + "'";
                        DataTable Dt = Db.Execute(sql);
                        return true;
                    }
                }
                Database Db1 = new Database();
                string sql1 = "UPDATE Kids_5 SET NameKids='" + Tmp1.NameKids + "', " +
                "LastNameKids='" + Tmp1.LastNameKids + "', " +
                "NameFather='" + Tmp1.NameFather + "'," +
                "NameMother='" + Tmp1.NameMother + "'," +
                "PhoneFather='" + Tmp1.PhoneFather + "'," +
                "PhoneMother='" + Tmp1.PhoneMother + "' WHERE IDkids='" + Tmp1.IDkids + "'";
                DataTable Dt1 = Db1.Execute(sql1);
                return true;
            }
            public static bool CheckReport(Parents Tmp1)
            {

                Database Db = new Database();

                {
                    string sql = "INSERT INTO ReportPersonal_15 ([IDteamMember],[IDkids],[Report], [DateReportPersonal]) VALUES('" + Tmp1.IDteamMember + "','" + Tmp1.IDkids + "','" + Tmp1.Report + "', '" + Tmp1.DateReportPersonal + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }


            }


            public static bool CheckEventUnusual(Parents Tmp1)
            {

                Database Db = new Database();
                Database Db1 = new Database();
                {
                    string sql = "INSERT INTO EventUnusual_12 ([IDteamMember],[IDkids],[DateEventUnusual], [DescriptionEvent]) VALUES('" + Tmp1.IDteamMember + "','" + Tmp1.IDkids + "','" + Tmp1.DateEventUnusual + "', '" + Tmp1.DescriptionEvent + "')";
                    DataTable Dt = Db.Execute(sql);
                    string sql1 = "select UserNameParents, NameFather, NameMother, FullName, PicKids from Kids_5 k5 join EventUnusual_12 e12 on k5.IDkids = e12.IDkids join TeamMember_3 t3 on e12.IDteamMember=t3.IDteamMember where e12.IDkids='" + Tmp1.IDkids + "' and e12.IDteamMember='" + Tmp1.IDteamMember + "' ";
                    DataTable Dt1 = Db1.Execute(sql1);
                    if (Dt1.Rows.Count > 0)

                    {
                        Tmp1.UserNameParents = (string)Dt1.Rows[0]["UserNameParents"];
                        Tmp1.NameFather = (string)Dt1.Rows[0]["NameFather"];
                        Tmp1.NameMother = (string)Dt1.Rows[0]["NameMother"];
                        Tmp1.FullName = (string)Dt1.Rows[0]["FullName"];
                        Tmp1.PicKids = (string)Dt1.Rows[0]["PicKids"];
                        return true;
                    }
                    return false;
                }
            }



            public static bool InsertPass(Parents Tmp1)
            {

                Database Db = new Database();
                {
                    string sql = "UPDATE Kids_5 SET PasswordParents = '" + Tmp1.PasswordParentsRecovery + "' where UserNameParents = '" + Tmp1.UserNameParents + "'";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }


            }


            public static DataTable ShowClass(int ID)
            {
                Database Db = new Database();
                string sql = "select c7.NameClass, c7.IDclass from Class_7 c7 join Kids_5 k5 on c7.IDclass=k5.IDclass " +
                             "where  IDkids='" + ID + "'";
                return Db.Execute(sql);
            }


            public static DataTable ListKids(int Class)
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids + ' ' + LastNameKids FullName, NameFather, PhoneFather, " +
                    "NameMother, PhoneMother from Kids_5 where IDclass = '" + Class + "'";
                return Db.Execute(sql);
            }

            public static DataTable ListKidsOPTION(int IDKIDS)
            {
                Database Db = new Database();
                string sql = " select NameKids + ' ' + LastNameKids FullName, NameFather, PhoneFather, NameMother, PhoneMother from Kids_5 k5 " +
                            "join Class_7 c7 on k5.IDclass=c7.IDclass " +
                            "WHERE k5.IDclass IN (SELECT k5.IDclass FROM Kids_5 k5 WHERE IDkids='" + IDKIDS + "') " +
                            "and IDteamMember IN (SELECT IDteamMember FROM Kids_5 k5 WHERE IDkids='" + IDKIDS + "')";
                return Db.Execute(sql);
            }




            public static string FullNameTM(int IDteamMember)
            {
                string tot = "";
                Database Db = new Database();
                string sql = " select distinct FullName from Kids_5 k5 join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember where k5.IDteamMember='" + IDteamMember + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    string Name = (string)Dt.Rows[0]["FullName"];
                    tot += " " + Name;
                }
                return tot;


            }


            public static string FullNameTM2(int IDteamMember)
            {
                string tot = "";
                Database Db = new Database();
                string sql = "select distinct FullName from Kids_5 k5 " +
                            "join Class_7 c7 on k5.IDclass=c7.IDclass " +
                            "join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember " +
                            "WHERE k5.IDclass IN (SELECT k5.IDclass FROM Kids_5 k5 WHERE IDkids = '" + IDteamMember + "') " +
                            "and k5.IDteamMember IN (SELECT IDteamMember FROM Kids_5 k5 WHERE IDkids = '" + IDteamMember + "')";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    string Name = (string)Dt.Rows[0]["FullName"];
                    tot += " " + Name;
                }
                return tot;
            }

            public static string TakePicture(int idk)
            {
                string tot = "";
                Database Db = new Database();
                string sql = "select distinct PicManager from Kids_5 k5 join Class_7 c7 on k5.IDclass=c7.IDclass " +
                            "join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember " +
                            "WHERE k5.IDclass IN (SELECT k5.IDclass FROM Kids_5 k5 WHERE IDkids ='" + idk + "') " +
                            "and k5.IDteamMember IN (SELECT IDteamMember FROM Kids_5 k5 WHERE IDkids ='" + idk + "')";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    string pic = (string)Dt.Rows[0]["PicManager"];
                    tot = pic;
                }
                return tot;
            }



            public static string ShowPhone(int idkids)
            {
                string tot = "";
                Database Db = new Database();
                string sql = "select distinct PhoneTeamMember from Kids_5 k5 " +
                            "join Class_7 c7 on k5.IDclass=c7.IDclass " +
                            "join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember " +
                            "WHERE k5.IDclass IN (SELECT k5.IDclass FROM Kids_5 k5 WHERE IDkids = '" + idkids + "') " +
                            "and k5.IDteamMember IN (SELECT IDteamMember FROM Kids_5 k5 WHERE IDkids = '" + idkids + "')";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    string phone = (string)Dt.Rows[0]["PhoneTeamMember"];
                    tot += " " + phone;
                }
                return tot;


            }


            public static DataTable ViewMail(int idkids)
            {
                Database Db = new Database();
                string sql = "select distinct UserNameTeamMember, PhoneTeamMember from Kids_5 k5 " +
                    "join Class_7 c7 on k5.IDclass=c7.IDclass " +
                    "join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember " +
                    "WHERE k5.IDclass IN (SELECT k5.IDclass FROM Kids_5 k5 WHERE IDkids = '" + idkids + "') " +
                    "and k5.IDteamMember IN (SELECT IDteamMember FROM Kids_5 k5 WHERE IDkids = '" + idkids + "')";
                return Db.Execute(sql);
            }


            public static string ViewNameKids(int IDkids)
            {
                string name = "";
                Database Db = new Database();
                string sql = "select NameKids from kids_5 where IDkids='" + IDkids + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    name = (string)Dt.Rows[0]["NameKids"];

                }
                return name;


            }


            public static DataTable viewEducationActByMonth(int IDkids)
            {
                Database Db = new Database();
                string sql = "select IDeducationAct, CAST (datepart(DAY, DateEducationAct) as varchar) +'-'+ " +
                                    "CAST(datepart(MONTH, DateEducationAct)as varchar) +'-'+  " +
                                    "CAST(datepart(YEAR, DateEducationAct)as varchar) DateEducationAct , Action from EducationAct_13  ea13 " +
                             "join Kids_5 k5 on ea13.IDclass=k5.IDclass " +
                             "where IDkids='" + IDkids + "' " +
                             "and DateEducationAct between dateadd(month,datediff(month,0,getdate()),0) " +
                             "and dateadd(day,-1,dateadd(month,datediff(month,-1,getdate()),0)) " +
                             "ORDER BY cast(DateEducationAct as date) ASC ";

                return Db.Execute(sql);
            }



            public static DataTable viewEducationActByDate(int IDkids, string from, string to)
            {
                Database Db = new Database();
                string sql = "select CAST(datepart(DAY, DateEducationAct) as varchar) +'-'+ " +
                                    "CAST(datepart(MONTH, DateEducationAct)as varchar) +'-'+ " +
                                    "CAST(datepart(YEAR, DateEducationAct)as varchar) DateEducationAct , Action " +
                             "from EducationAct_13 ea13 " +
                             "join Kids_5 k5 on ea13.IDclass=k5.IDclass " +
                             "where IDkids='" + IDkids + "' and DateEducationAct between CONVERT(datetime,'" + from + "') AND CONVERT(datetime,'" + to + "') " +
                             "ORDER BY cast(DateEducationAct as date) ASC";

                return Db.Execute(sql);
            }



            public static DataTable KindergartenOfTeacher(string UserNameParents)
            {
                Database Db = new Database();
                string sql = "select distinct  tm3.IDteamMember, FullName from Kids_5  k5 " +
                    "join TeamMember_3 tm3 on k5.IDteamMember=tm3.IDteamMember " +
                    "WHERE UserNameParents='" + UserNameParents + "' order by FullName asc";

                return Db.Execute(sql);
            }




            public static bool InsertNewMessage(int idsender, string pic, string name, int statussender, int idgetnotice, string picnotice, string namenotice, int statusgetnotice, string subjectmessage, string contentmessage, string NameFile, string datesendmessage)
            {
                Database Db = new Database();
                {
                    string sql = "INSERT INTO MyMessageBox_16 ([IDsender],[Picsender], [NaneSender],[StatusSender],[IDgetNotice], [PicGetNotice],[NameGetNotice], [StatusGetNotice], [SubjectMessage], [ContentMessage],[NameFile], [DateSendMessage]) " +
                    "VALUES('" + idsender + "', '" + pic + "','" + name + "','" + statussender + "','" + idgetnotice + "','" + picnotice + "','" + namenotice + "', '" + statusgetnotice + "','" + subjectmessage + "','" + contentmessage + "','" + NameFile + "','" + datesendmessage + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;
                }
            }



            public static string OnlyPicManager(int idtm)
            {
                string pic = "";
                Database Db = new Database();
                string sql = "select PicManager from TeamMember_3 " +
                             "where IDteamMember='" + idtm + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    pic = (string)Dt.Rows[0]["PicManager"];
                }
                return pic;
            }

            public static string OnlyNameManager(int idtm)
            {
                string name = "";
                Database Db = new Database();
                string sql = "select FullName from TeamMember_3 " +
                             "where IDteamMember='" + idtm + "'";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    name = (string)Dt.Rows[0]["FullName"];
                }
                return name;
            }


            public static DataTable ListKidsToMessage(string UserNameParents)
            {
                Database Db = new Database();
                string sql = "select IDkids, NameKids + ' ' + LastNameKids FullName, NameFather, PhoneFather, NameClass,NameMother, PhoneMother  from Kids_5 join Class_7 on Kids_5.IDclass=Class_7.IDclass where  Kids_5.IDclass in(select IDclass from Kids_5 where UserNameParents ='" + UserNameParents + "') and IDkids not in(select IDkids from Kids_5 where UserNameParents ='" + UserNameParents + "')";
                return Db.Execute(sql);
            }


            public static DataTable ShowMeMYNewMessage(string user, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDgetNotice in (select IDkids from Kids_5 where UserNameParents='" + user + "') and StatusGetNotice='" + StatusGet + "' ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }

            public static void UpdateMarkTORead(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 2 WHERE StatusGetNotice =1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }


            public static void UpdateToTheTrash(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 3 WHERE StatusGetNotice =1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }


            public static DataTable MyItems(string user)
            {
                Database Db = new Database();
                string sql = "select* from MyMessageBox_16 " +
                    "where IDsender in(select IDkids from Kids_5 where UserNameParents='" + user + "') and StatusSender = 1 " +
                    "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static DataTable MessageReaded(string user, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDgetNotice in(select IDkids from Kids_5 where UserNameParents='" + user + "') and StatusGetNotice='" + StatusGet + "' " +
                                "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }



            public static void UpdateToTheTrash1(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusGetNotice = 3 WHERE StatusGetNotice =2 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }


            public static void UpdateToTheTrash2(int idmassege)
            {
                Database Db = new Database();
                string sql = "UPDATE MyMessageBox_16 SET StatusSender= 3 WHERE StatusSender= 1 and IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }

            public static DataTable ShowMeMYTrashMessage(string user, int StatusGet)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 " +
                             "where IDgetNotice in(select IDkids from Kids_5 where UserNameParents='" + user + "') " +
                             "and StatusGetNotice='" + StatusGet + "' " +
                             "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static DataTable ShowMeMYTrashMessage1(string user, int StatusSender)
            {
                Database Db = new Database();
                string sql = "select * from MyMessageBox_16 where IDsender in(select IDkids from Kids_5 where UserNameParents='" + user + "') AND StatusSender='" + StatusSender + "' " +
                    "ORDER BY convert(datetime, DateSendMessage, 105) desc";
                return Db.Execute(sql);
            }


            public static DataTable viewReport(int IDkids)
            {
                Database Db = new Database();
                string sql = "select IDpresenceReport,IDteamMember, IDclass, CAST (datepart(DAY, DatePresenceReport) as varchar) +'-'+ " +
                                    "CAST(datepart(MONTH, DatePresenceReport)as varchar) +'-'+ " +
                                    "CAST(datepart(YEAR, DatePresenceReport)as varchar) DatePresenceReport,  " +
                    "IDkids, Status, Remarks from PresenceReport_10  pr10 " +
                    "where IDkids='" + IDkids + "' " +
                    "and DatePresenceReport between dateadd(month,datediff(month,0,getdate()),0) " +
                    "and dateadd(day,-1,dateadd(month,datediff(month,-1,getdate()),0)) " +
                    "ORDER BY cast(DatePresenceReport as date) ASC";
                return Db.Execute(sql);
            }



            public static DataTable viewBetweenDateReport(int ID, string From, string To)
            {
                Database Db = new Database();
                string sql = "select IDpresenceReport,IDteamMember,IDclass, " +
                    "CAST(datepart(DAY, DatePresenceReport) as varchar) +'-'+  " +
                    "CAST(datepart(MONTH, DatePresenceReport)as varchar) +'-'+ " +
                    "CAST(datepart(YEAR, DatePresenceReport)as varchar) DatePresenceReport, " +
                    "IDkids,Status,Remarks from PresenceReport_10  " +
                    "where IDkids= '" + ID + "' " +
                    "and DatePresenceReport BETWEEN CONVERT(datetime,'" + From + "') AND CONVERT(datetime,'" + To + "') " +
                    "order by DatePresenceReport asc";
                return Db.Execute(sql);
            }




            public static DataTable AllAdmindByParents(string UserNameParents)
            {
                Database Db = new Database();
                string sql = "select DISTINCT NameAdmin  + ' '  + LastNameAdmin NameAdmin, IDadmin  from Admin_2 a2 join  TeamMember_3 tm3 " +
                    "on a2.IDcompany=tm3.IDcompany join Kids_5 k5 " +
                    "on tm3.IDteamMember=k5.IDteamMember " +
                    "where UserNameParents='" + UserNameParents + "'";

                return Db.Execute(sql);
            }


            public static void TrashForever(int idmassege)
            {
                Database Db = new Database();
                string sql = "DELETE from MyMessageBox_16 WHERE IDmessage='" + idmassege + "'";
                DataTable Dt = Db.Execute(sql);
            }




            public static DataTable UserParents(int Class)
            {
                Database Db = new Database();
                string sql = "select distinct UserNameParents from Kids_5 where IDclass= '" + Class + "'";
                return Db.Execute(sql);
            }


            public static int UnreadMessages(string user)
            {
                int name = 0;
                Database Db = new Database();
                string sql = "select COUNT(IDmessage) as 'count' from MyMessageBox_16 mmb16 " +
                    "join Kids_5 k5 on mmb16.IDgetNotice=k5.IDkids  " +
                    "where UserNameParents='" + user + "' and StatusGetNotice=1  ";
                DataTable Dt = Db.Execute(sql);
                if (Dt.Rows.Count > 0)
                {

                    name = (int)Dt.Rows[0]["count"];
                }
                return name;
            }
        }
    }


}

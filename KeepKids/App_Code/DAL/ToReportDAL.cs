﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using KeepKids.BLL;


namespace KeepKids
{
    namespace DAL
    {
        public class ToReportDAL
        {

            public static DataTable ShowKidsBYclass(int ID, int Class)
            {
                Database Db = new Database();
                string sql = "select distinct k5.IDkids, t3.IDteamMember, NameKids,  LastNameKids,  NameClass, c7.IDclass, PicKids " +
                             "from TeamMember_3 t3 join Kids_5 k5 " +
                             "on t3.IDteamMember = k5.IDteamMember  join Kindergarten_6 k6 " +
                             "on t3.IDteamMember=k6.IDteamMember join Class_7 c7 " +
                             "on k5.IDclass=c7.IDclass " +
                             "where t3.IDteamMember = '" + ID + "' " +
                             "and c7.IDclass='" + Class + "'" +
                              "order by LastNameKids asc ";
                return Db.Execute(sql);
            }




            public static DataTable ShowKidsBYclassAdmin(int ID, int Class)
            {
                Database Db = new Database();
                string sql = "select distinct * from TeamMember_3 t3 " +
                             "join Kids_5 k5 on t3.IDteamMember = k5.IDteamMember " +
                             "join Kindergarten_6 k6 on t3.IDteamMember = k6.IDteamMember " +
                             "join Class_7 c7 on k5.IDclass=c7.IDclass " +
                             "join Admin_2 a2 on t3.IDcompany = a2.IDcompany " +
                             "where a2.IDcompany = '" + ID + "'  " +
                             "and c7.IDclass ='" + Class + "' " +
                             "order by LastNameKids asc";
                return Db.Execute(sql);
            }











            public static DataTable ShowClass(int ID)
            {
                Database Db = new Database();
                string sql = "select c7.NameClass, c7.IDclass from Class_7 c7 join Kindergarten_6 k6 " +
                    "on c7.IDkindergarten=k6.IDkindergarten    join TeamMember_3 tm3 " +
                    "on k6.IDteamMember=tm3.IDteamMember " +
                    "where tm3.IDteamMember='" + ID + "'";
                return Db.Execute(sql);
            }




            public static DataTable ShowClassAdmin(int ID)
            {
                Database Db = new Database();
                string sql = "select c7.NameClass, c7.IDclass from Class_7 c7 " +
                                                "join Kindergarten_6 k6 " +
                                "on c7.IDkindergarten=k6.IDkindergarten " +
                                                "join TeamMember_3 tm3 " +
                                "on k6.IDteamMember=tm3.IDteamMember " +
                                                "join Admin_2 a2 " +
                                "on tm3.IDcompany=a2.IDcompany " +
                                "where k6.IDkindergarten='" + ID + "'";
                return Db.Execute(sql);
            }




            public static DataTable NameKinderGardenMethod(int idcom, int idad)
            {
                Database Db = new Database();
                string sql = "select distinct k6.IDkindergarten, NameKindergarten from Class_7 c7 " +
                    "join Kindergarten_6 k6 on c7.IDkindergarten=k6.IDkindergarten " +
                    "join TeamMember_3 tm3  on k6.IDteamMember=tm3.IDteamMember " +
                    "join Admin_2 a2 on tm3.IDcompany='" + idcom + "' " +
                    "where a2.IDadmin='" + idad + "'";
                return Db.Execute(sql);
            }





            public static bool InsertPresenceReport(int idtm, int idclass, string date, int idk, string status, string remarks)
            {

                Database Db = new Database();

                {
                    string sql = "INSERT INTO PresenceReport_10 ([IDteamMember],[IDclass],[DatePresenceReport],[IDkids], [Status], [Remarks]) " +
                                 "VALUES('" + idtm + "', '" + idclass + "','" + date + "', '" + idk + "','" + status + "', '" + remarks + "')";
                    DataTable Dt = Db.Execute(sql);
                    return true;

                }

            }



            public static DataTable allParentsKinderGarden(int idad)
            {
                Database Db = new Database();
                string sql = "select distinct k6.IDkindergarten, NameKindergarten from Class_7 c7 " +
                    "join Kindergarten_6 k6 on c7.IDkindergarten=k6.IDkindergarten " +
                    "join TeamMember_3 tm3  on k6.IDteamMember=tm3.IDteamMember " +
                    "join Admin_2 a2 on tm3.IDcompany=a2.IDcompany " +
                    "where a2.IDadmin='" + idad + "'";
                return Db.Execute(sql);
            }


















            //public static string ShneorBachar(int[] MyList)
            //{
            //    string Shlomi_Licht = "";
            //    var arr2 = from i in MyList
            //               where i != 0
            //               orderby i
            //               select i;
            //    foreach (int i in arr2)
            //    {
            //        Shlomi_Licht += i;
            //    }
            //    return Shlomi_Licht;
            //}






        }
    }
}


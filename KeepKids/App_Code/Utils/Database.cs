﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace KeepKids
{

    public class Database
    {

        public SqlConnection Conn;
        public string Connstr;
        public Database()
        {
            Connstr = ConfigurationManager.ConnectionStrings["MainConn"].ToString();
            Conn = new SqlConnection(Connstr);
            Open();
            Close();
        }


        public void Open()
        {
            Conn.Open();
        }
        public void Close()
        {
            Conn.Close();
        }
        public SqlDataReader ExecuteReader(string sql)
        {
            SqlCommand Cmd = new SqlCommand(sql, Conn);
            return Cmd.ExecuteReader();
        }

        public DataTable Execute(string sql)
        {
            SqlCommand Cmd = new SqlCommand(sql, Conn);
            DataTable Dt = new DataTable();
            SqlDataAdapter Da = new SqlDataAdapter(Cmd);
            Da.Fill(Dt);

            return Dt;
        }


    }


}
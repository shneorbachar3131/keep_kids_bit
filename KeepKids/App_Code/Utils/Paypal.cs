﻿using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using KeepKids.utils;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Web.UI;
using System.Web;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace KeepKids
{
    public class Paypal
    {
        public Paypal() { }

        public class TokenResponse
        {
            [JsonProperty(PropertyName = "scope")]
            public string Scope { get; set; }

            [JsonProperty(PropertyName = "access_token")]
            public string AccessToken { get; set; }

            [JsonProperty(PropertyName = "token_type")]
            public string TokenType { get; set; }

            [JsonProperty(PropertyName = "app_id")]
            public string AppId { get; set; }

            [JsonProperty(PropertyName = "expires_in")]
            public int ExpiresIn { get; set; }

            [JsonProperty(PropertyName = "nonce")]
            public string Nonce { get; set; }
        }

        public class Amount
        {
            [JsonProperty(PropertyName = "currency_code")]
            public string CurrencyCode { get; set; }

            [JsonProperty(PropertyName = "value")]///
            public string Value { get; set; }
        }

        public class PurchaseUnit
        {
            [JsonProperty(PropertyName = "amount")]
            public Amount Amount { get; set; }
        }

        public class OrdersRequest
        {
            [JsonProperty(PropertyName = "intent")]
            public string Intent { get; set; }

            [JsonProperty(PropertyName = "purchase_units")]
            public PurchaseUnit[] PurchaseUnits { get; set; }
        }

        public static void CreateToken(string val)
        {
            var client = new HttpClient();
            byte[] authBytes = Encoding.ASCII.GetBytes("Ac2WXNDxnfx-R87KtrBm6AJ6uwB6amEtDS0NQyICAB1Qya9R3kE7v-1shFXPXld5GuBaxH2diA3XWQER:EF_WgFamYEyVPXc1wce_xxO2YoiZD7EcDiGvxeVrz0UOU3SEnosKQ0s9XqaoszrkpyA9oBhqLf_2LDdt");
            string base64Auth = Convert.ToBase64String(authBytes);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64Auth);

            var content = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("grant_type", "client_credentials") });
            var response = client.PostAsync(new Uri("https://api.sandbox.paypal.com/v1/oauth2/token"), content).Result;
            if (response.IsSuccessStatusCode)
            {
                var tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(response.Content.ReadAsStringAsync().Result);
                GetClient(tokenResponse.AccessToken, val);
            }
        }

        public static string ConvertToPay(string val)
        {
            var request = new OrdersRequest
            {
                Intent = "CAPTURE",
                PurchaseUnits = new PurchaseUnit[]{ new PurchaseUnit{
                Amount = new Amount
                        {
                            CurrencyCode = "ILS",
                            Value =val
                        }
                    }
                }
            };

            var jsonContent = JsonConvert.SerializeObject(request);
            return jsonContent;
        }

        public static HttpClient GetClient(string token, string val)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);


            var content = new StringContent(ConvertToPay(val), Encoding.ASCII, "application/json");

            var response = client.PostAsync(new Uri("https://api.sandbox.paypal.com/v2/checkout/orders"), content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseString = response.Content.ReadAsStringAsync().Result;
                string OnlyLink = "";
                for (int i = 149; i < responseString.Length; i++)
                {
                    if (i > 150 && i < 217)
                    {
                        OnlyLink += responseString[i];
                    }
                    else if (i > 217)
                    {
                        //Page page = HttpContext.Current.CurrentHandler as Page;
                        //ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "MyScript", "alert('" + OnlyLink + "');", true);
                        HttpContext.Current.Response.Redirect(OnlyLink, true);
                        break;

                    }
                }

            }
            return client;
        }
    }
}

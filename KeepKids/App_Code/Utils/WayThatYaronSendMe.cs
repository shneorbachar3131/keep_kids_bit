﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.UI;

namespace KeepKids
{
    public class WayThatYaronSendMe
    {


        public static class ClientHelper
        {
            // Basic auth
            public static HttpClient GetClient(string username, string password)
            {
                var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}")));
                var client = new HttpClient()
                {
                    DefaultRequestHeaders = { Authorization = authValue }
                    //Set some other client defaults like timeout / BaseAddress
                };
                GetClient("A21AAFJYKnrs0Z507UJB9bpMRvai2qydU1LTabgBsojUFZqZPSjnvkzap4tqMmmNjvzM4B2ncIDBv1hd1V3ht1jiLT-Za9nkQ");
                return client;
            }

            // Auth with bearer token
            public static HttpClient GetClient(string token)
            {
                var authValue = new AuthenticationHeaderValue("Bearer", token);

                var client = new HttpClient()
                {
                    DefaultRequestHeaders = { Authorization = authValue }
                    //Set some other client defaults like timeout / BaseAddress
                };
                return client;
            }
        }
        public static void func()
        {
            HttpClient client = ClientHelper.GetClient("Ac2WXNDxnfx-R87KtrBm6AJ6uwB6amEtDS0NQyICAB1Qya9R3kE7v-1shFXPXld5GuBaxH2diA3XWQER", "EF_WgFamYEyVPXc1wce_xxO2YoiZD7EcDiGvxeVrz0UOU3SEnosKQ0s9XqaoszrkpyA9oBhqLf_2LDdt");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var requestParams = new List<KeyValuePair<string, string>>
                {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
                };

            var content = new FormUrlEncodedContent(requestParams);
            // קריאב אסינכרונית בפוסט
            var response = client.PostAsync(new Uri("https://api.sandbox.paypal.com/v1/oauth2/token"), content).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                // by calling .Result you are synchronously reading the result
                string responseString = responseContent.ReadAsStringAsync().Result;


                Page page = HttpContext.Current.CurrentHandler as Page;
                ScriptManager.RegisterClientScriptBlock(page, typeof(Page), "MyScript", "alert('" + responseString + "');", true);

            }
        }


    }
}
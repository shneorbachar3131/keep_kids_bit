﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using KeepKids.BLL;


namespace KeepKids
{
    public class AdminsController : ApiController
    {
        // GET api/<controller>
        public string Get()
        {
            List<Admins> Lst = Admins.GetAll();
            return JsonConvert.SerializeObject(Lst);

        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            List<Admins> Lst = Admins.GetById(id);
            return JsonConvert.SerializeObject(Lst);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
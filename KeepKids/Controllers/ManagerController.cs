﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using KeepKids.BLL;

namespace KeepKids
{
    public class ManagerController : ApiController
    {
        // GET api/<controller>
        public string Get()
        {
            List<Manager> Lst = Manager.GetAll();
            return JsonConvert.SerializeObject(Lst);

        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            List<Manager> Lst = Manager.GetById(id);
            return JsonConvert.SerializeObject(Lst);
        }

        // POST api/<controller>
        public string Post([FromBody]Manager Mng)
        {
            Mng.CheckRegister();
            return JsonConvert.SerializeObject(Mng);

        }

        // PUT api/<controller>/5
        public string Put(int id, [FromBody] Manager Mng1)
        {

            Mng1.CheckUpdate();
            return JsonConvert.SerializeObject(Mng1);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {

            Manager.Delete(id);

        }
    }
}

//תבנית גייסון
//{"IDteamMember":271,
//"IDcompany":1000,
//"FullName":"ffff",
//"nameTeamMember":"FFF",
//"FamilyTeamMember":"FF",
//"PhoneTeamMember":"0987654367",
//"UserNameTeamMember":"ss@gmail.com",
//"PasswordTeamMember":"111111",
//"PicManager":"aaa.jpg"}


//  החזרה של תבנית גייסון
// "{\"IDteamMember\":271,
// \"IDcompany\":1000,
// \"IDjob\":null,\
// "FullName\":\"ffff\",
// \"nameTeamMember\":\"FFF\",
// \"FamilyTeamMember\":\"FF\",
//\"PhoneTeamMember\":\"0987654367\",
// \"UserNameTeamMember\":\"ssF@gmail.com\",
// \"PasswordTeamMember\":\"111111\",
// \"PasswordTMrecovery\":null,
// \"PicManager\":\"aaaF.jpg\",
// \"NameCompany\":null,
// \"DateEventUnusual\":null,
// \"DescriptionEvent\":null,
// \"IDkids\":0,\"NameKids\":null,
// \"PicKids\":null,\"NameFather\":null,
// \"NameMother\":null,\"PhoneFather\":null,
// \"PhoneMother\":null,\"Report\":null,
// \"DateReportPersonal\":null,
// \"IDsend\":0,
// \"ContentMessage\":null,
// \"NameKindergarten\":null}"
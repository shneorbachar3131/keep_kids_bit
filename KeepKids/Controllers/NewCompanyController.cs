﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using KeepKids.BLL;

namespace KeepKids.Controllers
{
    public class NewCompanyController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            int[] tot = new int[2];
            tot = Admins.GetCountClassByCompany(id);
            return JsonConvert.SerializeObject(tot);
        }

        // POST api/<controller>
        public string Post([FromBody]Admins DetailsCompany)
        {
            DetailsCompany.InsertCompany();
            return JsonConvert.SerializeObject(DetailsCompany);

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
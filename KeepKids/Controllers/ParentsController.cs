﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using KeepKids.BLL;
using System.Web.UI;
using KeepKids.DAL;

namespace KeepKids
{
    public class ParentsController : ApiController
    {
        // GET api/<controller>
        public string Get()
        {
            List<Parents> Lst = Parents.GetAll();
            return JsonConvert.SerializeObject(Lst);

        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            List<Parents> Lst = Parents.GetById(id);
            return JsonConvert.SerializeObject(Lst);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {

        }

        // PUT api/<controller>/5
        public string Put(int id, [FromBody]Parents Prns)
        {
            Prns.CheckUpdateSpecific();
            return JsonConvert.SerializeObject(Prns);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            ParentsDAL.Delete(id);
        }
    }
}
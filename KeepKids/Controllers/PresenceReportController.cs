﻿using KeepKids.BLL;
using KeepKids.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KeepKids.Controllers
{
    public class PresenceReportController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            List<Admins> Lst = Admins.GetPresenceReport(id);
            return JsonConvert.SerializeObject(Lst);
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public string Put(int id, [FromBody]Admins admn)
        {
            admn.CheckUpdateSpecific();
            return JsonConvert.SerializeObject(admn);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            AdminsDAL.Delete(id);
        }
    }
}
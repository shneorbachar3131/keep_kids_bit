﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterOfComany.aspx.cs" Inherits="KeepKids.RegisterOfComany" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard." />
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard" />
    <meta name="author" content="PIXINVENT" />
    <title>רשימת חברות</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png" />
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet" />

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors-rtl.min.css" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/bootstrap-extended.css" />
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/colors.css" />
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/components.css" />
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/custom-rtl.css" />
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/menu/menu-types/vertical-menu-modern.css" />
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/colors/palette-gradient.css" />
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style-rtl.css" />
    <script src="Admin/js/RegisterOfCompany.js"></script>
    <!-- END: Custom CSS-->
    <style>
        @keyframes offset {
            to {
                stroke-dashoffset: 0;
            }
        }

        @keyframes fill-blue {
            to {
                fill: #007dba;
                stroke: transparent;
            }
        }

        @keyframes fill-green {
            to {
                fill: #78be20;
                stroke: transparent;
            }
        }

        path {
            stroke-width: 2;
            stroke-dasharray: 3212;
            stroke-dashoffset: 3212;
            fill: transparent;
        }

        .blue {
            stroke: #007dba;
            animation: offset 10s linear forwards, fill-blue 1s 1s forwards;
        }

        .green {
            stroke: #ff6a00;
            animation: offset 10s linear forwards, fill-green 1s 1s forwards;
        }


        svg {
            margin: auto;
            display: block;
            width: 35%;
        }



        /*** Keyframes ***/

        @keyframes offset {
            100% {
                stroke-dashoffset: 0;
            }
        }

        @keyframes fill-it {
            100% {
                fill: #A167A5;
            }
        }



        /*** SVG Styles ***/

        .logo {
            stroke: #A167A5;
            stroke-width: 2;
            stroke-dasharray: 3212.015;
            stroke-dashoffset: 3212.015;
            animation: offset 10s linear forwards, fill-it 2s 8s forwards;
        }
    </style>

    <style>
        svg {
            overflow: visible;
        }



        #a, #b, #c, #d, #e, #f, #g, #h, #i, #j, #k, #l, #m {
            stroke: black;
            stroke-width: 1px;
            fill: transparent;
        }

        #a {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: a-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #b {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: b-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #c {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: c-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #d {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: d-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #e {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: e-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #f {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: f-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #g {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: g-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #h {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: h-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #i {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: i-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #j {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: j-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #k {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: k-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #l {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: l-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #m {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: m-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        @keyframes a-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes b-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes c-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes d-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes e-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes f-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes g-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes h-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes i-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes j-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes k-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes l-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes m-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }
    </style>

</head>
<body>
    <form runat="server">
        <div class="app-content content">
            <div class="content-wrapper">
                <div class="content-body">
                    <!-- Basic form layout section start -->
                    <div class="card">
                        <div class="row">
                            <div class="col-6">
                                <svg width="600" height="140">
                                    <path class="green" d="M5.2 53.7v.4c-.1 4.5 1.4 8.5 4.6 11.7 0 0 10 14.7 62.2 17 31.9 1.4 95.6-8.6 95.6-8.6l14.7-22.7s-79.4 8.6-109.8 8.6c-47.4 0-56.3-13.9-56.3-13.9-4.9-4.9-4.5-9.9-4.5-9.9-2.6 3.4-6.3 8.9-6.5 17.4" />
                                    <path class="green" d="M75.7 91.5c-21.6 0-36.6-3.7-46.4-5.9-10-2.2-17.1 2.7-19.5 5-3.3 3.1-4.7 7.1-4.6 11.7v.4c.1 8.4 3.9 14 6.4 17.5 0 0-.4-5 4.5-9.9 0 0 6.2-6.4 12.7-4 10.8 4.2 45.8 6.6 45.8 6.6 35 0 73.3-7.9 73.3-7.9l14.7-22.7c.1-.1-45.6 9.2-86.9 9.2" />
                                </svg>
                            </div>
                            <div class="col-6">
                                <svg width="600" height="140">
                                    <path class="green" d="M5.2 53.7v.4c-.1 4.5 1.4 8.5 4.6 11.7 0 0 10 14.7 62.2 17 31.9 1.4 95.6-8.6 95.6-8.6l14.7-22.7s-79.4 8.6-109.8 8.6c-47.4 0-56.3-13.9-56.3-13.9-4.9-4.9-4.5-9.9-4.5-9.9-2.6 3.4-6.3 8.9-6.5 17.4" />
                                    <path class="green" d="M75.7 91.5c-21.6 0-36.6-3.7-46.4-5.9-10-2.2-17.1 2.7-19.5 5-3.3 3.1-4.7 7.1-4.6 11.7v.4c.1 8.4 3.9 14 6.4 17.5 0 0-.4-5 4.5-9.9 0 0 6.2-6.4 12.7-4 10.8 4.2 45.8 6.6 45.8 6.6 35 0 73.3-7.9 73.3-7.9l14.7-22.7c.1-.1-45.6 9.2-86.9 9.2" />
                                </svg>
                            </div>

                        </div>



                        <h1 class="display-1 text-center">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 136" xml:space="preserve">
                                <path id="a" d="M514.33 38.7l-.84 8.26h-53.9c-5.69 0-10.76-1.31-15.2-3.92l-1.46-14.28 3.64-.98c1.03 5.13 4.53 7.7 10.5 7.7h45.36c4.48 0 8.45 1.08 11.9 3.22zm1.4 57.01c-.37 2.88-1.12 5.77-2.24 8.65-2.24-3.92-5.39-9.99-9.45-18.2-2.2-5.23-3.29-8.35-3.29-9.38v-2.94c.09-1.31.14-2.29.14-2.94 0-5.32-1.68-7.98-5.03-7.98h-35.51c-8.39 0-13.24-1.17-14.54-3.5l-.98-11.34c3.54 2.24 8.42 3.36 14.65 3.36h53.32l-.84 9.94c-2.99 0-4.48 3.21-4.48 9.63-.01 8.75 2.74 16.98 8.25 24.7zm-57.69 13.69c-1.52 2.8-3.6 5.6-6.25 8.4h-2.56c-1.49-4.67-2.24-11.48-2.24-20.44 0-3.17.24-6.95.71-11.34 3.02 8.4 6.47 16.2 10.34 23.38zm4.31-13.86c-.38 3.27-1.18 6.53-2.4 9.8-5.74-8.87-9.64-16.38-11.71-22.54.37-2.33 1.12-5.69 2.24-10.08l6.16.14-.7 3.36c.01 8.59 2.14 15.03 6.41 19.32zm49.6 13.02c-1.96 3.83-3.87 7.14-5.74 9.94l-2.52-.56c-2.33-6.81-3.5-14.75-3.5-23.8 0-.56.09-5.32.28-14.28 2.61 10.83 6.44 20.4 11.48 28.7z" />
                                <path id="b" d="M547.68 43.93c0 .37-.05 1.48-.14 3.34-.75-.28-1.96-.42-3.65-.42h-13.18c-2.34 0-4.16-.33-5.47-.99l-.98-15.26 3.92-1.82c.74 4.46 2.27 6.69 4.59 6.69h6.27c3.06 0 5.27.6 6.61 1.81 1.36 1.18 2.03 3.4 2.03 6.65zm61.29 12.83c0 8.96-3.31 20.67-9.94 35.14-7.75 6.44-18.29 9.66-31.64 9.66-11.95 0-21.09-2.57-27.44-7.7-.37-.93-.98-5.16-1.82-12.7-1.49-13.3-5.51-20.47-12.04-21.49l-.56-9.07c1.12.56 2.57.84 4.34.84h11.07c3.46 0 5.56.14 6.31.42-.28 3.28-.84 6.75-1.67 10.4l-2.64-.6c-1.39 0-2.09 1.03-2.09 3.08 0 11.4 3.05 18.92 9.14 22.56 4.13-11.72 6.19-22.05 6.19-30.98 0-.37-.05-2.33-.14-5.86 1.11.65 3.06.98 5.85.98h16.3c-.47 2.55-1.17 6.42-2.1 11.61-2.06-.84-3.97-1.25-5.75-1.25-1.5 0-3.5.33-6.03.98l-10.51 26.6c2.99 1.59 7.53 2.38 13.62 2.38 6.27 0 12.08-.88 17.41-2.63 6.74-2.13 11.46-5.36 14.18-9.71 1.59-2.68 2.39-5.13 2.39-7.35 0-4.25-1.74-7.07-5.22-8.46-1.98-.74-5.56-1.11-10.73-1.11l-.42-11.06h16.24c3.36 0 5.92 1.78 7.7 5.32zm-12.46 41.81c-7.57 13.48-17.85 20.21-30.84 20.21-9.53 0-16.54-3.39-21.03-10.17-1.4-2.17-2.57-4.9-3.5-8.19 4.95 3.75 13.73 5.62 26.35 5.62 12.34 0 22.01-2.49 29.02-7.47zm-18.03-51.75h-17.1c-2.71 0-4.63-.46-5.75-1.39l-1.82-15.41 4.06-1.54c.93 4.64 3.67 6.95 8.22 6.95h6.27c2.88 0 4.73 1.25 5.57 3.75.36 1.31.55 3.85.55 7.64zm30.77 3.92c-1.59-2.61-4.2-3.92-7.84-3.92h-16.52l-.56-15.68 4.06-.84c0 3.45 1.49 5.18 4.48 5.18h4.48c7.93 0 11.9 5.09 11.9 15.26z" />
                                <path id="c" d="M681.21 38.42l-1.12 8.54h-48.58c-3.73 0-7.82-.7-12.25-2.1l-2.03-15.82 3.92-1.4c.75 5.23 4.2 7.84 10.36 7.84h36.82c4.38 0 8.68.98 12.88 2.94zm-1.68 13.02l-.84 7.28c-3.83.66-6.21 1.61-7.14 2.88-.93 1.26-1.4 3.77-1.4 7.51 0 10.96 2.75 19.71 8.26 26.26-.56 3.56-1.4 6.84-2.52 9.83-3.36-5.69-5.69-9.85-6.97-12.46-2.29-4.48-4.12-8.87-5.49-13.16.19-3.27.28-6.63.28-10.08 0-2.24-.19-3.78-.56-4.62-.66-1.31-1.96-1.96-3.92-1.96h-30.56c-2.71 0-5.33-1.35-7.85-4.06l-1.12-9.52c4.19 1.4 8.1 2.1 11.74 2.1h48.09zm-5.32 57.96c-1.59 3.27-3.45 6.11-5.6 8.54h-2.1c-2.52-7.37-3.78-14.28-3.78-20.72 0-.93.19-5.55.56-13.86 2.14 7.66 5.78 16.34 10.92 26.04z" />
                                <path id="d" d="M755.83 95.68c-.37 3.27-1.07 6.39-2.1 9.38-4.17-6.44-7.81-13.44-10.92-21 .28-6.72.42-10.31.42-10.78 0-6.91-2.66-10.36-7.98-10.36h-33.32c-2.15 3.73-3.22 8.12-3.22 13.16 0 5.88 1.91 12.41 5.74 19.6 0 2.99-.66 6.35-1.98 10.08-4.72-7.09-8.54-14.23-11.46-21.42 1.12-6.72 1.68-11.25 1.68-13.58 0-5.23-1.26-8.82-3.78-10.78l.28-10.08c2.52 1.03 5.93 1.54 10.22 1.54h40.21c4.75 0 8.8.93 12.15 2.8-1.4 8.49-2.1 15.63-2.1 21.42 0 6.72 2.05 13.4 6.16 20.02zm-2.94-49.56l-.56 3.64c-3.73-1.87-7.98-2.8-12.74-2.8h-40.32c-4.01 0-7.33-.61-9.94-1.82l.56-15.96 4.06-.98c2.05 4.85 5.65 7.28 10.78 7.28h37.1c7.37 0 11.06 2.8 11.06 8.4v2.24zm-52.22 63.98c-.57 1.4-2.05 4.2-4.43 8.4h-2.85c-2.24-4.76-3.36-12.18-3.36-22.26 0-2.05.19-4.81.56-8.26 3.1 8.87 6.46 16.24 10.08 22.12zm51.38-.56c-.83 2.24-2.16 5.23-4.01 8.96h-3.18c-2.03-5.88-3.04-12.23-3.04-19.04 0-.93.23-4.76.69-11.48 3.04 8.59 6.22 15.78 9.54 21.56z" />
                                <path id="e" d="M876.78 38.7l-.84 8.26h-53.9c-5.69 0-10.76-1.31-15.2-3.92l-1.46-14.28 3.64-.98c1.03 5.13 4.53 7.7 10.5 7.7h45.36c4.48 0 8.45 1.08 11.9 3.22zm1.4 57.01c-.37 2.88-1.12 5.77-2.24 8.65-2.24-3.92-5.39-9.99-9.45-18.2-2.2-5.23-3.29-8.35-3.29-9.38v-2.94c.09-1.31.14-2.29.14-2.94 0-5.32-1.68-7.98-5.03-7.98H822.8c-8.39 0-13.24-1.17-14.54-3.5l-.98-11.34c3.54 2.24 8.42 3.36 14.65 3.36h53.32l-.84 9.94c-2.99 0-4.48 3.21-4.48 9.63-.01 8.75 2.75 16.98 8.25 24.7zM820.5 109.4c-1.52 2.8-3.6 5.6-6.25 8.4h-2.56c-1.49-4.67-2.24-11.48-2.24-20.44 0-3.17.24-6.95.71-11.34 3.01 8.4 6.46 16.2 10.34 23.38zm4.31-13.86c-.38 3.27-1.18 6.53-2.4 9.8-5.74-8.87-9.64-16.38-11.71-22.54.37-2.33 1.12-5.69 2.24-10.08l6.16.14-.7 3.36c0 8.59 2.14 15.03 6.41 19.32zm49.59 13.02c-1.96 3.83-3.87 7.14-5.74 9.94l-2.52-.56c-2.33-6.81-3.5-14.75-3.5-23.8 0-.56.09-5.32.28-14.28 2.62 10.83 6.44 20.4 11.48 28.7z" />
                                <path id="f" d="M942.12 51.16c-2.44-2.8-6.89-4.2-13.36-4.2h-31.78c-4.59 0-7.64-.93-9.14-2.8l-1.12-13.86 4.34-.98c.56 4.11 3.37 6.16 8.42 6.16h27.63c7.1 0 11.78 2.57 14.03 7.7.65 1.5.98 4.16.98 7.98zm5.5 44.52c-.56 3.45-1.39 6.72-2.49 9.8-4.16-5.79-7.71-12.93-10.67-21.42.28-5.97.42-9.29.42-9.94 0-4.11-.52-6.91-1.54-8.4-1.4-1.87-4.17-2.8-8.28-2.8h-27.52c-4.12 0-7.16-1.63-9.13-4.9l-.42-9.38c1.4 1.87 4.41 2.8 9 2.8h31.78c6.47 0 10.97 1.4 13.5 4.2 0 1.77-.18 4.41-.54 7.91-.36 3.5-.54 6.14-.54 7.91-.01 8.78 2.14 16.85 6.43 24.22zm-4.16 14c-1.5 3.17-3.38 6.07-5.63 8.68h-3.38c-1.03-4.48-1.54-9.15-1.54-14 0-3.64.37-9.01 1.12-16.1 2.07 6.63 5.22 13.77 9.43 21.42z" />
                                <path id="g" d="M1022.52 101.28l-2.23 11.62c-5.31 2.24-10.57 3.36-15.78 3.36h-34.49c-4.67 0-8.21 1.82-10.64 5.46l-2.94-1.4 3.08-13.02c3.83-2.71 8.31-4.06 13.44-4.06h31.64c6.95 0 12.92-.65 17.92-1.96zm1.96-10.5l-1.26 5.32c-4.81 1.77-11.02 2.66-18.62 2.66h-31.64c-4.29 0-8.49 1.12-12.6 3.36 1.68-5.88 2.8-9.24 3.36-10.08 1.49-2.15 4.57-3.22 9.24-3.22h30.52c2.15 0 3.55-.14 4.2-.42 1.59-4.01 2.38-8.68 2.38-14 0-7.65-3.37-11.48-10.11-11.48h-23.03c-6.37 0-10.63-1.68-12.78-5.04l-.56-8.96c3 1.68 7.07 2.52 12.22 2.52h29.08c5.12 0 9.22 1.35 12.3 4.06l-.82 25.76c0 2.33.8 4.43 2.41 6.3s3.51 2.94 5.71 3.22zm-7.02-45.22c0 .37-.05 1.77-.14 4.2-2.9-1.87-7.06-2.8-12.49-2.8h-29.04c-4.77 0-8.89-1.07-12.35-3.22l-.84-13.16 4.48-1.26c1.03 4.11 4.24 6.16 9.66 6.16h31.49c6.15 0 9.23 3.36 9.23 10.08z" />
                                <path id="h" d="M1100.78 95.68c-.37 3.27-1.07 6.39-2.1 9.38-4.17-6.44-7.81-13.44-10.92-21 .28-6.72.42-10.31.42-10.78 0-6.91-2.66-10.36-7.98-10.36h-33.32c-2.15 3.73-3.22 8.12-3.22 13.16 0 5.88 1.91 12.41 5.74 19.6 0 2.99-.66 6.35-1.98 10.08-4.72-7.09-8.54-14.23-11.46-21.42 1.12-6.72 1.68-11.25 1.68-13.58 0-5.23-1.26-8.82-3.78-10.78l.28-10.08c2.52 1.03 5.93 1.54 10.22 1.54h40.21c4.75 0 8.8.93 12.15 2.8-1.4 8.49-2.1 15.63-2.1 21.42 0 6.72 2.05 13.4 6.16 20.02zm-2.94-49.56l-.56 3.64c-3.73-1.87-7.98-2.8-12.74-2.8h-40.32c-4.01 0-7.33-.61-9.94-1.82l.56-15.96 4.06-.98c2.05 4.85 5.65 7.28 10.78 7.28h37.1c7.37 0 11.06 2.8 11.06 8.4v2.24zm-52.22 63.98c-.57 1.4-2.05 4.2-4.43 8.4h-2.85c-2.24-4.76-3.36-12.18-3.36-22.26 0-2.05.19-4.81.56-8.26 3.1 8.87 6.46 16.24 10.08 22.12zm51.38-.56c-.83 2.24-2.16 5.23-4.01 8.96h-3.18c-2.03-5.88-3.04-12.23-3.04-19.04 0-.93.23-4.76.69-11.48 3.05 8.59 6.22 15.78 9.54 21.56z" />
                                <path id="i" d="M1176.8 100.16l-.56 6.16c0 .75-.14 1.87-.42 3.36-1.31 4.11-5.27 6.25-11.9 6.44l-4.34.14c-2.24.09-4.2 1.54-5.88 4.34l-3.36-1.54 2.8-13.16c2.24-1.12 5.23-1.87 8.96-2.24 3.17-.37 6.3-.7 9.38-.98 1.4-.18 3.17-1.02 5.32-2.52zm51.52-4.48c-.28 2.71-1.11 5.97-2.49 9.8-1.02-1.59-1.94-3.03-2.77-4.34-.83-1.31-2.21-4.13-4.15-8.47-1.94-4.34-2.91-7.02-2.91-8.05 0-1.21.09-2.99.28-5.32.19-2.33.28-4.11.28-5.32 0-7.37-2.19-11.06-6.58-11.06h-36.96c-1.03 2.99-1.54 6.53-1.54 10.64 0 9.52 2.15 14.89 6.44 16.1l-.69 5.46c-2.49 1.96-6.23 3.22-11.21 3.78-6.16.65-10.17 1.35-12.04 2.1 1.4-5.6 2.45-8.73 3.15-9.38.7-.65 3.99-1.59 9.87-2.8l-1.82-18.2c0-.75-.09-1.91-.28-3.5-.65-5.51-3.27-9.1-7.84-10.78.93-2.99 1.59-6.11 1.96-9.38 5.22 2.99 10.44 4.48 15.65 4.48h38.39c4.87 0 8.67 1.59 11.39 4.76-1.16 6.63-1.73 11.81-1.73 15.54 0 9.06 1.86 17.04 5.6 23.94zm-2.54-49.98c0 1.31-.28 3.36-.84 6.16-2.8-3.27-6.77-4.9-11.9-4.9h-38.5c-5.32 0-10.5-1.54-15.54-4.62v-4.2c0-3.92-.56-7.7-1.68-11.34l4.9-1.82c.93 2.61 2.87 5.02 5.81 7.21 2.94 2.19 5.76 3.29 8.47 3.29h36.54c8.5 0 12.74 3.41 12.74 10.22zm-1.46 63.98l-4.37 8.82-3.39-.14c-1.31-5.41-1.96-11.15-1.96-17.22 0-2.15.28-6.16.84-12.04 2.44 8.22 5.4 15.08 8.88 20.58z" />
                                <path id="j" d="M1298.79 101.28l-1.96 7.84c-5.23 4.76-12.23 7.14-21 7.14h-23.52c-5.88 0-9.8 1.54-11.76 4.62l-2.94-1.4 2.94-13.86c4.48-1.59 8.21-2.38 11.2-2.38h36.68c3.54 0 7-.65 10.36-1.96zm4.15-28.28c0 7.75-.05 11.95-.14 12.6l-3.09 10.78c-4.12 1.59-7.86 2.38-11.23 2.38h-36.76c-3.65 0-7.16.7-10.52 2.1.37-1.31 1.03-3.17 1.96-5.6 1.96-4.29 5.88-6.44 11.76-6.44h30.1c4.2 0 8.03-.65 11.48-1.96 0-15.96-4.51-23.94-13.54-23.94h-29.59c-3.07 0-4.89 2.1-5.44 6.3.93.93 2.09 1.4 3.49 1.4.65 0 1.7-.05 3.14-.14 1.44-.09 2.49-.14 3.14-.14 3.07 0 5.72.83 7.96 2.49l-3.37 10.53c-3.28-1.49-6.18-2.24-8.71-2.24-4.78 0-8.81 1.82-12.09 5.46l-2.94-3.78c3.08-2.88 4.62-7.44 4.62-13.66 0-5.3-1.59-9.29-4.76-11.99 1.21-2.6 2.24-5.11 3.08-7.53 6.27 1.21 9.63 1.82 10.1 1.82h39.71c3.83 0 7.25 1.12 10.24 3.36.93 4.29 1.4 9.57 1.4 15.82V73zm-2.58-23.8c-2.71-1.49-5.75-2.24-9.11-2.24h-39.68c-1.22 0-2.76-.23-4.63-.7-2.62-.56-4.11-.89-4.49-.98 1.59-5.41 2.38-10.64 2.38-15.68l4.63-1.82c.84 3.17 2.2 5.32 4.06 6.44 1.5.84 3.97 1.26 7.43 1.26h18.23c5.61 0 10.12 1.03 13.53 3.08 3.42 2.06 5.97 5.6 7.65 10.64z" />
                                <path id="k" d="M1380.35 57.02l-7.14 35.72c-7.82 4.95-16.43 7.42-25.83 7.42-8.56 0-16.57-1.63-24.02-4.88l-2.23-21.07c-.19-1.95-.43-3.97-.71-6.07-.28-2.09-.95-3.93-2-5.51-1.81-2.79-4.22-4.37-7.23-4.74l2.24-9.38c5.41 1.96 10.69 2.94 15.82 2.94h39.34c3.55-.01 7.47 1.86 11.76 5.57zm.28-5.72c-3.55-2.89-7.51-4.34-11.9-4.34h-39.34c-5.13 0-10.27-.93-15.4-2.8 1.12-4.95 1.69-9.15 1.69-12.6 0-.56-.1-1.31-.29-2.24l4.34-1.68c.75 3.27 2.47 5.46 5.18 6.58 1.96.84 4.9 1.26 8.82 1.26h29.96c6.07 0 10.41 1.17 13.02 3.5 2.61 2.34 3.92 6.44 3.92 12.32zm-7.98 46.44c-3.07 13.28-12.64 19.93-28.71 19.93-12.36 0-19-4.95-19.93-14.84l-.28-2.94c7.06 3.18 14.96 4.76 23.7 4.76 8.63-.01 17.04-2.31 25.22-6.91zm-.42-25.2c0-2.69-.74-4.97-2.23-6.83-1.49-1.86-3.57-2.79-6.26-2.79h-37.02c-.37 6.6-.56 10.22-.56 10.87 0 6.59 1.85 11.28 5.57 14.07 3.25 2.32 8.3 3.48 15.17 3.48 5.66 0 10.07-.6 13.22-1.81 4.17-1.58 7.42-4.55 9.74-8.92 1.58-2.87 2.37-5.56 2.37-8.07z" />
                                <path id="l" d="M1421.93 44.44c0 1.96-.14 3.41-.42 4.34-1.96-1.21-4.9-1.82-8.82-1.82h-12.18c-4.01 0-7.14-.61-9.38-1.82l-2.38-15.82 3.92-.7c1.77 4.57 4.48 6.86 8.12 6.86h12.88c5.51 0 8.26 2.99 8.26 8.96zm3.64 51.1c0 3.08-.79 6.53-2.38 10.36a139.17 139.17 0 01-11.62-21.84c0-1.21.09-3.08.28-5.6.19-2.52.28-4.39.28-5.6 0-4.48-.19-7.26-.56-8.33-.37-1.07-1.39-1.61-3.06-1.61h-7.93c-3.89 0-6.54-1.35-7.92-4.06l-1.11-9.24c2.43 1.21 5.09 1.82 7.98 1.82h12.6c3.64 0 6.58.56 8.82 1.68-.37 1.77-.84 4.48-1.4 8.12-.75 6.16-1.12 10.36-1.12 12.6 0 8.12 2.38 15.36 7.14 21.7zm-4.48 14.14c-1.59 2.71-3.78 5.41-6.58 8.12l-2.38.14a74.236 74.236 0 01-1.81-16.24c0-3.83.32-8.45.97-13.86 2.15 6.72 5.41 14 9.8 21.84z" />
                                <path id="m" d="M1504.39 38.7l-.84 8.26h-53.9c-5.69 0-10.76-1.31-15.2-3.92l-1.46-14.28 3.64-.98c1.03 5.13 4.53 7.7 10.5 7.7h45.36c4.48 0 8.45 1.08 11.9 3.22zm1.4 57.01c-.37 2.88-1.12 5.77-2.24 8.65-2.24-3.92-5.39-9.99-9.45-18.2-2.2-5.23-3.29-8.35-3.29-9.38v-2.94c.09-1.31.14-2.29.14-2.94 0-5.32-1.68-7.98-5.03-7.98h-35.51c-8.39 0-13.24-1.17-14.54-3.5l-.98-11.34c3.54 2.24 8.42 3.36 14.65 3.36h53.32l-.84 9.94c-2.99 0-4.48 3.21-4.48 9.63-.01 8.75 2.74 16.98 8.25 24.7zm-57.69 13.69c-1.52 2.8-3.6 5.6-6.25 8.4h-2.56c-1.49-4.67-2.24-11.48-2.24-20.44 0-3.17.24-6.95.71-11.34 3.02 8.4 6.47 16.2 10.34 23.38zm4.31-13.86c-.38 3.27-1.18 6.53-2.4 9.8-5.74-8.87-9.64-16.38-11.71-22.54.37-2.33 1.12-5.69 2.24-10.08l6.16.14-.7 3.36c.01 8.59 2.14 15.03 6.41 19.32zm49.6 13.02c-1.96 3.83-3.87 7.14-5.74 9.94l-2.52-.56c-2.33-6.81-3.5-14.75-3.5-23.8 0-.56.09-5.32.28-14.28 2.61 10.83 6.44 20.4 11.48 28.7z" />
                            </svg>
                        </h1>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="form-body">
                                        <h4 class="form-section"><i class="la la-group"></i>פרטי חברה</h4>
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="projectinput1">שם החברה</label>
                                            <div class="col-md-9 mx-auto">
                                                <input type="text" id="NameOfCompany" class="form-control" placeholder="שם החברה" runat="server" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="projectinput2">כתובת החברה</label>
                                            <div class="col-md-9 mx-auto">
                                                <input type="text" id="AdressOfCompany" class="form-control" placeholder="דרך נמיר 5, תל אביב" runat="server" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="projectinput3">מייל חברה</label>
                                            <div class="col-md-9 mx-auto">
                                                <input type="email" id="MailOfCompany" class="form-control" placeholder="מייל חברה" runat="server" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="projectinput3">מספר כיתות</label>
                                            <div class="col-md-9 mx-auto">
                                                <select name="SumOfClass" id="SumOfClass" class="form-control" runat="server">
                                                    <option value="250">1</option>
                                                    <option value="500">2</option>
                                                    <option value="750">3</option>
                                                    <option value="1000">4</option>
                                                    <option value="1250">5</option>
                                                    <option value="1500">6</option>
                                                    <option value="1750">7</option>
                                                    <option value="2000">8</option>
                                                    <option value="2250">9</option>
                                                    <option value="2500">10</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="button" class="btn btn-warning mr-1" onclick="reload()">
                                            <i class="ft-x"></i>בטל
                                               
                                        </button>
                                        <button type="button" class="btn btn-primary" onclick="LoadData()" data-toggle="modal" data-target="#inlineForm100">
                                            <i class="la la-paypal"></i>עבור לתשלום
                                               
                                        </button>
                                        <div class="modalTOhere">
                                            <div class='modal fade text-left' id='inlineForm100' tabindex='-1' role='dialog' aria-labelledby='myModalLabel3333' aria-hidden='true'>
                                                <div class='modal-dialog' role='document'>
                                                    <div class='modal-content'>
                                                        <div class='modal-header'>
                                                            <label class='modal-title text-text-bold-600' id='myModalLabel3333'>העברה לתשלום</label>
                                                            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>

                                                        </div>
                                                        <div class='modal-body'>

                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <input type='hidden' name='on0' value='כמות כיתות' />כמות כיתות</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <select id='how'>
                                                                            <option value='250'>כיתה אחת ₪250.00 ILS</option>
                                                                            <option value='500'>שני כיתות ₪500.00 ILS</option>
                                                                            <option value='750'>שלוש כיתות ₪750.00 ILS</option>
                                                                            <option value='1000'>ארבע כיתות ₪1,000.00 ILS</option>
                                                                            <option value='1250'>חמש כיתות ₪1,250.00 ILS</option>
                                                                            <option value='1500'>שש כיתות ₪1,500.00 ILS</option>
                                                                            <option value='1750'>שבע כיתות ₪1,750.00 ILS</option>
                                                                            <option value='2000'>שמונה כיתות ₪2,000.00 ILS</option>
                                                                            <option value='2250'>תשע כיתות ₪2,250.00 ILS</option>
                                                                            <option value='2500'>עשר כיתות ₪2,500.00 ILS</option>
                                                                        </select></td>
                                                                </tr>
                                                            </table>

                                                            <asp:LinkButton ID="Conect" OnClick="Conect_Click" CssClass="btn btn-outline-success btn-lg" runat="server">המשך לתשלום</asp:LinkButton>


                                                        </div>
                                                        <div class='modal-footer'>
                                                            <input type='reset' class='btn btn-outline-secondary btn-lg' data-dismiss='modal' value='בטל' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- // Basic form layout section end -->
                </div>
            </div>
            <div class="sidenav-overlay"></div>
            <div class="drag-target"></div>
        </div>
    </form>
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT</a></span><span class="float-md-right d-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i><span id="scroll-top"></span></span></p>
    </footer>
    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

</body>
</html>


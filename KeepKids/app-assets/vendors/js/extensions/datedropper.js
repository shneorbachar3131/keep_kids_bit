jQuery.easing._dd_easing = function (d, a, i, s, e) {
    return -s * ((a = a / e - 1) * a * a * a - 1) + i
},
    function (d) {
        d.fn.dateDropper = function (a) {
            return d(this).each(function () {
                if (d(this).is("input") && "text" == d(this).attr("type")) {
                    var i, s, e, r, n = (new Date).getFullYear(),
                        t = (new Date).getDate(),
                        o = (new Date).getMonth(),
                        l = d(".dd-w").length,
                        u = '<div class="dd-w dd-init" id="dd-w-' + l + '"><div class="dd-o"></div><div class="dd-c"><div class="dd-w-c"><div class="dd-b dd-m"><div class="dd-ul"><a class="dd-n dd-n-left"><i class="dd-icon-left" ></i></a><a class="dd-n dd-n-right"><i class="dd-icon-right" ></i></a><ul></ul></div></div><div class="dd-b dd-d"><div class="dd-ul"><a class="dd-n dd-n-left"><i class="dd-icon-left" ></i></a><a class="dd-n dd-n-right"><i class="dd-icon-right" ></i></a><ul></ul></div></div><div class="dd-b dd-y"><div class="dd-ul"><a class="dd-n dd-n-left"><i class="dd-icon-left" ></i></a><a class="dd-n dd-n-right"><i class="dd-icon-right" ></i></a><ul></ul></div></div><div class="dd-s-b dd-s-b-m dd-trans"><div class="dd-s-b-ul"><ul></ul></div></div><div class="dd-s-b dd-s-b-d dd-trans"><div class="dd-s-b-ul"><ul></ul></div></div><div class="dd-s-b dd-s-b-y dd-trans"><div class="dd-s-b-ul"><ul></ul></div></div><div class="dd-s-b dd-s-b-s-y dd-trans"><div class="dd-s-b-ul"><ul></ul></div></div><div class="dd-s-b-s"><i class="dd-icon-close" ></i></div><div class="dd-b dd-sub-y"><div class="dd-ul"><a class="dd-n dd-n-left"><i class="dd-icon-left" ></i></a><a class="dd-n dd-n-right"><i class="dd-icon-right" ></i></a><ul></ul></div></div><div class="dd-s"><a><i class="dd-icon-check" ></i></a></div></div></div></div>';
                    d("body").append(u);
                    var c = d(this),
                        f = d("#dd-w-" + l),
                        b = function (d) {
                            return !(d % 4 || !(d % 100) && d % 400)
                        },
                        m = function (d) {
                            return d < 10 ? "0" + d : d
                        },
                        p = d.extend({
                            animate: !0,
                            init_animation: "fadein",
                            format: "m/d/Y",
                            lang: "en",
                            lock: !1,
                            Main_maxYear: n + 11,
                            minYear: 2016,
                            yearsRange: 10,
                            dropPrimaryColor: "#967ADC",
                            Main_dropTextColor: "#1D2B36",
                            Main_dropBackgroundColor: "#FFFFFF",
                            dropBorder: "1px solid #967ADC",
                            dropBorderRadius: 8,
                            dropShadow: "0 0 10px 0 rgba(0, 136, 204, 0.45)",
                            dropWidth: 124,
                            Main_dropTextWeight: "bold"
                        }, a),
                        h = null,
                        v = !1;
                    switch (d("<style>#dd-w-" + l + " { font-weight: " + p.Main_dropTextWeight + "; } #dd-w-" + l + " .dd-w-c,#dd-w-" + l + " .dd-ul li,#dd-w-" + l + " .dd-s-b-ul ul { width:" + p.dropWidth + "px; } #dd-w-" + l + " .dd-w-c{color:" + p.Main_dropTextColor + ";background:" + p.Main_dropBackgroundColor + ";border:" + p.dropBorder + ";box-shadow:" + p.dropShadow + ";border-radius:" + p.dropBorderRadius + "px}#dd-w-" + l + " .dd-w-c,#dd-w-" + l + " .dd-s-b{background:" + p.Main_dropBackgroundColor + "}#dd-w-" + l + " .dd-sun,#dd-w-" + l + " .dd-s-b-ul li.dd-on{color:" + p.dropPrimaryColor + "}#dd-w-" + l + " .dd-c .dd-s,#dd-w-" + l + " .dd-s-b-s,#dd-w-" + l + " .dd-s-b-sub-y,#dd-w-" + l + " .dd-sub-y{background:" + p.dropPrimaryColor + ";color:" + p.Main_dropBackgroundColor + "}#dd-w-" + l + " .dd-c .dd-s a,#dd-w-" + l + " .dd-c .dd-s a:hover{color:" + p.Main_dropBackgroundColor + "}#dd-w-" + l + " .dd-c:after{border-left:" + p.dropBorder + ";border-top:" + p.dropBorder + "}#dd-w-" + l + ".dd-bottom .dd-c:after{background:" + p.Main_dropBackgroundColor + "}#dd-w-" + l + ".dd-top .dd-c:after{background:" + p.dropPrimaryColor + "}#dd-w-" + l + " .dd-n,#dd-w-" + l + " .dd-sun{color:" + p.dropPrimaryColor + "}#dd-w-" + l + " .dd-sub-y .dd-n{color:" + p.Main_dropBackgroundColor + "} #dd-w-" + l + " .dd-c .dd-s:hover,#dd-w-" + l + " .dd-s-b-s:hover { background:" + function (d, a) {
                        var i = !1;
                        "#" == d[0] && (d = d.slice(1), i = !0);
                        var s = parseInt(d, 16),
                            e = (s >> 16) + a;
                        e > 255 ? e = 255 : e < 0 && (e = 0);
                        var r = (s >> 8 & 255) + a;
                        r > 255 ? r = 255 : r < 0 && (r = 0);
                        var n = (255 & s) + a;
                        return n > 255 ? n = 255 : n < 0 && (n = 0), (i ? "#" : "") + (n | r << 8 | e << 16).toString(16)
                    }(p.dropPrimaryColor, -20) + "; }</style>").appendTo("head"), p.lang) {
                        case "ar":
                            var g = ["جانفي", "فيفري", "مارس", "أفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"],
                                y = ["الأحد", "الإثنين", "الثلثاء", "الأربعاء", "الخميس", "الجمعة", "السبت"];
                            break;
                        case "it":
                            g = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"], y = ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"];
                            break;
                        case "hu":
                            g = ["január", "február", "március", "április", "május", "június", "július", "augusztus", "szeptember", "október", "november", "december"], y = ["vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat"];
                            break;
                        case "gr":
                            g = ["Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"], y = ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο"];
                            break;
                        case "es":
                            g = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"], y = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
                            break;
                        case "da":
                            g = ["januar", "februar", "marts", "april", "maj", "juni", "juli", "august", "september", "oktober", "november", "december"], y = ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"];
                            break;
                        case "de":
                            g = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"], y = ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"];
                            break;
                        case "nl":
                            g = ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"], y = ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"];
                            break;
                        case "fr":
                            g = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"], y = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
                            break;
                        case "pl":
                            g = ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"], y = ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"];
                            break;
                        case "pt":
                            g = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"], y = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"];
                            break;
                        case "si":
                            g = ["januar", "februar", "marec", "april", "maj", "junij", "julij", "avgust", "september", "oktober", "november", "december"], y = ["nedelja", "ponedeljek", "torek", "sreda", "četrtek", "petek", "sobota"];
                            break;
                        case "uk":
                            g = ["січень", "лютий", "березень", "квітень", "травень", "червень", "липень", "серпень", "вересень", "жовтень", "листопад", "грудень"], y = ["неділя", "понеділок", "вівторок", "середа", "четвер", "п'ятниця", "субота"];
                            break;
                        case "ru":
                            g = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"], y = ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"];
                            break;
                        case "tr":
                            g = ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"], y = ["Pazar", "Pazartesi", "Sali", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"];
                            break;
                        case "ko":
                            g = ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"], y = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
                            break;
                        case "fi":
                            g = ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"], y = ["Sunnuntai", "Maanantai", "Tiistai", "Keskiviikko", "Torstai", "Perjantai", "Lauantai"];
                            break;
                        default:
                            g = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], y = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
                    }
                    var k = function () {
                        f.find(".dd-d li,.dd-s-b li").show(), b(e) && 2 == i ? (f.find(".dd-d ul").width(29 * p.dropWidth), 30 != s && 31 != s || (s = 29), f.find("li[data-id=30],li[data-id=31]").hide()) : b(e) || 2 != i ? 4 == i || 6 == i || 9 == i || 11 == i ? (f.find(".dd-d ul").width(30 * p.dropWidth), 31 == s && (s = 30), f.find("li[data-id=31]").hide()) : f.find(".dd-d ul").width(31 * p.dropWidth) : (f.find(".dd-d ul").width(28 * p.dropWidth), 29 != s && 30 != s && 31 != s || (s = 28), f.find("li[data-id=29],li[data-id=30],li[data-id=31]").hide()), f.find(".dd-d li").each(function (a, s) {
                            var r = d(this).attr("data-id");
                            0 == (r = (r = new Date(i + "/" + r + "/" + e)).getDay()) || 6 == r ? d(this).addClass("dd-sun") : d(this).removeClass("dd-sun"), d(this).find("span").html(y[r])
                        }), f.find(".dd-s-b-d li").each(function (a, s) {
                            var r = d(this).attr("data-id");
                            0 == (r = (r = new Date(i + "/" + r + "/" + e)).getDay()) || 6 == r ? d(this).addClass("dd-sun") : d(this).removeClass("dd-sun"), d(this).find("span").html(y[r].substr(0, 3))
                        }), f.find(".dd-s-b li").removeClass("dd-on"), f.find('.dd-s-b-d li[data-id="' + s + '"],.dd-s-b-m li[data-id="' + i + '"],.dd-s-b-s-y li[data-id="' + e + '"],.dd-s-b-y li[data-id="' + r + '"]').addClass("dd-on"), p.animate ? f.hasClass("dd-init") ? (f.find(".dd-m .dd-ul").animate({
                            scrollLeft: f.find('.dd-m li[data-id="' + i + '"]').index() * p.dropWidth
                        }, 1200, "swing"), setTimeout(function () {
                            f.find(".dd-d .dd-ul").animate({
                                scrollLeft: f.find('.dd-d li[data-id="' + s + '"]').index() * p.dropWidth
                            }, 1200, "swing"), setTimeout(function () {
                                f.find(".dd-y .dd-ul").animate({
                                    scrollLeft: f.find('.dd-y li[data-id="' + e + '"]').index() * p.dropWidth
                                }, 1200, "swing", function () {
                                    v = !0, f.removeClass("dd-init")
                                })
                            }, 200)
                        }, 400)) : (f.find(".dd-d .dd-ul").stop().animate({
                            scrollLeft: f.find('.dd-d li[data-id="' + s + '"]').index() * p.dropWidth
                        }, 260), f.find(".dd-m .dd-ul").stop().animate({
                            scrollLeft: f.find('.dd-m li[data-id="' + i + '"]').index() * p.dropWidth
                        }, 260), f.find(".dd-y .dd-ul").stop().animate({
                            scrollLeft: f.find('.dd-y li[data-id="' + e + '"]').index() * p.dropWidth
                        }, 260), f.find(".dd-sub-y .dd-ul").stop().animate({
                            scrollLeft: f.find('.dd-sub-y li[data-id="' + r + '"]').index() * p.dropWidth
                        }, 260)) : (setTimeout(function () {
                            f.find(".dd-d .dd-ul").scrollLeft(f.find('.dd-d li[data-id="' + s + '"]').index() * p.dropWidth), f.find(".dd-m .dd-ul").scrollLeft(f.find('.dd-m li[data-id="' + i + '"]').index() * p.dropWidth), f.find(".dd-y .dd-ul").scrollLeft(f.find('.dd-y li[data-id="' + e + '"]').index() * p.dropWidth), f.find(".dd-sub-y .dd-ul").scrollLeft(f.find('.dd-sub-y li[data-id="' + r + '"]').index() * p.dropWidth)
                        }, 1), f.hasClass("dd-init") && (f.removeClass("dd-init"), v = !0)), M(r)
                    },
                        w = function () {
                            f.addClass("dd-bottom"), f.find(".dd-c").css({
                                top: c.offset().top + c.innerHeight() - 6,
                                left: c.offset().left + (c.innerWidth() / 2 - p.dropWidth / 2)
                            }).addClass("dd-" + p.init_animation)
                        },
                        C = function () {
                            f.find(".dd-c").addClass("dd-alert").removeClass("dd-" + p.init_animation), setTimeout(function () {
                                f.find(".dd-c").removeClass("dd-alert")
                            }, 500)
                        },
                        M = function (a) {
                            f.find(".dd-s-b-s-y ul").empty();
                            var i = parseInt(a),
                                s = i + (p.yearsRange - 1);
                            s > p.Main_maxYear && (s = p.Main_maxYear);
                            for (var n = i; n <= s; n++) {
                                if (n % p.yearsRange == 0) var t = n;
                                f.find(".dd-s-b-s-y ul").append('<li data-id="' + n + '" data-filter="' + t + '">' + n + "</li>")
                            }
                            f.find(".dd-s-b-s-y ul").append('<div class="dd-clear"></div>'), r = parseInt(a), f.find(".dd-sub-y .dd-ul").scrollLeft(f.find('.dd-sub-y li[data-id="' + r + '"]').index() * p.dropWidth), f.find(".dd-s-b-s-y li").each(function (a, i) {
                                d(this).click(function () {
                                    f.find(".dd-s-b-s-y li").removeClass("dd-on"), d(this).addClass("dd-on"), e = parseInt(d(this).attr("data-id")), f.find(".dd-s-b-y,.dd-s-b-s-y").removeClass("dd-show"), f.find(".dd-s-b-s,.dd-sub-y").hide(), k()
                                })
                            })
                        },
                        x = function () {
                            f.find(".dd-s-b").each(function (a, e) {
                                var r = d(this);
                                if (r.hasClass("dd-s-b-m") || r.hasClass("dd-s-b-d")) {
                                    if (r.hasClass("dd-s-b-m"))
                                        for (var n = 12, t = 0; t < n; t++) r.find("ul").append('<li data-id="' + (t + 1) + '">' + g[t].substr(0, 3) + "<span>" + m(t + 1) + "</span></li>");
                                    if (r.hasClass("dd-s-b-d"))
                                        for (n = 31, t = 0; t < n; t++) r.find("ul").append('<li data-id="' + (t + 1) + '">' + m(t + 1) + "<span></span></li>")
                                }
                                if (r.hasClass("dd-s-b-y"))
                                    for (t = p.minYear; t <= p.Main_maxYear; t++) t % p.yearsRange == 0 && r.find("ul").append('<li data-id="' + t + '">' + t + "</li>");
                                r.find("ul").append('<div class="dd-clear"></div>'), r.find("ul li").click(function () {
                                    (r.hasClass("dd-s-b-m") || r.hasClass("dd-s-b-d")) && (r.hasClass("dd-s-b-m") && (i = parseInt(d(this).attr("data-id"))), r.hasClass("dd-s-b-d") && (s = parseInt(d(this).attr("data-id"))), k(), r.removeClass("dd-show"), f.find(".dd-s-b-s").hide()), r.hasClass("dd-s-b-y") && (f.find(".dd-sub-y").show(), M(d(this).attr("data-id")), f.find(".dd-s-b-s-y").addClass("dd-show"))
                                });
                                var o = 0,
                                    l = !1;
                                r.on("mousewheel DOMMouseScroll", function (d) {
                                    l = !0, (d.originalEvent.wheelDeltaY < 0 || d.originalEvent.detail > 0) && (o = r.scrollTop() + 100), (d.originalEvent.wheelDeltaY > 0 || d.originalEvent.detail < 0) && (o = r.scrollTop() - 100), r.stop().animate({
                                        scrollTop: o
                                    }, 600, "_dd_easing", function () {
                                        l = !1
                                    })
                                }).on("scroll", function () {
                                    l || (o = r.scrollTop())
                                })
                            }), f.find(".dd-b").each(function (a, n) {
                                var t, o = d(this),
                                    l = 0;
                                if (o.hasClass("dd-m")) {
                                    for (var u = 0; u < 12; u++) o.find("ul").append('<li data-id="' + (u + 1) + '">' + g[u].substr(0, 3) + "</li>");
                                    o.find("li").click(function () {
                                        return "m" != p.format && "n" != p.format && "F" != p.format && "M" != p.format && void f.find(".dd-s-b-m").addClass("dd-show")
                                    })
                                }
                                if (o.hasClass("dd-d")) {
                                    for (u = 1; u <= 31; u++) o.find("ul").append('<li data-id="' + u + '"><strong>' + m(u) + "</strong><br><span></span></li>");
                                    o.find("li").click(function () {
                                        f.find(".dd-s-b-d").addClass("dd-show")
                                    })
                                }
                                if (o.hasClass("dd-y")) {
                                    for (u = p.minYear; u <= p.Main_maxYear; u++) {
                                        var c;
                                        u % p.yearsRange == 0 && (c = 'data-filter="' + u + '"'), o.find("ul").append('<li data-id="' + u + '" ' + c + ">" + u + "</li>")
                                    }
                                    o.find("li").click(function () {
                                        return "Y" != p.format && void f.find(".dd-s-b-y").addClass("dd-show")
                                    })
                                }
                                if (o.hasClass("dd-sub-y"))
                                    for (u = p.minYear; u <= p.Main_maxYear; u++) u % p.yearsRange == 0 && o.find("ul").append('<li data-id="' + u + '">' + u + "</li>");
                                o.find("ul").width(o.find("li").length * p.dropWidth), o.find(".dd-n").click(function () {
                                    var a, n, l;
                                    clearInterval(t), o.hasClass("dd-y") && (n = e), o.hasClass("dd-m") && (n = i), o.hasClass("dd-d") && (n = s), o.hasClass("dd-sub-y") && (n = r), d(this).hasClass("dd-n-left") ? l = (a = o.find('li[data-id="' + n + '"]').prev("li")).length && a.is(":visible") ? parseInt(a.attr("data-id")) : parseInt(o.find("li:visible:last").attr("data-id")) : l = (a = o.find('li[data-id="' + n + '"]').next("li")).length && a.is(":visible") ? parseInt(a.attr("data-id")) : parseInt(o.find("li:first").attr("data-id")), o.hasClass("dd-y") && (e = l), o.hasClass("dd-m") && (i = l), o.hasClass("dd-d") && (s = l), o.hasClass("dd-sub-y") && (r = l), k()
                                });
                                o.find(".dd-ul").on("scroll", function () {
                                    ! function () {
                                        if (v) {
                                            l = Math.round(o.find(".dd-ul").scrollLeft() / p.dropWidth);
                                            var d = parseInt(o.find("li").eq(l).attr("data-id"));
                                            o.hasClass("dd-y") && (e = d), o.hasClass("dd-m") && (i = d), o.hasClass("dd-d") && (s = d), o.hasClass("dd-sub-y") && (r = d)
                                        }
                                    }()
                                });
                                var b = !1;
                                o.find(".dd-ul").on("mousedown touchstart", function () {
                                    b || (b = !0), clearInterval(t), d(window).on("mouseup touchend touchmove", function () {
                                        b && (clearInterval(t), t = setTimeout(function () {
                                            k(), b = !1
                                        }, 780))
                                    })
                                }), "Y" == p.format && f.find(".dd-m,.dd-d").hide(), "m" != p.format && "n" != p.format && "F" != p.format && "M" != p.format || f.find(".dd-y,.dd-d").hide()
                            }), f.find(".dd-b li").click(function () {
                                return "m" != p.format && "n" != p.format && "F" != p.format && "M" != p.format && "Y" != p.format && void f.find(".dd-s-b-s").show()
                            }), f.find(".dd-s-b-s").click(function () {
                                f.find(".dd-s-b").removeClass("dd-show"), f.find(".dd-s-b-s").hide()
                            }), f.find(".dd-s").click(function () {
                                ! function () {
                                    if (p.lock) {
                                        var d = Date.parse(n + "-" + (o + 1) + "-" + t) / 1e3,
                                            a = Date.parse(e + "-" + i + "-" + s) / 1e3;
                                        if ("from" == p.lock) {
                                            if (a < d) return C(), !1
                                        } else if (a > d) return C(), !1
                                    }
                                    var r = (r = new Date(i + "/" + s + "/" + e)).getDay(),
                                        l = p.format.replace(/\b(d)\b/g, m(s)).replace(/\b(m)\b/g, m(i)).replace(/\b(Y)\b/g, e).replace(/\b(D)\b/g, y[r].substr(0, 3)).replace(/\b(l)\b/g, y[r]).replace(/\b(F)\b/g, g[i - 1]).replace(/\b(M)\b/g, g[i - 1].substr(0, 3)).replace(/\b(n)\b/g, i).replace(/\b(j)\b/g, s);
                                    c.val(l), f.find(".dd-c").addClass("dd-fadeout").removeClass("dd-" + p.init_animation), h = setTimeout(function () {
                                        f.hide(), f.find(".dd-c").removeClass("dd-fadeout")
                                    }, 400), c.change()
                                }()
                            }), f.find(".dd-o").click(function () {
                                f.find(".dd-c").addClass("dd-fadeout").removeClass("dd-" + p.init_animation), h = setTimeout(function () {
                                    f.hide(), f.find(".dd-c").removeClass("dd-fadeout")
                                }, 400)
                            }), k()
                        };
                    c.click(function () {
                        clearInterval(h), f.hasClass("dd-init") && (c.attr({
                            readonly: "readonly"
                        }).blur(), i = o + 1, s = t, e = n, parseInt(c.attr("data-d")) && parseInt(c.attr("data-d")) <= 31 && (s = parseInt(c.attr("data-d"))), parseInt(c.attr("data-m")) && parseInt(c.attr("data-m")) <= 11 && (i = parseInt(c.attr("data-m")) + 1), parseInt(c.attr("data-y")) && 4 == c.attr("data-y").length && (e = parseInt(c.attr("data-y"))), e > p.Main_maxYear && (p.Main_maxYear = e), e < p.minYear && (p.minYear = e), x()), f.show(), w()
                    }), c.bind("focusin focus", function (d) {
                        d.preventDefault()
                    }), d(window).resize(function () {
                        w()
                    })
                }
            })
        }
    }(jQuery);
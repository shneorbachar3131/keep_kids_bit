﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="KeepKids.index" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/plugins/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/colors/palette-callout.css">



    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/colors/palette-gradient.css">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/weather-icons/climacons.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/meteocons/style.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/pages/timeline.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css-rtl/pages/dashboard-ecommerce.css">
    <!-- END: Page CSS-->

    <style>
        svg {
            overflow: visible;
        }

        #K1, #E1, #E2, #P {
            stroke: red;
            stroke-width: 1px;
            fill: transparent;
        }

        #K2, #I, #D, #S {
            stroke: green;
            stroke-width: 1px;
            fill: transparent;
        }

        #K1 {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: K1-anim .75s linear 0s forwards,fill-it 0s 3.8s forwards;
        }

        #E1 {
            stroke-dasharray: 730px;
            stroke-dashoffset: -730px;
            animation: E1-anim .75s linear .80s forwards,fill-it 0s 3.9s forwards;
        }

        #E2 {
            stroke-dasharray: 730px;
            stroke-dashoffset: -730px;
            animation: E2-anim .75s linear 1.6s forwards,fill-it 0s 4s forwards;
        }

        #P {
            stroke-dasharray: 960px;
            stroke-dashoffset: -960px;
            animation: P-anim .75s linear 2.4s forwards,fill-it 0s 4.1s forwards;
        }

        #K2 {
            stroke-dasharray: 823px;
            stroke-dashoffset: -823px;
            animation: K2-anim .75s linear 2.4s forwards,fill-it1 0s 4.2s forwards;
        }

        #I {
            stroke-dasharray: 600px;
            stroke-dashoffset: -600px;
            animation: I-anim .75s linear 1.6s forwards,fill-it1 0s 4.3s forwards;
        }

        #D {
            stroke-dasharray: 621px;
            stroke-dashoffset: -621px;
            animation: D-anim .75s linear .80s forwards,fill-it1 .0s 4.4s forwards;
        }

        #S {
            stroke-dasharray: 611px;
            stroke-dashoffset: -611px;
            animation: D-anim .75s linear 0s forwards,fill-it1 0s 4.5s forwards;
        }


        @keyframes fill-it {
            70% {
                fill: gold;
            }

            80% {
                fill: olive;
            }

            90% {
                fill: orange;
            }

            100% {
                fill: red;
            }
        }

        @keyframes fill-it1 {

            100% {
                fill: green;
            }
        }



        @keyframes K1-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes E1-anim {
            from {
                stroke-dashoffset: -730px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes E2-anim {
            from {
                stroke-dashoffset: -730px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes P-anim {
            from {
                stroke-dashoffset: -960px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes K2-anim {
            from {
                stroke-dashoffset: -823px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes I-anim {
            from {
                stroke-dashoffset: -600px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes D-anim {
            from {
                stroke-dashoffset: -621px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }

        @keyframes S-anim {
            from {
                stroke-dashoffset: -611px;
            }

            to {
                stroke-dashoffset: 0px;
            }
        }
    </style>
    <%--3--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">

    <div class="text-center">
        <h1 class="text-center display-3 visible-xs (only) = d-block d-sm-none"><b class="text-success b"><span class="text-danger">Keep</span>Kids</b></h1>
        <h1 class="text-center display-1 hidden-xs (only) = d-none d-sm-block (same as hidden-xs-down)">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 233" xml:space="preserve">
                <path id="K1" d="M532.46 119.14v-3.22h57.34v3.22c-4.28.35-8.65 1.13-13.09 2.34a95.38 95.38 0 00-13.18 4.66c-4.35 1.9-9.27 4.29-14.77 7.18l-23.11 12.48 50.82 38.85c3.77 2.89 6.77 5 9.01 6.33s4.52 2.33 6.86 3.02c2.33.68 5.03 1.14 8.1 1.38v3.22H536v-3.22c10.23 0 15.34-1.41 15.34-4.22 0-.98-.51-1.98-1.53-3.02-1.02-1.03-2.4-2.2-4.12-3.49l-37.97-29-11.98 6.56v22.44c0 3.2.78 5.65 2.35 7.32 1.57 1.68 4.39 2.66 8.49 2.93 2.11.08 3.85.17 5.23.26 1.37.1 2.57.17 3.6.21v3.22H450v-3.22h2.97c5.62 0 9.78-.41 12.46-1.23 2.68-.82 4.41-2 5.18-3.54.77-1.54 1.15-3.6 1.15-6.18v-55.08c0-2.62-.48-4.62-1.44-6.01-.96-1.39-2.99-2.43-6.09-3.13-3.1-.7-7.85-1.05-14.24-1.05v-3.22h65.39v3.22h-1.34c-7.48 0-12.4.72-14.77 2.17-2.37 1.45-3.55 3.89-3.55 7.32v29.18l38.93-21.27c4.35-2.38 7.62-4.48 9.83-6.3 2.21-1.82 3.31-3.54 3.31-5.19 0-1.76-1.01-3.18-3.02-4.28-2.01-1.09-4.87-1.64-8.58-1.64h-3.73zm-51.4 66.04v-55.37c0-4.3-.77-7.16-2.3-8.58-1.53-1.43-5.24-2.12-11.12-2.08 3.13.86 5.13 1.91 5.99 3.16.86 1.25 1.29 4.08 1.29 8.5v55.31c0 2.93-.53 5-1.58 6.21s-3.21 2.23-6.47 3.05h2.4c3.9 0 6.68-.38 8.34-1.14 1.66-.76 2.65-1.8 2.97-3.11.32-1.31.48-3.29.48-5.95zm72.39-3.52l-38.83-29.77-4.31 2.4 36.15 28.13c2.75 2.03 4.78 3.71 6.09 5.04 1.31 1.33 1.97 2.64 1.97 3.93 0 1.68-1.57 3.01-4.7 3.98h2.49c2.56 0 4.63-.47 6.23-1.41 1.6-.94 2.4-2.03 2.4-3.28-.01-2.22-2.5-5.23-7.49-9.02zm15.54-62.52h-22.53c3 1.52 4.51 3.2 4.51 5.04 0 1.17-.48 2.38-1.44 3.63 4.35-2.42 7.77-4.23 10.26-5.42 2.48-1.18 5.55-2.27 9.2-3.25z" />
                <path id="E1" d="M682.7 184.07l3.84 1.82c-2.88 3.28-6.3 5.98-10.26 8.09-3.96 2.11-8.33 3.67-13.09 4.69-4.76 1.02-9.73 1.52-14.91 1.52-7.03 0-13.57-1.04-19.61-3.11s-10.82-5.06-14.33-8.96c-3.52-3.91-5.27-8.34-5.27-13.3 0-3.91 1.07-7.58 3.21-11.02 2.14-3.44 5.11-6.46 8.92-9.08 3.8-2.62 8.21-4.66 13.23-6.12s10.37-2.2 16.06-2.2c5.75 0 11.23.73 16.44 2.2 5.21 1.46 9.35 3.49 12.42 6.06 2.24 1.88 3.85 3.8 4.84 5.77.99 1.97 1.77 4.35 2.35 7.12h-57.91c.25 4.06 1.39 7.5 3.4 10.31 2.01 2.81 4.63 5.08 7.86 6.8 3.23 1.72 6.66 2.95 10.31 3.69 3.64.74 7.13 1.11 10.45 1.11 8.05 0 15.4-1.8 22.05-5.39zm-52.64-31.82c-11.95 5.78-17.93 13.36-17.93 22.73 0 6.8 3.56 12.35 10.69 16.67 7.13 4.32 15.36 6.47 24.69 6.47 5.43 0 10.88-.67 16.35-2.02 5.47-1.35 10.08-3.35 13.85-6.01-3.9 1.52-7.88 2.76-11.94 3.72-4.06.96-8.9 1.44-14.53 1.44-9.52 0-17.47-2.03-23.83-6.09-6.36-4.06-9.54-9.35-9.54-15.88 0-4.14 1.05-7.85 3.16-11.13 2.13-3.28 5.13-6.58 9.03-9.9zm-.86 11.54h33.46v-1.93c0-2.27-.67-4.29-2.01-6.06-1.34-1.78-3.18-3.14-5.51-4.1-2.33-.96-5-1.44-8.01-1.44-4.86 0-8.98 1.32-12.37 3.96-3.39 2.63-5.24 5.82-5.56 9.57zm36.53 0h6.71c-.19-4.37-1.66-7.61-4.41-9.7-2.75-2.09-7.35-3.49-13.81-4.19 4.09 1.13 7.03 2.61 8.82 4.42 1.79 1.82 2.68 4.58 2.68 8.29v1.18z" />
                <path id="E2" d="M770.72 184.07l3.84 1.82c-2.88 3.28-6.3 5.98-10.26 8.09-3.96 2.11-8.33 3.67-13.09 4.69-4.76 1.02-9.73 1.52-14.91 1.52-7.03 0-13.57-1.04-19.61-3.11s-10.82-5.06-14.33-8.96c-3.52-3.91-5.27-8.34-5.27-13.3 0-3.91 1.07-7.58 3.21-11.02 2.14-3.44 5.11-6.46 8.92-9.08 3.8-2.62 8.21-4.66 13.23-6.12s10.37-2.2 16.06-2.2c5.75 0 11.23.73 16.44 2.2 5.21 1.46 9.35 3.49 12.42 6.06 2.24 1.88 3.85 3.8 4.84 5.77.99 1.97 1.77 4.35 2.35 7.12h-57.91c.25 4.06 1.39 7.5 3.4 10.31 2.01 2.81 4.63 5.08 7.86 6.8 3.23 1.72 6.66 2.95 10.31 3.69 3.64.74 7.13 1.11 10.45 1.11 8.05 0 15.4-1.8 22.05-5.39zm-52.64-31.82c-11.95 5.78-17.93 13.36-17.93 22.73 0 6.8 3.56 12.35 10.69 16.67 7.13 4.32 15.36 6.47 24.69 6.47 5.43 0 10.88-.67 16.35-2.02 5.47-1.35 10.08-3.35 13.85-6.01-3.9 1.52-7.88 2.76-11.94 3.72-4.06.96-8.9 1.44-14.53 1.44-9.52 0-17.47-2.03-23.83-6.09-6.36-4.06-9.54-9.35-9.54-15.88 0-4.14 1.05-7.85 3.16-11.13 2.13-3.28 5.13-6.58 9.03-9.9zm-.86 11.54h33.46v-1.93c0-2.27-.67-4.29-2.01-6.06-1.34-1.78-3.18-3.14-5.51-4.1-2.33-.96-5-1.44-8.01-1.44-4.86 0-8.98 1.32-12.37 3.96-3.39 2.63-5.25 5.82-5.56 9.57zm36.53 0h6.71c-.19-4.37-1.66-7.61-4.41-9.7-2.75-2.09-7.35-3.49-13.81-4.19 4.09 1.13 7.03 2.61 8.82 4.42 1.79 1.82 2.68 4.58 2.68 8.29v1.18z" />
                <path id="P" d="M816.07 146.39v6.39c4.47-2.03 8.79-3.6 12.94-4.72 4.15-1.11 8.82-1.67 14-1.67 4.99 0 9.83.66 14.53 1.99 4.7 1.33 8.82 3.16 12.37 5.51s6.39 5.18 8.53 8.5c2.14 3.32 3.21 6.84 3.21 10.55 0 5.16-1.98 9.81-5.94 13.95-3.96 4.14-9.41 7.37-16.35 9.7-6.94 2.32-14.69 3.49-23.25 3.49-6.71 0-13.39-.88-20.04-2.64v20.27c0 1.52.25 2.74.77 3.66.51.92 1.6 1.59 3.26 2.02 1.66.43 3.99.64 7 .64h4.89v3.22h-50.05v-3.34c3.26-.08 5.69-.22 7.29-.41 1.6-.2 2.81-.66 3.64-1.41.83-.74 1.25-1.86 1.25-3.34v-55.43c0-3.01-.98-5.28-2.92-6.83-1.95-1.54-5.23-2.78-9.83-3.72v-2.4l30.97-3.98h3.73zm-12.56 69.26v-52.91c0-3.63-1.18-6.27-3.55-7.91-2.37-1.64-6.3-2.32-11.79-2.05 3.58 1.21 5.99 2.49 7.24 3.84 1.25 1.35 1.87 3.51 1.87 6.47v55.2c0 1.56-.29 2.8-.86 3.72-.58.92-1.66 1.75-3.26 2.49 4.35-.23 7.16-1 8.44-2.29 1.27-1.29 1.91-3.48 1.91-6.56zm12.56-58.6v34.57c6.07 3.44 11.82 5.16 17.26 5.16 4.41 0 8.44-.82 12.08-2.46 3.64-1.64 6.57-4.13 8.77-7.47 2.21-3.34 3.31-7.43 3.31-12.28 0-4.73-1.05-8.72-3.16-11.98-2.11-3.26-5-5.71-8.68-7.35-3.68-1.64-7.78-2.46-12.32-2.46-5.57 0-11.32 1.42-17.26 4.27zm15.34-6.09h2.3c8.12 0 14.64 2.06 19.56 6.18 4.92 4.12 7.38 9.93 7.38 17.43 0 5.78-1.22 10.23-3.64 13.36-2.43 3.13-5.79 5.76-10.07 7.91 4.47-1.02 8.18-2.5 11.12-4.45 2.94-1.95 5.16-4.39 6.66-7.32 1.5-2.93 2.25-6.21 2.25-9.84 0-4.37-.88-8.19-2.64-11.46-1.76-3.26-3.98-5.83-6.66-7.71s-5.55-3.25-8.58-4.13c-3.04-.88-5.93-1.32-8.68-1.32-2.74 0-5.74.45-9 1.35z" />
                <path id="K2" d="M974.27 119.14v-3.22h57.34v3.22c-4.28.35-8.65 1.13-13.09 2.34a95.38 95.38 0 00-13.18 4.66c-4.35 1.9-9.27 4.29-14.77 7.18l-23.11 12.48 50.82 38.85c3.77 2.89 6.77 5 9.01 6.33s4.52 2.33 6.86 3.02c2.33.68 5.03 1.14 8.1 1.38v3.22h-64.43v-3.22c10.23 0 15.34-1.41 15.34-4.22 0-.98-.51-1.98-1.53-3.02-1.02-1.03-2.4-2.2-4.12-3.49l-37.97-29-11.98 6.56v22.44c0 3.2.78 5.65 2.35 7.32 1.57 1.68 4.39 2.66 8.49 2.93 2.11.08 3.85.17 5.23.26 1.37.1 2.57.17 3.6.21v3.22h-65.39v-3.22h2.97c5.62 0 9.78-.41 12.46-1.23s4.41-2 5.18-3.54c.77-1.54 1.15-3.6 1.15-6.18v-55.08c0-2.62-.48-4.62-1.44-6.01-.96-1.39-2.99-2.43-6.09-3.13-3.1-.7-7.85-1.05-14.24-1.05v-3.22h65.39v3.22h-1.34c-7.48 0-12.4.72-14.77 2.17-2.37 1.45-3.55 3.89-3.55 7.32v29.18l38.93-21.27c4.35-2.38 7.62-4.48 9.83-6.3 2.21-1.82 3.31-3.54 3.31-5.19 0-1.76-1.01-3.18-3.02-4.28-2.01-1.09-4.87-1.64-8.58-1.64h-3.76zm-51.39 66.04v-55.37c0-4.3-.77-7.16-2.3-8.58-1.53-1.43-5.24-2.12-11.12-2.08 3.13.86 5.13 1.91 5.99 3.16.86 1.25 1.29 4.08 1.29 8.5v55.31c0 2.93-.53 5-1.58 6.21s-3.21 2.23-6.47 3.05h2.4c3.9 0 6.68-.38 8.34-1.14 1.66-.76 2.65-1.8 2.97-3.11.32-1.31.48-3.29.48-5.95zm72.39-3.52l-38.83-29.77-4.31 2.4 36.15 28.13c2.75 2.03 4.78 3.71 6.09 5.04 1.31 1.33 1.97 2.64 1.97 3.93 0 1.68-1.57 3.01-4.7 3.98h2.49c2.56 0 4.63-.47 6.23-1.41 1.6-.94 2.4-2.03 2.4-3.28-.02-2.22-2.51-5.23-7.49-9.02zm15.53-62.52h-22.53c3 1.52 4.51 3.2 4.51 5.04 0 1.17-.48 2.38-1.44 3.63 4.35-2.42 7.77-4.23 10.26-5.42 2.49-1.18 5.55-2.27 9.2-3.25z" />
                <path id="I" d="M1082.61 146.39v42.36c0 1.8.3 3.15.91 4.07.61.92 1.76 1.55 3.45 1.9 1.69.35 4.3.57 7.81.64v3.22H1050v-3.22c3.96-.08 6.73-.57 8.29-1.46 1.57-.9 2.35-2.5 2.35-4.8v-26.89c0-1.68-.93-3.24-2.78-4.69-1.85-1.44-4.48-2.52-7.86-3.22v-1.88l28.67-6.04h3.94zm-18.79 16v27.83c0 2.27-1.31 3.98-3.93 5.16 3.51 0 6.09-.49 7.72-1.46 1.63-.98 2.44-2.58 2.44-4.8v-27.48c0-2.5-.99-4.42-2.97-5.77-1.98-1.35-4.57-2.02-7.77-2.02-1.47 0-2.65.04-3.55.12 5.37 1.7 8.06 4.51 8.06 8.42zm7-44.53c2.43 0 4.7.36 6.81 1.08 2.11.72 3.77 1.71 4.99 2.96 1.21 1.25 1.82 2.62 1.82 4.1 0 1.48-.61 2.86-1.82 4.13-1.22 1.27-2.88 2.28-4.99 3.02-2.11.74-4.38 1.11-6.81 1.11-3.64 0-6.78-.82-9.4-2.46-2.62-1.64-3.93-3.57-3.93-5.8 0-1.44.59-2.8 1.77-4.07 1.18-1.27 2.81-2.27 4.89-2.99 2.08-.72 4.3-1.08 6.67-1.08zm1.63 1.87c-3.77 0-6.68.55-8.72 1.64-2.05 1.09-3.07 2.6-3.07 4.51 0 1.84.99 3.35 2.97 4.54 1.98 1.19 4.92 1.85 8.82 1.96-2.05-1.13-3.55-2.14-4.51-3.02-.96-.88-1.44-2-1.44-3.37 0-2.53 1.98-4.62 5.95-6.26z" />
                <path id="D" d="M1157.3 120.96l29.63-5.98h3.93v73.07c0 2.07.91 3.47 2.73 4.19 1.82.72 4.97 1.08 9.44 1.08v3.11l-34.04 3.75v-6.86c-9.27 4.57-18.67 6.86-28.19 6.86-5.31 0-10.26-.68-14.86-2.05-4.6-1.37-8.57-3.29-11.89-5.77-3.32-2.48-5.87-5.33-7.62-8.55-1.76-3.22-2.64-6.61-2.64-10.17 0-5.12 2-9.77 5.99-13.95s9.4-7.44 16.2-9.79c6.81-2.34 14.4-3.52 22.77-3.52 5.94 0 12.69.84 20.23 2.52v-14.4c0-3.2-.66-5.55-1.97-7.03-1.31-1.48-4.55-2.85-9.73-4.1v-2.41zm-18.5 28.18c-5.37.7-9.88 1.8-13.52 3.28-5.56 2.11-10 5.14-13.33 9.08-3.32 3.95-4.99 8.14-4.99 12.6 0 4.3 1.45 8.29 4.36 11.98 2.91 3.69 7.01 6.62 12.32 8.79 5.3 2.17 11.22 3.25 17.74 3.25 4.47 0 8.47-.41 11.98-1.23-2.94.16-4.79.23-5.56.23-4.22 0-8.39-.53-12.51-1.58s-7.88-2.61-11.27-4.66c-3.39-2.05-6.06-4.5-8.01-7.35-1.95-2.85-2.92-5.96-2.92-9.32.01-10.42 8.58-18.78 25.71-25.07zm30.2 39.97v-34.34c-2.24-1.64-4.84-2.91-7.81-3.81-2.97-.9-5.99-1.35-9.06-1.35-4.54 0-8.73.85-12.56 2.55-3.84 1.7-6.86 4.26-9.06 7.68-2.21 3.42-3.31 7.67-3.31 12.74 0 4.3.99 8.02 2.97 11.16 1.98 3.15 4.76 5.53 8.34 7.15s7.67 2.43 12.27 2.43c6.58 0 12.66-1.4 18.22-4.21zm-4.12-66.57h-3.26c4.79 1.29 7.75 2.69 8.87 4.19 1.12 1.5 1.68 3.51 1.68 6.01v64.51c2.11-.55 3.66-1.19 4.65-1.93.99-.74 1.49-1.85 1.49-3.34v-59.12c0-3.52-.86-6.11-2.59-7.79-1.74-1.69-5.35-2.53-10.84-2.53z" />
                <path id="S" d="M1212.14 195.73v-15.35h5.18c4.86 5.27 9.94 9.25 15.24 11.92 5.3 2.68 10.48 4.01 15.53 4.01 3.26 0 6.17-.64 8.72-1.93 2.56-1.29 3.84-2.75 3.84-4.39 0-2.15-.96-3.63-2.88-4.45-7.22-2.93-14.16-5.18-20.81-6.74-17.58-3.98-26.37-9.67-26.37-17.05 0-4.37 2.75-8.03 8.25-10.96 5.5-2.93 12.91-4.39 22.24-4.39 4.73 0 9.14.3 13.23.91 4.09.61 8.63 1.48 13.61 2.61l.96 13.89h-4.7c-5.18-4.69-10.31-8.22-15.39-10.61-5.08-2.38-10.08-3.57-15.01-3.57-3.07 0-5.61.62-7.62 1.85s-3.02 2.59-3.02 4.07c0 1.91 1.25 3.43 3.74 4.54 2.49 1.11 7.73 2.76 15.72 4.95 7.99 2.19 14.99 4.3 21 6.33 3.77 1.21 6.77 2.91 9.01 5.1 2.24 2.19 3.36 4.57 3.36 7.15 0 3.01-1.42 5.78-4.27 8.32-2.85 2.54-6.78 4.55-11.79 6.04-5.02 1.48-10.66 2.23-16.92 2.23-8.86-.03-19.15-1.52-30.85-4.48zm50.53-2.76c1.73-.98 3-1.86 3.84-2.67.83-.8 1.25-1.88 1.25-3.25 0-4.18-4.99-7.5-14.96-9.96-8.95-2.27-16.01-4.27-21.19-6.01-5.18-1.74-8.92-3.53-11.22-5.39-2.3-1.85-3.45-4.17-3.45-6.94 0-1.68.77-3.55 2.3-5.63-1.66 1.25-2.99 2.49-3.98 3.72-.99 1.23-1.49 2.74-1.49 4.54 0 3.71 2.17 6.72 6.52 9.02 4.35 2.31 10.8 4.64 19.37 7 8.56 2.36 14.73 4.3 18.5 5.8 3.77 1.5 5.66 3.58 5.66 6.24 0 .99-.38 2.17-1.15 3.53zm-46.98-7.44v7.32c3.2.86 8.5 1.9 15.92 3.11-3.96-1.68-6.9-3.18-8.82-4.51-1.92-1.33-4.29-3.3-7.1-5.92zm46.22-26.89l-.86-6.74c-4.22-1.05-7.51-1.78-9.88-2.17-2.37-.39-5.82-.59-10.35-.59 4.79 1.02 8.61 2.2 11.46 3.54 2.83 1.36 6.05 3.34 9.63 5.96z" />
            </svg><%--3--%>
   
        </h1>
        <h1>ממשק ניהול גני ילדים</h1>
    </div>
    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <!-- Basic Carousel start -->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="card-content">
                            <div class="card-body">
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner hidden-xs (only) = d-none d-sm-block (same as hidden-xs-down)" role="listbox">
                                        <div class="carousel-item active">
                                            <img src="../pics/imgdefualt1.jpg" class="img-fluid" alt="First slide" style="height: 500px; width: 1000px;">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="../pics/keep5.jpg" class="img-fluid" alt="Second slide" style="height: 500px; width: 1000px;">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="../pics/keep4.jpg" class="img-fluid" alt="Third slide" style="height: 500px; width: 1000px;">
                                        </div>
                                    </div>
                                    <div class="carousel-inner visible-xs (only) = d-block d-sm-none" role="listbox">
                                        <div class="carousel-item active">
                                            <img src="../pics/imgdefualt1.jpg" class="img-fluid" alt="First slide" style="height: 300px; width: 600px;">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="../pics/keep5.jpg" class="img-fluid" alt="Second slide" style="height: 300px; width: 600px;">
                                        </div>
                                        <div class="carousel-item">
                                            <img src="../pics/keep4.jpg" class="img-fluid" alt="Third slide" style="height: 300px; width: 600px;">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Carousel end -->



            </div>
            <div class="row pt-1">
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">דוחות אישיים</h3>
                                        <h6></h6>
                                    </div>
                                    <div>
                                        <i class="icon-user info font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="warning">פעילויות חינוכיות</h3>
                                        <h6></h6>
                                    </div>
                                    <div>
                                        <i class="icon-calendar warning font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="danger">אירועים חריגים</h3>
                                        <h6></h6>
                                    </div>
                                    <div>
                                        <i class="icon-bulb danger font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="success">שליחת תזכורות</h3>
                                        <h6></h6>
                                    </div>
                                    <div>
                                        <i class="icon-check success font-large-2 float-right"></i>
                                    </div>
                                </div>
                                <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8 col-md-6 col-sm-12">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <h3 class="dropdown-menu-header text-uppercase mb-1"><i class="icon-envelope-letter danger font-large-1 float-right"></i>צור איתנו קשר</h3>
                                <div class="form-body ">
                                    <div class="form-group row ">
                                        <asp:RequiredFieldValidator ID="TxtLastName" runat="server" ControlToValidate="Name" ErrorMessage="אנא הזן שם " Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <label class="col-sm-3 form-control-label" for="inputName1">שם</label>
                                        <div class="col-sm-9">
                                            <div class="position-relative has-icon-left">
                                                <asp:TextBox ID="Name" runat="server" CssClass="form-control" placeholder="ישראל ישראלי" />
                                                <div class="form-control-position pl-1"><i class="la la-user"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <asp:RequiredFieldValidator ID="TxtPhone" runat="server" ControlToValidate="Phone" ErrorMessage="אנא הזן מספר ניייד" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtPhone" runat="server" ControlToValidate="Phone" ErrorMessage="הזן מספר נייד תקין בעל 10 ספרות" ValidationExpression="[0-9]{10}" Text="**" Font-Bold="True" ForeColor="Red"></asp:RegularExpressionValidator>
                                        <label class="col-sm-3 form-control-label" for="inputName1">נייד</label>
                                        <div class="col-sm-9">
                                            <div class="position-relative has-icon-left">
                                                <asp:TextBox ID="Phone" runat="server" TextMode="Phone" CssClass="form-control" placeholder="מספר הנייד שלך" />
                                                <div class="form-control-position pl-1"><i class="la la-phone"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <asp:RequiredFieldValidator ID="TxEmail" runat="server" ControlToValidate="Email" ErrorMessage="אנא הזן כתובת מייל" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="REVTxtEmail" runat="server" ControlToValidate="Email" ErrorMessage="הזן מייל תקין" Text="**" Font-Bold="True" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        <label class="col-sm-3 form-control-label" for="inputEmail1">מייל</label>
                                        <div class="col-sm-9">
                                            <div class="position-relative has-icon-left">
                                                <asp:TextBox ID="Email" runat="server" TextMode="Email" CssClass="form-control" placeholder="iarsel@gamil.com" />
                                                <div class="form-control-position pl-1"><i class="la la-envelope-o"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <asp:RequiredFieldValidator ID="TxtMessage" runat="server" ControlToValidate="Message" ErrorMessage="אנא הזן תוכן להודעה" Font-Bold="True" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <label class="col-sm-3 form-control-label" for="inputMessage1">תוכן ההודעה</label>
                                        <div class="col-sm-9">
                                            <div class="position-relative has-icon-left">
                                                <asp:TextBox ID="Message" runat="server" CssClass="form-control" TextMode="multiline" Columns="50" Rows="2" placeholder="שלח לנו את הודעתך" />
                                                <div class="form-control-position pl-1"><i class="la la-commenting-o"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <asp:LinkButton ID="SendRequest" OnClick="SendRequest_Click" CssClass="btn bg-blue  white buttonAnimation  float-right" data-animation="flash" runat="server"><i class="la la-paper-plane-o pr-1"></i>הרשמה</asp:LinkButton>
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                                            <div class="col-md-4 text-center mb-2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <h3>הדרכה לשימוש בממשק</h3>
                            </div>
                            <div class="embed-responsive embed-responsive-item embed-responsive-16by9">
                                <iframe class="img-thumbnail" src="https://www.youtube.com/embed/5Q1m5mOYreo"></iframe>
                            </div>
                            <div class="card-body">
                                <p class="card-text text-center">
                                    פירוט יסודי ומעמיק, אודות השימוש בממשק שלנו והמפרט העשיר מלא הפיצ'רים, שיקלו בהרבה על איכות החיים, הן מצד אנשי הצוות, והן מצד הורי החניכים.<br />
                                    אנו מקווים שתפיקו את המירב בשימוש בפלטפורמה נפלאה זו
                                    <br />
                                    <br />
                                    <b>צוות KeepKids</b>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer1" runat="server">

    <script src="app-assets/vendors/js/animation/jquery.appear.js"></script>
    <script src="app-assets/js/scripts/animation/animation.js"></script>



    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/charts/chartist.min.js"></script>
    <script src="app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js"></script>
    <script src="app-assets/vendors/js/charts/raphael-min.js"></script>
    <script src="app-assets/vendors/js/charts/morris.min.js"></script>
    <script src="app-assets/vendors/js/timeline/horizontal-timeline.js"></script>
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->



</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Footer2" runat="server">
</asp:Content>
